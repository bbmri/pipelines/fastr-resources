import fastr 
fastr.config.source_job_limit = 1
import os
import datetime 
import sys
import argparse
import distutils
import json
from str2bool import str2bool
from os import walk
import pdb #pdb.set_trace()  	

def source_data_for_dict(Network_Input_Mode, Experiments):
	
	# This builds the input data for the ICV node. 
	# Here you can change values of the parameters (not recommended). You can also change them in the parameters file. 
	# However, values here replace the values in the parameters files. 
	# For detailed description of the different parameters - see the documentations or the tool (ICV.xml) definition file. 
	
	# Possible paramaters:
	# 'Input ICV model': ... ,  Not required. Can be set to: Three_Tesla_Adults_Model or Three_Tesla_Kids_Model or OneHalf_Tesla_Adults_Model or OneHalf_Tesla_Kids_Model or Custom_Model. In the last case the progeram also requires a string with the uri of the custom model files.    
	# 'Parameters_File': ... , Not required. If absent the program will read the default parameter file at parameter folder in the tool home directory 
	# 'Input_To_Process': ... , Use the Network mode (either XNAT, Files or Folder to create a URI to a the files that are needed to be processed
	#  'Linear_Estimate': ..., Not required. Overrides a value from the parameter file
	# 'Num_Of_Registrations': ... Not required. Overrides a value from the parameter file
	# 'Registration_Blur_FWHM': ... , Not required. Overrides a value from the parameter file
	# 'Num_Of_Itterations': ... , Not required. Overrides a value from the parameter file
	# 'Step_Size': ..., Not required. Overrides a value from the parameter file
	# 'Blur_FWHM': ..., Not required. Overrides a value from the parameter file
	# 'Weight': ..., Not required. Overrides a value from the parameter file
	# 'Stiffness': ..., Not required. Overrides a value from the parameter file
	# 'Similarity': ... , Not required. Overrides a value from the parameter file
	# 'Debug':  ... ,  Not required. Overrides a value from the parameter file
	# 'Clobber': ...,  Not required. Overrides a value from the parameter file
	# 'Non_linear': ... ,  Not required. Overrides a value from the parameter file
	# 'Mask_To_Binary_Min': .... ,  Not required. Overrides a value from the parameter file
	# 'RUN_GUI': ... , Can be either True or False. If False one of the following three values: Calc_Crude, Calc_Fine or Clac_Ave_Brain must exist and be True. 
	# 'Calc_Crude': ... , Calculate the ICV mask on a more crude level. For most purpose is enough. See remark above in 'RUN_GUI'
	# 'Calc_Fine': ... , Calculate the ICV mask in detailed. See remark above in 'RUN_GUI' 
	# 'Clac_Ave_Brain': .... , Calculate an average MRI brain image from couple of subjects. See remark above in 'RUN_GUI'
	# 'ICV_Output_Directory'.... , A Folder that contains all the outputs files 
	
	#Split_Sample_Name=Sample_Name.split('.',Sample_Name.count('.'))
	#Sample_ID=Split_Sample_Name [0]
	#Final_Output_Dir_Address = Sample_ID +'_Analysis/'
	#Input = Source_Name 
			
	sourcedata = {'Input_To_Process': {} , 'ICV_Subject_Name': {}, 'Calc_Crude': {}, 'Calc_Fine': {}, }
	
	for Experiment in Experiments ['Subject_Name'] : 
		
		if Experiments ['Run_Type'] [Experiment] == 'Calc_Crude':
			Calc_Crude = True
			Calc_Fine = False
		elif Experiments ['Run_Type'] [Experiment] == 'Calc_Fine':
			Calc_Crude = False
			Calc_Fine = True
		else: 
			print ('unrecognize running type (Calc_Fine or Calc_Crude) ')
			exit	
	
		sourcedata ['ICV_Subject_Name'] [Experiment]  = Experiments ['Subject_Name'][Experiment]
		sourcedata ['Input_To_Process'] [Experiment] = Experiments ['t1_source'][Experiment]
		sourcedata ['Calc_Crude'] [Experiment]  = Calc_Crude
		sourcedata ['Calc_Fine'] [Experiment]  = Calc_Fine	
		
	sourcedata.update(Experiments)
			
	return sourcedata 
	
def source_data(Source_Name, Sample_Name, Output_Base_Dir, Network_Input_Mode ,args, Experiment=''):
	
	# This builds the input data for the ICV node. 
	# Here you can change values of the parameters (not recommended). You can also change them in the parameters file. 
	# However, values here replace the values in the parameters files. 
	# For detailed description of the different parameters - see the documentations or the tool (ICV.xml) definition file. 
	
	# Possible paramaters:
	# 'Input ICV model': ... ,  Not required. Can be set to: Three_Tesla_Adults_Model or Three_Tesla_Kids_Model or OneHalf_Tesla_Adults_Model or OneHalf_Tesla_Kids_Model or Custom_Model. In the last case the progeram also requires a string with the uri of the custom model files.    
	# 'Parameters_File': ... , Not required. If absent the program will read the default parameter file at parameter folder in the tool home directory 
	# 'Input_To_Process': ... , Use the Network mode (either XNAT, Files or Folder to create a URI to a the files that are needed to be processed
	#  'Linear_Estimate': ..., Not required. Overrides a value from the parameter file
	# 'Num_Of_Registrations': ... Not required. Overrides a value from the parameter file
	# 'Registration_Blur_FWHM': ... , Not required. Overrides a value from the parameter file
	# 'Num_Of_Itterations': ... , Not required. Overrides a value from the parameter file
	# 'Step_Size': ..., Not required. Overrides a value from the parameter file
	# 'Blur_FWHM': ..., Not required. Overrides a value from the parameter file
	# 'Weight': ..., Not required. Overrides a value from the parameter file
	# 'Stiffness': ..., Not required. Overrides a value from the parameter file
	# 'Similarity': ... , Not required. Overrides a value from the parameter file
	# 'Debug':  ... ,  Not required. Overrides a value from the parameter file
	# 'Clobber': ...,  Not required. Overrides a value from the parameter file
	# 'Non_linear': ... ,  Not required. Overrides a value from the parameter file
	# 'Mask_To_Binary_Min': .... ,  Not required. Overrides a value from the parameter file
	# 'RUN_GUI': ... , Can be either True or False. If False one of the following three values: Calc_Crude, Calc_Fine or Clac_Ave_Brain must exist and be True. 
	# 'Calc_Crude': ... , Calculate the ICV mask on a more crude level. For most purpose is enough. See remark above in 'RUN_GUI'
	# 'Calc_Fine': ... , Calculate the ICV mask in detailed. See remark above in 'RUN_GUI' 
	# 'Clac_Ave_Brain': .... , Calculate an average MRI brain image from couple of subjects. See remark above in 'RUN_GUI'
	# 'ICV_Output_Directory'.... , A Folder that contains all the outputs files 
	
	Split_Sample_Name=Sample_Name.split('.',Sample_Name.count('.'))
	#Sample_ID=Split_Sample_Name [0]
	#Final_Output_Dir_Address = Sample_ID +'_Analysis/'
	
	Input = Source_Name 
	if args.Calc == 'Calc_Crude':
		Calc_Crude = True
		Calc_Fine = False
	elif args.Calc == 'Calc_Fine':
			Calc_Crude = False
			Calc_Fine = True
				
	sourcedata = {'Delete_Files': {}, 'Model': {} , 'Input_To_Process': {} , 'ICV_Subject_Name': {}, 
	'Linear_Estimate': {}, 'Num_Of_Registrations': {}, 'Registration_Blur_FWHM': {}, 'Linear_Normalization': {},
	'Num_Of_Iterations': {} , 'Step_Size': {}, 'Blur_FWHM': {}, 'Weight': {}, 'Stiffness': {}, 'Similarity': {}, 'Sub_lattice': {}, 'Non_linear': {}, 'NonLinear_Normalization': {},
	'Clobber': {}, 'Debug': {}, 'Mask_To_Binary_Min': {}, 'Calc_Crude': {}, 'Calc_Fine': {}, }
	
	sourcedata ['Delete_Files'] [Experiment]  = args.Delete_Files
	sourcedata ['Model'] [Experiment]  = args.Model_Name
	sourcedata ['Input_To_Process'] [Experiment] = Input
	sourcedata ['ICV_Subject_Name'] [Experiment]  = Split_Sample_Name [0]
	sourcedata ['Linear_Estimate'] [Experiment]  = args.Linear_Estimate
	sourcedata ['Num_Of_Registrations'] [Experiment]  = args.Num_Of_Registrations
	sourcedata ['Registration_Blur_FWHM'] [Experiment]  = args.Registration_Blur_FWHM
	sourcedata ['Linear_Normalization'] [Experiment]  = args.Linear_Normalization
	sourcedata ['Num_Of_Iterations'] [Experiment]  = args.Num_Of_Iterations
	sourcedata ['Step_Size'] [Experiment]  = args.Step_Size
	sourcedata ['Blur_FWHM'] [Experiment]  = args.Blur_FWHM
	sourcedata ['Weight'] [Experiment]  = args.Weight
	sourcedata ['Stiffness'] [Experiment]  = args.Stiffness
	sourcedata ['Similarity'] [Experiment]  = args.Similarity
	sourcedata ['Sub_lattice'] [Experiment]  =  args.Sub_lattice
	sourcedata ['Debug'] [Experiment]  = args.Debug
	sourcedata ['Clobber'] [Experiment]  =args.Clobber
	sourcedata ['Non_linear'] [Experiment]  = args.Non_linear
	sourcedata ['NonLinear_Normalization'] [Experiment]  = args.NonLinear_Normalization
	sourcedata ['Mask_To_Binary_Min'] [Experiment]  = args.Mask_To_Binary_Min
	sourcedata ['Calc_Crude'] [Experiment]  = Calc_Crude
	sourcedata ['Calc_Fine'] [Experiment]  = Calc_Fine	
			
	return sourcedata 	

def sink_data(Output_Base_Dir,Sample_Name,Type, query_string=""):
	
	# This builds the output data for the ICV program
	# 'ICV_Output_ICV_Mask': .... , The Intracranial Volume mask that the program created
	# 'ICV_Output_ICV_Volume': .... , A Biomarker text file with the size in mm of the Intracranial Volume
	# 'ICV_Output_ICV_Parameters': .... , The parameters that were used to create the Intracranial Volume mask
	# 'ICV_Output_Log': .... ,  A log file with all the processes and bash commands that the program produced
	#ICV_Original_Subject : ...., The original file in a nii.gz format
	
	st=datetime.datetime.now().strftime("%Y_%m_%d_%H_%M%p")
		
	if Type == 'DICOM' :
		Suffix1 ='nii.gz'
		Suffix2='nii.gz'
	elif Type == 'MINC' :
		Suffix1 ='mnc'
		Suffix2='mnc.gz'
	elif Type == 'NIFTI' :
		Suffix1 ='nii'
		Suffix2='nii.gz'
	else:
		Suffix1 ='nii.gz'
		Suffix2 ='nii.gz'
	
	if Output_Base_Dir.find ('xnat://') == 0  or Sample_Name == '':
		if Type == 'DICOM' :
				sinkdata = {
				'ICV_Original_Subject' : Output_Base_Dir + '/{sample_id}/{sample_id}.' + Suffix1 + query_string,
				'ICV_Output_ICV_Mask' : Output_Base_Dir + '/{sample_id}/{sample_id}_icmask.' + Suffix2 + query_string,
				'ICV_Output_ICV_Volume' : Output_Base_Dir + '/{sample_id}/{sample_id}_icmask_statistics.txt' + query_string,
				'ICV_Output_ICV_Parameters' : Output_Base_Dir +  '/{sample_id}/{sample_id}_ic_parameters.txt' + query_string,
				'ICV_Output_Log': Output_Base_Dir +  '/{sample_id}/{sample_id}_log.txt' + query_string,	
				'ICV_Output_ICV_JPG':  Output_Base_Dir + '/{sample_id}/{sample_id}_icmask.jpg' + query_string,
				}  
		elif Type == 'NIFTI' or Type == 'MINC':		
				sinkdata = {
				'ICV_Output_ICV_Mask' : Output_Base_Dir + '/{sample_id}/{sample_id}_icmask.' + Suffix2 + query_string,
				'ICV_Output_ICV_Volume' : Output_Base_Dir + '/{sample_id}/{sample_id}_icmask_statistics.txt' + query_string,
				'ICV_Output_ICV_Parameters' : Output_Base_Dir +  '/{sample_id}/{sample_id}_ic_parameters.txt' + query_string,
				'ICV_Output_Log': Output_Base_Dir +  '/{sample_id}/{sample_id}_log.txt' + query_string,	
				'ICV_Output_ICV_JPG':  Output_Base_Dir + '/{sample_id}/{sample_id}_icmask.jpg' + query_string,
				}  
	else:	
		Split_Sample_Name=Sample_Name.split('.',Sample_Name.count('.'))
		Sample_ID=Split_Sample_Name [0]
		Final_Output_Dir_Address ='/'+ Sample_ID +'_Analysis/'	
		if Type == 'DICOM' :	 		
				sinkdata = {
				'ICV_Original_Subject' : Output_Base_Dir + '/' + Sample_ID  + '/' + Sample_ID + '.' + Suffix1 + query_string,
				'ICV_Output_ICV_Mask' : Output_Base_Dir + '/' + Sample_ID + Final_Output_Dir_Address + Sample_ID + '_icmask_' + st + '.' + Suffix2 + query_string,
				'ICV_Output_ICV_Volume' : Output_Base_Dir + '/' + Sample_ID + Final_Output_Dir_Address + Sample_ID +'_icmask_statistics_' +st + '.txt' + query_string,
				'ICV_Output_ICV_Parameters' : Output_Base_Dir +  '/' + Sample_ID + Final_Output_Dir_Address + Sample_ID +'_ic_parameters_' + st + '.txt' + query_string,
				'ICV_Output_Log': Output_Base_Dir +  '/' + Sample_ID + Final_Output_Dir_Address + Sample_ID + '_log_' + st + '.txt'+ query_string,
				'ICV_Output_ICV_JPG':  Output_Base_Dir + '/' + Sample_ID + Final_Output_Dir_Address + Sample_ID + '_icmask_' + st + '.jpg' + query_string,	
				}
		elif Type == 'NIFTI' or Type == 'MINC':		
				sinkdata = {
				'ICV_Output_ICV_Mask' : Output_Base_Dir + '/' + Sample_ID + Final_Output_Dir_Address + Sample_ID + '_icmask_' + st + '.' + Suffix2 + query_string,
				'ICV_Output_ICV_Volume' : Output_Base_Dir + '/' + Sample_ID + Final_Output_Dir_Address + Sample_ID +'_icmask_statistics_' +st + '.txt' + query_string,
				'ICV_Output_ICV_Parameters' : Output_Base_Dir +  '/' + Sample_ID + Final_Output_Dir_Address + Sample_ID +'_ic_parameters_' + st + '.txt' + query_string,
				'ICV_Output_Log': Output_Base_Dir +  '/' + Sample_ID + Final_Output_Dir_Address + Sample_ID + '_log_' + st + '.txt'+ query_string,	
				'ICV_Output_ICV_JPG': Output_Base_Dir + '/' + Sample_ID + Final_Output_Dir_Address + Sample_ID + '_icmask_' + st + '.jpg' + query_string,	
				}
				
	return sinkdata


def create_network(Input_Mode,Calc,Type):
	
	# Building up the ICV Node
	network = fastr.Network(id_='IntracranialVolume')
	ICV_Build_Model = network.create_node(fastr.toollist['ICV'],id_='IntracranialVolume',stepid='Main_Operation')
	ICV_Build_Model.required_memory = '8g'
		
	# Building up the ICV input part of the Network		
	ICV_Delete_Files = network.create_source(fastr.typelist['Boolean'],id_='Delete_Files',stepid='Delete_Files')
	
	ICV_Model = network.create_source(fastr.typelist['String'],id_='Model',stepid='Model_Name')	
	
	if Input_Mode == 'Folder':
		ICV_Input_Folder = network.create_source(fastr.typelist['Directory'],id_='Input_To_Process',stepid='Program_File_Arguments')
		#ICV_Input_Folder.required_memory      = '8g'
	elif Input_Mode == 'File':
		ICV_Input_File = network.create_source(fastr.typelist['ICVInputFiletypes'],id_='Input_To_Process',stepid='Program_File_Arguments')
		#ICV_Input_File.required_memory      = '8g'
	elif Input_Mode == 'XNAT':
		if Type == 'DICOM':
			ICV_Input_XNAT = network.create_source(fastr.typelist['DicomImageFile'],id_='Input_To_Process',stepid='Program_File_Arguments')
			#ICV_Input_XNAT.required_memory      = '8g'
		elif Type == 'NIFTI' or Type == 'MINC':		
			ICV_Input_XNAT = network.create_source(fastr.typelist['NiftiImageFile'],id_='Input_To_Process',stepid='Program_File_Arguments')
			#ICV_Input_XNAT.required_memory      = '8g'
	
	ICV_Subject_Name = network.create_source (fastr.typelist['String'],id_='ICV_Subject_Name',stepid='Subject_Name')			
	
	ICV_Linear_Estimate = network.create_source(fastr.typelist['String'],id_='Linear_Estimate',stepid='Program_Linear_Registration_Arguments')
	ICV_Num_Of_Registrations = network.create_source(fastr.typelist['Int'],id_='Num_Of_Registrations',stepid='Program_Linear_Registration_Arguments')	
	ICV_Registration_Blur_FWHM = network.create_source(fastr.typelist['Int'],id_='Registration_Blur_FWHM',stepid='Program_Linear_Registration_Arguments')
	ICV_Linear_Normalization = network.create_source(fastr.typelist['Boolean'],id_='Linear_Normalization',stepid='ICV_Program_Linear_Registration_Arguments')
	
	ICV_Number_Of_Iterations = network.create_source(fastr.typelist['String'],id_='Num_Of_Iterations',stepid='Program_Basic_Arguments')
	ICV_Step_Size = network.create_source(fastr.typelist['String'],id_='Step_Size',stepid='Program_Basic_Arguments')
	ICV_Blur_FWHM = network.create_source(fastr.typelist['String'],id_='Blur_FWHM',stepid='Program_Basic_Arguments')
	
	ICV_Weight = network.create_source(fastr.typelist['Float'],id_='Weight',stepid='Program_Extended_Arguments')
	ICV_Stiffness = network.create_source(fastr.typelist['Float'],id_='Stiffness',stepid='Program_Extended_Arguments')
	ICV_Similarity = network.create_source(fastr.typelist['Float'],id_='Similarity',stepid='Program_Extended_Arguments')
	ICV_Sub_lattice = network.create_source(fastr.typelist['Int'],id_='Sub_lattice',stepid='Program_Extended_Arguments')
	ICV_Debug = network.create_source(fastr.typelist['Boolean'],id_='Debug',stepid='Program_Extended_Arguments')
	ICV_Clobber = network.create_source(fastr.typelist['Boolean'],id_='Clobber',stepid='Program_Extended_Arguments')
	ICV_Non_linear = network.create_source(fastr.typelist['String'],id_='Non_linear',stepid='Program_Extended_Arguments')
	ICV_NonLinear_Normalization = network.create_source(fastr.typelist['Boolean'],id_='NonLinear_Normalization',stepid='ICV_Program_Extended_Arguments')
	
	ICV_Mask_To_Binary_Min = network.create_source(fastr.typelist['Float'],id_='Mask_To_Binary_Min',stepid='Program_Extended_Arguments')

	if Calc == 'Calc_Crude':
		ICV_Operation_Type = network.create_source(fastr.typelist['Boolean'],id_='Calc_Crude',stepid='Operation_Type')
	elif Calc == 'Calc_Fine':
			ICV_Operation_Type = network.create_source(fastr.typelist['Boolean'],id_='Calc_Fine',stepid='Operation_Type')
	
	# Link the sources to the ICV tool
	
	ICV_Input_Link1 = network.create_link(ICV_Delete_Files.output, ICV_Build_Model.inputs['Delete_Files'])
	
	ICV_Input_link2 = network.create_link(ICV_Model.output, ICV_Build_Model.inputs['Model'])
	if Input_Mode == 'Folder':
		ICV_Input_link3 = network.create_link(ICV_Input_Folder.output, ICV_Build_Model.inputs['Processed_Folder'])
	elif Input_Mode == 'File':
		ICV_Input_link3 = network.create_link(ICV_Input_File.output, ICV_Build_Model.inputs['Processed_File'])
	elif Input_Mode == 'XNAT':
		if Type == 'DICOM':
			ICV_Input_link3 = network.create_link(ICV_Input_XNAT.output, ICV_Build_Model.inputs['Processed_XNAT_DICOM'])
		elif Type == 'NIFTI' or Type == 'MINC':	
			ICV_Input_link3 = network.create_link(ICV_Input_XNAT.output, ICV_Build_Model.inputs['Processed_File'])
	
	ICV_Input_Link4 = network.create_link(ICV_Linear_Estimate.output, ICV_Build_Model.inputs['Linear_Estimate']) 
	ICV_Input_link5 = network.create_link(ICV_Num_Of_Registrations.output, ICV_Build_Model.inputs['Num_Of_Registrations'])
	ICV_Input_link6 = network.create_link(ICV_Registration_Blur_FWHM.output, ICV_Build_Model.inputs['Registration_Blur_FWHM'])
	ICV_Input_link7 = network.create_link(ICV_Linear_Normalization.output, ICV_Build_Model.inputs['Linear_Normalization'])
	
	ICV_Input_link8 = network.create_link(ICV_Number_Of_Iterations.output, ICV_Build_Model.inputs['Num_Of_Iterations'])
	ICV_Input_link9 = network.create_link(ICV_Step_Size.output, ICV_Build_Model.inputs['Step_Size'])
	ICV_Input_link10 = network.create_link(ICV_Blur_FWHM.output, ICV_Build_Model.inputs['Blur_FWHM'])
	
	ICV_Input_Link11 = network.create_link(ICV_Weight.output, ICV_Build_Model.inputs['Weight'])
	ICV_Input_Link12 = network.create_link(ICV_Stiffness.output, ICV_Build_Model.inputs['Stiffness'])
	ICV_Input_Link13 = network.create_link(ICV_Similarity.output, ICV_Build_Model.inputs['Similarity'])
	ICV_Input_Link14 = network.create_link(ICV_Sub_lattice.output, ICV_Build_Model.inputs['Sub_lattice'])
	ICV_Input_Link15 = network.create_link(ICV_Debug.output, ICV_Build_Model.inputs['Debug'])
	ICV_Input_Link16 = network.create_link(ICV_Clobber.output, ICV_Build_Model.inputs['Clobber'])
	ICV_Input_Link17 = network.create_link(ICV_Non_linear.output, ICV_Build_Model.inputs['Non_linear'])
	ICV_Input_link18 = network.create_link(ICV_NonLinear_Normalization.output, ICV_Build_Model.inputs['NonLinear_Normalization'])
	
	ICV_Input_Link19 = network.create_link(ICV_Mask_To_Binary_Min.output, ICV_Build_Model.inputs['Mask_To_Binary_Min'])   
	
	if Calc == 'Calc_Crude':
		ICV_Input_link20 = network.create_link(ICV_Operation_Type.output, ICV_Build_Model.inputs['Calc_Crude'])
	elif Calc == 'Calc_Fine':
			ICV_Input_link20 = network.create_link(ICV_Operation_Type.output, ICV_Build_Model.inputs['Calc_Fine'])
	
	ICV_Input_link21 = network.create_link(ICV_Subject_Name.output, ICV_Build_Model.inputs['Subject_Name'])
	
	# Create the sinks and 
	
	if Type == 'DICOM' or Type == 'NIFTI':	
		ICV_Output_ICV_Mask = network.create_sink (fastr.typelist['NiftiImageFileCompressed'],id_='ICV_Output_ICV_Mask',stepid='ICV_Files_Outputs')
		ICV_Output_ICV_Mask.required_memory      = '4g'
	elif Type == 'MINC' :
		ICV_Output_ICV_Mask = network.create_sink (fastr.typelist['CompressedMincImageFile'],id_='ICV_Output_ICV_Mask',stepid='ICV_Files_Outputs')
		ICV_Output_ICV_Mask.required_memory      = '4g'

	ICV_Output_ICV_Parameters = network.create_sink (fastr.typelist['TxtFile'],id_='ICV_Output_ICV_Parameters',stepid='ICV_Files_Outputs')
	ICV_Output_ICV_Parameters.required_memory      = '4g'

	ICV_Output_ICV_Mask_BioMarker = network.create_sink (fastr.typelist['TxtFile'],id_='ICV_Output_ICV_Volume',stepid='ICV_Files_Outputs')
	ICV_Output_ICV_Mask_BioMarker.required_memory      = '4g'

	ICV_Output_ICV_Log = network.create_sink (fastr.typelist['TxtFile'],id_='ICV_Output_Log',stepid='ICV_Files_Outputs')
	ICV_Output_ICV_Log.required_memory      = '4g'


	if Type == 'DICOM':
		ICV_Output_Original_Subject = network.create_sink (fastr.typelist['NiftiImageFileCompressed'],id_='ICV_Original_Subject',stepid='ICV_Files_Outputs')
		ICV_Output_Original_Subject.required_memory      = '8g'

	ICV_Output_ICV_JPG = network.create_sink (fastr.typelist['JPGFile'],id_='ICV_Output_ICV_JPG',stepid='ICV_Files_Outputs')
	ICV_Output_ICV_JPG.required_memory      = '4g'
		
	# Create the links to the sinks 
	
	if Type == 'DICOM' or Type == 'NIFTI':	
		ICV_Output_link1 = network.create_link(ICV_Build_Model.outputs['icv_output_nii_mask'],ICV_Output_ICV_Mask.input)	
	elif Type == 'MINC' :
		ICV_Output_link1 = network.create_link(ICV_Build_Model.outputs['icv_output_mnc_mask'],ICV_Output_ICV_Mask.input)	
	ICV_Output_link2 = network.create_link(ICV_Build_Model.outputs['icv_parameters_used'],ICV_Output_ICV_Parameters.input)
	ICV_Output_link3 = network.create_link(ICV_Build_Model.outputs['icv_volume'],ICV_Output_ICV_Mask_BioMarker.input)
	ICV_Output_link4 = network.create_link(ICV_Build_Model.outputs['icv_log'],ICV_Output_ICV_Log.input)	
	ICV_Output_link5 = network.create_link(ICV_Build_Model.outputs['icv_jpg'],ICV_Output_ICV_JPG.input)	
	if Type == 'DICOM':
		ICV_Output_link6 = network.create_link(ICV_Build_Model.outputs['icv_original_file_In_nii'],ICV_Output_Original_Subject.input)					
												 
	return network

	
def JSON_Read_Arguments (dict_file, experiments):

	with open(dict_file) as input_file:
		data = json.load(input_file)
	uri_source_path = { 't1_source': {} , 'Subject_Name': {}, 'Experiment': {}, 'type': {}, 'FileType': {}, 'Delete_Files': {},
	'Linear_Estimate': {}, 'Num_Of_Registrations': {}, 'Registration_Blur_FWHM': {}, 'Linear_Normalization': {},
	'Num_Of_Iterations': {}, 'Step_Size': {}, 'Blur_FWHM': {}, 'Weight': {}, 'Stiffness': {}, 'Similarity': {},
	'Sub_lattice': {}, 'Debug': {}, 'Clobber': {}, 'Non_linear': {}, 'NonLinear_Normalization': {},
	'Mask_To_Binary_Min': {}, 'Model': {}, 'Run_Type': {}, }	
		
	for experiment in experiments:
		if experiment in data:
			experiment_data = data[experiment]
			uri_source_path['Subject_Name'][experiment] = experiment_data['subject'] 
			uri_source_path['Experiment'] [experiment] = experiment
			if 't1w_dicom' in experiment_data:
				uri_source_path['t1_source'][experiment] = experiment_data['t1w_dicom']
				uri_source_path['type'] [experiment] = 'XNAT_Folder'
				uri_source_path['FileType'] [experiment] = 'DICOM'
			elif 't1w_nifti' in experiment_data:
				uri_source_path['t1_source'][experiment] = experiment_data['t1w_nifti']
				uri_source_path['type'] [experiment] = 'File'
				uri_source_path['FileType'] [experiment] = 'NIFTI'
			elif 't1w_mnc' in experiment_data:
				uri_source_path['t1_source'][experiment] = experiment_data['t1w_mnc']
				uri_source_path['type'] [experiment] = 'File'
				uri_source_path['FileType'] [experiment] = 'MINC'
			if 'Delete_Files' in experiment_data: 
				uri_source_path['Delete_Files'] [experiment] = experiment_data['Delete_Files']
			else : 
				uri_source_path['Delete_Files'] [experiment] = ''
			if 'Linear_Estimate' in experiment_data: 
				uri_source_path['Linear_Estimate'] [experiment] = experiment_data['Linear_Estimate']
			else : 
				uri_source_path['Linear_Estimate'] [experiment] = ''
			if 'Num_Of_Registrations' in experiment_data: 
				uri_source_path['Num_Of_Registrations'] [experiment] = experiment_data['Num_Of_Registrations']
			else : 
				uri_source_path['Num_Of_Registrations'] [experiment] = ''
			if 'Registration_Blur_FWHM' in experiment_data: 
				uri_source_path['Registration_Blur_FWHM'] [experiment] = experiment_data['Registration_Blur_FWHM']
			else : 
				uri_source_path['Registration_Blur_FWHM'] [experiment] = ''	
			if 'Linear_Normalization' in experiment_data: 
				uri_source_path['Linear_Normalization'] [experiment] = experiment_data['Linear_Normalization']
			else : 
				uri_source_path['Linear_Normalization'] [experiment] = ''
			if 'Num_Of_Iterations' in experiment_data: 
				uri_source_path['Num_Of_Iterations'] [experiment] = experiment_data['Num_Of_Iterations']
			else : 
				uri_source_path['Num_Of_Iterations'] [experiment] = ''	
			if 'Step_Size' in experiment_data: 
				uri_source_path['Step_Size'] [experiment] = experiment_data['Step_Size']
			else : 
				uri_source_path['Step_Size'] [experiment] = ''
			if 'Blur_FWHM' in experiment_data: 
				uri_source_path['Blur_FWHM'] [experiment] = experiment_data['Blur_FWHM']
			else : 
				uri_source_path['Blur_FWHM'] [experiment] = ''			
			if 'Weight' in experiment_data: 
				uri_source_path['Weight'] [experiment] = experiment_data['Weight']
			else : 
				uri_source_path['Weight'] [experiment] = ''
			if 'Stiffness' in experiment_data: 
				uri_source_path['Stiffness'] [experiment] = experiment_data['Stiffness']
			else : 
				uri_source_path['Stiffness'] [experiment] = ''
			if 'Similarity' in experiment_data: 
				uri_source_path['Similarity'] [experiment] = experiment_data['Similarity']
			else : 
				uri_source_path['Similarity'] [experiment] = ''
			if 'Sub_lattice' in experiment_data: 
				uri_source_path['Sub_lattice'] [experiment] = experiment_data['Sub_lattice']
			else : 
				uri_source_path['Sub_lattice'] [experiment] = ''
			if 'Debug' in experiment_data: 
				uri_source_path['Debug'] [experiment] = experiment_data['Debug']
			else : 
				uri_source_path['Debug'] [experiment] = ''
			if 'Clobber' in experiment_data: 
				uri_source_path['Clobber'] [experiment] = experiment_data['Clobber']
			else : 
				uri_source_path['Clobber'] [experiment] = ''
			if 'Non_linear' in experiment_data: 
				uri_source_path['Non_linear'] [experiment] = experiment_data['Non_linear']
			else : 
				uri_source_path['Non_linear'] [experiment] = ''
			if 'NonLinear_Normalization' in experiment_data: 
				uri_source_path['NonLinear_Normalization'] [experiment] = experiment_data['NonLinear_Normalization']
			else : 
				uri_source_path['NonLinear_Normalization'] [experiment] = ''
			if 'Mask_To_Binary_Min' in experiment_data: 
				uri_source_path['Mask_To_Binary_Min'] [experiment] = experiment_data['Mask_To_Binary_Min']
			else : 
				uri_source_path['Mask_To_Binary_Min'] [experiment] = ''
			if 'Model' in experiment_data: 
				uri_source_path['Model'] [experiment] = experiment_data['Model']
			else : 
				uri_source_path['Model'] [experiment] = ''
			if 'Run_Type' in experiment_data: 
				uri_source_path['Run_Type'] [experiment] = experiment_data['Run_Type']
			else : 
				uri_source_path['Run_Type'] [experiment] = ''								
		else :
			print ('Error: Did not find name(s) of the subject %s in the json directory. Please try again' %(experiment))	
	return uri_source_path


def main():
			
	# Parse the program command line parameters  
	parser = argparse.ArgumentParser()
	parser.add_argument('-Dict', type=unicode, required=False, default='', help='Dictionary file with source data. One can set this argument with the Subject_Name or the -Input_URI directly')
	parser.add_argument('-Subject_Name', dest='Subject_Name', help='Name of the subject. Used for creating the name of the output files. Is also used to read Subject name(s) from the dictionary',
		type=str, required=False, nargs='+', default=['ICV_Default_Subject']) 
	parser.add_argument('-Input_URI', dest='Rel_Input_URI', help='URI of the input. Can be either XNAT URI or a Folder URI relative to the vfs://icv_data location.', 
		type=str, required=False, default='')
	parser.add_argument('-Input_Mode', dest='Network_Input_Mode', help='Determines if the analyse a folder with dcm file or a niftii/minc single file. Optional for the -Input_URI case.', 
		type=str, default='') 
	parser.add_argument('-Output_URI', dest='Rel_Output_URI', help='URI of the output folder relative to the vfs://icv_data location', 
		type=str, default='')
	parser.add_argument('-Temp', type=unicode, dest='Temp_Dir',required=False, default='vfs://tmp', help='Temp directory')
	parser.add_argument('-query', type=unicode, required=False, help='URL query string that will be appended to Fastr output.', default="" )
	parser.add_argument('-Model', dest='Model_Name', help='Optional: Name of the model mask to use ' 
		'Should be either OneHalf_Tesla_Adults_Model, OneHalf_Tesla_Kids_Model, Three_Tesla_Adults_Model, Three_Tesla_Kids_Model ' 
		'or a string with the address of the custom ICV model (without the vfs:// prefix). Default is: Three_Tesla_Adults_Model.',
		type=str, default='Three_Tesla_Adults_Model')
	parser.add_argument('-Delete_Files', dest='Delete_Files', help='Delete the files from the ICV temporary folder. Default = True', 
		type=lambda x:bool(distutils.util.strtobool(x)), default=True)	
	parser.add_argument('-Debug', dest='Debug', help='Print out debug info. Default = True', 
		type=lambda x:bool(distutils.util.strtobool(x)), default=True)
	parser.add_argument('-Clobber', dest='Clobber', help='"Overwrite output file. Default = True', 
		type=lambda x:bool(distutils.util.strtobool(x)), default=True)	
	parser.add_argument('-Linear_Estimate', dest='Linear_Estimate', help='Type of estimation of transformation relative to previous transformation'
		'Options:Estimate_Center|Estimate_Scales|Estimate_Translation|Estimate_None. Default = Estimate_Translation.', 
		type=str, default='Estimate_Translation') 
	parser.add_argument('-Num_Of_Registrations', dest='Num_Of_Registrations', help='Number of linear registration. Default = 4.', 
		type=int, default=4) 
	parser.add_argument('-Registration_Blur_FWHM', dest='Registration_Blur_FWHM', help='Blur size before linear registration. Default = 1.', 
		type=int, default=1)  
	parser.add_argument('-Linear_Normalization', dest='Linear_Normalization', help='"Normalize intensities of the registraion when averaging linear registrations = False', 
		type=lambda x:bool(distutils.util.strtobool(x)), default=False)
	parser.add_argument('-Num_Of_Iterations', dest='Num_Of_Iterations', help='Number of iterations in each non-linear registration. Default = "{5,8,10,8}" ', 
		type=str, default='"{5,8,10,8}"') 
	parser.add_argument('-Step_Size', dest='Step_Size', help='Step size in each non-linear registration. Default = "{16,8,2,1}"', 
		type=str, default='"{16,8,2,1}"') 
	parser.add_argument('-Blur_FWHM', dest='Blur_FWHM', help='Blur amount before in each non-linear registration. Default = "{16,8,2,1}"', 
		type=str, default='"{8,8,2,1}"') 
	parser.add_argument('-Weight', dest='Weight', help='Weighting factor for each iteration in non-linear optimization. Default = 1.0', 
		type=float, default=1.0) 
	parser.add_argument('-Stiffness', dest='Stiffness', help='Weighting factor for smoothing between non-linear iterations. Default = 1.0', 
		type=float, default=1.0) 
	parser.add_argument('-Similarity', dest='Similarity', help='Weighting factor for  r=similarity*w + cost(1*w). Default = 0.3', 
		type=float, default=0.3) 
	parser.add_argument('-Sub_lattice', dest='Sub_lattice', help='Number of nodes along diameter of local sub-lattice. Default = 6', 
		type=int, default=6) 
	parser.add_argument('-Non_linear', dest='Non_linear', help='Recover nonlinear deformation field. Optional arg {xcorr|diff|sqdiff|label|chamfer|corrcoeff|opticalflow} sets objective function.  Default = corrcoeff', 
		type=str, default='corrcoeff') 
	parser.add_argument('-NonLinear_Normalization', dest='NonLinear_Normalization', help='"Normalize intensities of the registraion when averaging nonlinear registrations = False', 
		type=bool, default=False)
	parser.add_argument('-Mask_To_Binary_Min', dest='Mask_To_Binary_Min', help='Value to below which pixel are disregard in the mask file.  Default = 0.5', 
		type=float, default=0.5) 
	parser.add_argument('-Calc', dest='Calc', help='Type of calculation: Crude (Calc_Crude) or Fine (Calc_Fine).  Default = Calc_Crude', 
		type=str, default='Calc_Crude') 	
	parser.add_argument('-Verbose', dest='Verbose', help='Determine if the program prints all the source and sink data', type=bool, default=False) 
	parser.add_argument('-Type', dest='Type', help='Determine if the program analyze a NIFTII (or MINC) or DICOM (dcm). Can be NIFTI/MINC/DICOM', type=str, default='DICOM')   
		
	args = parser.parse_args()
	
	# Define the parameters that will be used to build the network:
	
	# Case A the parameters are read from a json DICT file
	if  args.Dict !='':
		Sources=JSON_Read_Arguments(args.Dict, args.Subject_Name)
		
		STR = Sources ['FileType'] [args.Subject_Name[0]]
		for i in range(1, len(args.Subject_Name)): 
			if Sources ['FileType'] [args.Subject_Name[i]] != STR : 
				print ('cannot run network with different file types. Please run each file type separately')  
				return 1
				
		if Sources ['FileType'] [args.Subject_Name[0]] == 'DICOM' :
			Network_Input_Mode='XNAT'
			FileType = 'DICOM'
		elif  Sources ['FileType'] [args.Subject_Name[0]] == 'NIFTI' :
			Network_Input_Mode='File'
			FileType = 'NIFTI'
		elif  Sources ['FileType'] [args.Subject_Name[0]] == 'MINC' :
			Network_Input_Mode='File'
			FileType = 'MINC'	
		else: 
			Network_Input_Mode='Folder'
			FileType = args.Type		
		
		for i in range(0, len(args.Subject_Name)): 
			
			#Sample_Name = Sources ['Subject_Name'] [args.Subject_Name[i]]
			#Source_Path = Sources ['t1_source'] [args.Subject_Name[i]]
			#Experiment =  Sources ['Experiment'] [args.Subject_Name[i]]
	
			# Check per individual parameters
			if 	Sources ['Delete_Files'] [args.Subject_Name[i]] == '':
				Sources ['Delete_Files'] [args.Subject_Name[i]] = args.Delete_Files 
			else : 
				Sources ['Delete_Files'] [args.Subject_Name[i]] = str2bool(Sources ['Delete_Files'] [args.Subject_Name[i]])	
				 
			if 	Sources ['Debug'] [args.Subject_Name[i]] == '':
				Sources ['Debug'] [args.Subject_Name[i]] = args.Debug
			else :
				Sources ['Debug'] [args.Subject_Name[i]] = str2bool(Sources ['Debug'] [args.Subject_Name[i]])
				
			if 	Sources ['Clobber'] [args.Subject_Name[i]] == '':
				Sources ['Clobber'] [args.Subject_Name[i]] = args.Clobber
			else: 
				Sources ['Clobber'] [args.Subject_Name[i]] = str2bool(Sources ['Clobber'] [args.Subject_Name[i]])	
				
			if 	Sources ['Linear_Estimate'] [args.Subject_Name[i]] == '':
				Sources ['Linear_Estimate'] [args.Subject_Name[i]] = args.Linear_Estimate 
				
			if 	Sources ['Num_Of_Registrations'] [args.Subject_Name[i]] == '':
				Sources ['Num_Of_Registrations'] [args.Subject_Name[i]] = args.Num_Of_Registrations
			else:
				Sources ['Num_Of_Registrations'] [args.Subject_Name[i]] = int (Sources ['Num_Of_Registrations'] [args.Subject_Name[i]])	 
				
			if 	Sources ['Registration_Blur_FWHM'] [args.Subject_Name[i]] == '':
				Sources ['Registration_Blur_FWHM'] [args.Subject_Name[i]] = args.Registration_Blur_FWHM 
			else :
				Sources ['Registration_Blur_FWHM'] [args.Subject_Name[i]] = int (Sources ['Registration_Blur_FWHM'] [args.Subject_Name[i]])	
				
			if 	Sources ['Linear_Normalization'] [args.Subject_Name[i]] == '':
				Sources ['Linear_Normalization'] [args.Subject_Name[i]] = args.Linear_Normalization
			else: 
				Sources ['Linear_Normalization'] [args.Subject_Name[i]] = str2bool(Sources ['Linear_Normalization'] [args.Subject_Name[i]]) 
				 
			if 	Sources ['Num_Of_Iterations'] [args.Subject_Name[i]] == '':
				Sources ['Num_Of_Iterations'] [args.Subject_Name[i]] = args.Num_Of_Iterations 
				
			if 	Sources ['Step_Size'] [args.Subject_Name[i]] == '':
				Sources ['Step_Size'] [args.Subject_Name[i]]  = args.Step_Size 
				
			if 	Sources ['Blur_FWHM'] [args.Subject_Name[i]] == '':
				Sources ['Blur_FWHM'] [args.Subject_Name[i]] = args.Blur_FWHM 
				
			if 	Sources ['Weight'] [args.Subject_Name[i]] == '':
				Sources ['Weight'] [args.Subject_Name[i]] = args.Weight 
			else: 
				Sources ['Weight'] [args.Subject_Name[i]] = float (Sources ['Weight'] [args.Subject_Name[i]])	
				
			if 	Sources ['Stiffness'] [args.Subject_Name[i]] == '':
				Sources ['Stiffness'] [args.Subject_Name[i]] = args.Stiffness
			else: 
				Sources ['Stiffness'] [args.Subject_Name[i]] = float (Sources ['Stiffness'] [args.Subject_Name[i]])	 
				 
			if 	Sources ['Similarity'] [args.Subject_Name[i]] == '':
				Sources ['Similarity'] [args.Subject_Name[i]] = args.Similarity 
			else:
				Sources ['Similarity'] [args.Subject_Name[i]] = float (Sources ['Similarity'] [args.Subject_Name[i]])	
				
			if 	Sources ['Sub_lattice'] [args.Subject_Name[i]] == '':
				Sources ['Sub_lattice'] [args.Subject_Name[i]] = args.Sub_lattice 
			else: 
				Sources ['Sub_lattice'] [args.Subject_Name[i]] == int (Sources ['Sub_lattice'] [args.Subject_Name[i]])	
				
			if 	Sources ['Non_linear'] [args.Subject_Name[i]] == '':
				Sources ['Non_linear'] [args.Subject_Name[i]] = args.Non_linear 
				
			if 	Sources ['NonLinear_Normalization'] [args.Subject_Name[i]] == '':
				Sources ['NonLinear_Normalization'] [args.Subject_Name[i]] = args.NonLinear_Normalization 
			else:
				 Sources ['NonLinear_Normalization'] [args.Subject_Name[i]] = str2bool(Sources ['NonLinear_Normalization'] [args.Subject_Name[i]])
				 
			if 	Sources ['Mask_To_Binary_Min'] [args.Subject_Name[i]] == '':
				Sources ['Mask_To_Binary_Min'] [args.Subject_Name[i]]= args.Mask_To_Binary_Min 
			else:
				Sources ['Mask_To_Binary_Min'] [args.Subject_Name[i]] = float(Sources ['Mask_To_Binary_Min'] [args.Subject_Name[i]])
				
			if 	Sources ['Model'] [args.Subject_Name[i]] == '':
				Sources ['Model'] [args.Subject_Name[i]] = args.Model_Name
				
			if 	Sources ['Run_Type'] [args.Subject_Name[i]] == '':
				Sources ['Run_Type'] [args.Subject_Name[i]] = args.Calc 
						
		# End of per individual parameters check block 
		 
		# Run the network
	
		Temp_Dir_Rel_Index = args.Temp_Dir.find ('vfs://tmp')
		if Temp_Dir_Rel_Index == 0 :
			Temp_Dir=  args.Temp_Dir  
		else : 
			Temp_Dir= 'vfs://tmp/' + args.Temp_Dir  
			
		Output_Base_Dir_Rel_Index = args.Rel_Output_URI.find ('vfs://icv_data')
		Output_XNAT_Rel_Index=args.Rel_Output_URI.find ('xnat://')		
		if Output_Base_Dir_Rel_Index == 0 or Output_XNAT_Rel_Index == 0 :
			Output_Base_Dir= args.Rel_Output_URI # The Based directory of the results without pre-padded vfs type address 
		else:
			Output_Base_Dir= 'vfs://icv_data/' + args.Rel_Output_URI  # The Based directory of the results with pre-padded vfs address.
			
		# Get the source and sink data 
		sourcedata = source_data_for_dict(Network_Input_Mode, Sources)
		sinkdata   = sink_data(Output_Base_Dir, '', FileType, args.query)
		# Build the network
		network    = create_network(Network_Input_Mode,args.Calc, FileType)

		# Print the source and the sink data if needed
		if args.Verbose:
			print 'mode is: \n'
			print Network_Input_Mode
			print 'The final destenation of the output files is: \n'
			print Output_Base_Dir
			print 'The source data is:\n'
			print sourcedata
			print '\n'
			print 'The sink data is:\n'
			print sinkdata
			print '\n'
	
		# Execute the network    
		# # network.draw_network(name = network.id, img_format='svg',draw_dimension = True) # Causes concurrency problems when using qsub (instead of drmaa)
		network.execute (sourcedata,sinkdata,tmpdir= Temp_Dir) 
		
			
	# Case B - Run the network if the JSON dict file was not defined, but the network process an XNAT entry 	
	# The XNAT_URI to process, for example: 
	# 'xnat://xnat.bmia.nl/data/archive/projects/bbmri-wp3-dev/subjects/BMIAXNAT_S01330/experiments/BMIAXNAT_E05072/scans/501/resources/DICOM/files' 	
	elif  args.Network_Input_Mode == 'XNAT' :
			
			Source_Path=args.Rel_Input_URI 
			
			Sample_Name = args.Subject_Name [0]
			
			Output_Base_Dir_Rel_Index = args.Rel_Output_URI.find ('vfs://icv_data')
			Output_XNAT_Rel_Index=args.Rel_Output_URI.find ('xnat://')		
			if Output_Base_Dir_Rel_Index == 0 or Output_XNAT_Rel_Index == 0 :
				Output_Base_Dir= args.Rel_Output_URI # The Based directory of the results without pre-padded vfs type address 
			else:
				Output_Base_Dir= 'vfs://icv_data/' + args.Rel_Output_URI  # The Based directory of the results with pre-padded vfs address.
			
			NIFTI_URI=args.Rel_Input_URI.find('.nii')
			NIFTIGZ_URI=args.Rel_Input_URI.find('.nii.gz')
			MNC_URI=args.Rel_Input_URI.find('.mnc')
			MNCGZ_URI=args.Rel_Input_URI.find('.mnc.gz')
			if NIFTI_URI>0 or NIFTIGZ_URI>0 or MNC_URI>0 or MNCGZ_URI>0 :
				Network_Input_Mode='File' 
			else : 
				Network_Input_Mode='XNAT'  
			
			#Sets the sample_id of the network	
			Temp_Folder_Name=Sample_Name
			
			if 	NIFTI_URI > 0 or NIFTIGZ_URI>0 :
				FileType = 'NIFTI'
			elif MNC_URI > 0 or MNCGZ_URI >0 :
				FileType = 'MINC'
			else :
				FileType = 'DICOM' 
				
			Temp_Dir_Rel_Index = args.Temp_Dir.find ('vfs://tmp') 
			if Temp_Dir_Rel_Index == 0 :
				Temp_Dir =  args.Temp_Dir + '/' + Temp_Folder_Name	
			else : 
				Temp_Dir= 'vfs://tmp/' + args.Temp_Dir + '/' + Temp_Folder_Name
				
			# Get the source and sink data 
			sourcedata = source_data(Source_Path,Sample_Name,Output_Base_Dir,Network_Input_Mode, args, Temp_Folder_Name)
			sinkdata   = sink_data(Output_Base_Dir,Sample_Name, FileType, args.query)
			# Build the network
			network    = create_network(Network_Input_Mode, args.Calc, FileType)
	
			# Print the source and the sink data if needed
			if args.Verbose:
				print 'mode is: \n'
				print Network_Input_Mode
				print 'The final destenation of the output files is: \n'
				print Output_Base_Dir
				print 'The source data is:\n'
				print sourcedata
				print '\n'
				print 'The sink data is:\n'
				print sinkdata
				print '\n'
		
			# Execute the network    
			# network.draw_network(name = network.id, img_format='svg',draw_dimension = True)
			network.execute (sourcedata,sinkdata,tmpdir= Temp_Dir) 
	
	# For local analysis network_Input_Mode can be either 'File' or 'Folder'. The First case applies if one wants to process a single mnc or nii file. 
	# The second case applies if one wants to process a process a single niftii or minc file		
	# Case C - Run the network if the JSON dict file was not defined, but the network process a File entry 		
	elif args.Network_Input_Mode == 'File' : 
		
		Source_Path='vfs://icv_data/' + args.Rel_Input_URI # The path to process relative to the 'vfs://icv_data/' that should be defined in the FASTR .config file 
		
		# Decide if the File type is NIFTI/MINC or DICOM
		NIFTI_URI=args.Rel_Input_URI.find('.nii')
		NIFTIGZ_URI=args.Rel_Input_URI.find('.nii.gz')
		MNC_URI=args.Rel_Input_URI.find('.mnc')
		MNCGZ_URI=args.Rel_Input_URI.find('.mnc.gz')
		if 	NIFTI_URI > 0 or NIFTIGZ_URI>0 :
				FileType = 'NIFTI'
		elif MNC_URI > 0 or MNCGZ_URI >0 :
				FileType = 'MINC'
		else :
				FileType = 'DICOM' 
		
		if args.Subject_Name [0] == 'ICV_Default_Subject': 
			Split_URI=args.Rel_Input_URI.split('/',args.Rel_Input_URI.count('/'))
			if args.Rel_Input_URI.count ('/') > 0 : #The base name for the output directory
				Sample_Name = Split_URI[1]
				if Sample_Name.count('.') > 0 : 
					Split_Dot_URI = Sample_Name.split ('.',1)
					if MNCGZ_URI >0 :
						Sample_Name = Split_Dot_URI [0] + '_'
					else :
						Sample_Name = Split_Dot_URI [0]
			else :
				Sample_Name=Split_URI[0]
				if Sample_Name.count('.') > 0  :
					Split_Dot_URI = Sample_Name.split ('.',1)
					if MNCGZ_URI >0 :
						Sample_Name = Split_Dot_URI [0] + '_'
					else :
						Sample_Name = Split_Dot_URI [0]
		else :  
			Sample_Name = args.Subject_Name [0]
			Split_URI=args.Rel_Input_URI.split('/',args.Rel_Input_URI.count('/'))
			if Split_URI[1].count('.') > 0 : 
				Split_Dot_URI = Split_URI[1].split ('.',1)
				if Split_Dot_URI [0] == Sample_Name and  MNCGZ_URI >0 :
					Sample_Name = Sample_Name + '_'
		
		Output_Base_Dir_Rel_Index = args.Rel_Output_URI.find ('vfs://icv_data')
		if Output_Base_Dir_Rel_Index == 0 :
			Output_Base_Dir= args.Rel_Output_URI # The Based directory of the results without pre-padded vfs type address 
		else:
			Output_Base_Dir= 'vfs://icv_data/' + args.Rel_Output_URI  # The Based directory of the results with pre-padded vfs address.
			
		Network_Input_Mode='File'
		
		# Set the sample_id of the network
		Temp_Folder_Name = Sample_Name
			
		Temp_Dir_Rel_Index = args.Temp_Dir.find ('vfs://tmp') 
		if Temp_Dir_Rel_Index == 0 :
			Temp_Dir=  args.Temp_Dir + '/' + Temp_Folder_Name	
		else : 
			Temp_Dir= 'vfs://tmp/' + args.Temp_Dir + '/' + Temp_Folder_Name				

		# Get the source and sink data 
		sourcedata = source_data(Source_Path, Sample_Name, Output_Base_Dir, Network_Input_Mode, args, Temp_Folder_Name)
		sinkdata   = sink_data(Output_Base_Dir, Sample_Name, FileType, args.query)

		# Build the network
		network    = create_network(Network_Input_Mode, args.Calc, FileType)

		# Print the source and the sink data if needed
		if args.Verbose:
			print 'mode is: \n'
			print Network_Input_Mode
			print 'The final destenation of the output files is: \n'
			print Output_Base_Dir
			print 'The source data is:\n'
			print sourcedata
			print '\n'
			print 'The sink data is:\n'
			print sinkdata
			print '\n'

		# Execute the network    
		# network.draw_network(name = network.id, img_format='svg',draw_dimension = True)
		network.execute (sourcedata,sinkdata,tmpdir= Temp_Dir) 	
				
	# Case D -  Run the network if the JSON dict file was not defined, but the network process a single Folder entry 	
	elif args.Network_Input_Mode == 'Folder' :
		
		Expanded_ICV_Data_Path = fastr.vfs.url_to_path('vfs://icv_data/') 	
		Folder_Path = Expanded_ICV_Data_Path + args.Rel_Input_URI
		for subdir, dirs, files in os.walk(Folder_Path):
			break
		Number_Of_Files = len (files)
		Number_Of_Subject_Names = len (args.Subject_Name)
		Index = 0
		
		for file in files:	
			
			DCM_Index = False
			
			NIFTI_URI = file.find('.nii')
			NIFTIGZ_URI = file.find('.nii.gz')
			MNC_URI = file.find('.mnc')
			MNCGZ_URI = file.find('.mnc.gz')
			DCM_URI = file.find ('dcm')
			
			if 	NIFTI_URI > 0 or NIFTIGZ_URI>0 :
				FileType = 'NIFTI'
				Network_Input_Mode='File'
				Source_Path = 'vfs://icv_data/' + args.Rel_Input_URI + '/' + file # The path to process relative to the 'vfs://icv_data/' that should be defined in the FASTR .config file 
			elif MNC_URI > 0 or MNCGZ_URI >0 :
				FileType = 'MINC'
				Network_Input_Mode='File'
				Source_Path = 'vfs://icv_data/' + args.Rel_Input_URI + '/' + file # The path to process relative to the 'vfs://icv_data/' that should be defined in the FASTR .config file 
			elif  DCM_URI >0 :
				FileType = 'DICOM'
				DCM_Index = True
				Network_Input_Mode='Folder'
				Source_Path = 'vfs://icv_data/' + args.Rel_Input_URI # The path to process relative to the 'vfs://icv_data/' that should be defined in the FASTR .config file 
			else : 
				continue 
				
			if Index>Number_Of_Subject_Names-1 or args.Subject_Name [0] == 'ICV_Default_Subject': 
				Split_URI = file.split ('.',1)
				Sample_Name = Split_URI [0]	
				if MNCGZ_URI >0 :
					Sample_Name = Sample_Name + '_'
				else :
					Sample_Name = Sample_Name 
			else :  
				Sample_Name = args.Subject_Name [Index]	
				if file.count('.') > 0 : 
					Split_Dot_URI = file.split ('.',1)
					if Split_Dot_URI [0] == Sample_Name and  MNCGZ_URI >0 :
						Sample_Name = Sample_Name + '_'	
	
			Output_Base_Dir_Rel_Index = args.Rel_Output_URI.find ('vfs://icv_data')
			if Output_Base_Dir_Rel_Index == 0 :
				Output_Base_Dir= args.Rel_Output_URI # The Based directory of the results without pre-padded vfs type address 
			else:
				Output_Base_Dir= 'vfs://icv_data/' + args.Rel_Output_URI  # The Based directory of the results with pre-padded vfs address.
			
			# Set the sample_id of the network
			Temp_Folder_Name = Sample_Name
								
			Temp_Dir_Rel_Index = args.Temp_Dir.find ('vfs://tmp') 
			if Temp_Dir_Rel_Index == 0 :
				Temp_Dir=  args.Temp_Dir + '/' + Temp_Folder_Name	
			else : 
				Temp_Dir= 'vfs://tmp/' + args.Temp_Dir + '/' + Temp_Folder_Name				

			# Get the source and sink data 
			sourcedata = source_data(Source_Path, Sample_Name, Output_Base_Dir, Network_Input_Mode, args, Temp_Folder_Name)
			sinkdata   = sink_data(Output_Base_Dir, Sample_Name, FileType, args.query)

			# Build the network
			network    = create_network(Network_Input_Mode, args.Calc, FileType)

			# Print the source and the sink data if needed
			if args.Verbose:
				print 'mode is: \n'
				print Network_Input_Mode
				print 'The final destenation of the output files is: \n'
				print Output_Base_Dir
				print 'The source data is:\n'
				print sourcedata
				print '\n'
				print 'The sink data is:\n'
				print sinkdata
				print '\n'

			# Execute the network    
			# network.draw_network(name = network.id, img_format='svg',draw_dimension = True)
			network.execute (sourcedata,sinkdata,tmpdir= Temp_Dir) 
			
			if 	DCM_Index :
				break
				
			Index = Index + 1
									
	# Case E -  Print an error massage since the the Network mode was not defined	
	else:
		print 'Cannot process the script since no network mode was defined or no json dictionary file was defined, please run the --help flag'
	
	return 0
	
if __name__ == '__main__':
    main();

