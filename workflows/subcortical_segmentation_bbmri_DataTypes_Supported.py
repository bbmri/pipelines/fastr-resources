#!/usr/bin/env python

import fastr
import argparse
import json
import datetime
import pdb

# Defaults
CMA_labels = {'Left-Thalamus-Proper':     10,
              'Left-Caudate':             11,
              'Left-Putamen':             12,
              'Left-Pallidum':            13,
              'Brain-Stem_4th-Ventricle': 16,
              'Left-Hippocampus':         17,
              'Left-Amygdala':            18,
              'Left-Accumbens-area':      26,
              'Right-Thalamus-Proper':    49,
              'Right-Caudate':            50,
              'Right-Putamen':            51,
              'Right-Pallidum':           52,
              'Right-Hippocampus':        53,
              'Right-Amygdala':           54,
              'Right-Accumbens-area':     58}


def source_data(dict_file, experiments=[]):

    with open(dict_file) as input_file:
        data = json.load(input_file)

    if not experiments:
        [experiments.append(key) for key,value in data.iteritems()]

    sourcedata = { 't1_source': {}, 'label_source': CMA_labels }
    for experiment in experiments:
        if experiment in data:
            experiment_data = data[experiment]
            print experiment
            if 't1w_dicom' in experiment_data:
                sourcedata['t1_source'][experiment] = experiment_data['t1w_dicom']
	    elif 't1w_nifti' in experiment_data:
		sourcedata['t1_source'][experiment] = experiment_data['t1w_nifti']
	    elif 't1w_mnc' in experiment_data:
		sourcedata['t1_source'][experiment] = experiment_data['t1w_mnc']	

    return sourcedata


def sink_data(basedir, query_string=""):

    # e.g. basedir      = xnat://xnat.bmia.nl/data/archive/projects/bbmri-wp3-dev/experiments/{sample_id}/assessors/{sample_id}_subcortical_DCCN/resources/run_{timestamp}/files/
    #      query_string = "?assessors_type=xnat:qcAssessmentData&resources_type=xnat:resourceCatalog"

    st=datetime.datetime.now().strftime("%Y_%m_%d_%H_%M%p")

    if basedir.find ('xnat://') == 0 :
    	sinkdata = {'segmentation_image':  basedir + '/{sample_id}/first_seg_{sample_id}.nii.gz' + query_string,
        	    't1_to_std_mat':       basedir + '/{sample_id}/first_seg_to_std_mat_{sample_id}{ext}' + query_string,
               	    'subcortical_volumes': basedir + '/{sample_id}/vol_{sample_id}.txt' + query_string,
                    't1_image':            basedir + '/{sample_id}/t1w_{sample_id}{ext}' + query_string,
                    'QC_report':           basedir + '/{sample_id}/slicesdir' + query_string}
    else: 
	sinkdata = {'segmentation_image':  'vfs://results/' + basedir + '/{sample_id}/first_seg_{sample_id}_' + st + '.nii.gz' ,
                    't1_to_std_mat':       'vfs://results/' + basedir + '/{sample_id}/first_seg_to_std_mat_{sample_id}_' + st + '{ext}',
                    'subcortical_volumes': 'vfs://results/' + basedir +  '/{sample_id}/vol_{sample_id}_' + st + '.txt',
                    't1_image':            'vfs://results/' + basedir + '/{sample_id}/t1w_{sample_id}_' + st + '{ext}',
                    'QC_report':           'vfs://results/' + basedir + '/{sample_id}/slicesdir_' + st}

    return sinkdata

def create_network(ScanType):

    # Create the network nodes
    network           = fastr.Network(                                  id_='subcortical_segmentation')
    if ScanType == 'Dicom':
    	t1_source     = network.create_source('DicomImageFile',         id_='t1_source')
	dcm2nii_node  = network.create_node('DicomToNiftiLongPath',     id_='dicom_to_nifti')
	t1_sink       = network.create_sink('NiftiImageFileCompressed', id_='t1_image')
    elif ScanType == 'Nifti':
	t1_source     = network.create_source('NiftiImageFile',         id_='t1_source')
    elif ScanType == 'Minc':
	t1_source     = network.create_source('MincImageFile',          id_='t1_source')
	mnc2nii       = network.create_node('Mnc2Nii',                  id_='mnc2nii') 		
    label_source      = network.create_source('Int',                    id_='label_source')
    edge_source       = network.create_constant('Float', [0.5],         id_='bin_edge')
    first_seg_node    = network.create_node('RunFirstAllSGE',           id_='first_segmentation')
    add_node          = network.create_node('Add',                      id_='add')
    subtract_node     = network.create_node('Subtract',                 id_='subtract')
    volume_node       = network.create_node('FSLStats',                 id_='volume_stats')
    slicesdir_node    = network.create_node('FirstROISlicesDir',        id_='slicesdir_report')
    volumesum_node    = network.create_node('FSLStatsVolume',           id_='aggregate_volumes')
    volume_sink       = network.create_sink('TxtFile',                  id_='subcortical_volumes')
    t12std_mat_sink   = network.create_sink('MatFile',                  id_='t1_to_std_mat')
    slicesdir_sink    = network.create_sink('Directory',                id_='QC_report')
    segmentation_sink = network.create_sink('NiftiImageFileCompressed', id_='segmentation_image')

    # Manage the data flow / cardinalities
    volume_node.inputs['image'].input_group = 't1_source'

    # Set the memory requirements
    t1_source.required_time           = '600'
    t1_source.required_memory         = '3g'
    first_seg_node.required_memory    = '8g'
    first_seg_node.required_time      = '1800'
    volume_node.required_memory       = '3g'
    subtract_node.required_memory     = '3g'
    add_node.required_memory          = '3g'
    slicesdir_node.required_memory    = '3g' 
    add_node.required_memory          = '3g'
    volumesum_node.required_memory    = '4g'
    if ScanType == 'Dicom': 
	dcm2nii_node.required_memory  = '8g'
	t1_sink.required_time         = '600'
	t1_sink.required_memory       = '3g'
    elif ScanType == 'Minc':
	mnc2nii.required_memory       = '8g'
    segmentation_sink.required_memory = '4g'
    slicesdir_sink.required_memory    = '4g' 
    t12std_mat_sink.required_memory   = '4g'
    volume_sink.required_memory       = '4g'
    slicesdir_sink.required_memory    = '4g'
    volume_sink.required_memory       = '4g'
    


    # Set the datatype of the sources/constants to avoid complains from fastr (this shouldn't be necessary, really (can be figured out at runtime or by propagation))
    if ScanType == 'Dicom':
    	dcm2nii_node.outputs['image'].datatype          = fastr.typelist['NiftiImageFileCompressed']
    	dcm2nii_node.outputs['image_reorient'].datatype = fastr.typelist['NiftiImageFileCompressed']
    	dcm2nii_node.outputs['image_cropped'].datatype  = fastr.typelist['NiftiImageFileCompressed']

    # Create the network links
    # Volume = fslstats output_name_all_fast_firstseg -l 16.5 -u 17.5 -V where the first number of the output is the number of voxels and the second is the volume in mm3
    label_source.output                     >> subtract_node.inputs['left_hand']
    edge_source.output                      >> subtract_node.inputs['right_hand']
    label_source.output                     >> add_node.inputs['left_hand']
    edge_source.output                      >> add_node.inputs['right_hand']
    if ScanType == 'Dicom':
    	t1_source.output                    >> dcm2nii_node.inputs['dicom_image']
	dcm2nii_node.outputs['image_cropped'] >> first_seg_node.inputs['t1_image']
    elif ScanType == 'Nifti':
	t1_source.output                    >> first_seg_node.inputs['t1_image']
    elif ScanType == 'Minc':
	t1_source.output                    >> mnc2nii.inputs ['Input_File']
	mnc2nii.outputs ['Converted_Image']  >> first_seg_node.inputs['t1_image']	
    first_seg_node.outputs['firstseg']      >> volume_node.inputs['image']
    subtract_node.outputs['result']         >> volume_node.inputs['lower_threshold']
    add_node.outputs['result']              >> volume_node.inputs['upper_threshold']
    if ScanType == 'Dicom':
    	dcm2nii_node.outputs['image_cropped'] >> slicesdir_node.inputs['t1_image']     #).collapse = 't1_source'
    elif ScanType == 'Nifti':
	t1_source.output                    >> slicesdir_node.inputs['t1_image']	
    elif ScanType == 'Minc':
	mnc2nii.outputs ['Converted_Image']  >> slicesdir_node.inputs['t1_image'] 	
    first_seg_node.outputs['firstseg']      >> slicesdir_node.inputs['label_image']  #).collapse = 't1_source'
    (volume_node.outputs['nonzero_volume']  >> volumesum_node.inputs['volume_stats']).collapse = 'label_source'
    volumesum_node.outputs['volume_csv']    >> volume_sink.input
    if ScanType == 'Dicom':	
    	dcm2nii_node.outputs['image_cropped']   >> t1_sink.input
    first_seg_node.outputs['firstseg']      >> segmentation_sink.input
    first_seg_node.outputs['t1_to_std_mat'] >> t12std_mat_sink.input
    slicesdir_node.outputs['report']        >> slicesdir_sink.input

    return network


def main():

    # Parsing arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--subjects', type=unicode, required=False, nargs='*', default=[], help='Subject name(s) from the dictionary, leave empty to process all source data')
    parser.add_argument('--dict',     type=unicode, required=True,  help='Dictionary file with source data')
    parser.add_argument('--temp',     type=unicode, required=True,  help='Temp directory')
    parser.add_argument('--out',      type=unicode, required=True,  help='Fastr output directory (e.g., a url starting with vfs://)')
    parser.add_argument('--query',    type=unicode, required=False, help='URL query string that will be appended to Fastr output')
    parser.add_argument('--scan_type',type=unicode, required=True, help='Set Input scans in dicom / nifti / minc format')

    args = parser.parse_args()
    print (args.scan_type) 
    if (not ((args.scan_type == 'Nifti') or (args.scan_type == 'Minc') or (args.scan_type == 'Dicom'))):
	exit('Must define the scan type and it should be either Nifti, or Dicom, or Minc')

    # Get the source- and sink-data and execute the network
    sourcedata = source_data(args.dict, args.subjects)
    sinkdata   = sink_data(args.out, args.query)

    print('source data:')
    for sample in sourcedata['t1_source']:
        print('  {}'.format(sample))
    print('sink_data:')
    print(json.dumps(sinkdata, indent=2))
    
    Add_Base_Temp_Location = args.temp.find('vfs://')
    if (Add_Base_Temp_Location < 0):	
    	TmpDir = 'vfs://tmp/' + args.temp
    else:
	TmpDir = args.temp 	

    network = create_network(args.scan_type)
    network.draw_network(name=network.id, draw_dimension=True)
    network.execute(sourcedata, sinkdata, tmpdir=TmpDir)


if __name__ == '__main__':
    main()
