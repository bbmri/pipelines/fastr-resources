# -*- coding: utf-8 -*-
"""
Created on Wed Aug 16 13:42:51 2017

@author: Seyed Mostafa Kia (m.kia83@gmail.com), Marcel Zwiers (m.zwiers@donders.ru.nl)
"""

#!/usr/bin/env python

import fastr
import argparse
import json

#smooth_kernel = 5.0

def source_data(dict_file, mask, basis, kernel, subjects=[]):
    
    with open(dict_file) as input_file:
        data = json.load(input_file)
        
    if not subjects:
        [subjects.append(key) for key,value in data.iteritems()]
        
    sourcedata = {'t1_image': {}, 'mask': mask,'basis': basis, 'kernel':kernel}
    for subject in subjects:
        if subject in data:
            subject_data = data[subject]
            sourcedata['t1_image'][subject] = subject_data['t1w_dicom']
            
    return sourcedata


def sink_data(basedir, query_string=""):

    sinkdata = {'coefficients':  basedir + 'coefficients.txt' + unicode(query_string),
                'negloglik':     basedir + 'negloglik.txt' + unicode(query_string),
                'explainedvar':  basedir + 'explainedvar.txt' + unicode(query_string)
                }

    return sinkdata
    
def create_network():
    # Creating the nodes
    network             = fastr.Network(                                    id_='trend_surface_modelling') 
    t1_image            = network.create_source('DicomImageFile',           id_='t1_image',             stepid='anatomical_preprocessing')
    dcm2nii_T1_node     = network.create_node('DicomToNiftiLongPath',       id_='dicom_to_nifti_T1',    stepid='anatomical_preprocessing')
    anat_node           = network.create_node('FSLAnat',                    id_='anat_preprocessing',   stepid='anatomical_preprocessing')
    smoothing_source    = network.create_source('Float',                    id_='kernel',               stepid='anatomical_preprocessing')
    #smoothing_constant  = network.create_constant('Float', smooth_kernel,   id_='smooth_kernel',        stepid='anatomical_preprocessing')
    smooth_node         = network.create_node('FSLMaths',                   id_='smoothing',            stepid='anatomical_preprocessing')
    basis_source        = network.create_source('Int',                      id_='basis',                stepid='TSM')
    mask_source         = network.create_source('NiftiImageFileCompressed', id_='mask',                 stepid='TSM')        
    merge_node          = network.create_node('FSLMerge',                   id_='t1_merge',             stepid='TSM')    
    tsm_node            = network.create_node('trendsurf',                  id_='tsm',                  stepid='TSM')
    tsm_sink            = network.create_sink('TxtFile',                    id_='coefficients',         stepid='TSM')
    nll_sink            = network.create_sink('TxtFile',                    id_='negloglik',            stepid='TSM')
    exv_sink            = network.create_sink('TxtFile',                    id_='explainedvar',         stepid='TSM')

    # Set the datatype of the sources to avoid complains from fastr (this shouldn't be necessary, really (can be figured out at runtime or by propagation))
    #dcm2nii_T1_node.outputs['image_cropped'].datatype   =   fastr.typelist['NiftiImageFileCompressed']
    #merge_node.outputs['image'].datatype                =   fastr.typelist['NiftiImageFileCompressed']

    anat_node.required_time         = '7200'
    anat_node.required_memory       = '4g'
    tsm_node.required_time          = '18000'
    tsm_node.required_memory        = '16g'
    
    # Creating the links
    t1_image.output                                     >>  dcm2nii_T1_node.inputs['dicom_image']
    dcm2nii_T1_node.outputs['image_cropped']            >>  anat_node.inputs['image']
    (True,)                                             >>  anat_node.inputs['no_subcort_seg']
    smoothing_source.output                             >>  smooth_node.inputs['operator1_number']
    ('-s',)                                             >>  smooth_node.inputs['operator1'] 
    anat_node.outputs['T1_to_MNI_nonlin_image']         >>  smooth_node.inputs['image1']
    (smooth_node.outputs['output_image']                >>  merge_node.inputs['images']).collapse = 't1_image'
    (True,)                                             >>  merge_node.inputs['t_flag']
    mask_source.output                                  >>  tsm_node.inputs['mask']    
    basis_source.output                                 >>  tsm_node.inputs['basis']
    merge_node.outputs['image']                         >>  tsm_node.inputs['images']
    tsm_node.outputs['tsf_coefs']                       >>  tsm_sink.input
    tsm_node.outputs['negloglik']                       >>  nll_sink.input
    tsm_node.outputs['explainded_variance']             >>  exv_sink.input
    
    return network


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--subjects', type=unicode, required=False, nargs='*', default=[], help='Subject name(s) from the dictionary, leave empty to process all source data')
    parser.add_argument('--dict',     type=unicode, required=True,  help='Dictionary file with source data')
    parser.add_argument('--temp',     type=unicode, required=True,  help='Temp directory')
    parser.add_argument('--out',      type=unicode, required=True,  help='Fastr output directory (e.g., a url starting with vfs://)')
    parser.add_argument('--mask',     type=unicode, required=True,  help='Mask.')
    parser.add_argument('--basis',    type=unicode, required=True,  help='The degree of plynomial basis functions.')
    parser.add_argument('--kernel',   type=unicode, required=False, default=5.0, help='Kernel size in mm for smoothing (default=5).')
    parser.add_argument('--query',    type=unicode, required=False, help='URL query string that will be appended to Fastr output')
    args = parser.parse_args()

    # Get the source- and sink-data and execute the network
    sourcedata = source_data(args.dict, args.mask, args.basis, args.kernel, args.subjects)
    sinkdata   = sink_data(args.out, args.query)

    print('source data:')
    for sample in sourcedata['t1_image']:
        print('  {}'.format(sample))
    print('sink_data:')
    print(json.dumps(sinkdata, indent=2))
    
    network = create_network()
    network.draw_network(name=network.id, img_format='svg',draw_dimension=True)
    network.execute(sourcedata, sinkdata, tmpdir=args.temp)

if __name__ == '__main__':
    main()
