# -*- coding: utf-8 -*-
"""
Created on Fri May 19 14:15:32 2017

@authors: Seyed Mostafa Kia (m.kia83@gmail.com), Marcel Zwiers (m.zwiers@donders.ru.nl)
"""

#!/usr/bin/env python

import fastr
import argparse
import json


erode_kernel = 3         # WM/CSF erosion kernel box <size> : all voxels in a box of width <size> centered on target voxel
smooth_kernel = 5.0
bptf   = [50, -1]  # Highpass/lowpass filter frequency: high/lowpass_sigma = halfmax / TR = 0.5 / (high/lowpass_freq * TR) => bptf = 0.5 / high/lowpass_freq
mask_thr = 15.0

def source_data(dict_file, master_setup, subjects=[]):
    
    with open(dict_file) as input_file:
        data = json.load(input_file)
        
    if not subjects:
        [subjects.append(key) for key,value in data.iteritems()]
        
    sourcedata = {'t1_image': {}, 'fmri_images1': {}, 'fmri_images2': {}, 'fmri_images3': {}, 'fmri_images4': {}, 'fmri_images5': {}, 
                  'scale': 5, 'master_setup': master_setup}#, 'fmri_tr': 2.0}
    for subject in subjects:
        if subject in data:
            subject_data = data[subject]
            sourcedata['t1_image'][subject] = subject_data['t1']
            sourcedata['fmri_images1'][subject] = subject_data['fmri_echo1']
            sourcedata['fmri_images2'][subject] = subject_data['fmri_echo2']
            sourcedata['fmri_images3'][subject] = subject_data['fmri_echo3']
            sourcedata['fmri_images4'][subject] = subject_data['fmri_echo4']
            sourcedata['fmri_images5'][subject] = subject_data['fmri_echo5']
            
    return sourcedata


def sink_data(basedir, query_string=""):

    sinkdata = {'seg_results':  basedir + '/{sample_id}/group_functional_segmentation_{sample_id}.nii.gz' + unicode(query_string),
                'dualreg_spatialmaps':  basedir + '/{sample_id}/subject_functional_segmentation_{sample_id}.nii.gz' + unicode(query_string)
                }

    return sinkdata

def create_network():
    # Creating the nodes
    network             = fastr.Network(                                    id_='ICP_workflow') 
    fmri_source1        = network.create_source('DicomImageFile',           id_='fmri_images1',         stepid='fmri_preprocessing')
    fmri_source2        = network.create_source('DicomImageFile',           id_='fmri_images2',         stepid='fmri_preprocessing')
    fmri_source3        = network.create_source('DicomImageFile',           id_='fmri_images3',         stepid='fmri_preprocessing')
    fmri_source4        = network.create_source('DicomImageFile',           id_='fmri_images4',         stepid='fmri_preprocessing')
    fmri_source5        = network.create_source('DicomImageFile',           id_='fmri_images5',         stepid='fmri_preprocessing')
    #tr_source           = network.create_source('Float',                    id_='fmri_tr',              stepid='fmri_preprocessing')
    scanparam_node      = network.create_node('ExtractParam',               id_='extract_TR',           stepid='fmri_preprocessing')
    feat_source         = network.create_source('FsfFile',                  id_='master_setup',         stepid='fmri_preprocessing')
    bptf_constant       = network.create_constant('Float', bptf,            id_='bandpass_halfmax',     stepid='fmri_preprocessing')
    smoothing_constant  = network.create_constant('Float', smooth_kernel,   id_='smooth_kernel',        stepid='fmri_preprocessing')
    dcm2nii_echo1_node  = network.create_node('DicomToNiftiLongPath',       id_='dicom_to_nifti_echo1', stepid='fmri_preprocessing')
    dcm2nii_echo2_node  = network.create_node('DicomToNiftiLongPath',       id_='dicom_to_nifti_echo2', stepid='fmri_preprocessing')
    dcm2nii_echo3_node  = network.create_node('DicomToNiftiLongPath',       id_='dicom_to_nifti_echo3', stepid='fmri_preprocessing')
    dcm2nii_echo4_node  = network.create_node('DicomToNiftiLongPath',       id_='dicom_to_nifti_echo4', stepid='fmri_preprocessing')
    dcm2nii_echo5_node  = network.create_node('DicomToNiftiLongPath',       id_='dicom_to_nifti_echo5', stepid='fmri_preprocessing')
    echo_combine_node   = network.create_node('FSLMaths',                   id_='combine_echoes',       stepid='fmri_preprocessing')
    reorient_node       = network.create_node('FSLReorient2Std',            id_='reorient2std',         stepid='fmri_preprocessing')
    feat_node           = network.create_node('Feat',                       id_='feat_preprocessing',   stepid='fmri_preprocessing')
    aroma_node          = network.create_node('ICAAroma',                   id_='ica_aroma_denoising',  stepid='fmri_preprocessing')
    filter_node         = network.create_node('FSLMaths',                   id_='maths_bandpass_smooth',stepid='fmri_preprocessing')
    divide_node         = network.create_node('Divide',                     id_='bandpass_sigma',       stepid='fmri_preprocessing')
    divide_node_tr      = network.create_node('Divide',                     id_='tr_ms2sec',            stepid='fmri_preprocessing')
    
    t1_image            = network.create_source('DicomImageFile',           id_='t1_image',             stepid='anatomical_preprocessing')
    erode_constant      = network.create_constant('Float', erode_kernel,    id_='erod_kernel',          stepid='anatomical_preprocessing')
    mask_thr_constant   = network.create_constant('Float', mask_thr,        id_='binarize_thr',         stepid='anatomical_preprocessing')
    dcm2nii_T1_node     = network.create_node('DicomToNiftiLongPath',       id_='dicom_to_nifti_T1',    stepid='anatomical_preprocessing')
    anat_node           = network.create_node('FSLAnat',                    id_='anat_preprocessing',   stepid='anatomical_preprocessing')
    warp_csf_node       = network.create_node('ApplyWarp',                  id_='apply_warp_csf',       stepid='anatomical_preprocessing')
    erode_csf_node      = network.create_node('FSLMaths',                   id_='maths_erode_csf',      stepid='anatomical_preprocessing')
    mask_csf_node       = network.create_node('Image2Mask',                 id_='binarize_csf',         stepid='anatomical_preprocessing')
    warp_wm_node        = network.create_node('ApplyWarp',                  id_='apply_warp_wm',        stepid='anatomical_preprocessing')
    erode_wm_node       = network.create_node('FSLMaths',                   id_='maths_erode_wm',       stepid='anatomical_preprocessing')
    mask_wm_node        = network.create_node('Image2Mask',                 id_='binarize_wm',          stepid='anatomical_preprocessing')
        
    scale               = network.create_source('Int',                      id_='scale',                stepid='ICP')
    warp_br_node        = network.create_node('ApplyWarp',                  id_='apply_warp_br',        stepid='ICP')
    roi_node            = network.create_node('FSLMaths',                   id_='ROI',                  stepid='ICP')
    warp_gm_node        = network.create_node('ApplyWarp',                  id_='apply_warp_gm',        stepid='ICP')
    mask_gm_node        = network.create_node('Image2Mask',                 id_='binarize_gm',          stepid='ICP')
    tsmean_wm_node      = network.create_node('FSLMeants',                  id_='meants_wm',            stepid='ICP')
    tsmean_csf_node     = network.create_node('FSLMeants',                  id_='meants_csf',           stepid='ICP')
    tsmean_all_node     = network.create_node('TXTconcat',                  id_='meants_all',           stepid='ICP')
    nuisance_reg_node   = network.create_node('FSLglm',                     id_='nuisance_regression',  stepid='ICP')
    mask_fmri_node      = network.create_node('FSLMaths',                   id_='masking_fmri',         stepid='ICP')
    tsmean_fmri_node    = network.create_node('FSLMeants',                  id_='meants_fmri',          stepid='ICP')
    mean_fmri_node      = network.create_node('FSLMaths',                   id_='mean_fmri',            stepid='ICP')
    std_fmri_node       = network.create_node('FSLMaths',                   id_='std_fmri',             stepid='ICP')
    norm_fmri_node      = network.create_node('FSLMaths',                   id_='norm_fmri',            stepid='ICP')
    icor_node           = network.create_node('ICor',                       id_='instanteneous_corr',   stepid='ICP')
    icor_remean_node    = network.create_node('FSLMaths',                   id_='icor_remean',          stepid='ICP')
    ica_node            = network.create_node('Melodic',                    id_='ica',                  stepid='ICP')
    merge_node          = network.create_node('ProbMerge',                  id_='merge_probmaps',       stepid='ICP')
    segmentation_sink   = network.create_sink('NiftiImageFileCompressed',   id_='seg_results',          stepid='ICP')
    
    dualreg_node        = network.create_node('DualRegression',             id_='dual_regression',      stepid='dual_regression')
    dualreg_seg_node    = network.create_node('FSLMaths',                   id_='dual_reg_seg',         stepid='dual_regression')
    dualreg_space_sink  = network.create_sink('NiftiImageFileCompressed',   id_='dualreg_spatialmaps',  stepid='dual_regression')
    
    
    dcm2nii_echo1_node.required_time    = '300'
    dcm2nii_echo2_node.required_time    = '300'
    dcm2nii_echo3_node.required_time    = '300'
    dcm2nii_echo4_node.required_time    = '300'
    dcm2nii_echo5_node.required_time    = '300'
    dcm2nii_echo1_node.required_memory  = '1g'
    dcm2nii_echo2_node.required_memory  = '1g'
    dcm2nii_echo3_node.required_memory  = '1g'
    dcm2nii_echo4_node.required_memory  = '1g'
    dcm2nii_echo5_node.required_memory  = '1g'
    
    dcm2nii_T1_node.required_time       = '300'
    dcm2nii_T1_node.required_memory     = '1g'
    
    fmri_source1.required_time          = '3600'   
    fmri_source2.required_time          = '3600'   
    fmri_source3.required_time          = '3600'   
    fmri_source4.required_time          = '3600'   
    fmri_source5.required_time          = '3600'
    fmri_source1.required_memory        = '1g'
    fmri_source2.required_memory        = '1g'
    fmri_source3.required_memory        = '1g'
    fmri_source4.required_memory        = '1g'
    fmri_source5.required_memory        = '1g'
    
    t1_image.required_time              = '1800'
    t1_image.required_memory            = '1g'
    
    divide_node.required_time           = '100'
    divide_node.required_memory         = '100m'
    
    divide_node_tr.required_time        = '100'
    divide_node_tr.required_memory      = '100m'
    
    scanparam_node.required_time        = '100'
    scanparam_node.required_memory      = '100m'
    
    echo_combine_node.required_time     = '3600'
    echo_combine_node.required_memory   = '8g'
    
    reorient_node.required_time         = '300'    
    reorient_node.required_memory       = '1g'    
    
    anat_node.required_time             = '3600'
    anat_node.required_memory           = '8g'
    
    warp_csf_node.required_time         = '300'
    warp_csf_node.required_memory       = '1g'
    erode_csf_node.required_time        = '300'
    erode_csf_node.required_memory      = '1g'
    mask_csf_node.required_time         = '300'
    mask_csf_node.required_memory       = '1g'
    warp_wm_node.required_time          = '300'
    warp_wm_node.required_memory        = '1g'
    erode_wm_node.required_time         = '300'
    erode_wm_node.required_memory       = '1g'
    mask_wm_node.required_time          = '300'
    mask_wm_node.required_memory        = '1g'
    
    feat_node.required_time             = '3600'   
    feat_node.required_memory           = '8g'
    aroma_node.required_time            = '30000'
    aroma_node.required_memory          = '8g'
    filter_node.required_time           = '3600' 
    filter_node.required_memory         = '1g'
    
    warp_br_node.required_time          = '300'
    warp_br_node.required_memory        = '1g'
    roi_node.required_time              = '300'
    roi_node.required_memory            = '1g'
    warp_gm_node.required_time          = '300'
    warp_gm_node.required_memory        = '1g'
    mask_gm_node.required_time          = '300'
    mask_gm_node.required_memory        = '1g'
    tsmean_wm_node.required_time        = '300'
    tsmean_wm_node.required_memory      = '1g'
    tsmean_csf_node.required_time       = '300'
    tsmean_csf_node.required_memory     = '1g'
    tsmean_all_node.required_time       = '300'
    tsmean_all_node.required_memory     = '1g'
    
    nuisance_reg_node.required_time     = '3600' 
    nuisance_reg_node.required_memory   = '8g'
    
    mask_fmri_node.required_time        = '300'
    mask_fmri_node.required_memory      = '1g'
    tsmean_fmri_node.required_time      = '300'
    tsmean_fmri_node.required_memory    = '1g'
    mean_fmri_node.required_time        = '300'
    mean_fmri_node.required_memory      = '1g'
    std_fmri_node.required_time         = '300'
    std_fmri_node.required_memory       = '1g'
    norm_fmri_node.required_time        = '300'
    norm_fmri_node.required_memory      = '1g'
    icor_node.required_time             = '300'
    icor_node.required_memory           = '1g'
    icor_remean_node.required_time      = '300'
    icor_remean_node.required_memory    = '1g'
    
    ica_node.required_time              = '3600'   
    ica_node.required_memory            = '10g'
    dualreg_node.required_time          = '3600'   
    dualreg_node.required_memory        = '4g'
    
    merge_node.required_time            = '300'   
    merge_node.required_memory          = '1g'
    dualreg_seg_node.required_time      = '300'   
    dualreg_seg_node.required_memory    = '1g'
        
        
    # Set the datatype of the sources to avoid complains from fastr (this shouldn't be necessary, really (can be figured out at runtime or by propagation))
    dcm2nii_echo1_node.outputs['image'].datatype        =   fastr.typelist['NiftiImageFileCompressed']
    dcm2nii_echo2_node.outputs['image'].datatype        =   fastr.typelist['NiftiImageFileCompressed']
    dcm2nii_echo3_node.outputs['image'].datatype        =   fastr.typelist['NiftiImageFileCompressed']
    dcm2nii_echo4_node.outputs['image'].datatype        =   fastr.typelist['NiftiImageFileCompressed']
    dcm2nii_echo5_node.outputs['image'].datatype        =   fastr.typelist['NiftiImageFileCompressed']
    dcm2nii_T1_node.outputs['image_cropped'].datatype   =   fastr.typelist['NiftiImageFileCompressed']
    reorient_node.outputs['reoriented_image'].datatype  =   fastr.typelist['NiftiImageFileCompressed']
    merge_node.outputs['output'].datatype               =   fastr.typelist['NiftiImageFileCompressed']
    divide_node.outputs['result'].datatype              =   fastr.typelist['Float']
    divide_node_tr.outputs['result'].datatype           =   fastr.typelist['Float']
    divide_node_tr.inputs['right_hand'].datatype        =   fastr.typelist['Float']
    divide_node_tr.inputs['left_hand'].datatype         =   fastr.typelist['Float']
    filter_node.inputs['operator2_number'].datatype     =   fastr.typelist['Float']
    scanparam_node.outputs['value_number']              =   fastr.typelist['Float']
    
    # Creating the links
    fmri_source1.output                                 >>  dcm2nii_echo1_node.inputs['dicom_image']
    fmri_source2.output                                 >>  dcm2nii_echo2_node.inputs['dicom_image']
    fmri_source3.output                                 >>  dcm2nii_echo3_node.inputs['dicom_image']
    fmri_source4.output                                 >>  dcm2nii_echo4_node.inputs['dicom_image']
    fmri_source5.output                                 >>  dcm2nii_echo5_node.inputs['dicom_image']
    t1_image.output                                     >>  dcm2nii_T1_node.inputs['dicom_image']
    dcm2nii_echo1_node.outputs['image']                 >>  echo_combine_node.inputs['image1']
    dcm2nii_echo2_node.outputs['image']                 >>  echo_combine_node.inputs['image2']
    dcm2nii_echo3_node.outputs['image']                 >>  echo_combine_node.inputs['image3']
    dcm2nii_echo4_node.outputs['image']                 >>  echo_combine_node.inputs['image4']
    dcm2nii_echo5_node.outputs['image']                 >>  echo_combine_node.inputs['image5']
    ('-add',)                                           >>  echo_combine_node.inputs['operator1']
    ('-add',)                                           >>  echo_combine_node.inputs['operator2']
    ('-add',)                                           >>  echo_combine_node.inputs['operator3']
    ('-add',)                                           >>  echo_combine_node.inputs['operator4']
    ('-div',)                                           >>  echo_combine_node.inputs['operator5']
    (5.0,)                                              >>  echo_combine_node.inputs['operator5_number']
    echo_combine_node.outputs['output_image']           >>  reorient_node.inputs['image']
    dcm2nii_echo1_node.outputs['report']                >>  scanparam_node.inputs['report']
    ('TR',)                                             >>  scanparam_node.inputs['key']
    reorient_node.outputs['reoriented_image']           >>  feat_node.inputs['fmri_images']
    feat_source.output                                  >>  feat_node.inputs['setup_file']
    #tr_source.output                                    >>  feat_node.inputs['TR']
    dcm2nii_T1_node.outputs['image_cropped']            >>  anat_node.inputs['image']
    anat_node.outputs['biascorr_brain_image']           >>  feat_node.inputs['t1_brain_image']
    #tr_source.output                                    >>  divide_node.inputs['right_hand']
    divide_node_tr.outputs['result']                    >>  divide_node.inputs['right_hand']
    bptf_constant.output                                >>  divide_node.inputs['left_hand']
    scanparam_node.outputs['value_number']              >>  divide_node_tr.inputs['left_hand']
    (1000.0,)                                           >>  divide_node_tr.inputs['right_hand']
    feat_node.outputs['feat_directory']                 >>  aroma_node.inputs['feat_directory']
    aroma_node.outputs['denoised_nonaggr_images']       >>  filter_node.inputs['image1']
    ('-bptf',)                                          >>  filter_node.inputs['operator1']
    (divide_node.outputs['result']                      >>  filter_node.inputs['operator1_number']).collapse = 'bandpass_halfmax'
    divide_node_tr.outputs['result']                    >>  feat_node.inputs['TR']  
    smoothing_constant.output                           >>  filter_node.inputs['operator2_number']
    ('-s',)                                             >>  filter_node.inputs['operator2']
    (True,)                                             >>  anat_node.inputs['no_subcort_seg']    
    anat_node.outputs['fast_pve_0_image']               >>  erode_csf_node.inputs['image1']
    anat_node.outputs['fast_pve_2_image']               >>  erode_wm_node.inputs['image1']
    ('-kernel',)                                        >>  erode_csf_node.inputs['operator1']
    ('-kernel',)                                        >>  erode_wm_node.inputs['operator1']
    ('box',)                                            >>  erode_csf_node.inputs['operator1_string']
    ('box',)                                            >>  erode_wm_node.inputs['operator1_string']
    erode_constant.output                               >>  erode_csf_node.inputs['operator1_number']
    erode_constant.output                               >>  erode_wm_node.inputs['operator1_number']
    ('-ero',)                                           >>  erode_csf_node.inputs['operator2']
    ('-ero',)                                           >>  erode_wm_node.inputs['operator2']
    erode_csf_node.outputs['output_image']              >>  warp_csf_node.inputs['source_image']
    erode_wm_node.outputs['output_image']               >>  warp_wm_node.inputs['source_image']
    feat_node.outputs['example_image']                  >>  warp_csf_node.inputs['reference_image']
    feat_node.outputs['example_image']                  >>  warp_wm_node.inputs['reference_image']
    feat_node.outputs['highres2example_func']           >>  warp_csf_node.inputs['pre_mat']
    feat_node.outputs['highres2example_func']           >>  warp_wm_node.inputs['pre_mat']
    warp_wm_node.outputs['warped_image']                >>  mask_wm_node.inputs['image']
    mask_thr_constant.output                            >>  mask_wm_node.inputs['percentile']
    warp_csf_node.outputs['warped_image']               >>  mask_csf_node.inputs['image']
    mask_thr_constant.output                            >>  mask_csf_node.inputs['percentile']
    anat_node.outputs['fast_pve_1_image']               >>  warp_gm_node.inputs['source_image']
    feat_node.outputs['example_image']                  >>  warp_gm_node.inputs['reference_image']
    feat_node.outputs['highres2example_func']           >>  warp_gm_node.inputs['pre_mat']
    warp_gm_node.outputs['warped_image']                >>  mask_gm_node.inputs['image']
    mask_thr_constant.output                            >>  mask_gm_node.inputs['percentile']
    anat_node.outputs['biascorr_brain_mask_image']      >>  warp_br_node.inputs['source_image']
    feat_node.outputs['example_image']                  >>  warp_br_node.inputs['reference_image']
    feat_node.outputs['highres2example_func']           >>  warp_br_node.inputs['pre_mat']
    mask_gm_node.outputs['mask']                        >>  roi_node.inputs['image1']
    ('-mas',)                                           >>  roi_node.inputs['operator1']
    warp_br_node.outputs['warped_image']                >>  roi_node.inputs['image2']
    filter_node.outputs['output_image']                 >>  tsmean_wm_node.inputs['fmri_image']
    filter_node.outputs['output_image']                 >>  tsmean_csf_node.inputs['fmri_image']
    mask_wm_node.outputs['mask']                        >>  tsmean_wm_node.inputs['mask']    
    mask_csf_node.outputs['mask']                       >>  tsmean_csf_node.inputs['mask']
    tsmean_wm_node.outputs['output']                    >>  tsmean_all_node.inputs['ts1']
    tsmean_csf_node.outputs['output']                   >>  tsmean_all_node.inputs['ts2']
    tsmean_all_node.outputs['output']                   >>  nuisance_reg_node.inputs['design']
    filter_node.outputs['output_image']                 >>  nuisance_reg_node.inputs['image']
    nuisance_reg_node.outputs['residual']               >>  mask_fmri_node.inputs['image1']
    ('-mas',)                                           >>  mask_fmri_node.inputs['operator1']
    roi_node.outputs['output_image']                    >>  mask_fmri_node.inputs['image2']
    mask_fmri_node.outputs['output_image']              >>  tsmean_fmri_node.inputs['fmri_image']
    roi_node.outputs['output_image']                    >>  tsmean_fmri_node.inputs['mask']
    mask_fmri_node.outputs['output_image']              >>  mean_fmri_node.inputs['image1']
    ('-Tmean',)                                         >>  mean_fmri_node.inputs['operator1']
    mask_fmri_node.outputs['output_image']              >>  std_fmri_node.inputs['image1']
    ('-Tstd',)                                          >>  std_fmri_node.inputs['operator1']
    mask_fmri_node.outputs['output_image']              >>  norm_fmri_node.inputs['image1']
    ('-sub',)                                           >>  norm_fmri_node.inputs['operator1']
    ('-div',)                                           >>  norm_fmri_node.inputs['operator2']
    ('-mas',)                                           >>  norm_fmri_node.inputs['operator3']
    mean_fmri_node.outputs['output_image']              >>  norm_fmri_node.inputs['image2']
    std_fmri_node.outputs['output_image']               >>  norm_fmri_node.inputs['image3']
    roi_node.outputs['output_image']                    >>  norm_fmri_node.inputs['image4']
    norm_fmri_node.outputs['output_image']              >>  icor_node.inputs['fmri_image']
    tsmean_fmri_node.outputs['output']                  >>  icor_node.inputs['mean_ts']
    icor_node.outputs['output']                         >>  icor_remean_node.inputs['image1']
    ('-add',)                                           >>  icor_remean_node.inputs['operator1']
    ('-mas',)                                           >>  icor_remean_node.inputs['operator2']
    ('-nan',)                                           >>  icor_remean_node.inputs['operator3']
    mean_fmri_node.outputs['output_image']              >>  icor_remean_node.inputs['image2']
    roi_node.outputs['output_image']                    >>  icor_remean_node.inputs['image3']
    (icor_remean_node.outputs['output_image']           >>  ica_node.inputs['fmri_images']).collapse = 'fmri_images1'
    scale.output                                        >>  ica_node.inputs['dimension']
    (True,)                                             >>  ica_node.inputs['out_stats']    
    (True,)                                             >>  ica_node.inputs['report']
    ('=pow3',)                                          >>  ica_node.inputs['nonlinearity']
    ica_node.outputs['output_dir']                      >>  merge_node.inputs['ica_dir']
    roi_node.outputs['output_image']                    >>  merge_node.inputs['mask']
    #ica_node.outputs['output_dir']                      >>  icp_sink.input
    merge_node.outputs['output']                        >>  segmentation_sink.input
    
    ica_node.outputs['ica_templates']                   >>  dualreg_node.inputs['group_ica_maps']
    filter_node.outputs['output_image']                 >>  dualreg_node.inputs['fmri_images']
    dualreg_node.outputs['dr_stage2']                   >>  dualreg_seg_node.inputs['image1']
    ('-Tmaxn',)                                         >>  dualreg_seg_node.inputs['operator1']
    ('-add',)                                           >>  dualreg_seg_node.inputs['operator2']
    (1,)                                                >>  dualreg_seg_node.inputs['operator2_number']
    ('-mas',)                                           >>  dualreg_seg_node.inputs['operator3']
    roi_node.outputs['output_image']                    >>  dualreg_seg_node.inputs['image4']
    dualreg_seg_node.outputs['output_image']            >>  dualreg_space_sink.input
    return network


def main():

    # Parsing arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--subjects', type=unicode, required=False, nargs='*', default=[], help='Subject name(s) from the dictionary, leave empty to process all source data')
    parser.add_argument('--dict',     type=unicode, required=True,  help='Dictionary file with source data')
    parser.add_argument('--temp',     type=unicode, required=True,  help='Temp directory')
    parser.add_argument('--out',      type=unicode, required=True,  help='Fastr output directory (e.g., a url starting with vfs://)')
    parser.add_argument('--setup',    type=unicode, required=True,  help='Master setup file for FEAT.')
    parser.add_argument('--query',    type=unicode, required=False, help='URL query string that will be appended to Fastr output')
    args = parser.parse_args()

    # Get the source- and sink-data and execute the network
    sourcedata = source_data(args.dict, args.setup, args.subjects)
    sinkdata   = sink_data(args.out, args.query)

    print('source data:')
    for sample in sourcedata['t1_image']:
        print('  {}'.format(sample))
    print('sink_data:')
    print(json.dumps(sinkdata, indent=2))
    
    network = create_network()
    network.draw_network(name=network.id, img_format='svg',draw_dimension=True)
    network.execute(sourcedata, sinkdata, tmpdir=args.temp)


if __name__ == '__main__':
    main()
