import fastr 
import os
import argparse
import xnat
import json
import pdb #pdb.set_trace()

def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

def source_data_XNAT(xnat, project, label, Subject_Name, Subject_Label, Aggregated_Action_File, Action): 
	
	sourcedata = {'XNAT_Server': {}, 'Project': {}, 'Label': {}, 'Subject_Name': {},  'Subject_Label': {}, 'Aggregated_Action_File': {} , 'Action': {},}
	
	sourcedata	['XNAT_Server'] = xnat
	sourcedata	['Project'] = project
	sourcedata	['Label'] = label
	sourcedata	['Subject_Name'] = Subject_Name
	sourcedata	['Subject_Label'] = Subject_Label
	sourcedata	['Aggregated_Action_File'] = Aggregated_Action_File
	sourcedata	['Action'] = Action		
	
	return sourcedata   	


def sink_data(temp_dir):
		
	sinkdata = {'Subject_Action_File': {},}
	
	sinkdata ['Subject_Action_File'] = temp_dir + '/Clean_XNAT_Output/Subject_Action_File.txt'
				
	return sinkdata
	
def create_network():
	
	network = fastr.Network(id_='Clean_XNAT')
	
	Aggregated_Action_File = network.create_source(fastr.typelist['TxtFile'],id_='Aggregated_Action_File',stepid='Sources')
	Subject_Name = 	network.create_source(fastr.typelist['String'],id_='Subject_Name',stepid='Sources')
	
	Script_Results = network.create_sink (fastr.typelist['Int'],id_='Subject_Action_File',stepid='Sinks')		
	Script_Results.required_memory = '4g'
		
	Clean_XNAT_Subject = network.create_node(fastr.toollist['Clean_XNAT'],id_='Clean_Subject_Result_File',stepid='Operation')	
	Clean_XNAT_Subject.required_memory = '8g'
	
	Subject_Label = network.create_source(fastr.typelist['String'],id_='Subject_Label',stepid='Sources')
	XNAT_Server = network.create_source(fastr.typelist['String'],id_='XNAT_Server',stepid='Sources')
	Project = network.create_source(fastr.typelist['String'],id_='Project',stepid='Sources')
	Label = network.create_source(fastr.typelist['String'],id_='Label',stepid='Sources')
	Action = network.create_source(fastr.typelist['Boolean'],id_='Action',stepid='Sources')
	
	Subject_Name.output >> Clean_XNAT_Subject.inputs ['Subject_Name']
	Subject_Label.output >> Clean_XNAT_Subject.inputs ['Subject_Label']
	XNAT_Server.output >> Clean_XNAT_Subject.inputs ['XNAT_Server']
	Project.output >> Clean_XNAT_Subject.inputs ['Project']
	Label.output >> Clean_XNAT_Subject.inputs ['Label']
	Aggregated_Action_File.output >> Clean_XNAT_Subject.inputs ['Out_File']	
	Action.output >> Clean_XNAT_Subject.inputs ['Delete_Files']		
	
	Sink_link = network.create_link(Clean_XNAT_Subject.outputs['Return_Value'], Script_Results.inputs['input'])
	
	return network
	
def pad_to_aggregated_action_file (file_name, Subject_Name) :

	Aggregated_File_Exist = os.path.isfile(file_name) 
	
	if Aggregated_File_Exist :
		Aggregated_file = open(file_name, 'a')
		Aggregated_file.write("{}\t{}\t{}\n".format(Subject_Name, '-', '-' ))
		Aggregated_file.close()
		return 1
	else : 
		return 0

def JSON_Read_Arguments (dict_file):

	with open(dict_file) as input_file:
		data = json.load(input_file)
		input_file.close
		
	uri_source_path = data	

	return uri_source_path

def main():
	
	# Parse the program command line parameters  
	parser = argparse.ArgumentParser()
	parser.add_argument('--xnat', type=unicode, dest='xnat', default='xnat.bmia.nl', required=False, help='xnat host url')
	parser.add_argument('--project', type=unicode, dest='project', default='bbmri-wp3-dev', required=False, help='xnat project')
	parser.add_argument('--subjects', type=unicode, required=False, nargs='+', help='xnat subjects')
	parser.add_argument('--label', type=unicode, dest='label', required=True, help='Label to use for the xnat search')
	parser.add_argument('--Dict', type=unicode, dest='Dict', required=True, default='', help='Dictionary file with source data.')
	parser.add_argument('--Aggregated_Action_File', dest='Aggregated_Action_File', type=unicode, required=True, default='', help='File that will include all the aggregated statistics results')
	parser.add_argument('--Temp', type=unicode, dest='Temp_Dir',required=False, default='vfs://tmp', help='Temp directory')
	parser.add_argument('--Action', type=str2bool, dest='Action', required=False, default=False, help='Whether to actually delete files or not')
	
	args = parser.parse_args()
	
	# Checking that the aggregated statistics file exist and creating it if it does not. 
	
	Aggregated_File_Exist = os.path.isfile(args.Aggregated_Action_File) 
	
	if not Aggregated_File_Exist : 
		file = open(args.Aggregated_Action_File, 'w+')
		file.close()
		
	Dict_File_Exist = os.path.isfile(args.Dict) 
	
	if Dict_File_Exist :
		Data=JSON_Read_Arguments (args.Dict)
	else:
		print 'Can not find the Dict file to read. Double check the dict file path'
		return 0
	n=0	
	for item in Data.iteritems():
		if args.subjects is not None :
				if item[0] not in args.subjects :
					continue
		Subject_Name=item [0]
		Subject_Label = item [1] ['subject']
		if 't1w_dicom' in item [1]:
			File_Address = item [1]['t1w_dicom']
		elif 't1w_nifti' in item [1]:	
			File_Address = item [1]['t1w_nifti']
		elif 't1w_minc' in item [1]:	
			File_Address = item [1]['t1w_minc']
			
		Temp_Dir = 'vfs://tmp' + '/' + args.Temp_Dir	+ '/' + Subject_Name
		
		# Get the sink data 
		sinkdata   = sink_data(Temp_Dir)	
		
		#XNAT_Path=Read_Stat_File_Address.find ('xnat://')
		
		# Get the source data 
		sourcedata = source_data_XNAT(args.xnat, args.project, args.label, Subject_Label, Subject_Name, args.Aggregated_Action_File, args.Action)
		# Build the network

		network    = create_network()			
			
		# Execute the network 
		if n ==  0 :  
			network.draw_network(name = network.id, img_format='svg',draw_dimension = True)
		network.execute (sourcedata,sinkdata,tmpdir= Temp_Dir) 	
		
		# Read the Script Output file and add a line padded with zeros if needed
		
		Script_Output_File = fastr.vfs.url_to_path(Temp_Dir) + '/Clean_XNAT_Output/Subject_Action_File.txt'
		
		Script_Output_File_Exist = os.path.isfile(Script_Output_File) 
		
		if Script_Output_File_Exist :
			Script_file = open(Script_Output_File, 'r')
			Output=Script_file.readline()
			Script_file.close()
			if Output != '1' :
				pad_to_aggregated_action_file (args.Aggregated_Action_File, Subject_Name)
		else : 
			pad_to_aggregated_action_file (args.Aggregated_Action_File, Subject_Name)
		++n			
	
if __name__ == '__main__':
    main();

