import fastr 
import os
import datetime 
import sys
import argparse
import json
import distutils
from str2bool import str2bool
from os import walk
import pdb #pdb.set_trace()  	

def merge_two_dicts(x, y):
    z = x.copy()   # start with x's keys and values
    z.update(y)    # modifies z with y's keys and values & returns None
    return z

def source_data(args_Dictionary, Sources, File_Type, Input_Folder, Temp_Dir):
	
	if Input_Folder == 'None' : 
		temp_sourcedata  = { 'Input_Folder': {},'File_Type': {} , 'ABM_Clac_Ave_Brain': {}, }	
		temp_sourcedata ['File_Type'] = File_Type
		temp_sourcedata ['ABM_Clac_Ave_Brain'] = True
		temp_sourcedata ['Input_Folder'] = Temp_Dir + '/Input_Folder'
		
		temp_Sources = {}
		
		n=1
		for key, value in Sources.iteritems():	
			
			if 't1w_dicom' in value :
				temp_Sources ['Source_Name_'+str(n)] = key
				temp_Sources ['Folder_Name_'+str(n)] = key
				temp_Sources ['Source_'+str(n)] = value ['t1w_dicom']
			elif 't1w_nifti' in value :	
				temp_Sources ['Source_Name_'+str(n)] = key + '.mnc'
				temp_Sources ['Source_'+str(n)] = value ['t1w_nifti']
			elif 't1w_minc' in value :	
				temp_Sources ['Source_Name_'+str(n)] = key
				temp_Sources ['Source_'+str(n)] = value ['t1w_minc']
			n=n+1		
				
		Combined_Dict = merge_two_dicts(temp_sourcedata, temp_Sources)
		sourcedata  = merge_two_dicts(args_Dictionary, Combined_Dict)
			
	else : 
		
		temp_sourcedata  = { 'Input_Folder': {}, 'File_Type': {} , 'ABM_Clac_Ave_Brain': {}, }	
		
		temp_sourcedata ['Input_Folder'] = Input_Folder
		temp_sourcedata ['File_Type'] = File_Type
		temp_sourcedata ['ABM_Clac_Ave_Brain'] = True
		
		sourcedata  = merge_two_dicts(args_Dictionary, temp_sourcedata)
		
	return sourcedata 

def sink_data(Output_Base_Dir,Sample_Name,Type, query_string=""):
	
	# This function builds the output data for the network 
	
	# 'ABM_Output_Brain': .... ,  The average brain that the image created 
	# 'ABM_Output_Parameters': .... , The parameters that were used to create the average brain
	# 'ABM_Output_Log': .... , A log file with all the processes and bash commands that the program produced
	# 'ABM_Output_JPG': .... , a jpg file showing several slices of the average brain
	
	# 'ICV_Output_ICV_Mask': .... , The Intracranial Volume mask that the program created
	# 'ICV_Output_ICV_Volume': .... , A Biomarker text file with the size in mm of the Intracranial Volume
	# 'ICV_Output_ICV_Parameters': .... , The parameters that were used to create the Intracranial Volume mask
	# 'ICV_Output_Log': .... ,  A log file with all the processes and bash commands that the program produced
	# 'ICV_Original_Subject' : ...., The original file in a nii.gz format
	# 'ICV_Output_ICV_JPG': .... , a jpg file showing several slices of the mask outline overlaid over the subject brain 
	
	st=datetime.datetime.now().strftime("%Y_%m_%d_%H_%M%p")
	Split_Sample_Name=Sample_Name.split('.',Sample_Name.count('.'))
	Sample_ID=Split_Sample_Name [0]
	Final_Output_Dir_Address ='/'+ Sample_ID +'_Analysis/'
		
	if Type == 'MINC' :
		Suffix1 ='mnc'
		Suffix2 ='mnc.gz'
		Suffix3 ='mnc.gz'
	elif Type == 'NIFTI' :
		Suffix1 ='nii'
		Suffix2 ='nii.gz'
		Suffix3 ='mnc.gz'
	else:
		Suffix1 ='nii.gz'
		Suffix2 ='nii.gz'
		Suffix3 ='mnc.gz'
	
	if Output_Base_Dir.find ('xnat://') == 0 :
			
			sinkdata = {
			
			'ABM_Output_Brain' :  Output_Base_Dir +  '/{sample_id}/{sample_id}_'+ Suffix2 + query_string,
			'ABM_Output_Brain_Minc' :  Output_Base_Dir +  '/{sample_id}/{sample_id}_'+ Suffix3 + query_string,
			'ABM_Output_Parameters' : Output_Base_Dir +  '/{sample_id}/{sample_id}_abm_parameters.txt' + query_string,
			'ABM_Output_Log': Output_Base_Dir +  '/{sample_id}/{sample_id}_abm_log.txt' + query_string,	
			'ABM_Output_JPG':  Output_Base_Dir + '/{sample_id}/{sample_id}_abm.jpg' + query_string,
			
			'ICV_Output_ICV_Mask' : Output_Base_Dir + '/{sample_id}/{sample_id}_icmask.' + Suffix2 + query_string,
			'ICV_Output_ICV_Mask_Minc' : Output_Base_Dir + '/{sample_id}/{sample_id}_icmask.' + Suffix3 + query_string,
			'ICV_Output_ICV_Volume' : Output_Base_Dir + '/{sample_id}/{sample_id}_icmask_statistics.txt' + query_string,
			'ICV_Output_ICV_Parameters' : Output_Base_Dir +  '/{sample_id}/{sample_id}_ic_parameters.txt' + query_string,
			'ICV_Output_Log': Output_Base_Dir +  '/{sample_id}/{sample_id}_log.txt' + query_string,	
			'ICV_Output_ICV_JPG':  Output_Base_Dir + '/{sample_id}/{sample_id}_icmask.jpg' + query_string,
			
			}  
	else:	
		
			sinkdata = {
			
			'ABM_Output_Brain' :  Output_Base_Dir +  '/' + Sample_ID + Final_Output_Dir_Address + Sample_ID  +'_' + st + '.' + Suffix2 + query_string,
			'ABM_Output_Brain_Minc' :  Output_Base_Dir  +  '/' + Sample_ID + Final_Output_Dir_Address + Sample_ID  +'_' + st + '.' + Suffix3 + query_string,
			'ABM_Output_Parameters' : Output_Base_Dir +  '/' + Sample_ID + Final_Output_Dir_Address + Sample_ID +'_abm_parameters_' + st + '.txt' + query_string,
			'ABM_Output_Log': Output_Base_Dir +  '/' + Sample_ID + Final_Output_Dir_Address + Sample_ID + '_abm_log_' + st + '.txt'+ query_string,	
			'ABM_Output_JPG': Output_Base_Dir + '/' + Sample_ID + Final_Output_Dir_Address + Sample_ID + '_abm_' + st + '.jpg' + query_string,	
			
			'ICV_Output_ICV_Mask' : Output_Base_Dir + '/' + Sample_ID + Final_Output_Dir_Address + Sample_ID + '_icmask_' + st + '.' + Suffix2 + query_string,
			'ICV_Output_ICV_Mask_Minc' : Output_Base_Dir+ '/' + Sample_ID + Final_Output_Dir_Address + Sample_ID + '_icmask_' + st + '.' + Suffix3 + query_string,
			'ICV_Output_ICV_Volume' : Output_Base_Dir + '/' + Sample_ID + Final_Output_Dir_Address + Sample_ID +'_icmask_statistics_' +st + '.txt' + query_string,
			'ICV_Output_ICV_Parameters' : Output_Base_Dir +  '/' + Sample_ID + Final_Output_Dir_Address + Sample_ID +'_ic_parameters_' + st + '.txt' + query_string,
			'ICV_Output_Log': Output_Base_Dir +  '/' + Sample_ID + Final_Output_Dir_Address + Sample_ID + '_log_' + st + '.txt'+ query_string,	
			'ICV_Output_ICV_JPG': Output_Base_Dir + '/' + Sample_ID + Final_Output_Dir_Address + Sample_ID + '_icmask_' + st + '.jpg' + query_string,	
	
			}
				
	return sinkdata


def create_network(Sources, Files_Type , Calc):
	
	Type = Files_Type
	
	# Building up the ABM and the ICV Nodes
	network = fastr.Network(id_='Average_Brain')
	ABM_Build_Model = network.create_node(fastr.toollist['ABM'],id_='Average_Brain_Model',stepid='Main_Calculation')	
	ICV_Build_Model = network.create_node(fastr.toollist['ICV'],id_='IntracranialVolume',stepid='Mask_Calculation')	
	
	# Building up the ABM input part of the Network	
	ABM_Delete_Files = network.create_source(fastr.typelist['Boolean'],id_='ABM_Delete_Files',stepid='ABM_Delete_Files')
		
	ABM_Subject_Name = network.create_source (fastr.typelist['String'],id_='ABM_Name',stepid='ABM_Subject_Name')			
	
	ABM_Linear_Estimate = network.create_source(fastr.typelist['String'],id_='ABM_Linear_Estimate',stepid='ABM_Program_Linear_Registration_Arguments')
	ABM_Num_Of_Registrations = network.create_source(fastr.typelist['Int'],id_='ABM_Num_Of_Registrations',stepid='ABM_Program_Linear_Registration_Arguments')	
	ABM_Registration_Blur_FWHM = network.create_source(fastr.typelist['Int'],id_='ABM_Registration_Blur_FWHM',stepid='ABM_Program_Linear_Registration_Arguments')
	ABM_Linear_Normalization = network.create_source(fastr.typelist['Boolean'],id_='ABM_Linear_Normalization',stepid='ABM_Program_Linear_Registration_Arguments')
	
	ABM_Number_Of_Iterations = network.create_source(fastr.typelist['String'],id_='ABM_Num_Of_Iterations',stepid='ABM_Program_Basic_Arguments')
	ABM_Step_Size = network.create_source(fastr.typelist['String'],id_='ABM_Step_Size',stepid='ABM_Program_Basic_Arguments')
	ABM_Blur_FWHM = network.create_source(fastr.typelist['String'],id_='ABM_Blur_FWHM',stepid='ABM_Program_Basic_Arguments')
	
	ABM_Weight = network.create_source(fastr.typelist['Float'],id_='ABM_Weight',stepid='ABM_Program_Extended_Arguments')
	ABM_Stiffness = network.create_source(fastr.typelist['Float'],id_='ABM_Stiffness',stepid='ABM_Program_Extended_Arguments')
	ABM_Similarity = network.create_source(fastr.typelist['Float'],id_='ABM_Similarity',stepid='ABM_Program_Extended_Arguments')
	ABM_Sub_lattice = network.create_source(fastr.typelist['Int'],id_='ABM_Sub_lattice',stepid='ABM_Program_Extended_Arguments')
	ABM_Debug = network.create_source(fastr.typelist['Boolean'],id_='ABM_Debug',stepid='ABM_Program_Extended_Arguments')
	ABM_Clobber = network.create_source(fastr.typelist['Boolean'],id_='ABM_Clobber',stepid='ABM_Program_Extended_Arguments')
	ABM_Non_linear = network.create_source(fastr.typelist['String'],id_='ABM_Non_linear',stepid='ABM_Program_Extended_Arguments')
	ABM_NonLinear_Normalization = network.create_source(fastr.typelist['Boolean'],id_='ABM_NonLinear_Normalization',stepid='ABM_Program_Extended_Arguments')
	
	ABM_Operation_Type = network.create_source(fastr.typelist['Boolean'],id_='ABM_Clac_Ave_Brain',stepid='ABM_Operation_Type')
	
	ABM_First_Subject = network.create_source(fastr.typelist['String'],id_='ABM_First_Subject',stepid='ABM_First_Subject')
	
		
	# Building up the ICV input part of the Network	
	ICV_Delete_Files = network.create_source(fastr.typelist['Boolean'],id_='ICV_Delete_Files',stepid='ICV_Delete_Files')
		
	ICV_Model = network.create_source(fastr.typelist['String'],id_='ICV_Model_Name',stepid='ICV_Model_Name')	
	
	ICV_Linear_Estimate = network.create_source(fastr.typelist['String'],id_='ICV_Linear_Estimate',stepid='ICV_Program_Linear_Registration_Arguments')
	ICV_Num_Of_Registrations = network.create_source(fastr.typelist['Int'],id_='ICV_Num_Of_Registrations',stepid='ICV_Program_Linear_Registration_Arguments')	
	ICV_Registration_Blur_FWHM = network.create_source(fastr.typelist['Int'],id_='ICV_Registration_Blur_FWHM',stepid='ICV_Program_Linear_Registration_Arguments')
	ICV_Linear_Normalization = network.create_source(fastr.typelist['Boolean'],id_='ICV_Linear_Normalization',stepid='ICV_Program_Linear_Registration_Arguments')
	
	ICV_Number_Of_Iterations = network.create_source(fastr.typelist['String'],id_='ICV_Num_Of_Iterations',stepid='ICV_Program_Basic_Arguments')
	ICV_Step_Size = network.create_source(fastr.typelist['String'],id_='ICV_Step_Size',stepid='ICV_Program_Basic_Arguments')
	ICV_Blur_FWHM = network.create_source(fastr.typelist['String'],id_='ICV_Blur_FWHM',stepid='ICV_Program_Basic_Arguments')
	
	ICV_Weight = network.create_source(fastr.typelist['Float'],id_='ICV_Weight',stepid='ICV_Program_Extended_Arguments')
	ICV_Stiffness = network.create_source(fastr.typelist['Float'],id_='ICV_Stiffness',stepid='ICV_Program_Extended_Arguments')
	ICV_Similarity = network.create_source(fastr.typelist['Float'],id_='ICV_Similarity',stepid='ICV_Program_Extended_Arguments')
	ICV_Sub_lattice = network.create_source(fastr.typelist['Int'],id_='ICV_Sub_lattice',stepid='ICV_Program_Extended_Arguments')
	ICV_Debug = network.create_source(fastr.typelist['Boolean'],id_='ICV_Debug',stepid='ICV_Program_Extended_Arguments')
	ICV_Clobber = network.create_source(fastr.typelist['Boolean'],id_='ICV_Clobber',stepid='ICV_Program_Extended_Arguments')
	ICV_Non_linear = network.create_source(fastr.typelist['String'],id_='ICV_Non_linear',stepid='ICV_Program_Extended_Arguments')
	ICV_NonLinear_Normalization = network.create_source(fastr.typelist['Boolean'],id_='ICV_NonLinear_Normalization',stepid='ICV_Program_Extended_Arguments')
	
	ICV_Mask_To_Binary_Min = network.create_source(fastr.typelist['Float'],id_='ICV_Mask_To_Binary_Min',stepid='ICV_Program_Extended_Arguments')

	if Calc == 'Calc_Crude':
		ICV_Operation_Type = network.create_source(fastr.typelist['Boolean'],id_='Calc_Crude',stepid='ICV_Operation_Type')
	elif Calc == 'Calc_Fine':
		ICV_Operation_Type = network.create_source(fastr.typelist['Boolean'],id_='Calc_Fine',stepid='ICV_Operation_Type')
		
	# Link the sources to the ABM tool
	
	ABM_Input_Link1 = network.create_link(ABM_Delete_Files.output, ABM_Build_Model.inputs['Delete_Files'])
		
	ABM_Input_Link2 = network.create_link(ABM_Linear_Estimate.output, ABM_Build_Model.inputs['Linear_Estimate']) 
	ABM_Input_link3 = network.create_link(ABM_Num_Of_Registrations.output, ABM_Build_Model.inputs['Num_Of_Registrations'])
	ABM_Input_link4 = network.create_link(ABM_Registration_Blur_FWHM.output, ABM_Build_Model.inputs['Registration_Blur_FWHM'])
	
	ABM_Input_link5 = network.create_link(ABM_Number_Of_Iterations.output, ABM_Build_Model.inputs['Num_Of_Iterations'])
	ABM_Input_link6 = network.create_link(ABM_Step_Size.output, ABM_Build_Model.inputs['Step_Size'])
	ABM_Input_link7 = network.create_link(ABM_Blur_FWHM.output, ABM_Build_Model.inputs['Blur_FWHM'])
	ABM_Input_link8 = network.create_link(ABM_Linear_Normalization.output, ABM_Build_Model.inputs['Linear_Normalization'])
	
	ABM_Input_Link9 = network.create_link(ABM_Weight.output, ABM_Build_Model.inputs['Weight'])
	ABM_Input_Link10 = network.create_link(ABM_Stiffness.output, ABM_Build_Model.inputs['Stiffness'])
	ABM_Input_Link11 = network.create_link(ABM_Similarity.output, ABM_Build_Model.inputs['Similarity'])
	ABM_Input_Link12 = network.create_link(ABM_Sub_lattice.output, ABM_Build_Model.inputs['Sub_lattice'])
	ABM_Input_Link13 = network.create_link(ABM_Debug.output, ABM_Build_Model.inputs['Debug'])
	ABM_Input_Link14 = network.create_link(ABM_Clobber.output, ABM_Build_Model.inputs['Clobber'])
	ABM_Input_Link15 = network.create_link(ABM_Non_linear.output, ABM_Build_Model.inputs['Non_linear'])
	ABM_Input_link16 = network.create_link(ABM_NonLinear_Normalization.output, ABM_Build_Model.inputs['NonLinear_Normalization'])
	
	ABM_Input_link17 = network.create_link(ABM_Subject_Name.output, ABM_Build_Model.inputs['Subject_Name'])
	
	ABM_Input_Link18 = network.create_link(ABM_Operation_Type.output, ABM_Build_Model.inputs['Clac_Ave_Brain'])
	
	ABM_Input_Link19 = network.create_link(ABM_First_Subject.output, ABM_Build_Model.inputs['ABM_First_Subject'])
		
	# Link the sources to the ICV tool
	ICV_Input_Link1 = network.create_link(ICV_Delete_Files.output, ICV_Build_Model.inputs['Delete_Files'])
	
	ICV_Input_link2 = network.create_link(ICV_Model.output, ICV_Build_Model.inputs['Model'])
	
	if len(Sources) == 0 :
	
		if Type == 'NIFTI':	
			ICV_Input_link3 = network.create_link(ABM_Build_Model.outputs ['abm_output_nii'], ICV_Build_Model.inputs['Processed_File'])
		elif Type == 'MINC' : 	
			ICV_Input_link3 = network.create_link(ABM_Build_Model.outputs ['abm_output_mnc'], ICV_Build_Model.inputs['Processed_File'])
	else : 
		ICV_Input_link3 = network.create_link(ABM_Build_Model.outputs ['abm_output_mnc'], ICV_Build_Model.inputs['Processed_File'])	
		Convert_ABM_back_to_NIFTI_node = network.create_node(fastr.toollist['mnc2nii'],id_='mnc2nii',stepid='Back Files_Conversions')
		Convert_ABM_back_to_NIFTI_link = network.create_link(ABM_Build_Model.outputs ['abm_output_mnc'], Convert_ABM_back_to_NIFTI_node.inputs['Processed_MINC'])
		GZ_ABM_Node = network.create_node(fastr.toollist['GZ_Nifti_File'],id_='ABM_GZ_Nifti_File',stepid='gz_files')
		GZ_Link = network.create_link(Convert_ABM_back_to_NIFTI_node.outputs['output_file'], GZ_ABM_Node.inputs['Input_File'])			
	
	ICV_Input_Link4 = network.create_link(ICV_Linear_Estimate.output, ICV_Build_Model.inputs['Linear_Estimate']) 
	ICV_Input_link5 = network.create_link(ICV_Num_Of_Registrations.output, ICV_Build_Model.inputs['Num_Of_Registrations'])
	ICV_Input_link6 = network.create_link(ICV_Registration_Blur_FWHM.output, ICV_Build_Model.inputs['Registration_Blur_FWHM'])
	
	ICV_Input_link7 = network.create_link(ICV_Number_Of_Iterations.output, ICV_Build_Model.inputs['Num_Of_Iterations'])
	ICV_Input_link8 = network.create_link(ICV_Step_Size.output, ICV_Build_Model.inputs['Step_Size'])
	ICV_Input_link9 = network.create_link(ICV_Blur_FWHM.output, ICV_Build_Model.inputs['Blur_FWHM'])
	ICV_Input_link10 = network.create_link(ICV_Linear_Normalization.output, ICV_Build_Model.inputs['Linear_Normalization'])
	
	ICV_Input_Link11 = network.create_link(ICV_Weight.output, ICV_Build_Model.inputs['Weight'])
	ICV_Input_Link12 = network.create_link(ICV_Stiffness.output, ICV_Build_Model.inputs['Stiffness'])
	ICV_Input_Link13 = network.create_link(ICV_Similarity.output, ICV_Build_Model.inputs['Similarity'])
	ICV_Input_Link14 = network.create_link(ICV_Sub_lattice.output, ICV_Build_Model.inputs['Sub_lattice'])
	ICV_Input_Link15 = network.create_link(ICV_Debug.output, ICV_Build_Model.inputs['Debug'])
	ICV_Input_Link16 = network.create_link(ICV_Clobber.output, ICV_Build_Model.inputs['Clobber'])
	ICV_Input_Link17 = network.create_link(ICV_Non_linear.output, ICV_Build_Model.inputs['Non_linear'])
	ICV_Input_link18 = network.create_link(ICV_NonLinear_Normalization.output, ICV_Build_Model.inputs['NonLinear_Normalization'])
	
	ICV_Input_Link19 = network.create_link(ICV_Mask_To_Binary_Min.output, ICV_Build_Model.inputs['Mask_To_Binary_Min'])   
	
	if Calc == 'Calc_Crude':
		ICV_Input_link20 = network.create_link(ICV_Operation_Type.output, ICV_Build_Model.inputs['Calc_Crude'])
	elif Calc == 'Calc_Fine':
		ICV_Input_link20 = network.create_link(ICV_Operation_Type.output, ICV_Build_Model.inputs['Calc_Fine'])
	
	ICV_Input_link21 = network.create_link(ABM_Subject_Name.output, ICV_Build_Model.inputs['Subject_Name'])
	
	# Create the ABM sinks 
	if len(Sources) == 0 :
		if Type == 'NIFTI':	
			ABM_Output_Brain = network.create_sink (fastr.typelist['NiftiImageFileCompressed'],id_='ABM_Output_Brain',stepid='ABM_Files_Outputs')	
		elif Type == 'MINC' :
			ABM_Output_Brain = network.create_sink (fastr.typelist['CompressedMincImageFile'],id_='ABM_Output_Brain',stepid='ABM_Files_Outputs')
	else :
		if Type == 'NIFTI':	
			ABM_Output_Brain = network.create_sink (fastr.typelist['NiftiImageFileCompressed'],id_='ABM_Output_Brain',stepid='ABM_Files_Outputs')
			ABM_Output_Brain_In_Minc = 	network.create_sink (fastr.typelist['CompressedMincImageFile'],id_='ABM_Output_Brain_Minc',stepid='ABM_Files_Outputs')
		elif Type == 'MINC' :
			ABM_Output_Brain = network.create_sink (fastr.typelist['CompressedMincImageFile'],id_='ABM_Output_Brain',stepid='ABM_Files_Outputs')		

	ABM_Output_Parameters = network.create_sink (fastr.typelist['TxtFile'],id_='ABM_Output_Parameters',stepid='ABM_Files_Outputs')	
	ABM_Output_Log = network.create_sink (fastr.typelist['TxtFile'],id_='ABM_Output_Log',stepid='ABM_Files_Outputs')
	ABM_Output_JPG = network.create_sink (fastr.typelist['JPGFile'],id_='ABM_Output_JPG',stepid='ABM_Files_Outputs')
	
	# Create the ICV sinks 
	if len(Sources) == 0 :
		if Type == 'NIFTI':	
			ICV_Output_ICV_Mask = network.create_sink (fastr.typelist['NiftiImageFileCompressed'],id_='ICV_Output_ICV_Mask',stepid='ICV_Files_Outputs')
		elif Type == 'MINC' :
			ICV_Output_ICV_Mask = network.create_sink (fastr.typelist['CompressedMincImageFile'],id_='ICV_Output_ICV_Mask_Minc',stepid='ICV_Files_Outputs')
	else : 
		if Type == 'NIFTI':	
			ICV_Output_ICV_Mask = network.create_sink (fastr.typelist['NiftiImageFileCompressed'],id_='ICV_Output_ICV_Mask',stepid='ICV_Files_Outputs')
			ICV_Output_ICV_Mask_In_Minc = network.create_sink (fastr.typelist['CompressedMincImageFile'],id_='ICV_Output_ICV_Mask_Minc',stepid='ICV_Files_Outputs')
		elif Type == 'MINC' :
			ICV_Output_ICV_Mask = network.create_sink (fastr.typelist['CompressedMincImageFile'],id_='ICV_Output_ICV_Mask',stepid='ICV_Files_Outputs')
					
	ICV_Output_ICV_Parameters = network.create_sink (fastr.typelist['TxtFile'],id_='ICV_Output_ICV_Parameters',stepid='ICV_Files_Outputs')	
	ICV_Output_ICV_Mask_BioMarker = network.create_sink (fastr.typelist['TxtFile'],id_='ICV_Output_ICV_Volume',stepid='ICV_Files_Outputs')	
	ICV_Output_ICV_Log = network.create_sink (fastr.typelist['TxtFile'],id_='ICV_Output_Log',stepid='ICV_Files_Outputs')
	ICV_Output_ICV_JPG = network.create_sink (fastr.typelist['JPGFile'],id_='ICV_Output_ICV_JPG',stepid='ICV_Files_Outputs')
	
	# Create the links to the ABM sinks 
	
	if len(Sources) == 0 :
		if Type == 'NIFTI':	
			ABM_Output_Link1 = network.create_link(ABM_Build_Model.outputs['abm_output_nii'], ABM_Output_Brain.input)
		elif Type == 'MINC' :
			 ABM_Output_Link1 = network.create_link(ABM_Build_Model.outputs['abm_output_mnc'], ABM_Output_Brain.input)
	else :
		if Type == 'NIFTI':	
			ABM_Output_Link1 = network.create_link(ABM_Build_Model.outputs['abm_output_mnc'], ABM_Output_Brain_In_Minc.input)
			ABM_Output_Link5 = network.create_link(GZ_ABM_Node.outputs['output_file'], ABM_Output_Brain.input)
		elif Type == 'MINC' :
			ABM_Output_Link1 = network.create_link(ABM_Build_Model.outputs['abm_output_mnc'], ABM_Output_Brain.input)
			 		 
	ABM_Output_Link2 = network.create_link(ABM_Build_Model.outputs['abm_parameters_used'], ABM_Output_Parameters.input)
	ABM_Output_Link3 = network.create_link(ABM_Build_Model.outputs['abm_log'], ABM_Output_Log.input)	
	ABM_Output_Link4 = network.create_link(ABM_Build_Model.outputs['abm_jpg'], ABM_Output_JPG.input)	
	
	# Create the links to the ICV sinks 
	
	if len(Sources) == 0 :
		if  Type == 'NIFTI':	
			ICV_Output_Link1 = network.create_link(ICV_Build_Model.outputs['icv_output_nii_mask'],ICV_Output_ICV_Mask.input)	
		elif Type == 'MINC' :
			ICV_Output_Link1 = network.create_link(ICV_Build_Model.outputs['icv_output_mnc_mask'],ICV_Output_ICV_Mask.input)
	else: 
		if  Type == 'NIFTI':	
			ICV_Output_Link1 = network.create_link(ICV_Build_Model.outputs['icv_output_mnc_mask'],ICV_Output_ICV_Mask_In_Minc.input)
			Convert_ICV_Mask_back_to_NIFTI_node = network.create_node(fastr.toollist['mnc2nii'],id_='icv_mask_mnc2nii',stepid='Back Files_Conversions')
			Convert_ICV_Mask_back_to_NIFTI_link = network.create_link(ICV_Build_Model.outputs['icv_output_mnc_mask'], Convert_ICV_Mask_back_to_NIFTI_node.inputs['Processed_MINC'])
			GZ_ICV_Mask_Node = network.create_node(fastr.toollist['GZ_Nifti_File'],id_='ICV_Mask_GZ_Nifti_File',stepid='gz_files')
			GZ_ICV_Mask_link = network.create_link(Convert_ICV_Mask_back_to_NIFTI_node.outputs['output_file'],GZ_ICV_Mask_Node.inputs['Input_File'])
			ICV_Output_Link5 = network.create_link(GZ_ICV_Mask_Node.outputs['output_file'],ICV_Output_ICV_Mask.input)	
		elif Type == 'MINC' :
			ICV_Output_Link1 = network.create_link(ICV_Build_Model.outputs['icv_output_mask_in_minc'],ICV_Output_ICV_Mask.input)
						
	ICV_Output_Link2 = network.create_link(ICV_Build_Model.outputs['icv_parameters_used'],ICV_Output_ICV_Parameters.input)
	ICV_Output_Link3 = network.create_link(ICV_Build_Model.outputs['icv_volume'],ICV_Output_ICV_Mask_BioMarker.input)
	ICV_Output_Link4 = network.create_link(ICV_Build_Model.outputs['icv_log'],ICV_Output_ICV_Log.input)	
	ICV_Output_Link5 = network.create_link(ICV_Build_Model.outputs['icv_jpg'],ICV_Output_ICV_JPG.input)	
		
	# In case that the input is not a folder but a list of files, building the first input part of the network
	
	if len(Sources) == 0 :
	
		ABM_Input_Folder = network.create_source(fastr.typelist['Directory'],id_='Input_Folder',stepid='AMB_Input_Folder')
		ABM_Input_Link1 = network.create_link(ABM_Input_Folder.output ,ABM_Build_Model.inputs['Processed_Folder'])
	
	else : 	
	
		n=1
		ABM_Input_Folder = network.create_source(fastr.typelist['Directory'],id_='Input_Folder',stepid='AMB_Input_Folder')
		
		for key, value in Sources.iteritems():
			Source_Location = 'Source_'+str(n)
			if 't1w_dicom' in value :
				Source_Name = 'Source_Name_'+str(n)	
				Folder_Name = 'Folder_Name_'+str(n)
				Input_DICOM = network.create_source(fastr.typelist['DicomImageFile'],id_=Source_Location,stepid='Input_Arguments'+str(n))
				Input_Name = network.create_source(fastr.typelist['String'],id_=Source_Name,stepid='Input_Arguments'+str(n))
				Input_Folder_Name = network.create_source(fastr.typelist['String'],id_=Folder_Name,stepid='Input_Arguments'+str(n))
				
				Dicom_Convert = network.create_node(fastr.toollist['dcm2mnc'],id_='dcm2mnc'+str(n),stepid='Files_Conversions'+str(n))
					
				Convert_Link = network.create_link(Input_DICOM.output,Dicom_Convert.inputs['Processed_DICOM'])
				Convert_Directory_Link = network.create_link(Input_Folder_Name.output,Dicom_Convert.inputs['Directory_Name'])
				Convert_Name_Link = network.create_link(Input_Name.output,Dicom_Convert.inputs['Subject_Name'])	
				
				Create_Symbolic_Minc_From_Original_Dicom = network.create_node(fastr.toollist['mnc2mnc'],id_='mnc2mnc'+str(n), stepid='Files_Conversions'+str(n))
				
				Symbolic_Minc_Link = network.create_link(Dicom_Convert.outputs['output_file'],Create_Symbolic_Minc_From_Original_Dicom.inputs['Processed_Minc'])
				Convert_Directory_Link = network.create_link(ABM_Input_Folder.output,Create_Symbolic_Minc_From_Original_Dicom.inputs['Output_Dir'])	
				
				if n == 1 :
					File_Dictionary = (Create_Symbolic_Minc_From_Original_Dicom.outputs['output_file_mnc'],)
				else:		
					File_Dictionary = File_Dictionary + (Create_Symbolic_Minc_From_Original_Dicom.outputs['output_file_mnc'],)
				
			elif 't1w_nifti' in value :	
				Source_Output = 'Source_Name_'+str(n)
				Input_NIFTII = network.create_source(fastr.typelist['NiftiImageFileUncompressed'],id_='Source_'+str(n),stepid='Input_Arguments'+str(n))
				Output_Name = network.create_source(fastr.typelist['String'],id_=Source_Output,stepid='Input_Arguments'+str(n))
				
				NIFTII_Convert = network.create_node(fastr.toollist['nii2mnc'],id_='nii2mnc'+str(n),stepid='Files_Conversions'+str(n))	
				
				Convert_Link = network.create_link(Input_NIFTII.output,NIFTII_Convert.inputs['Processed_NIFTII'])
				Convert_Name_Link = network.create_link(Output_Name.output,NIFTII_Convert.inputs['Output_File_Name'])	
				
				Create_Symbolic_Minc_From_Original_Nifti = network.create_node(fastr.toollist['mnc2mnc'],id_='mnc2mnc'+str(n), stepid='Files_Conversions'+str(n))
				
				Symbolic_Minc_Link = network.create_link(NIFTII_Convert.outputs['output_file'],Create_Symbolic_Minc_From_Original_Nifti.inputs['Processed_Minc'])
				Convert_Directory_Link = network.create_link(ABM_Input_Folder.output,Create_Symbolic_Minc_From_Original_Nifti.inputs['Output_Dir'])	
				
				if n == 1 : 
					File_Dictionary = (Create_Symbolic_Minc_From_Original_Nifti.outputs['output_file_mnc'],)
				else : 
					File_Dictionary = File_Dictionary + (Create_Symbolic_Minc_From_Original_Nifti.outputs['output_file_mnc'],)
				
			elif 't1w_minc' in value :		
				Input_MINC = network.create_source(fastr.typelist['MincImageFile'],id_=Source_Location,stepid='Input_Arguments'+str(n))
				
				MINC_Convert = network.create_node(fastr.toollist['mnc2mnc'],id_='mnc2mnc'+str(n),stepid='Files_Conversions'+str(n))
					
				Convert_Link = network.create_link(Input_MINC.output,MINC_Convert.inputs['Processed_Minc'])
				Convert_Directory_Link = network.create_link(ABM_Input_Folder.output,MINC_Convert.inputs['Output_Dir'])
				
				if n == 1 : 
					File_Dictionary = (MINC_Convert.outputs['output_file_mnc'],)
				else:
					File_Dictionary = File_Dictionary + (MINC_Convert.outputs['output_file_mnc'],)
				
			n=n+1
		
		Files_To_Directory_Node = network.create_node(fastr.toollist['Collapse_Minc_To_Folder'],id_='Collapse_Minc_To_Folder',stepid='Collect_Converted_Files')
					
		Files_To_Directory_Node.inputs['Image_Name'] = File_Dictionary
		
		ABM_Input_Link1 = network.create_link(Files_To_Directory_Node.outputs['output_directory'] ,ABM_Build_Model.inputs['Processed_Folder'])	
															 
	return network

def JSON_Read_Sources (dict_file):

	with open(dict_file) as input_file:
		data = json.load(input_file)

	return data
	
def JSON_Read_Parameters_File (paramters_file):
	
	# This builds the input data for the ICV node. 
	# Here you can change values of the parameters (not recommended). You can also change them in the parameters file. 
	# However, values here replace the values in the parameters files. 
	# For detailed description of the different parameters - see the documentations or the tool (ICV.xml) definition file. 
	
	# Possible paramaters:
	# 'ABM_Name' : ... . Not required.  If absent the program will use the default = Test_Average_Brain
	# 'ABM_Parameters_File': ... , Not required. If absent the program will read the default parameter file at parameter folder in the tool home directory 
	# 'ABM_Linear_Estimate': ..., Not required. Overrides a value from the ICV tool parameter file (not to be confused from the JSON file read by this program)
	# 'ABM_Num_Of_Registrations': ... Not required. Overrides a value from the ICV tool parameter file (not to be confused from the JSON file read by this program)
	# 'ABM_Registration_Blur_FWHM': ... , Not required. Overrides a value from the ICV tool parameter file (not to be confused from the JSON file read by this program)
	# 'ABM_Num_Of_Itterations': ... , Not required. Overrides a value from the ICV tool parameter file (not to be confused from the JSON file read by this program)
	# 'ABM_Step_Size': ..., Not required. Overrides a value from the ICV tool parameter file (not to be confused from the JSON file read by this program)
	# 'ABM_Blur_FWHM': ..., Not required. Overrides a value from the ICV tool parameter file (not to be confused from the JSON file read by this program)
	# 'ABM_Weight': ..., Not required. Overrides a value from the ICV tool parameter file (not to be confused from the JSON file read by this program)
	# 'ABM_Stiffness': ..., Not required. Overrides a value from the ICV tool parameter file (not to be confused from the JSON file read by this program)
	# 'ABM_Similarity': ... , Not required. Overrides a value from the ICV tool parameter file (not to be confused from the JSON file read by this program)
	# 'ABM_Debug':  ... ,  Not required. Overrides a value from the ICV tool parameter file (not to be confused from the JSON file read by this program)
	# 'ABM_Clobber': ...,  Not required. Overrides a value from the ICV tool parameter file (not to be confused from the JSON file read by this program)
	# 'ABM_Non_linear': ... ,  Not required. Overrides a value from the ICV tool parameter file (not to be confused from the JSON file read by this program)
	
	# 'ICV_Model_Name': ... ,  Not required. Can be set to: Three_Tesla_Adults_Model or Three_Tesla_Kids_Model or OneHalf_Tesla_Adults_Model or OneHalf_Tesla_Kids_Model or Custom_Model. In the last case the progeram also requires a string with the uri of the custom model files.    
	# 'ICV_Parameters_File': ... , Not required. If absent the program will read the default parameter file at parameter folder in the tool home directory 
	# 'ICV_Linear_Estimate': ..., Not required. Overrides a value from the ICV tool parameter file (not to be confused from the JSON file read by this program)
	# 'ICV_Num_Of_Registrations': ... Not required. Overrides a value from the ICV tool parameter file (not to be confused from the JSON file read by this program)
	# 'ICV_Registration_Blur_FWHM': ... , Not required. Overrides a value from the ICV tool parameter file (not to be confused from the JSON file read by this program)
	# 'ICV_Num_Of_Itterations': ... , Not required. Overrides a value from the ICV tool parameter file (not to be confused from the JSON file read by this program)
	# 'ICV_Step_Size': ..., Not required. Overrides a value from the ICV tool parameter file (not to be confused from the JSON file read by this program)
	# 'ICV_Blur_FWHM': ..., Not required. Overrides a value from the ICV tool parameter file (not to be confused from the JSON file read by this program)
	# 'ICV_Weight': ..., Not required. Overrides a value from the ICV tool parameter file (not to be confused from the JSON file read by this program)
	# 'ICV_Stiffness': ..., Not required. Overrides a value from the ICV tool parameter file (not to be confused from the JSON file read by this program)
	# 'ICV_Similarity': ... , Not required. Overrides a value from the ICV tool parameter file (not to be confused from the JSON file read by this program)
	# 'ICV_Debug':  ... ,  Not required. Overrides a value from the ICV tool parameter file (not to be confused from the JSON file read by this program)
	# 'ICV_Clobber': ...,  Not required. Overrides a value from the ICV tool parameter file (not to be confused from the JSON file read by this program)
	# 'ICV_Non_linear': ... ,  Not required. Overrides a value from the ICV tool parameter file (not to be confused from the JSON file read by this program)
	# 'ICV_Mask_To_Binary_Min': .... ,  Not required. Overrides a value from the ICV tool parameter file (not to be confused from the JSON file read by this program)
	# 'Calc_Crude': ... , Calculate the ICV mask on a more crude level. For most purpose is enough. 
	# 'Calc_Fine': ... , Calculate the ICV mask in detailed.  

	with open(paramters_file) as input_file:
		data = json.load(input_file)
	parameters = { 'ABM_Delete_Files': {}, 'ABM_Name': {}, 'ABM_First_Subject' : {},
	'ABM_Linear_Estimate': {}, 'ABM_Num_Of_Registrations': {}, 'ABM_Registration_Blur_FWHM': {}, 'ABM_Linear_Normalization': {},
	'ABM_Num_Of_Iterations': {}, 'ABM_Step_Size': {}, 'ABM_Blur_FWHM': {}, 'ABM_Weight': {}, 'ABM_Stiffness': {}, 'ABM_Similarity': {},
	'ABM_Sub_lattice': {}, 'ABM_Debug': {}, 'ABM_Clobber': {}, 'ABM_Non_linear': {}, 'ABM_NonLinear_Normalization': {},
	'ICV_Delete_Files' : {}, 'ICV_Linear_Estimate': {}, 'ICV_Num_Of_Registrations': {}, 'ICV_Registration_Blur_FWHM': {}, 'ICV_Linear_Normalization': {},
	'ICV_Num_Of_Iterations': {}, 'ICV_Step_Size': {}, 'ICV_Blur_FWHM': {}, 'ICV_Weight': {}, 'ICV_Stiffness': {}, 'ICV_Similarity': {},
	'ICV_Sub_lattice': {}, 'ICV_Debug': {}, 'ICV_Clobber': {}, 'ICV_Non_linear': {}, 'ICV_NonLinear_Normalization': {},
	'ICV_Mask_To_Binary_Min': {}, 'ICV_Model_Name': {}, 'ICV_Run_Type': {},  }	
	
	if 'ABM_Delete_Files' in data: 
		parameters['ABM_Delete_Files']  = str2bool(data['ABM_Delete_Files'])
	else : 
		parameters['ABM_Delete_Files']  = str2bool ('True')	
	if 'ABM_Name' in data: 
		parameters['ABM_Name'] = data ['ABM_Name']
	else :
		parameters['ABM_Name'] = 'Test_Average_Brain'
	if 	'ABM_First_Subject' in data : 
		parameters['ABM_First_Subject'] = data ['ABM_First_Subject']
	else : 
		parameters['ABM_First_Subject'] = 'None'
	if 'ABM_Linear_Estimate' in data: 
		parameters['ABM_Linear_Estimate']  = data['ABM_Linear_Estimate']
	else : 
		parameters['ABM_Linear_Estimate']  = 'Estimate_Translation'
	if 'ABM_Num_Of_Registrations' in data: 
		parameters['ABM_Num_Of_Registrations']  = int (data['ABM_Num_Of_Registrations'])
	else : 
		uri_source_path['ABM_Num_Of_Registrations']  = int ('4')
	if 'ABM_Registration_Blur_FWHM' in data: 
		parameters['ABM_Registration_Blur_FWHM']  = int (data['ABM_Registration_Blur_FWHM'])
	else : 
		parameters['ABM_Registration_Blur_FWHM']  = int('1')	
	if 'ABM_Linear_Normalization' in data: 
		parameters['ABM_Linear_Normalization']  = str2bool(data['ABM_Linear_Normalization'])
	else : 
		parameters['ABM_Linear_Normalization']  = str2bool ('False')		
	if 'ABM_Num_Of_Iterations' in data: 
		parameters['ABM_Num_Of_Iterations'] = data['ABM_Num_Of_Iterations']
	else : 
		parameters['ABM_Num_Of_Iterations']  = '"{5,8,10,8}"'	
	if 'ABM_Step_Size' in data: 
		parameters['ABM_Step_Size']  = data['ABM_Step_Size']
	else : 
		parameters['ABM_Step_Size']  = '"{16,8,2,1}"'
	if 'ABM_Blur_FWHM' in data: 
		parameters['ABM_Blur_FWHM']  = data['ABM_Blur_FWHM']
	else : 
		parameters['ABM_Blur_FWHM']  = '"{8,8,2,1}"'			
	if 'ABM_Weight' in data: 
		parameters['ABM_Weight']  = float (data['ABM_Weight'])
	else : 
		parameters['ABM_Weight']  = float ('1.0')
	if 'ABM_Stiffness' in data: 
		parameters['ABM_Stiffness']  = float (data['ABM_Stiffness'])
	else : 
		parameters['ABM_Stiffness']  = float ('1.0')
	if 'ABM_Similarity' in data: 
		parameters['ABM_Similarity'] = float (data['ABM_Similarity'])
	else : 
		parametersh['ABM_Similarity']  = float ('0.3')
	if 'ABM_Sub_lattice' in data: 
		parameters['ABM_Sub_lattice']  = int (data['ABM_Sub_lattice'])
	else : 
		parameters['ABM_Sub_lattice']  = int ('6')
	if 'ABM_Debug' in data: 
		parameters['ABM_Debug']  = str2bool(data['ABM_Debug'])
	else : 
		parameters['ABM_Debug']  = str2bool ('True')
	if 'ABM_Clobber' in data: 
		parameters['ABM_Clobber']  = str2bool (data['ABM_Clobber'])
	else : 
		parameters['ABM_Clobber']  = str2bool ('True')
	if 'ABM_Non_linear' in data: 
		parameters['ABM_Non_linear']  = data['ABM_Non_linear']
	else : 
		parameters['ABM_Non_linear']  = 'corrcoeff'	
	if 'ABM_NonLinear_Normalization' in data: 
		parameters['ABM_NonLinear_Normalization']  = str2bool(data['ABM_NonLinear_Normalization'])
	else : 
		parameters['ABM_NonLinear_Normalization']  = str2bool ('False')	
	
	if 'ICV_Delete_Files' in data: 
		parameters['ICV_Delete_Files']  = str2bool(data['ICV_Delete_Files'])
	else : 
		parameters['ICV_Delete_Files']  = str2bool ('True')	
	if 'ICV_Linear_Estimate' in data: 
		parameters['ICV_Linear_Estimate'] = data['ICV_Linear_Estimate']
	else : 
		parameters['ICV_Linear_Estimate'] = 'Estimate_Translation'
	if 'ICV_Num_Of_Registrations' in data: 
		parameters['ICV_Num_Of_Registrations'] = int (data['ICV_Num_Of_Registrations'])
	else : 
		parameters['ICV_Num_Of_Registrations']  = int ('4')
	if 'ICV_Registration_Blur_FWHM' in data: 
		parameters['ICV_Registration_Blur_FWHM'] = int (data['ICV_Registration_Blur_FWHM'])
	else : 
		parameters['ICV_Registration_Blur_FWHM'] = int ('1')	
	if 'ICV_Num_Of_Iterations' in data: 
		parameters['ICV_Num_Of_Iterations'] = data['ICV_Num_Of_Iterations']
	else : 
		parameters['ICV_Num_Of_Iterations'] = '"{5,8,10,8}"'	
	if 'ICV_Step_Size' in data: 
		parameters['ICV_Step_Size'] = data['ICV_Step_Size']
	else : 
		parameters['ICV_Step_Size'] = '"{16,8,2,1}"'
	if 'ICV_Blur_FWHM' in data: 
		parameters['ICV_Blur_FWHM'] = data['ICV_Blur_FWHM']
	else : 
		parameters['ICV_Blur_FWHM'] = '"{8,8,2,1}"'			
	if 'ICV_Weight' in data: 
		parameters['ICV_Weight'] = float (data['ICV_Weight'])
	else : 
		parameters['ICV_Weight'] = float ('1.0')
	if 'ICV_Stiffness' in data: 
		parameters['ICV_Stiffness'] = float (data['ICV_Stiffness'])
	else : 
		parameters['ICV_Stiffness'] = float ('1.0')
	if 'ICV_Similarity' in data: 
		parameters['ICV_Similarity'] = float (data['ICV_Similarity'])
	else : 
		parameters['ICV_Similarity']  = float ('0.3')
	if 'ICV_Sub_lattice' in data: 
		parameters['ICV_Sub_lattice'] = int (data['ICV_Sub_lattice'])
	else : 
		parameters['ICV_Sub_lattice'] = int ('6')
	if 'ICV_Debug' in data: 
		parameters['ICV_Debug'] = str2bool (data['ICV_Debug'])
	else : 
		parameters['ICV_Debug'] = str2bool ('True')
	if 'ICV_Clobber' in data: 
		parameters['ICV_Clobber'] = str2bool (data['ICV_Clobber'])
	else : 
		parameters['ICV_Clobber'] = str2bool ('True')
	if 'ICV_Non_linear' in data: 
		parameters['ICV_Non_linear'] = data['ICV_Non_linear']
	else : 
		parameters['ICV_Non_linear'] = 'corrcoeff'	
	if 'ICV_NonLinear_Normalization' in data: 
		parameters['ICV_NonLinear_Normalization']  = str2bool(data['ICV_NonLinear_Normalization'])
	else : 
		parameters['ICV_NonLinear_Normalization']  = str2bool ('False')	
	if 'ICV_Mask_To_Binary_Min' in data: 
		parameters['ICV_Mask_To_Binary_Min'] = float (data['ICV_Mask_To_Binary_Min'])
	else : 
		parameters['ICV_Mask_To_Binary_Min'] = float ('0.5')
	if 'ICV_Model' in data: 
		parameters['ICV_Model_Name'] = data['ICV_Model']
	else : 
		parameters['ICV_Model_Name'] = 'Three_Tesla_Adults_Model'
	if 'ICV_Run_Type' in data: 
		parameters['ICV_Run_Type'] = data['ICV_Run_Type']
	else : 
		parameters['ICV_Run_Type'] = 'Calc_Fine'								
	
	return parameters	
	
	
def dict_compare(d1, d2):
    d1_keys = set(d1.keys())
    d2_keys = set(d2.keys())
    intersect_keys = d1_keys.intersection(d2_keys)
    added = d1_keys - d2_keys
    removed = d2_keys - d1_keys
    modified = {o : (d1[o], d2[o]) for o in intersect_keys if d1[o] != d2[o]}
    same = set(o for o in intersect_keys if d1[o] == d2[o])
    return added, removed, modified, same	

def main():
			
	# Parse the program command line parameters  
	parser = argparse.ArgumentParser()
	# Define flags for general parameters and these that are used to direct input and output. 
	parser.add_argument('-Verbose', dest='Verbose', help='Determine if the program prints all the source and sink data', type=bool, default=False) 
	parser.add_argument('-Dict', type=unicode, dest='Dict', required=False, default='', help='Dictionary file with source data.')
	parser.add_argument('-Parameters_File', type=unicode, dest='Parameters_File', required=False, default='', help='Dictionary file with source data.')
	parser.add_argument('-Input_URI', dest='Rel_Input_URI', help='URI of the folder with input input files. Can be be uses instead of -Dict. The URI is relative to the vfs://data location.', 
		type=str, required=False, default='')
	parser.add_argument('-Output_URI', dest='Rel_Output_URI', help='URI of the output folder relative to the vfs://icv_data location', type=str, default='')
	parser.add_argument('-Temp', type=unicode, dest='Temp_Dir',required=False, default='vfs://tmp', help='Temp directory relavive to vfs://tmp')
	parser.add_argument('-query', type=unicode, required=False, help='URL query string that will be appended to Fastr output.', default="" )
	
	# Define flags for parameters that will be used to build the Model Brain
	parser.add_argument('-ABM_Delete_Files', dest='ABM_Delete_Files', help='Delete the files from the ABM temporary folder. Default = True', 
		type=lambda x:bool(distutils.util.strtobool(x)), default=True)
	parser.add_argument('-ABM_Name', dest='ABM_Name', help='Name of the Average Brain output. Default = Test_Average_Brain', 
		type=str, default='Test_Average_Brain')
	parser.add_argument('-ABM_First_Subject', dest='ABM_First_Subject', help='Name of the file with the first subject that everyother file will be registered to in the first registration . Default = empty string', 
		type=str, default='None')	
	parser.add_argument('-ABM_Debug', dest='ABM_Debug', help='Print out debug info. Default = True', 
		type=lambda x:bool(distutils.util.strtobool(x)), default=True)
	parser.add_argument('-ABM_Clobber', dest='ABM_Clobber', help='"Overwrite output file. Default = True', 
		type=lambda x:bool(distutils.util.strtobool(x)), default=True)	
	parser.add_argument('-ABM_Linear_Estimate', dest='ABM_Linear_Estimate', help='Type of estimation of transformation relative to previous transformation during the model brain building'
		'Options:Estimate_Center|Estimate_Scales|Estimate_Translation|Estimate_None. Default = Estimate_Translation.', 
		type=str, default='Estimate_Translation') 
	parser.add_argument('-ABM_Num_Of_Registrations', dest='ABM_Num_Of_Registrations', help='Number of linear registration during the model brain building. Default = 4.', 
		type=int, default=4) 
	parser.add_argument('-ABM_Registration_Blur_FWHM', dest='ABM_Registration_Blur_FWHM', help='Blur size before linear registration during the model brain building. Default = 1.', 
		type=int, default=1)  
	parser.add_argument('-ABM_Linear_Normalization', dest='ABM_Linear_Normalization', help='Normalize the intensities when averaging the linear registrations to produce new average brain. Default = False.', 
		type=lambda x:bool(distutils.util.strtobool(x)), default=False)
	parser.add_argument('-ABM_Num_Of_Iterations', dest='ABM_Num_Of_Iterations', help='Number of iterations in each non-linear registration during the model brain building. Default = "{5,8,10,8}" ', 
		type=str, default='"{5,8,10,8}"') 
	parser.add_argument('-ABM_Step_Size', dest='ABM_Step_Size', help='Step size in each non-linear registration during the model brain building. Default = "{16,8,2,1}"', 
		type=str, default='"{16,8,2,1}"') 
	parser.add_argument('-ABM_Blur_FWHM', dest='ABM_Blur_FWHM', help='Blur amount before in each non-linear registration during the model brain building. Default = "{8,8,2,1}"', 
		type=str, default='"{8,8,2,1}"') 
	parser.add_argument('-ABM_Weight', dest='ABM_Weight', help='Weighting factor for each iteration in non-linear optimization during the model brain building. Default = 1.0', 
		type=float, default=1.0) 
	parser.add_argument('-ABM_Stiffness', dest='ABM_Stiffness', help='Weighting factor for smoothing between non-linear iterations during the model brain building. Default = 1.0', 
		type=float, default=1.0) 
	parser.add_argument('-ABM_Similarity', dest='ABM_Similarity', help='Weighting factor for  r=similarity*w + cost(1*w) during the model brain building. Default = 0.3', 
		type=float, default=0.3) 
	parser.add_argument('-ABM_Sub_lattice', dest='ABM_Sub_lattice', help='Number of nodes along diameter of local sub-lattice during the model brain building. Default = 6', 
		type=int, default=6) 
	parser.add_argument('-ABM_Non_linear', dest='ABM_Non_linear', help='Recover nonlinear deformation field during the model brain building.' 
		'Optional arg {xcorr|diff|sqdiff|label|chamfer|corrcoeff|opticalflow} sets objective function.  Default = corrcoeff', 
		type=str, default='corrcoeff')
	parser.add_argument('-ABM_NonLinear_Normalization', dest='ABM_NonLinear_Normalization', help='Normalize the intensities when averaging the nonlinear registrations to produce new average brain. Default = False.', 
		type=lambda x:bool(distutils.util.strtobool(x)), default=False) 
	
	# Define flags for parameters that will be used to build the final Intracranial Mask 
	parser.add_argument('-ICV_Model', dest='ICV_Model_Name', help='Optional: Name of the model mask to use during the mask building' 
		'Should be either OneHalf_Tesla_Adults_Model, OneHalf_Tesla_Kids_Model, Three_Tesla_Adults_Model, Three_Tesla_Kids_Model ' 
		'or a string with the address of the custom ICV model (without the vfs:// prefix). Default is: Three_Tesla_Adults_Model.',
		type=str, default='Three_Tesla_Adults_Model')
	parser.add_argument('-ICV_Delete_Files', dest='ICV_Delete_Files', help='Delete the files from the ICV temporary folder. Default = True', 
		type=lambda x:bool(distutils.util.strtobool(x)), default=True)
	parser.add_argument('-ICV_Debug', dest='ICV_Debug', help='Print out debug info. Default = True', 
		type=lambda x:bool(distutils.util.strtobool(x)), default=True)
	parser.add_argument('-ICV_Clobber', dest='ICV_Clobber', help='"Overwrite output file. Default = True', 
		type=lambda x:bool(distutils.util.strtobool(x)), default=True)	
	parser.add_argument('-ICV_Linear_Estimate', dest='ICV_Linear_Estimate', help='Type of estimation of transformation relative to previous transformation during the mask building'
		'Options:Estimate_Center|Estimate_Scales|Estimate_Translation|Estimate_None. Default = Estimate_Translation.', 
		type=str, default='Estimate_Translation') 
	parser.add_argument('-ICV_Num_Of_Registrations', dest='ICV_Num_Of_Registrations', help='Number of linear registration during the mask building. Default = 4.', 
		type=int, default=4) 
	parser.add_argument('-ICV_Registration_Blur_FWHM', dest='ICV_Registration_Blur_FWHM', help='Blur size before linear registration during the mask building. Default = 1.', 
		type=int, default=1)  
	parser.add_argument('-ICV_Linear_Normalization', dest='ICV_Linear_Normalization', help='Normalize the intensities when averaging the linear registrations to produce new average brain. Default = False.', 
		type=lambda x:bool(distutils.util.strtobool(x)), default=False)
	parser.add_argument('-ICV_Num_Of_Iterations', dest='ICV_Num_Of_Iterations', help='Number of iterations in each non-linear registration during the mask building. Default = "{5,8,10,8}" ', 
		type=str, default='"{5,8,10,8}"') 
	parser.add_argument('-ICV_Step_Size', dest='ICV_Step_Size', help='Step size in each non-linear registration during the mask building. Default = "{16,8,2,1}"', 
		type=str, default='"{16,8,2,1}"') 
	parser.add_argument('-ICV_Blur_FWHM', dest='ICV_Blur_FWHM', help='Blur amount before in each non-linear registration during the mask building. Default = "{16,8,2,1}"', 
		type=str, default='"{8,8,2,1}"') 
	parser.add_argument('-ICV_Weight', dest='ICV_Weight', help='Weighting factor for each iteration in non-linear optimization during the mask building. Default = 1.0', 
		type=float, default=1.0) 
	parser.add_argument('-ICV_Stiffness', dest='ICV_Stiffness', help='Weighting factor for smoothing between non-linear iterations during the mask building. Default = 1.0', 
		type=float, default=1.0) 
	parser.add_argument('-ICV_Similarity', dest='ICV_Similarity', help='Weighting factor for  r=similarity*w + cost(1*w) during the mask building. Default = 0.3', 
		type=float, default=0.3) 
	parser.add_argument('-ICV_Sub_lattice', dest='ICV_Sub_lattice', help='Number of nodes along diameter of local sub-lattice during the mask building. Default = 6', 
		type=int, default=6) 
	parser.add_argument('-ICV_Non_linear', dest='ICV_Non_linear', help='Recover nonlinear deformation field during the mask building.' 
		'Optional arg {xcorr|diff|sqdiff|label|chamfer|corrcoeff|opticalflow} sets objective function.  Default = corrcoeff', 
		type=str, default='corrcoeff') 
	parser.add_argument('-ICV_NonLinear_Normalization', dest='ICV_NonLinear_Normalization', help='Normalize the intensities when averaging the nonlinear registrations to produce new average brain. Default = False', 
		type=lambda x:bool(distutils.util.strtobool(x)), default=False) 
	parser.add_argument('-ICV_Mask_To_Binary_Min', dest='ICV_Mask_To_Binary_Min', help='Value to below which pixel are disregard in the mask file during the mask building.  Default = 0.5', 
		type=float, default=0.5) 
	parser.add_argument('-ICV_Run_Type', dest='ICV_Run_Type', help='Type of calculation: Crude (Calc_Crude) or Fine (Calc_Fine) during the mask building.  Default = Calc_Crude', 
		type=str, default='Calc_Fine') 	
		
	args = parser.parse_args()
	
	if (args.Dict != '' and args.Rel_Input_URI != ''):
		print 'Cannot run the program with both -Dict and -Input_URI set.'
		sys.exit(1)
	
	# Define the parameters that will be used to build the network:
	args_Dictionary = vars(args)
	if args.Parameters_File != '' :	
		Parameters = JSON_Read_Parameters_File (args.Parameters_File)
		# Check per individual parameters
		added, removed, modified, same = dict_compare (args_Dictionary, Parameters)	
		Modified_Keys = set(modified.keys())
		for Modified_Key in Modified_Keys :
			args_Dictionary [Modified_Key] = Parameters [Modified_Key]
		# End of per individual parameters check block 
		
	if args_Dictionary ['ICV_Run_Type'] == 'Calc_Crude' :
		args_Dictionary ['Calc_Crude'] = True
	elif args_Dictionary ['ICV_Run_Type'] == 'Calc_Fine' :	
		args_Dictionary ['Calc_Fine'] = True
		
	# Case A the parameters are read fro a json DICT file
	if  args.Dict !='':
		
		Sources = JSON_Read_Sources (args.Dict)
		Files_Type = 'MINC'
		for key, value in Sources.iteritems():
			if ('t1w_nifti' in value) or ('t1w_dicom' in value) : 
				 Files_Type = 'NIFTI'
				 break
		Folder_Path = 'None'
				
	# Case B -  Run the network if the JSON dict file was not defined, but the network process a single Folder entry 	
	elif args.Rel_Input_URI != '' :
		
		Sources = {}
		Source_Type = 'Folder'
		Expanded_ICV_Data_Path = fastr.vfs.url_to_path('vfs://icv_data/') 	
		Folder_Path = Expanded_ICV_Data_Path + args.Rel_Input_URI
		Files_Type = 'MINC'
		for subdir, dirs, files in os.walk(Folder_Path):
			for file in files:	
				NIFTI_URI = file.find('.nii')
				NIFTIGZ_URI = file.find('.nii.gz')
				if 	NIFTI_URI > 0 or NIFTIGZ_URI>0 :
					Files_Type = 'NIFTI'
					break
			break
											
	# Case C -  Print an error massage since the the Network mode was not defined	
	else:
		print 'Cannot process the script since no no input folder was defined or no input json dictionary file was defined, please run the --help flag'
		sys.exit(2)
		
	Output_Base_Dir_Rel_Index = args.Rel_Output_URI.find ('vfs://icv_data')
	XNAT_Output_Base_Dir_Rel_index = args.Rel_Output_URI.find ('xnat://')
	if Output_Base_Dir_Rel_Index == 0 or XNAT_Output_Base_Dir_Rel_index == 0 :
		Output_Base_Dir= args.Rel_Output_URI # The Based directory of the results without pre-padded vfs type address 
	else:
		Output_Base_Dir= 'vfs://icv_data/' + args.Rel_Output_URI  # The Based directory of the results with pre-padded vfs address.			
		
	# Set the sample_id of the network
	Temp_Folder_Name = args.ABM_Name
							
	Temp_Dir_Rel_Index = args.Temp_Dir.find ('vfs://tmp') 
	if Temp_Dir_Rel_Index == 0 :
		Temp_Dir=  args.Temp_Dir + '/' + Temp_Folder_Name	
	else : 
		Temp_Dir= 'vfs://tmp/' + args.Temp_Dir + '/' + Temp_Folder_Name		
		
	# Get the sink data 
	sourcedata = source_data(args_Dictionary, Sources, Files_Type, Folder_Path, Temp_Dir)
	sinkdata   = sink_data(Output_Base_Dir, args.ABM_Name, Files_Type, args.query)

	# Build the network
	network    = create_network(Sources, Files_Type, args_Dictionary['ICV_Run_Type'])

	# Print the source and the sink data if needed
	if args.Verbose:
		print 'mode is: \n'
		print Network_Input_Mode
		print 'The final destenation of the output files is: \n'
		print Output_Base_Dir
		print 'The source data is:\n'
		print sourcedata
		print '\n'
		print 'The sink data is:\n'
		print sinkdata
		print '\n'
	# Execute the network    
	network.draw_network(name = network.id, img_format='svg',draw_dimension = True)
	network.execute (sourcedata,sinkdata,tmpdir= Temp_Dir) 				
	
if __name__ == '__main__':
    main();
