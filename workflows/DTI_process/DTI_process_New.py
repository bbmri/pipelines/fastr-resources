import fastr 
fastr.config.source_job_limit = 1
import os
import datetime 
import sys
import argparse
import distutils
import json
from str2bool import str2bool
from os import walk
import pdb #pdb.set_trace() 
import sys 

def source_data(JSON_Sources, acqparams_1, acqparams_2, White_Matter_Atlas, LowerCingulum_Atlas, FA_Model_Brain, T1w_Model_Brain, DWI_Model_Brain, Num_FA_Values, Number_Of_Streamlines, freesurfer_LUT, mrtrix_LUT, Max_forward_Dist_Search, Max_reverse_Dist_Search, Min_tracking_Length, Sift_Number_Of_Fibers, Use_Tensor_Model, Alternative_Parcellation_Atlas, Parcellation_Atlas_Left_Hemisphere, Parcellation_Atlas_Right_Hemisphere, Connectivity_Atlas_Name):

	sourcedata = {
		'Acqparams_1': {}, 'Acqparams_2': {}, 'JHU_ICMB_Atals' : {}, 'FMRIB58_FA_1mm_Atlas': {}, 'Fslmaths_operator': {}, 'B1_Correction_Source': {},
		'Flsmaths_Slices_Operator1': {}, 'Flsmaths_Slices_Operator2': {} , 'Flsmaths_Slices_Operator3': {},
		'Flsmaths_Slices_Operator1_number': {}, 'Flsmaths_Slices_Operator2_number': {}, 
		'Dwiresponse_algorithm': {}, 'Tckgen_select': {}, 'Freesurfer_LUT': {}, 'Mrtrix_LUT': {},
		'Max_forward_Dist_Search': {}, 'Max_reverse_Dist_Search': {}, 'Min_tracking_Length': {}, 'Sift_Number_Of_Fibers': {}, 'Tcksample_Method': {}, 
		'Subject_Name': {}, 'LowerCingulum_1mm': {}, 'MNI152_T1_1mm': {},
		'JHU_ICBM_DWI_1mm': {},
		} 
		
	Slices = [str(int(x)) for x in range (1,Num_FA_Values+1)]

	for Experiment in JSON_Sources['Experiments'].keys(): 
		#sourcedata['Acqparams_1']  = acqparams_1
		#sourcedata['Acqparams_2']  = acqparams_2
		for Experiment in JSON_Sources['Experiments'] :
			sourcedata['Acqparams_1'] [Experiment]  = acqparams_1
			sourcedata['Acqparams_2'] [Experiment] = acqparams_2
			sourcedata ['JHU_ICMB_Atals'][Experiment]  = White_Matter_Atlas
			sourcedata ['FMRIB58_FA_1mm_Atlas'][Experiment]  = FA_Model_Brain
			sourcedata ['LowerCingulum_1mm'][Experiment]  = LowerCingulum_Atlas
			sourcedata ['MNI152_T1_1mm'][Experiment]  = T1w_Model_Brain
			sourcedata ['JHU_ICBM_DWI_1mm'][Experiment]  = DWI_Model_Brain
			sourcedata ['Freesurfer_LUT'][Experiment]  = freesurfer_LUT
			sourcedata ['Mrtrix_LUT'][Experiment]  = mrtrix_LUT
			sourcedata ['Fslmaths_operator'][Experiment]  = '-mas'
			sourcedata ['Dwiresponse_algorithm'][Experiment] = 'dhollander'
			sourcedata ['Tckgen_select'][Experiment] = Number_Of_Streamlines
			sourcedata ['Flsmaths_Slices_Operator1'][Experiment] = 'thr'
			sourcedata ['Flsmaths_Slices_Operator2'][Experiment] = 'uthr'
			sourcedata ['Flsmaths_Slices_Operator3'][Experiment] = 'bin'
			sourcedata ['Flsmaths_Slices_Operator1_number'] [Experiment]= Slices
			sourcedata ['Flsmaths_Slices_Operator2_number'] [Experiment]= Slices
			sourcedata ['Max_forward_Dist_Search'] [Experiment] = Max_forward_Dist_Search
			sourcedata ['Max_reverse_Dist_Search'] [Experiment] = Max_reverse_Dist_Search
			sourcedata ['Min_tracking_Length'] [Experiment] = Min_tracking_Length
			sourcedata ['Sift_Number_Of_Fibers'] [Experiment] = Sift_Number_Of_Fibers
			sourcedata ['B1_Correction_Source'] [Experiment] = '-fsl'
			sourcedata ['Tcksample_Method'] [Experiment] = 'mean'
			sourcedata ['Subject_Name'] [Experiment] = Experiment
			
	sourcedata.update(JSON_Sources)

	if Use_Tensor_Model == True : 
		Tractography_Source = {'Tckgen_Algorithm' :{},}
		for Experiment in JSON_Sources['Experiments'] : 
			Tractography_Source ['Tckgen_Algorithm'] [Experiment] = 1
		sourcedata.update (Tractography_Source)

	if Alternative_Parcellation_Atlas == True :
		Parcellation_Atlas_Sources = {'Parcellation_Atlas_Left_Hemisphere': {}, 'Parcellation_Atlas_Right_Hemisphere': {}, 'Connectivity_Atlas_Name': {}, } 	
		for Experiment in JSON_Sources['Experiments'] :
			Parcellation_Atlas_Sources ['Parcellation_Atlas_Left_Hemisphere']['Experiments'] = Parcellation_Atlas_Left_Hemisphere
			Parcellation_Atlas_Sources ['Parcellation_Atlas_Right_Hemisphere']['Experiments'] = Parcellation_Atlas_Right_Hemisphere	
			Parcellation_Atlas_Sources ['Connectivity_Atlas_Name']['Experiments'] = Connectivity_Atlas_Name	
		sourcedata.update (Parcellation_Atlas_Sources)		

	return sourcedata 

def sink_data(Output_Base_Dir, Keep_Original, PreProcessing, Get_FA_Values, Get_Diffusivity_Values, FiberTracking , Keep_Fibers, Connectivity , JSON_Sources, query_string=""):

	st=datetime.datetime.now().strftime("%Y_%m_%d_%H_%M%p")

	if Output_Base_Dir.find ('xnat://') == 0 :
		if Keep_Original == True : 
			Original_sinkdata = {
				'Original_Niftii' : Output_Base_Dir + '/{sample_id}/{sample_id}_Original.nii.gz' +  query_string,
				'Original_Bval' : Output_Base_Dir + '/{sample_id}/{sample_id}_Original_bval.bval' +  query_string,
				'Original_Bvec' : Output_Base_Dir + '/{sample_id}/{sample_id}_Original_bvec.bvec' +  query_string,
			}

		if PreProcessing == True : 
			PreProcessing_sinkdata = {
				'DWI_Corrected_File' : Output_Base_Dir + '/{sample_id}/{sample_id}_DWI_Corrected_File.nii.gz' +  query_string,
				'Rotated_Bvec' : Output_Base_Dir + '/{sample_id}/{sample_id}_Rotated_bvec.bvec' +  query_string,
				'Movement_RMS' : Output_Base_Dir + '/{sample_id}/{sample_id}_DWI_Corrected_File.eddy_movement_rms' +  query_string,
				'Outlier_Map' : Output_Base_Dir + '/{sample_id}/{sample_id}_DWI_Corrected_File.eddy_outlier_map' +  query_string,
				'Outlier_N_Stdev_Map' : Output_Base_Dir + '/{sample_id}/{sample_id}_DWI_Corrected_File.eddy_outlier_n_stdev_map' +  query_string,
				'Outlier_Report' : Output_Base_Dir + '/{sample_id}/{sample_id}_DWI_Corrected_File.eddy_outlier_report' +  query_string,
				'Parameters' : Output_Base_Dir + '/{sample_id}/{sample_id}_DWI_Corrected_File.eddy_parameters' +  query_string,
				'Post_Eddy_Shell_Alignment_Parameters' : Output_Base_Dir + '/{sample_id}/{sample_id}_DWI_Corrected_File.eddy_post_eddy_shell_alignment_parameters' +  query_string,
				'Mask_Image' : Output_Base_Dir + '/{sample_id}/{sample_id}_Mask_Image.nii.gz' +  query_string,
			}
				
		if Get_FA_Values == True : 
			FA_Values_sinkdata = {
				'FA_Biomarker' : Output_Base_Dir + '/{sample_id}/{sample_id}_FA.txt' +  query_string,
				'FA_Biomarker_Skeleton' : Output_Base_Dir + '/{sample_id}/{sample_id}_FA_Skeleton.txt' +  query_string,
				'Mean_FA_Skeleton' : Output_Base_Dir + '/{sample_id}/{sample_id}_Mean_FA_Skeleton.nii.gz' +  query_string,
				'All_FA_Skeleton' : Output_Base_Dir + '/{sample_id}/{sample_id}_All_FA_Skeleton.nii.gz' +  query_string,
				'Mean_FA' : Output_Base_Dir + '/{sample_id}/{sample_id}_Mean_FA.nii.gz' +  query_string,
				'All_FA' : Output_Base_Dir + '/{sample_id}/{sample_id}_All_FA.nii.gz' +  query_string,
				'T1w_Composite_Figure' : Output_Base_Dir + '/{sample_id}/{sample_id}_T1w_Composite.png' +  query_string,
				'DWI_Composite_Figure' : Output_Base_Dir + '/{sample_id}/{sample_id}_DWI_Composite.png'  +  query_string,
			}

		if Get_Diffusivity_Values == True :
			Diffusivity_Values_sinkdata = {
				'All_MD_Skeleton' : Output_Base_Dir + '/{sample_id}/{sample_id}_All_MD_Skeleton.nii.gz' +  query_string,
				'All_MD' : Output_Base_Dir + '/{sample_id}/{sample_id}_All_MD.nii.gz' +  query_string,
				'All_RD_Skeleton' : Output_Base_Dir + '/{sample_id}/{sample_id}_All_RD_Skeleton.nii.gz' +  query_string,
				'All_RD' : Output_Base_Dir + '/{sample_id}/{sample_id}_All_RD.nii.gz' +  query_string,
				'All_AD_Skeleton' : Output_Base_Dir + '/{sample_id}/{sample_id}_All_AD_Skeleton.nii.gz' +  query_string,
				'All_AD' : Output_Base_Dir + '/{sample_id}/{sample_id}_All_AD.nii.gz' +  query_string,
				'MD_Biomarker' : Output_Base_Dir + '/{sample_id}/{sample_id}_MD.txt' +  query_string,
				'MD_Biomarker_Skeleton' : Output_Base_Dir + '/{sample_id}/{sample_id}_MD_Skeleton.txt' +  query_string,
				'RD_Biomarker' : Output_Base_Dir + '/{sample_id}/{sample_id}_RD.txt' +  query_string,
				'RD_Biomarker_Skeleton' : Output_Base_Dir + '/{sample_id}/{sample_id}_RD_Skeleton.txt' +  query_string,
				'AD_Biomarker' : Output_Base_Dir + '/{sample_id}/{sample_id}_AD.txt' +  query_string,
				'AD_Biomarker_Skeleton' : Output_Base_Dir + '/{sample_id}/{sample_id}_AD_Skeleton.txt' +  query_string,
			}

		if (FiberTracking == True) and (Keep_Fibers == True):
			Fiber_Tracking_sinkdata = {
				'Fiber_Tracking_Image_Det' : Output_Base_Dir + '/{sample_id}/{sample_id}_Deterministic_Fibers.tck.gz' +  query_string,
				'Fiber_Tracking_Image_Prob' : Output_Base_Dir + '/{sample_id}/{sample_id}_Probabilistic_Fibers.tck.gz' +  query_string,
				'Fiber_Tracking_Seeding_Image' : Output_Base_Dir + '/{sample_id}/{sample_id}_Seeding_Image.png' + query_string,
				'Gmwmi_Seeding' : Output_Base_Dir + '/{sample_id}/{sample_id}_Gmwmi_Seeding.nii.gz' + query_string,
				}
		elif (FiberTracking == True) :
				Fiber_Tracking_sinkdata = {
				'Fiber_Tracking_Seeding_Image' : Output_Base_Dir + '/{sample_id}/{sample_id}_Seeding_Image.png' + query_string,
				'Gmwmi_Seeding' : Output_Base_Dir + '/{sample_id}/{sample_id}_Gmwmi_Seeding.nii.gz' + query_string,
				}

		if Connectivity == True :
			Connectivity_sinkdata = {
				'Connectivity_Matrix_Det' : Output_Base_Dir + '/{sample_id}/{sample_id}_Deteterministic_Connectivity_Matrix.csv' +  query_string,
				'Connectivity_Matrix__Det_Stat' : Output_Base_Dir + '/{sample_id}/{sample_id}_Deteministic_Connectivity_Matrix_Stat.txt' +  query_string,
				'Connectivity_Matrix_Det_Image' : Output_Base_Dir + '/{sample_id}/{sample_id}_Deteterministic_Connectivity_Matrix.pdf' +  query_string,
				'Mean_Connectivity_Matrix_Det' : Output_Base_Dir + '/{sample_id}/{sample_id}_Mean_Deterministic_Connectivity_Matrix.csv' +  query_string,
				'Mean_Connectivity_Matrix_Det_Stat' : Output_Base_Dir + '/{sample_id}/{sample_id}_Mean_Deterministic_Connectivity_Matrix_Stat.txt' +  query_string,
				'Mean_Connectivity_Matrix_Det_Image' : Output_Base_Dir + '/{sample_id}/{sample_id}_Mean_Deterministic_Connectivity_Matrix.pdf' +  query_string,
				'Connectivity_Matrix_Prob' : Output_Base_Dir + '/{sample_id}/{sample_id}_Probabilistic_Connectivity_Matrix.csv' +  query_string,
				'Connectivity_Matrix__Prob_Stat' : Output_Base_Dir + '/{sample_id}/{sample_id}_Probabilistic_Connectivity_Matrix_Stat.txt' +  query_string,
				'Connectivity_Matrix_Prob_Image' : Output_Base_Dir + '/{sample_id}/{sample_id}_Probabilistic_Connectivity_Matrix.pdf' +  query_string,
				'Mean_Connectivity_Matrix_Prob' : Output_Base_Dir + '/{sample_id}/{sample_id}_Mean_Probabilisitc_Connectivity_Matrix.csv' +  query_string,
				'Mean_Connectivity_Matrix_Prob_Stat' : Output_Base_Dir + '/{sample_id}/{sample_id}_Mean_Probabilistic_Connectivity_Matrix_Stat.txt' +  query_string,
				'Mean_Connectivity_Matrix_Prob_Image' : Output_Base_Dir + '/{sample_id}/{sample_id}_Mean_Probabilisitc_Connectivity_Matrix.pdf' +  query_string,
				}

	else: 
		if Keep_Original == True : 
			Original_sinkdata = {
				'Original_Niftii' : 'vfs://results/' + Output_Base_Dir  + '/{sample_id}/{sample_id}_Original_' + st + '.nii.gz',
				'Original_Bval' : 'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_Original_bval_' + st + '.bval',
				'Original_Bvec' : 'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_Original_bvec_' + st + '.bvec',
			}

		if PreProcessing == True : 
			PreProcessing_sinkdata = {
				'DWI_Corrected_File' : 'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_DWI_Corrected_File_' + st + '.nii.gz',
				'Rotated_Bvec' : 'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_Rotated_bvec_' + st + '.bvec',
				'Movement_RMS' : 'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_DWI_Corrected_File_' + st + '_eddy_movement_rms',
				'Outlier_Map' : 'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_DWI_Corrected_File_' + st + '_eddy_outlier_map',
				'Outlier_N_Stdev_Map' : 'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_DWI_Corrected_File_' + st + '_eddy_outlier_n_stdev_map',
				'Outlier_Report' : 'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_DWI_Corrected_File_' + st + '_eddy_outlier_report',
				'Parameters' : 'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_DWI_Corrected_File_' + st + '_eddy_parameters',
				'Post_Eddy_Shell_Alignment_Parameters' : 'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_DWI_Corrected_File_' + st + '_eddy_post_eddy_shell_alignment_parameters',
				'Mask_Image' : 'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_Mask_Image_' + st + '.nii.gz',
			}

		if Get_FA_Values == True :
			FA_Values_sinkdata = {
				'FA_Biomarker': 'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_FA_' + st + '.txt' ,
				'FA_Biomarker_Skeleton': 'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_FA_Skeleton_' + st + '.txt' ,
				'Mean_FA_Skeleton' : 'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_Mean_FA_Skeleton_' + st + '.nii.gz',
				'Mean_FA' : 'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_Mean_FA_' + st + '.nii.gz',
				'All_FA_Skeleton' :'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_All_FA_Skeleton_' + st + '.nii.gz',
				'All_FA' :'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_All_FA_' + st + '.nii.gz',
				'T1w_Composite_Figure' :'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_T1w_Composite_' + st + '.png', 
				'DWI_Composite_Figure' :'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_DWI_Composite_' + st + '.png',
			}

		if Get_Diffusivity_Values == True :
			Diffusivity_Values_sinkdata = {
				'All_MD' : 'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_All_MD_' + st + '.nii.gz',
				'All_MD_Skeleton' : 'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_All_MD_Skeleton_' + st + '.nii.gz',
				'All_RD_Skeleton' : 'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_All_RD_Skeleton_' + st + '.nii.gz',
				'All_RD' : 'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_All_RD_' + st + '.nii.gz',
				'All_AD_Skeleton' : 'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_All_AD_Skeleton_' + st + '.nii.gz',
				'All_AD' : 'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_All_AD_' + st + '.nii.gz',
				'MD_Biomarker': 'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_MD_' + st + '.txt' ,
				'MD_Biomarker_Skeleton': 'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_MD_Skeleton_' + st + '.txt' ,
				'RD_Biomarker': 'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_RD_' + st + '.txt' ,
				'RD_Biomarker_Skeleton': 'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_RD_Skeleton_' + st + '.txt' ,
				'AD_Biomarker': 'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_AD_' + st + '.txt' ,
				'AD_Biomarker_Skeleton': 'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_AD_Skeleton_' + st + '.txt' ,
			}

		if (FiberTracking == True) and (Keep_Fibers == True): 
			Fiber_Tracking_sinkdata = {
				'Fiber_Tracking_Image_Det' : 'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_Deterministic_Fibers_' + st +'.tck.gz' ,
				'Fiber_Tracking_Image_Prob' : 'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_Probabilistic_Fibers_' + st +'.tck.gz' ,
				'Fiber_Tracking_Seeding_Image' : 'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_Seeding_Image_' + st +'.png',
				'Gmwmi_Seeding' : 'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_Gmwmi_Seeding_' + st + '.nii.gz'
				}
		elif (FiberTracking == True) :
			Fiber_Tracking_sinkdata = {
				'Fiber_Tracking_Seeding_Image' : 'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_Seeding_Image_' + st +'.png',
				'Gmwmi_Seeding' : 'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_Gmwmi_Seeding_' + st + '.nii.gz'         
				}

		if Connectivity == True :
			Connectivity_sinkdata = {
				'Connectivity_Matrix_Det' : 'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_Deterministic_Connectivity_Matrix_' + st + '.csv',
				'Connectivity_Matrix_Det_Stat' : 'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_Deterministic_Connectivity_Matrix_Stat_' + st + '.txt',
				'Connectivity_Matrix_Det_Image' : 'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_Deterministic_Connectivity_Matrix_' + st + '.pdf',
				'Mean_Connectivity_Matrix_Det' : 'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_Mean_Deterministic_Connectivity_Matrix_' + st + '.csv',
				'Mean_Connectivity_Matrix_Det_Stat' : 'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_Mean_Deterministic_Connectivity_Matrix_Stat_' + st + '.txt',
				'Mean_Connectivity_Matrix_Det_Image' : 'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_Mean_Deterministic_Connectivity_Matrix_' + st + '.pdf',
				'Connectivity_Matrix_Prob' : 'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_Probabilistic_Connectivity_Matrix_' + st + '.csv',
				'Connectivity_Matrix_Prob_Stat' : 'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_Probabilistic_Connectivity_Matrix_Stat_' + st + '.txt',
				'Connectivity_Matrix_Prob_Image' : 'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_Probabilistic_Connectivity_Matrix_' + st + '.pdf',
				'Mean_Connectivity_Matrix_Prob' : 'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_Mean_Probabilistic_Connectivity_Matrix_' + st + '.csv',
				'Mean_Connectivity_Matrix_Prob_Stat' : 'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_Mean_Probabilistic_Connectivity_Matrix_Stat_' + st + '.txt',
				'Mean_Connectivity_Matrix_Prob_Image' : 'vfs://results/' + Output_Base_Dir + '/{sample_id}/{sample_id}_Mean_Probabilistic_Connectivity_Matrix_' + st + '.pdf',
				}

	sinkdata = {}

	if Keep_Original == True : 
		sinkdata.update(Original_sinkdata)

	if PreProcessing == True : 
		sinkdata.update(PreProcessing_sinkdata)

	if Get_FA_Values == True : 
		sinkdata.update(FA_Values_sinkdata)

	if Get_Diffusivity_Values == True :
		sinkdata.update(Diffusivity_Values_sinkdata)

	if ((FiberTracking == True) and (Keep_Fibers == True)):
		sinkdata.update(Fiber_Tracking_sinkdata)
	elif (FiberTracking == True) : 
		sinkdata.update(Fiber_Tracking_sinkdata)

	if Connectivity == True :
		sinkdata.update(Connectivity_sinkdata)

	return sinkdata


def create_network(Dicom2NiftiConvert, T1w_Source_Nifti, FreeSurfer, PreProcessing, Degibbs_Correct, mrdegibbs_axes, BiasCorrect, Mask_In_GZ_From, Get_FA_Values, Get_Diffusivity_Values, FiberTracking, Keep_Fibers, Connectivity, Keep_Original, Eddy_Correction_Type, Run_topup, Use_deafult_acpq_and_index ,topupcnf, tbss_threshold, Number_Of_FA_Slices, Do_Fit, Use_Tensor_Model , Alternative_Parcellation_Atlas, fsl_Version, Fivettgen_Algorithm) :

	fsl_Version_Underscores = fsl_Version.replace('.', '_')
	fsl_Verion_Number = fsl_Version.replace('.', '')

	# Build the Network
	network = fastr.Network(id_='Process_DTI')

	# Download the files and and convert them to niftii
	if Dicom2NiftiConvert == True : 
		Subject_Files = network.create_source (fastr.typelist['DicomImageFile'],id_='DWI_Source',stepid='Sources')
		Subject_Files.required_memory = '8g'
		Dicom_convert = network.create_node (fastr.toollist['dcm2nii-DWI'],id_='Dcm2nii',stepid='Source_Conversion')
		Subject_Files.output >> Dicom_convert.inputs ['Processed_DICOM']
		Dicom_convert.required_time   = '18000'
		Dicom_convert.required_memory = '8g'
	else : 
		# Create the DTI, bval and bvec sources
		DWI_Source = network.create_source (fastr.typelist['NiftiImageFile'],id_='DWI_Source',stepid='Sources')
		bval_Source = network.create_source (fastr.typelist['BValueFile'],id_='bval_Source',stepid='Sources')
		bval_Source.required_memory = '8g'
		bvec_Source = network.create_source (fastr.typelist['BVectorFile'],id_='bvec_Source',stepid='Sources')
		bvec_Source.required_memory = '8g'

	# Download the T1w scan and convert it to niftii
	if (T1w_Source_Nifti != True) and ((FreeSurfer == True) or (Get_FA_Values == True) or (Connectivity == True)) :
		T1w_Subject_Files  = network.create_source (fastr.typelist['DicomImageFile'],id_='T1w_Source',stepid='Sources')
		T1w_Subject_Files.required_memory = '8g'
		T1_Source = network.create_node (fastr.toollist['DicomToNiftiLongPath'],id_='T1w_Dcm2nii',stepid='Source_Conversion')
		T1w_Subject_Files.output >> T1_Source.inputs ['dicom_image']
		T1_Source.required_time   = '9000'
		T1_Source.required_memory = '8g'
	elif ((FreeSurfer == True) or (Get_FA_Values == True) or (Connectivity == True)):
		T1_Source = network.create_source (fastr.typelist['NiftiImageFile'],id_='T1w_Source',stepid='Sources')
		T1_Source.required_memory = '8g'

	# Run the FreeSurfer pipeline
	if FreeSurfer == True :
		Parcellation_Atlas =  network.create_node (fastr.toollist['recon-all-GM-WM-parcellation'],id_='Parcellation_Atlas',stepid='FreeSurfer')
		Parcellation_Atlas.required_memory = '16g'
		if (T1w_Source_Nifti != True): 
			T1_Source.outputs ['image'] >> Parcellation_Atlas.inputs ['T1_File']
		else:
			T1_Source.output >> Parcellation_Atlas.inputs ['T1_File']
		Subject_Name = network.create_source (fastr.typelist['String'],id_='Subject_Name',stepid='Sources')
		Subject_Name.output >> Parcellation_Atlas.inputs ['Name']
		Parcellation_Atlas.inputs ['all'] = ['-all']
		if  Alternative_Parcellation_Atlas == True :
			Parcellation_Atlas_Left_Hemisphere = network.create_source (fastr.typelist['AnnotFile'],id_='Parcellation_Atlas_Left_Hemisphere',stepid='Connectivity_Sources')
			Parcellation_Atlas_Left_Hemisphere.required_memory = '8g'
			Parcellation_Atlas_Right_Hemisphere = network.create_source (fastr.typelist['AnnotFile'],id_='Parcellation_Atlas_Right_Hemisphere',stepid='Connectivity_Sources')
			Parcellation_Atlas_Right_Hemisphere.required_memory = '8g'
	elif  (FreeSurfer != True) : 
		# Create a Freesurfer output sources 
		if (Connectivity == True) :
			T1w_Model_Brain_For_Connectivity = network.create_source (fastr.typelist['MgzImageFile'],id_='T1w_Model_Brain_For_Connectivity_source',stepid='Connectivity_Sources')
			T1w_Model_Brain_For_Connectivity.required_memory = '8g'
			if  Alternative_Parcellation_Atlas == True :
				Parcellation_Atlas_Left_Hemisphere = network.create_source (fastr.typelist['AnnotFile'],id_='Parcellation_Atlas_Left_Hemisphere',stepid='Connectivity_Sources')
				Parcellation_Atlas_Left_Hemisphere.required_memory = '8g'
				Parcellation_Atlas_Right_Hemisphere = network.create_source (fastr.typelist['AnnotFile'],id_='Parcellation_Atlas_Right_Hemisphere',stepid='Connectivity_Sources')
				Parcellation_Atlas_Right_Hemisphere.required_memory = '8g'
				FreeSurfer_SUBJECTS_DIR = network.create_source (fastr.typelist['Directory'],id_='Freesurfer_SUBJECTS_DIR_source',stepid='Connectivity_Sources')
				FreeSurfer_SUBJECTS_DIR.required_memory = '8g'
				FreeSurfer_Subject_ID = network.create_source (fastr.typelist['String'],id_='Freesurfer_Subject_ID_source',stepid='Connectivity_Sources')
				FreeSurfer_Subject_ID.required_memory = '8g'
			else:
				Parcellation_Atlas = network.create_source (fastr.typelist['MgzImageFile'],id_='Parcellation_Atlas_source',stepid='Connectivity_Sources')
				Parcellation_Atlas.required_memory = '8g'
		if ( FiberTracking == True) :
			Freesurfer_Extracted_Brain = network.create_source (fastr.typelist['MgzImageFile'],id_='Freesurfer_Extracted_Brain_source',stepid='Connectivity_Sources')
			Freesurfer_Extracted_Brain.required_memory = '8g'
			if ( Fivettgen_Algorithm == 'Freesurfer' ) :
				Freesurfer_FiveTissues_File = network.create_source (fastr.typelist['MgzImageFile'],id_='Freesurfer_Five_Tissues_Segmented_source',stepid='Connectivity_Sources')
				Freesurfer_FiveTissues_File.required_memory = '8g'

	# Perform PreProcessing step to calculate the corrected dti image
	if PreProcessing == True :

		# Convert the dwi file to a mask file using dwi2mask before any correction 
		Mask_Formation_Uncorrected =  network.create_node (fastr.toollist['dwi2mask'],id_='Dwi2mask_uncorrected',stepid='MRI_Correction')
		if Dicom2NiftiConvert == True : 		
			Dicom_convert.outputs['DWI_File'] >>  Mask_Formation_Uncorrected.inputs ['DWI_File']
			Dicom_convert.outputs['BVEC_File'] >>  Mask_Formation_Uncorrected.inputs ['BVEC_File']
			Dicom_convert.outputs['BVAL_File'] >>  Mask_Formation_Uncorrected.inputs ['BVAL_File']
		else :
			DWI_Source.output >>  Mask_Formation_Uncorrected.inputs ['DWI_File']
			bval_Source.output >> Mask_Formation_Uncorrected.inputs ['BVAL_File']
			bvec_Source.output >> Mask_Formation_Uncorrected.inputs ['BVEC_File']
		Mask_Formation_Uncorrected.required_memory = '8g'

		# Denoise the mask
		Denoise = network.create_node (fastr.toollist['dwidenoise'],id_='Dwidenoise',stepid='MRI_Correction')
		Mask_Formation_Uncorrected.outputs['Mask_File'] >> Denoise.inputs['MASK_File']
		if Dicom2NiftiConvert == True : 
			Dicom_convert.outputs['DWI_File'] >> Denoise.inputs['DWI_File']
		else : 
			DWI_Source.output >> Denoise.inputs['DWI_File']
		Denoise.required_memory = '8g'

		if Degibbs_Correct == True:
			# run degibbs correction
			Unringing = network.create_node (fastr.toollist['mrdegibbs'],id_='Mrdegibbs',stepid='MRI_Correction')
			Unringing.required_memory = '8g'
			Denoise.outputs['Denoised_File'] >> Unringing.inputs ['DWI_File']
			Unringing.inputs ['axes'] = [mrdegibbs_axes]

		if BiasCorrect == True :
			# B1 field inhomogeneity correction
			B1_correction = network.create_node (fastr.toollist['Dwibiascorrect_fsl/'+ fsl_Version ],id_='Dwibiascorrect',stepid='MRI_Correction')
			if Dicom2NiftiConvert == True : 
				Dicom_convert.outputs['BVEC_File'] >> B1_correction.inputs['BVEC_File']
				Dicom_convert.outputs['BVAL_File'] >> B1_correction.inputs['BVAL_File']
			else : 
				bval_Source.output >> B1_correction.inputs['BVAL_File']
				bvec_Source.output >> B1_correction.inputs['BVEC_File']
			if Degibbs_Correct == True:
				Unringing.outputs ['Unringing_File'] >> B1_correction.inputs['Denoised_DWI_File']
			else:
				Denoise.outputs['Denoised_File'] >> B1_correction.inputs['Denoised_DWI_File']
			B1_Correction_Source =  network.create_source (fastr.typelist['String'],id_='B1_Correction_Source',stepid='MRI_Correction_Sources')
			B1_Correction_Type_Link = network.create_link (B1_Correction_Source.output, B1_correction.inputs['fsl_Correct'])
			B1_correction.required_memory = '8g'

		if Eddy_Correction_Type == 'eddy_openmp' :

			# Determine all diffusion unweighted images
			B0indices = network.create_node (fastr.toollist['b0indices'],id_='B0indices',stepid='Correct_Eddy')
			B0indices.required_memory = '8g'
			if Dicom2NiftiConvert == True : 
				Dicom_convert.outputs['BVAL_File'] >> B0indices.inputs['BVAL_File']
			else : 
				bval_Source.output >> B0indices.inputs['BVAL_File']

			# Select volumes from a 4D time series and output a subset 4D volume
			SelectVectors = network.create_node (fastr.toollist['fslselectvols/' + fsl_Version ],id_='Fslselectvols',stepid='Correct_Eddy')
			SelectVectors.required_memory = '8g'
			B0indices.outputs ['Return_String'] >> SelectVectors.inputs['b0indices']
			SelectVectors.inputs['Output_Mean'] = ['-m']
			if BiasCorrect == True :
				B1_correction.outputs['B1_Corrected_File'] >> SelectVectors.inputs['DWI_File']
			elif Degibbs_Correct == True:
				Unringing.outputs ['Unringing_File'] >> SelectVectors.inputs['DWI_File']
			else: 
				Denoise.outputs['Denoised_File'] >> SelectVectors.inputs['DWI_File']

			# Calculate mean accros time for the diffusion unweighted series  
			Unweighted_Diffusion_Mean = network.create_node (fastr.toollist['FSLMaths/' + fsl_Version ],id_='DWI_Unweighted_Diffusion_Mean',stepid='Correct_Eddy')
			Unweighted_Diffusion_Mean.required_memory = '8g'
			SelectVectors.outputs ['B0s'] >> Unweighted_Diffusion_Mean.inputs['image1']
			Unweighted_Diffusion_Mean.inputs['operator1'] = ['-Tmean']

			# Extracting the brain mask 
			Brain_Mask = network.create_node (fastr.toollist['fsl.Bet/' + fsl_Version ],id_='Brain_Mask',stepid='Correct_Eddy')
			Brain_Mask.required_memory = '8g'
			Unweighted_Diffusion_Mean.outputs['output_image'] >> Brain_Mask.inputs['image']
			Brain_Mask.outputs['mask_image'] = [' ']

			if (Use_deafult_acpq_and_index == True) : 
				# Writing an index file for using with the eddy correction node
				Index_File = network.create_node (fastr.toollist['dwi-fslinfo/' + fsl_Version ],id_='Dwi_fslinfo',stepid='Correct_Eddy')
				Index_File.required_memory = '8g'
				if BiasCorrect == True :
					B1_correction.outputs['B1_Corrected_File'] >> Index_File .inputs ['Input_File']
				elif Degibbs_Correct == True :
					Unringing.outputs ['Unringing_File'] >> Index_File .inputs ['Input_File']
				else: 
					Denoise.outputs['Denoised_File'] >> Index_File .inputs ['Input_File']

				# Producing an acqparams file for the use with the eddy correction node
				acqparams_File = network.create_node (fastr.toollist['acqparams_file'],id_='Acqparams_file',stepid='Correct_Eddy')
				acqparams_File.required_memory = '8g'
				acqparams_1 = network.create_source (fastr.typelist['Float'],id_='Acqparams_1',stepid='correction_Sources')
				acqparams_2 = network.create_source (fastr.typelist['Float'],id_='Acqparams_2',stepid='Correction_Sources')
				#acqparams_link_1 = network.create_link(acqparams_1.output, acqparams_File.inputs['acqparams_1'])
				#acqparams_link_2 = network.create_link(acqparams_2.output, acqparams_File.inputs['acqparams_2'])
				B0indices.outputs ['Return_String'] >> acqparams_File.inputs['b0indeces']
				acqparams_1.output >> acqparams_File.inputs['acqparams_1']
				acqparams_2.output >> acqparams_File.inputs['acqparams_2']
				#acqparams_link_1.collapse = 'acqparams_1'	
				#acqparams_link_2.collapse = 'acqparams_2'

			else : 
				acqp_source = network.create_source (fastr.typelist['TxtFile'],id_='Fullacqp_source',stepid='Correction_Sources')
				index_file_source = network.create_source (fastr.typelist['TxtFile'],id_='Index_file_source',stepid='Correction_Sources')
				datain_source = network.create_source (fastr.typelist['TxtFile'],id_='B0acqp_source',stepid='Correction_Sources')

			# Produce bval and bvec files to be used with eddy correction 
			Bval_Bvec_Files = network.create_node (fastr.toollist['write_bval_bvec'],id_='Write_bval_bvec',stepid='Correct_Eddy')
			Bval_Bvec_Files.required_memory = '8g'
			if Dicom2NiftiConvert == True : 
				Dicom_convert.outputs['BVEC_File'] >> Bval_Bvec_Files.inputs['In_BVEC_File']
				Dicom_convert.outputs['BVAL_File'] >> Bval_Bvec_Files.inputs['In_BVAL_File']
			else: 
				bval_Source.output >> Bval_Bvec_Files.inputs['In_BVAL_File']
				bvec_Source.output >> Bval_Bvec_Files.inputs['In_BVEC_File']

			if (Run_topup == True):

				# Read the bval file and output a list of start point and length of the b=0 sequences
				b0trail = network.create_node (fastr.toollist['b0trail'],id_='B0trail',stepid='Correct_Eddy')
				b0trail.required_memory = '8g'
				if Dicom2NiftiConvert == True :
					Dicom_convert.outputs['BVAL_File'] >> b0trail.inputs ['BVAL_File']
				else :
					bval_Source.output >> b0trail.inputs ['BVAL_File']

				# extract the b=0 slices from the DTI file and merged them together as an input for topup
				b0merged = network.create_node (fastr.toollist['Merge_b0_slices/' + fsl_Version ],id_='B0merged',stepid='Correct_Eddy')
				b0merged.required_memory = '8g'
				b0trail.outputs ['Return_String'] >> b0merged.inputs ['slices']
				if BiasCorrect == True :
					B1_correction.outputs['B1_Corrected_File'] >> b0merged.inputs ['input_file']
				elif Degibbs_Correct == True :
					Unringing.outputs ['Unringing_File'] >> b0merged.inputs ['input_file']
				else: 
					Denoise.outputs['Denoised_File'] >> b0merged.inputs ['input_file']

				# run topup to correct susceptibility induced distortions
				topup = network.create_node (fastr.toollist['topup/' + fsl_Version ],id_='Topup',stepid='Correct_Eddy') 
				topup.required_memory = '8g'
				b0merged.outputs ['merged_file'] >> topup.inputs ['imain']
				if (Use_deafult_acpq_and_index == True) :
					acqparams_topup_link = network.create_link(acqparams_File.outputs['Output_acqparams_File'], topup.inputs ['datain'])
					acqparams_topup_link.input_group = 'moving'
				else:
					datain_source.output >> topup.inputs ['datain']
				topup.inputs ['out_base_name'] = ['topup_corrected']
				#topup.inputs ['config'] = [topupcnf]
				topup_constant = network.create_constant('CnfFile', [topupcnf], id_='Topup_Cnf_const', stepid='Correct_Eddy', nodegroup='topup')
				topup_constant.required_memory = '8g'
				topup_constant.output >> topup.inputs ['config']

				# Create a symbolic link from the topup file to the eddy folder
				topup_Symbolic_link = network.create_node (fastr.toollist['Create_DTI_Symbolic_Link'],id_='Topup_Symbolic_link',stepid='Correct_Eddy')
				topup_Symbolic_link.required_memory = '8g'
				topup.outputs ['out_image'] >> topup_Symbolic_link.inputs ['File']
				topup.outputs ['out_move_parameter'] >> topup_Symbolic_link.inputs ['Second_File']
				topup_Symbolic_link.inputs ['Replace'] = ['True']
				topup_Symbolic_link.inputs ['Pattern1'] = ['/Topup/']
				topup_Symbolic_link.inputs ['Pattern2'] = ['/Eddy_openmp/']
				topup_Symbolic_link.inputs['Extension'] = ['.txt']
				topup_Symbolic_link_const = network.create_constant('Directory', ['vfs://dump/'], id_='Topup_Symbolic_link_const', stepid='Correct_Eddy', nodegroup='topup')
				topup_Symbolic_link_const.required_memory = '8g'
				topup_Symbolic_link_const.output >> topup_Symbolic_link.inputs['Directory']

			# Run Eddy correction
			Eddy_Correction = network.create_node (fastr.toollist['eddy_openmp/'+ fsl_Version],id_='Eddy_openmp',stepid='Correct_Eddy')
			if BiasCorrect == True :
				B1_correction.outputs['B1_Corrected_File'] >> Eddy_Correction.inputs ['imain']
			elif Degibbs_Correct == True :
				Unringing.outputs ['Unringing_File'] >> Eddy_Correction.inputs ['imain'] 
			else: 
				Denoise.outputs['Denoised_File'] >> Eddy_Correction.inputs ['imain']
			if (Run_topup == True):
				topup_Symbolic_link.outputs ['Return_String'] >> Eddy_Correction.inputs ['topup']
			Brain_Mask.outputs['mask_image'] >> Eddy_Correction.inputs ['mask']
			if (Use_deafult_acpq_and_index == True) :
				acqparams_eddy_link = network.create_link(acqparams_File.outputs['Output_acqparams_File'], Eddy_Correction.inputs ['acqp'])
				acqparams_eddy_link.input_group = 'moving'
				Index_File.outputs ['Output_File'] >> Eddy_Correction.inputs ['index']
			else :
				acqp_source.output >> Eddy_Correction.inputs ['acqp']
				index_file_source.output >> Eddy_Correction.inputs ['index']
			if int(fsl_Verion_Number) > 509 :
				Eddy_Correction.inputs ['repol'] = ['--repol']
			Bval_Bvec_Files.outputs ['Out_BVEC_File'] >> Eddy_Correction.inputs ['bvec']
			Bval_Bvec_Files.outputs ['Out_BVAL_File'] >> Eddy_Correction.inputs ['bval']
			Eddy_Correction.required_memory = '16g'

		elif Eddy_Correction_Type == 'eddy_correct' :

			# Produce bval and bvec files to be used with eddy correction 
			Bval_Bvec_Files = network.create_node (fastr.toollist['write_bval_bvec'],id_='Write_bval_bvec',stepid='Correct_Eddy')
			Bval_Bvec_Files.required_memory = '8g'
			if Dicom2NiftiConvert == True : 
				Dicom_convert.outputs['BVEC_File'] >> Bval_Bvec_Files.inputs['In_BVEC_File']
				Dicom_convert.outputs['BVAL_File'] >> Bval_Bvec_Files.inputs['In_BVAL_File']
			else :
				bval_Source.output >> Bval_Bvec_Files.inputs['In_BVAL_File']
				bvec_Source.output >> Bval_Bvec_Files.inputs['In_BVEC_File']

			# Run Eddy correction
			Eddy_Correction = network.create_node (fastr.toollist['eddy_correct/' + fsl_Version],id_='Eddy_correct',stepid='Correct_Eddy')
			if BiasCorrect == True :
				B1_correction.outputs['B1_Corrected_File'] >> Eddy_Correction.inputs ['4dinput']
			else: 
				Denoise.outputs['Denoised_File'] >> Eddy_Correction.inputs ['4dinput']
			Refference_Number_source = network.create_constant('Int', [64], id_='Refference_Number_source')
			Refference_Number_source.output >> Eddy_Correction.inputs['reference_no'] 
			Eddy_Correction.required_memory = '8g'

		else :
			print 'Eddy correction type must be either eddy_openmp or eddy_correct'
			exit(1)

		# Convert the dwi file to a mask file using dwi2mask before any correction
		Mask_Formation=  network.create_node (fastr.toollist['dwi2mask'],id_='Dwi2mask',stepid='MRI_Correction')
		Mask_Formation.required_memory = '8g'
		Eddy_Correction.outputs ['DWI_Corrected_File'] >> Mask_Formation.inputs ['DWI_File']
		if Eddy_Correction_Type == 'eddy_openmp' :
			Eddy_Correction.outputs ['Rotated_Bvecs'] >> Mask_Formation.inputs ['BVEC_File']
		elif Eddy_Correction_Type == 'eddy_correct' :
			Bval_Bvec_Files.outputs ['Out_BVEC_File'] >> Mask_Formation.inputs ['BVEC_File']
		if Dicom2NiftiConvert == True :
			Dicom_convert.outputs ['BVAL_File'] >>  Mask_Formation.inputs ['BVAL_File']
		else :
			bval_Source.output >> Mask_Formation.inputs ['BVAL_File']

		# GZ Eddy corrected files
		#GZ_Eddy_Corrected = network.create_node (fastr.toollist['GZ_File'],id_='GZ_Eddy_Corrected',stepid='Correct_Eddy')
		#GZ_Eddy_Corrected.required_memory = '8g'
		#Eddy_Correction.outputs ['DWI_Corrected_File'] >> GZ_Eddy_Corrected.inputs['Input_File']

		# GZ the Mask File
		#GZ_Mask_File = network.create_node (fastr.toollist['GZ_File'],id_='GZ_Mask_File',stepid='Correct_Eddy')
		#GZ_Mask_File.required_memory = '8g'
		#Mask_Formation.outputs['Mask_File'] >> GZ_Mask_File.inputs['Input_File']

	# Compute simple FA sliced biomarkers
	if Get_FA_Values == True : 

		# Create skeleton mask
		Simple_FA_MD = network.create_node (fastr.toollist['dtifit/' + fsl_Version ],id_='Dtifit',stepid='Compute_FA_Values')
		Simple_FA_MD.required_memory = '8g'
		if PreProcessing == True : 
			Eddy_Correction.outputs ['DWI_Corrected_File'] >> Simple_FA_MD.inputs ['DWI_File']
			Mask_Formation.outputs['Mask_File'] >> Simple_FA_MD.inputs ['Mask']
			if Eddy_Correction_Type == 'eddy_openmp' :
				Eddy_Correction.outputs ['Rotated_Bvecs'] >> Simple_FA_MD.inputs ['bvec']
			elif Eddy_Correction_Type == 'eddy_correct' :
				Bval_Bvec_Files.outputs ['Out_BVEC_File'] >> Simple_FA_MD.inputs ['bvec']
			if Dicom2NiftiConvert == True : 
				Dicom_convert.outputs['BVAL_File'] >>  Simple_FA_MD.inputs ['bval']
			else :
				bval_Source.output >> Simple_FA_MD.inputs ['bval']
		else : 
			Eddy_Correction = network.create_source (fastr.typelist['NiftiImageFile'],id_='Eddy_corrected_source',stepid='Calc_FA_Sources')
			Eddy_Correction.required_memory = '8g'
			Eddy_Correction.output >> Simple_FA_MD.inputs ['DWI_File']
			Rotated_Bvec = network.create_source (fastr.typelist['BVectorFile'],id_='Eddy_rotated_bvec_source',stepid='Calc_FA_Sources')
			Rotated_Bvec.required_memory = '8g'
			Rotated_Bvec.output >> Simple_FA_MD.inputs ['bvec']
			if Mask_In_GZ_From==True:
				Un_GZ_Mask = network.create_node (fastr.toollist['Un_GZ_Nifti_File'],id_='Un_GZ_Nifti_File',stepid='UnZip_Files')
				Un_GZ_Mask.required_memory = '8g'
				Mask_Formation = network.create_source (fastr.typelist['NiftiImageFileCompressed'],id_='Mask_File_source',stepid='Calc_FA_Sources')
				Mask_Formation.required_memory = '8g'
				Mask_Formation.output >> Un_GZ_Mask.inputs ['Input_File']
				Un_GZ_Mask.outputs ['Output_File'] >> Simple_FA_MD.inputs ['Mask'] 
			else:
				Mask_Formation = network.create_source (fastr.typelist['NiftiImageFileUncompressed'],id_='Mask_File_source',stepid='Calc_FA_Sources')
				Mask_Formation.required_memory = '8g'
				Mask_Formation.output >> Simple_FA_MD.inputs ['Mask']
			if Dicom2NiftiConvert == True :
				Dicom_convert.outputs['BVAL_File'] >>  Simple_FA_MD.inputs ['bval']
			else :
				bval_Source.output >> Simple_FA_MD.inputs ['bval']

		# Run tbss analysis
		TBSS_Analysis = network.create_node (fastr.toollist['tbss/' + fsl_Version],id_='Tbss',stepid='Compute_FA_Values')
		TBSS_Analysis.required_memory = '8g'
		Simple_FA_MD.outputs['FA'] >> TBSS_Analysis.inputs['Input_Files']
		TBSS_Threshold = network.create_constant ('Float', [tbss_threshold])
		TBSS_Threshold.output >> TBSS_Analysis.inputs['tbss_4_prestat_treshold']

		# Run fslmaths to produces a general FA mask file
		Skeleton_Atlas = network.create_node (fastr.toollist['FSLMathsBinary/' + fsl_Version ],id_='FSLMaths_Skeleton',stepid='Compute_FA_Values')
		Skeleton_Atlas.required_memory = '8g'
		Atlas_Source   = network.create_source (fastr.typelist['NiftiImageFile'],id_='JHU_ICMB_Atals',stepid='Atlas_Sources')
		Atlas_Source.required_memory = '8g'
		Operator_Source   = network.create_source ('String',id_='Fslmaths_operator',stepid='FSLMaths_Sources')
		Skeleton_Atlas_Operator_Link = network.create_link(Atlas_Source.output, Skeleton_Atlas.inputs ['left_hand'])
		Skeleton_Atlas_Operator_Link = network.create_link(Operator_Source.output, Skeleton_Atlas.inputs ['operator'])
		Skeleton_Atlas.inputs ['right_hand'] = TBSS_Analysis.outputs ['mean_FA_skeleton_Mask']

		# Slice the mask to produce Number_Of_FA_Slices masks for the tbss case
		Slices=list(range(1,Number_Of_FA_Slices+1))
		Mask_Slices = network.create_node (fastr.toollist['fslmaths_mask_binary/' + fsl_Version ],id_='Fslmaths_mask_binary',nodegroup ='Mask_Slices',stepid='Compute_FA_Values')
		Mask_Slices.required_memory = '8g'
		Mask_Slices_Link_1 = network.create_link(Skeleton_Atlas.outputs['result'], Mask_Slices.inputs ['image1'])
		Mask_Slices_Source_1 = network.create_source (fastr.typelist['String'],id_='Flsmaths_Slices_Operator1',stepid='Atlas_Sources')
		Mask_Slices_Link_2 = network.create_link(Mask_Slices_Source_1.output, Mask_Slices.inputs['operator1'])
		Mask_Slices_Source_2 = network.create_source (fastr.typelist['String'],id_='Flsmaths_Slices_Operator2',stepid='Atlas_Sources')
		Mask_Slices_Link_3 = network.create_link(Mask_Slices_Source_2.output, Mask_Slices.inputs['operator2'])
		Mask_Slices_Source_3 = network.create_source (fastr.typelist['String'],id_='Flsmaths_Slices_Operator3',stepid='Atlas_Sources')
		Mask_Slices_Link_4 = network.create_link(Mask_Slices_Source_3.output, Mask_Slices.inputs['operator3'])
		Mask_Slices_Source_4 = network.create_source (fastr.typelist['AnyType'],id_='Flsmaths_Slices_Operator1_number',stepid='Atlas_Sources')
		Mask_Slices_Link_5 = network.create_link(Mask_Slices_Source_4.output, Mask_Slices.inputs['operator1_number'])
		Mask_Slices_Source_5 = network.create_source (fastr.typelist['AnyType'],id_='Flsmaths_Slices_Operator2_number',stepid='Atlas_Sources')
		Mask_Slices_Link_6 = network.create_link(Mask_Slices_Source_5.output, Mask_Slices.inputs['operator2_number'])

		# Slice the mask to produce Number_Of_FA_Slices masks for the full case
		Mask_Slices_Full = network.create_node (fastr.toollist['fslmaths_mask_binary/' + fsl_Version ],id_='Fslmaths_mask_full',nodegroup ='Mask_Slices',stepid='Compute_FA_Values')
		Mask_Slices_Full.required_memory = '8g'
		Mask_Slices_Link_Full_1 = network.create_link(Atlas_Source.output, Mask_Slices_Full.inputs ['image1'])
		Mask_Slices_Link_Full_2 = network.create_link(Mask_Slices_Source_1.output, Mask_Slices_Full.inputs['operator1'])
		Mask_Slices_Link_Full_3 = network.create_link(Mask_Slices_Source_2.output, Mask_Slices_Full.inputs['operator2'])
		Mask_Slices_Link_Full_4 = network.create_link(Mask_Slices_Source_3.output, Mask_Slices_Full.inputs['operator3'])
		Mask_Slices_Link_Full_5 = network.create_link(Mask_Slices_Source_4.output, Mask_Slices_Full.inputs['operator1_number'])
		Mask_Slices_Link_Full_6 = network.create_link(Mask_Slices_Source_5.output, Mask_Slices_Full.inputs['operator2_number'])

		# Calculate an FA value for each slice for the tbss case
		Single_FA_Value = network.create_node (fastr.toollist['fslmeants_mask_value/' + fsl_Version ],id_='FA_Fslmeants',nodegroup ='FA_Slices',stepid='Compute_FA_Values')
		Single_FA_Value.required_memory = '8g'
		TBSS_Analysis.outputs['all_FA_skeletonised'] >> Single_FA_Value.inputs ['mri_image']
		Single_FA_Value_Mask_Link = network.create_link(Mask_Slices.outputs['output_image'], Single_FA_Value.inputs ['mask'])

		# Write single FA values file for the tbss case
		Final_FA_Values_File = network.create_node (fastr.toollist['Write_Diffusivity_Values_File'],id_='Write_FA_File',stepid='Compute_FA_Values')
		Final_FA_Values_File.required_memory = '8g'
		FA_Files_Link= network.create_link (Single_FA_Value.outputs ['output_file'], Final_FA_Values_File.inputs['Input_Files'])
		
		# Calsulate an FA value for each slice for the whole FA scan
		Single_FA_Value_Full = network.create_node (fastr.toollist['fslmeants_mask_value/' + fsl_Version ],id_='FA_Fslmeants_Full',nodegroup ='FA_Slices',stepid='Compute_FA_Values')
		Single_FA_Value_Full.required_memory = '8g'
		TBSS_Analysis.outputs ['all_FA'] >> Single_FA_Value_Full.inputs ['mri_image']
		Single_FA_Full_Value_Mask_Link = network.create_link(Mask_Slices_Full.outputs['output_image'], Single_FA_Value_Full.inputs ['mask'])

		# Write single FA values file for the whole FA scan
		Final_FA_Full_Values_File = network.create_node (fastr.toollist['Write_Diffusivity_Values_File'],id_='Write_FA_Full_File',stepid='Compute_FA_Values')
		Final_FA_Full_Values_File.required_memory = '8g'
		FA_Full_Files_Link= network.create_link (Single_FA_Value_Full.outputs ['output_file'], Final_FA_Full_Values_File.inputs['Input_Files'])	

		# Create sources for the composite png files
		T1_Model_Brain = network.create_source (fastr.typelist['NiftiImageFile'],id_='MNI152_T1_1mm',stepid='Sources')
		T1_Model_Brain.required_memory = '8g'
		DWI_Model_Brain = network.create_source (fastr.typelist['NiftiImageFile'],id_='JHU_ICBM_DWI_1mm',stepid='Sources')
		DWI_Model_Brain.required_memory = '8g'

		# Register the T1w image to the model brain linearly
		T1w_Flirt = network.create_node (fastr.toollist['flirt/' + fsl_Version ],id_='T1w_Flirt',stepid='Create Composite png files')
		T1w_Flirt.required_memory = '8g'
		if (T1w_Source_Nifti != True):
			T1_Source.outputs ['image'] >> T1w_Flirt.inputs ['in']
		else:
			T1_Source.output >> T1w_Flirt.inputs ['in']
		T1_Model_Brain.output >> T1w_Flirt.inputs ['ref']

		# Register the T1w image to the model brain non-linearly
		T1w_Fnirt = network.create_node (fastr.toollist['fnirt/' + fsl_Version ],id_='T1w_Fnirt',stepid='Create Composite png files')
		T1w_Fnirt.required_memory = '8g'
		T1w_Flirt.outputs ['out'] >> T1w_Fnirt.inputs ['in']
		T1_Model_Brain.output >> T1w_Fnirt.inputs ['ref']

		# Create the FA tbss sekelton over the T1w png file
		T1w_Composite = network.create_node (fastr.toollist['Write_Composite_PNG_File'],id_='T1w_Composite',stepid='Create Composite png files')
		T1w_Composite.required_memory = '8g'
		T1w_Fnirt.outputs ['iout'] >> T1w_Composite.inputs ['BackGround_File']
		#T1w_ApplyWrap.outputs ['warped_image'] >> T1w_Composite.inputs ['BackGround_File']
		TBSS_Analysis.outputs['all_FA_skeletonised'] >> T1w_Composite.inputs ['ForGround_File']

		# read the b=0 slices if needed
		if not ('B0indices' in locals()):
			B0indices = network.create_node (fastr.toollist['b0indices'],id_='B0indices',stepid='Correct_Eddy')
			B0indices.required_memory = '8g'
			if Dicom2NiftiConvert == True :
				Dicom_convert.outputs['BVAL_File'] >> B0indices.inputs['BVAL_File']
			else :
				bval_Source.output >> B0indices.inputs['BVAL_File']		# read the b=0 slices if needed

		# Read the first b=0 slice
		Read_First_b0 = network.create_node (fastr.toollist['FirstB0'],id_='First_B0',stepid='Create Composite png files')
		Read_First_b0.required_memory = '8g'
		B0indices.outputs ['Return_String'] >> Read_First_b0.inputs ['B0_String']

		# Extract the first b=0 slice from the DWI file
		DWI_roi = network.create_node (fastr.toollist['FSLroi/' + fsl_Version ],id_='DWI_roi',stepid='Create Composite png files')
		DWI_roi.required_memory = '8g'
		if PreProcessing == True :
			Eddy_Correction.outputs ['DWI_Corrected_File'] >> DWI_roi.inputs ['input']
		else :
			Eddy_Correction.output >> DWI_roi.inputs ['input']
		Read_First_b0.outputs ['Return_Value'] >> DWI_roi.inputs ['Index_1']
		DWI_roi.inputs ['Index_2'] = [1]
		
		# Register the DWI image to the model brain linearly
		#DWI_Flirt = network.create_node (fastr.toollist['flirt/' + fsl_Version ],id_='DWI_Flirt',stepid='Create Composite png files')
		#DWI_Flirt.required_memory = '8g'
		#DWI_roi.outputs ['output'] >> DWI_Flirt.inputs ['in']
		#DWI_Model_Brain.output >> DWI_Flirt.inputs ['ref']

		# Register the DWI image to the model brain non-linearly
		#DWI_Fnirt = network.create_node (fastr.toollist['fnirt/' + fsl_Version ],id_='DWI_Fnirt',stepid='Create Composite png files')
		#DWI_Fnirt.required_memory = '8g'
		#DWI_Flirt.outputs ['out'] >> DWI_Fnirt.inputs ['in']
		#DWI_Model_Brain.output >> DWI_Fnirt.inputs ['ref']

		# Apply wrap over the T1w image to move the DWI file to the FA space
		DWI_ApplyWrap = network.create_node (fastr.toollist['ApplyWarp/' + fsl_Version ],id_='DWI_Wrap', stepid='Create Composite png files')
		DWI_ApplyWrap.required_memory = '8g'
		DWI_roi.outputs ['output'] >> DWI_ApplyWrap.inputs ['source_image']
		DWI_Model_Brain.output >> DWI_ApplyWrap.inputs ['reference_image']
		TBSS_Analysis.outputs ['FA_Wrap'] >> DWI_ApplyWrap.inputs ['warp_coef_image']
                
		# Create the FA tbss sekelton over the T1w png file
		DWI_Composite = network.create_node (fastr.toollist['Write_Composite_PNG_File'],id_='DWI_Composite',stepid='Create Composite png files')
		DWI_Composite.required_memory = '16g'
		#DWI_Fnirt.outputs ['iout'] >> DWI_Composite.inputs ['BackGround_File']
		DWI_ApplyWrap.outputs ['warped_image'] >> DWI_Composite.inputs ['BackGround_File']
		TBSS_Analysis.outputs['all_FA_skeletonised'] >> DWI_Composite.inputs ['ForGround_File']

	# Compute the other diffusivity values (Mean diffusivity, Axial Diffusivity Radial Diffusivity)
	if (Get_Diffusivity_Values == True) and (Get_FA_Values == True) :

		# Read FA Threshold value 
		Read_Threshold = network.create_node (fastr.toollist['Read_tbss_Threshold'],id_='Read_Threshold',nodegroup ='Non_FA_Nodes',stepid='Compute_Diffusivity_Values')
		Read_Threshold.required_memory = '8g' 
		TBSS_Analysis.outputs ['Stat_Directory'] >> Read_Threshold.inputs ['Threshold_File']

		# Create Atlas sources

		LowerCingulum_1mm = network.create_source (fastr.typelist['NiftiImageFile'],id_='LowerCingulum_1mm',stepid='Non_FA_Sources')
		LowerCingulum_1mm.required_memory = '6g'
		FMRIB58_FA_1mm_Atlas = network.create_source  (fastr.typelist['NiftiImageFile'],id_='FMRIB58_FA_1mm_Atlas',stepid='Atlas_Sources')
		FMRIB58_FA_1mm_Atlas.required_memory = '6g'

		if FreeSurfer == False :
			Subject_Name = network.create_source (fastr.typelist['String'],id_='Subject_Name',stepid='Non_FA_Sources')

		# Calculate Mean Diffusivity

		# Prepare Mean Diffusivity Folder Structure
		Mean_Diff_Folders = network.create_node (fastr.toollist['Make_tbss_Non_FA_Structure_Directories'],id_='MD_Folders',nodegroup ='MD_Slices',stepid='Compute_MD_Values')
		Mean_Diff_Folders.required_memory = '8g'
		Subject_Name.output >> Mean_Diff_Folders.inputs ['Name']
		Mean_Diff_Folders.inputs ['Type'] = ['MD']

		#Create link from dtifit file to MD folders
		Mean_Diff_SymbLink = network.create_node (fastr.toollist['Create_DTI_Symbolic_Link'],id_='MD_SymbLink',nodegroup ='MD_Slices',stepid='Compute_MD_Values')
		Mean_Diff_SymbLink.required_memory = '8g' 
		Mean_Diff_Folders.outputs ['Diff_Directory'] >> Mean_Diff_SymbLink.inputs['Directory']
		Simple_FA_MD.outputs ['MD'] >> Mean_Diff_SymbLink.inputs['File']
		Mean_Diff_SymbLink.inputs['Extension'] = ['.nii.gz'] 

		# Move MD image 
		MD_Move_Image = network.create_node (fastr.toollist['immv/' + fsl_Version ],id_='MD_Move',nodegroup ='MD_Slices',stepid='Compute_MD_Values') 
		MD_Move_Image.required_memory = '8g'
		Mean_Diff_Folders.outputs ['Diff_Directory'] >> MD_Move_Image.inputs ['Out_Directory']
		Mean_Diff_Folders.outputs ['Origdata_Directory']>> MD_Move_Image.inputs ['In_Directory']

		#Wrap the MD image to the FMRIB58_FA_1mm space
		MD_Wrap = network.create_node (fastr.toollist['ApplyWarp/' + fsl_Version ],id_='MD_Wrap',nodegroup ='MD_Slices',stepid='Compute_MD_Values') 
		MD_Wrap.required_memory = '8g'
		Mean_Diff_SymbLink.outputs ['Output_Sym_Link'] >> MD_Wrap.inputs ['source_image']
		FMRIB58_FA_1mm_Atlas.output >> MD_Wrap.inputs ['reference_image']
		TBSS_Analysis.outputs ['FA_Wrap'] >> MD_Wrap.inputs ['warp_coef_image']

		# Produce a MD mask for further analysis
		MD_Mask = network.create_node (fastr.toollist['FSLMaths/' + fsl_Version ],id_='MD_Mask',nodegroup ='MD_Slices',stepid='Compute_MD_Values')
		MD_Mask.required_memory = '8g'
		MD_Wrap.outputs ['warped_image'] >> MD_Mask.inputs['image1']
		MD_Mask.inputs ['operator1'] = ['-mas'] 
		TBSS_Analysis.outputs ['mean_FA_Mask'] >> MD_Mask.inputs['image2']

		# Run the tbss_skeleton on the MD
		MD_tbss =  network.create_node (fastr.toollist['tbss_skeleton/' + fsl_Version ],id_='MD_tbss',nodegroup ='MD_Slices',stepid='Compute_MD_Values')
		MD_tbss.required_memory = '8g'
		Read_Threshold.outputs ['Return_String'] >> MD_tbss.inputs ['Threshold']
		TBSS_Analysis.outputs ['mean_FA_skeleton_mask_dst'] >> MD_tbss.inputs ['Distance_Map']
		TBSS_Analysis.outputs ['mean_FA'] >> MD_tbss.inputs ['Input_Image']
		TBSS_Analysis.outputs ['all_FA'] >> MD_tbss.inputs ['Four_D_data']
		LowerCingulum_1mm.output >> MD_tbss.inputs ['Search_Rule_Mask']
		MD_tbss.inputs ['projected_4Ddata'] = ['all_MD_skeletonised']
		MD_Mask.outputs ['output_image'] >> MD_tbss.inputs ['Alternative_4Ddata']

		# Calsulate an MD value for each slice for the tbss case
		Single_MD_Value = network.create_node (fastr.toollist['fslmeants_mask_value/' + fsl_Version ],id_='MD_Fslmeants',nodegroup ='MD_Slices',stepid='Compute_MD_Values')
		Single_MD_Value.required_memory = '8g'
		MD_tbss.outputs['Output_Image'] >> Single_MD_Value.inputs ['mri_image']
		Single_MD_Value_Mask_Link = network.create_link(Mask_Slices.outputs['output_image'], Single_MD_Value.inputs ['mask'])

		# Write single MD values file for the tbss case
		Final_MD_Values_File = network.create_node (fastr.toollist['Write_Diffusivity_Values_File'],id_='Write_MD_File',stepid='Compute_MD_Values')
		Final_MD_Values_File.required_memory = '8g'
		MD_Files_Link= network.create_link (Single_MD_Value.outputs ['output_file'], Final_MD_Values_File.inputs['Input_Files'])

		# Calculate an RD value for each slice for the whole wrapped MD scan
		Single_MD_Value_Full = network.create_node (fastr.toollist['fslmeants_mask_value/' + fsl_Version ],id_='MD_Full_Fslmeants',nodegroup ='RD_Slices',stepid='Compute_RD_Values')
		Single_MD_Value_Full.required_memory = '8g'
		MD_Wrap.outputs ['warped_image'] >> Single_MD_Value_Full.inputs ['mri_image']
		Single_MD_Full_Value_Mask_Link = network.create_link(Mask_Slices_Full.outputs['output_image'], Single_MD_Value_Full.inputs ['mask'])

		# Write single RD values file for the whole wrapped MD scan
		Final_MD_Full_Values_File = network.create_node (fastr.toollist['Write_Diffusivity_Values_File'],id_='Write_MD_Full_File',stepid='Compute_RD_Values')
		Final_MD_Full_Values_File.required_memory = '8g'
		MD_Full_Files_Link= network.create_link (Single_MD_Value_Full.outputs ['output_file'], Final_MD_Full_Values_File.inputs['Input_Files'])

		# --- Calculate Axial Diffusivity

		# Prepare Axial Diffusivity Folder Structure
		Axial_Diff_Folders = network.create_node (fastr.toollist['Make_tbss_Non_FA_Structure_Directories'],id_='AD_Folders',nodegroup ='AD_Slices',stepid='Compute_AD_Values')
		Axial_Diff_Folders.required_memory = '8g'
		Subject_Name.output >> Axial_Diff_Folders.inputs ['Name']
		Axial_Diff_Folders.inputs ['Type'] = ['AD']

		# Create link from dtifit file to AD folders
		Axial_Diff_SymbLink = network.create_node (fastr.toollist['Create_DTI_Symbolic_Link'],id_='AD_SymbLink',nodegroup ='AD_Slices',stepid='Compute_AD_Values')
		Axial_Diff_SymbLink.required_memory = '8g'
		Axial_Diff_Folders.outputs ['Diff_Directory'] >> Axial_Diff_SymbLink.inputs['Directory']
		Simple_FA_MD.outputs ['L1_EigenVale'] >> Axial_Diff_SymbLink.inputs['File']
		Axial_Diff_SymbLink.inputs['Extension'] = ['.nii.gz']

		# Move AD image
		AD_Move_Image = network.create_node (fastr.toollist['immv'],id_='AD_Move',nodegroup ='AD_Slices',stepid='Compute_AD_Values')
		AD_Move_Image.required_memory = '8g'
		Axial_Diff_Folders.outputs ['Diff_Directory'] >> AD_Move_Image.inputs ['Out_Directory']
		Axial_Diff_Folders.outputs ['Origdata_Directory']>> AD_Move_Image.inputs ['In_Directory']

		# Wrap the AD image to the FMRIB58_FA_1mm space
		AD_Wrap = network.create_node (fastr.toollist['ApplyWarp/' + fsl_Version ],id_='AD_Wrap',nodegroup ='AD_Slices',stepid='Compute_AD_Values')
		AD_Wrap.required_memory = '8g'
		Axial_Diff_SymbLink.outputs ['Output_Sym_Link'] >> AD_Wrap.inputs ['source_image']
		FMRIB58_FA_1mm_Atlas.output >> AD_Wrap.inputs ['reference_image']
		TBSS_Analysis.outputs ['FA_Wrap'] >> AD_Wrap.inputs ['warp_coef_image']

		# Produce a AD mask for further analysis
		AD_Mask = network.create_node (fastr.toollist['FSLMaths/' + fsl_Version ],id_='AD_Mask',nodegroup ='AD_Slices',stepid='Compute_AD_Values')
		AD_Mask.required_memory = '8g'
		AD_Wrap.outputs ['warped_image'] >> AD_Mask.inputs['image1']
		AD_Mask.inputs ['operator1'] = ['-mas']
		TBSS_Analysis.outputs ['mean_FA_Mask'] >> AD_Mask.inputs['image2']

		# Run the tbss_skeleton on the AD
		AD_tbss =  network.create_node (fastr.toollist['tbss_skeleton/' + fsl_Version ],id_='AD_tbss',nodegroup ='AD_Slices',stepid='Compute_AD_Values')
		AD_tbss.required_memory = '8g'
		Read_Threshold.outputs ['Return_String'] >> AD_tbss.inputs ['Threshold']
		TBSS_Analysis.outputs ['mean_FA_skeleton_mask_dst'] >> AD_tbss.inputs ['Distance_Map']
		TBSS_Analysis.outputs ['mean_FA'] >> AD_tbss.inputs ['Input_Image']
		TBSS_Analysis.outputs ['all_FA'] >> AD_tbss.inputs ['Four_D_data']
		LowerCingulum_1mm.output >> AD_tbss.inputs ['Search_Rule_Mask']
		AD_tbss.inputs ['projected_4Ddata'] = ['all_AD_skeletonised']
		AD_Mask.outputs ['output_image'] >> AD_tbss.inputs ['Alternative_4Ddata']

		# Calsulate an AD value for each slice for the tbss case
		Single_AD_Value = network.create_node (fastr.toollist['fslmeants_mask_value/' + fsl_Version ],id_='AD_Fslmeants',nodegroup ='AD_Slices',stepid='Compute_AD_Values')
		Single_AD_Value.required_memory = '8g'
		AD_tbss.outputs['Output_Image'] >> Single_AD_Value.inputs ['mri_image']
		Single_AD_Value_Mask_Link = network.create_link(Mask_Slices.outputs['output_image'], Single_AD_Value.inputs ['mask'])

		# Write single AD values file for the tbss case
		Final_AD_Values_File = network.create_node (fastr.toollist['Write_Diffusivity_Values_File'],id_='Write_AD_File',stepid='Compute_AD_Values')
		Final_AD_Values_File.required_memory = '8g'
		AD_Files_Link= network.create_link (Single_AD_Value.outputs ['output_file'], Final_AD_Values_File.inputs['Input_Files'])

		# Calculate an RD value for each slice for the whole wrapped AD scan
		Single_AD_Value_Full = network.create_node (fastr.toollist['fslmeants_mask_value/' + fsl_Version ],id_='AD_Full_Fslmeants',nodegroup ='RD_Slices',stepid='Compute_RD_Values')
		Single_AD_Value_Full.required_memory = '8g'
		AD_Wrap.outputs ['warped_image'] >> Single_AD_Value_Full.inputs ['mri_image']
		Single_AD_Full_Value_Mask_Link = network.create_link(Mask_Slices_Full.outputs['output_image'], Single_AD_Value_Full.inputs ['mask'])

		# Write single RD values file for the whole wrapped AD scan
		Final_AD_Full_Values_File = network.create_node (fastr.toollist['Write_Diffusivity_Values_File'],id_='Write_AD_Full_File',stepid='Compute_RD_Values')
		Final_AD_Full_Values_File.required_memory = '8g'
		AD_Full_Files_Link= network.create_link (Single_AD_Value_Full.outputs ['output_file'], Final_AD_Full_Values_File.inputs['Input_Files'])

		# Calculate Radial Diffusivity

		# Prepare Radial Diffusivity Folder Structure
		Radial_Diff_Folders = network.create_node (fastr.toollist['Make_tbss_Non_FA_Structure_Directories'],id_='RD_Folders',nodegroup ='RD_Slices',stepid='Compute_RD_Values')
		Radial_Diff_Folders.required_memory = '8g'
		Subject_Name.output >> Radial_Diff_Folders.inputs ['Name']
		Radial_Diff_Folders.inputs ['Type'] = ['RD']

		# Compute radial diffusivity from DTI eigen vectors
		Radial_Diffusivity = network.create_node (fastr.toollist['FSLMaths/' + fsl_Version ],id_='Radial_Diffusivity',stepid='Compute_RD_Values')
		Radial_Diffusivity.required_memory = '8g'
		Simple_FA_MD.outputs ['L2_EigenVale'] >> Radial_Diffusivity.inputs['image1']
		Radial_Diffusivity.inputs ['operator1'] = ['-add'] 
		Simple_FA_MD.outputs ['L3_EigenVale'] >> Radial_Diffusivity.inputs['image2']
		Radial_Diffusivity.inputs ['operator2'] = ['-div']
		Radial_Diffusivity.inputs ['operator2_number'] = [2] 

		#Create link from dtifit file to RD folders
		Radial_Diff_SymbLink = network.create_node (fastr.toollist['Create_DTI_Symbolic_Link'],id_='RD_SymbLink',nodegroup ='RD_Slices',stepid='Compute_RD_Values')
		Radial_Diff_SymbLink.required_memory = '8g'
		Radial_Diff_Folders.outputs ['Diff_Directory'] >> Radial_Diff_SymbLink.inputs['Directory']
		Radial_Diffusivity.outputs ['output_image'] >> Radial_Diff_SymbLink.inputs['File']
		Radial_Diff_SymbLink.inputs['Extension'] = ['.nii.gz']

		# Move RD image
		RD_Move_Image = network.create_node (fastr.toollist['immv/' + fsl_Version ],id_='RD_Move',nodegroup ='RD_Slices',stepid='Compute_RD_Values')
		RD_Move_Image.required_memory = '8g'
		Radial_Diff_Folders.outputs ['Diff_Directory'] >> RD_Move_Image.inputs ['Out_Directory']
		Radial_Diff_Folders.outputs ['Origdata_Directory'] >> RD_Move_Image.inputs ['In_Directory']

		# Wrap the RD image to the FMRIB58_FA_1mm space
		RD_Wrap = network.create_node (fastr.toollist['ApplyWarp/' + fsl_Version ],id_='RD_Wrap',nodegroup ='RD_Slices',stepid='Compute_RD_Values')
		RD_Wrap.required_memory = '8g'
		Radial_Diff_SymbLink.outputs ['Output_Sym_Link'] >> RD_Wrap.inputs ['source_image']
		FMRIB58_FA_1mm_Atlas.output >> RD_Wrap.inputs ['reference_image']
		TBSS_Analysis.outputs ['FA_Wrap'] >> RD_Wrap.inputs ['warp_coef_image']

		# Produce a RD mask for further analysis
		RD_Mask = network.create_node (fastr.toollist['FSLMaths/' + fsl_Version ],id_='RD_Mask',nodegroup ='RD_Slices',stepid='Compute_RD_Values')
		RD_Mask.required_memory = '8g'
		RD_Wrap.outputs ['warped_image'] >> RD_Mask.inputs['image1']
		RD_Mask.inputs ['operator1'] = ['-mas']
		TBSS_Analysis.outputs ['mean_FA_Mask'] >> RD_Mask.inputs['image2']

		# Run the tbss_skeleton on the RD
		RD_tbss =  network.create_node (fastr.toollist['tbss_skeleton/' + fsl_Version ],id_='RD_tbss',nodegroup ='RD_Slices',stepid='Compute_RD_Values')
		RD_tbss.required_memory = '8g'
		Read_Threshold.outputs ['Return_String'] >> RD_tbss.inputs ['Threshold']
		TBSS_Analysis.outputs ['mean_FA_skeleton_mask_dst'] >> RD_tbss.inputs ['Distance_Map']
		TBSS_Analysis.outputs ['mean_FA'] >> RD_tbss.inputs ['Input_Image']
		TBSS_Analysis.outputs ['all_FA'] >> RD_tbss.inputs ['Four_D_data']
		LowerCingulum_1mm.output >> RD_tbss.inputs ['Search_Rule_Mask']
		RD_tbss.inputs ['projected_4Ddata'] = ['all_RD_skeletonised']
		RD_Mask.outputs ['output_image'] >> RD_tbss.inputs ['Alternative_4Ddata']

		# Calculate an RD value for each slice for the tbss cytoskeleton
		Single_RD_Value = network.create_node (fastr.toollist['fslmeants_mask_value/' + fsl_Version ],id_='RD_Fslmeants',nodegroup ='RD_Slices',stepid='Compute_RD_Values')
		Single_RD_Value.required_memory = '8g'
		RD_tbss.outputs['Output_Image'] >> Single_RD_Value.inputs ['mri_image']
		Single_RD_Value_Mask_Link = network.create_link(Mask_Slices.outputs['output_image'], Single_RD_Value.inputs ['mask'])

		# Write single RD values file for the tbss cytoskeleton
		Final_RD_Values_File = network.create_node (fastr.toollist['Write_Diffusivity_Values_File'],id_='Write_RD_File',stepid='Compute_RD_Values')
		Final_RD_Values_File.required_memory = '8g'
		RD_Files_Link= network.create_link (Single_RD_Value.outputs ['output_file'], Final_RD_Values_File.inputs['Input_Files'])

		# Calculate an RD value for each slice for the whole wrapped RD scan
		Single_RD_Value_Full = network.create_node (fastr.toollist['fslmeants_mask_value/' + fsl_Version ],id_='RD_Full_FSLMEANTS',nodegroup ='RD_Slices',stepid='Compute_RD_Values')
		Single_RD_Value_Full.required_memory = '8g'
		RD_Wrap.outputs ['warped_image'] >> Single_RD_Value_Full.inputs ['mri_image']
		Single_RD_Full_Value_Mask_Link = network.create_link(Mask_Slices_Full.outputs['output_image'], Single_RD_Value_Full.inputs ['mask'])

		# Write single RD values file for the whole wrapped RD scan
		Final_RD_Full_Values_File = network.create_node (fastr.toollist['Write_Diffusivity_Values_File'],id_='Write_RD_Full_File',stepid='Compute_RD_Values')
		Final_RD_Full_Values_File.required_memory = '8g'
		RD_Full_Files_Link= network.create_link (Single_RD_Value_Full.outputs ['output_file'], Final_RD_Full_Values_File.inputs['Input_Files'])

	# Compute a fibertracking tck file
	if FiberTracking == True :

		if ( Fivettgen_Algorithm == 'Freesurfer' ): 
			# Convert the the freesurfer brain mask in a standard space to nifti format
			Freesurfer_Brain_Convert = network.create_node (fastr.toollist['mrconvert_to_nifti'],id_='Freesurfer_Brain_Convert',stepid='Track_MW_Fibers_Preparations')
			Freesurfer_Brain_Convert.required_memory = '8g'
			if FreeSurfer == True :
				Parcellation_Atlas.outputs['Brain_File'] >> Freesurfer_Brain_Convert.inputs ['Input_File_Mgz']
			else:
				Freesurfer_Extracted_Brain.output >> Freesurfer_Brain_Convert.inputs ['Input_File_Mgz']
 
			# Create a 5 tissue type image from the T1w image
			FiveTissue = network.create_node (fastr.toollist['5ttgen_fsl_' + fsl_Version_Underscores ],id_='5ttgen',stepid='Track_MW_Fibers_Preparations')
			FiveTissue.required_memory = '8g'
			FiveTissue.inputs ['Algorithm'] = ['freesurfer']
			FiveTissue.inputs ['nocrop'] = ['-nocrop']
			if FreeSurfer == True :
				Parcellation_Atlas.outputs['FiveTissues_File'] >> FiveTissue.inputs ['Aseg_File']
			else:
				Freesurfer_FiveTissues_File.output >> FiveTissue.inputs ['Aseg_File']
		elif ( Fivettgen_Algorithm == 'fsl' ):
			# Create a 5 tissue type image from the T1w image
			FiveTissue = network.create_node (fastr.toollist['5ttgen_fsl_' + fsl_Version_Underscores ],id_='5ttgen',stepid='Track_MW_Fibers_Preparations')
			FiveTissue.required_memory = '8g'
			FiveTissue.inputs ['Algorithm'] = ['fsl']
			FiveTissue.inputs ['nocrop'] = ['-nocrop']
			if (T1w_Source_Nifti != True):
				T1_Source.outputs ['image'] >> FiveTissue.inputs ['T1_File']
			else:
				T1_Source.output >> FiveTissue.inputs ['T1_File']

			# Extract the brain from the T1w image
			Extract_Brain = network.create_node (fastr.toollist['Bet/' + fsl_Version ],id_='Bet',stepid='Track_MW_Fibers_Preparations')
			Extract_Brain.required_memory = '8g'
			if (T1w_Source_Nifti != True):
				T1_Source.outputs ['image'] >> Extract_Brain.inputs ['image']
			else:
				T1_Source.output >> Extract_Brain.inputs ['image']

		if not ('B0indices' in locals()):
			B0indices = network.create_node (fastr.toollist['b0indices'],id_='B0indices',stepid='Correct_Eddy')
			B0indices.required_memory = '8g'
			if Dicom2NiftiConvert == True :
				Dicom_convert.outputs['BVAL_File'] >> B0indices.inputs['BVAL_File']
			else :
				bval_Source.output >> B0indices.inputs['BVAL_File']             # read the b=0 slices if needed

		# Select volumes from a 4D time series and output a subset 4D volume of the corrected DWI image
		DWI_Corrected_SelectVolumes = network.create_node (fastr.toollist['fslselectvols/' + fsl_Version ],id_='DWI_Corrected_fslselectvols',stepid='Track_MW_Fibers_Preparations')
		DWI_Corrected_SelectVolumes.required_memory = '8g'
		B0indices.outputs ['Return_String'] >> DWI_Corrected_SelectVolumes.inputs['b0indices']
		DWI_Corrected_SelectVolumes.inputs['Output_Mean'] = ['-m']
		if PreProcessing == True :
			Eddy_Correction.outputs ['DWI_Corrected_File'] >>  DWI_Corrected_SelectVolumes.inputs ['DWI_File']
		else:
			if not ('Eddy_Correction' in locals()):
				Eddy_Correction = network.create_source (fastr.typelist['NiftiImageFile'],id_='Eddy_corrected_source',stepid='WM_Tracking_Sources')
				Eddy_Correction.required_memory = '8g'
			Eddy_Correction.output >> DWI_Corrected_SelectVolumes.inputs ['DWI_File']

		# Calculate Mean B=0 corrected DWI Image
		Unweighted_Corrected_Diffusion_Mean = network.create_node (fastr.toollist['FSLMaths/' + fsl_Version ],id_='Mean_Corrected_Unweighted_Scan',stepid='Track_MW_Fibers_Preparation')
		Unweighted_Corrected_Diffusion_Mean.required_memory = '8g'
		DWI_Corrected_SelectVolumes.outputs ['B0s'] >> Unweighted_Corrected_Diffusion_Mean.inputs['image1']
		Unweighted_Corrected_Diffusion_Mean.inputs['operator1'] = ['-Tmean']

		# Convert the Five tissue file to a nifti format
		#FiveTissues_Convert = network.create_node (fastr.toollist['mrconvert_to_nifti'],id_='FiveTissues_Convert',stepid='Track_MW_Fibers_Preparations')
		#FiveTissues_Convert.required_memory = '8g'
		#FiveTissue.outputs ['out_5TT_File'] >> FiveTissues_Convert.inputs ['Input_File_Mif']

		# Convert the extracted brain to mif
		#Extract_Brain_Mif = network.create_node (fastr.toollist['mrconvert_to_mif'],id_='Extrected_Brain_Mif',stepid='Track_MW_Fibers_Preparations')
		#Extract_Brain_Mif.required_memory = '8g'
		#Extract_Brain.outputs ['brain_image'] >> Extract_Brain_Mif.inputs ['Input_File_Nifti']

		# Register the DWI image to the T1w image so that it can be used for the five tissues corregistration
		Dwi_To_T1w = network.create_node (fastr.toollist['flirt/' + fsl_Version ],id_='Dwi2T1w',stepid='Track_MW_Fibers_Preparations')
		Dwi_To_T1w.required_memory = '8g'
		Dwi_To_T1w.inputs ['interp'] = ['nearestneighbour']
		Dwi_To_T1w.inputs ['dof'] = [6]
		Unweighted_Corrected_Diffusion_Mean.outputs ['output_image'] >> Dwi_To_T1w.inputs ['in']
		if ( Fivettgen_Algorithm == 'Freesurfer' ):
			Freesurfer_Brain_Convert.outputs ['Output_Nifti_File'] >> Dwi_To_T1w.inputs ['ref']
		elif ( Fivettgen_Algorithm == 'fsl' ):
			Extract_Brain.outputs ['brain_image'] >> Dwi_To_T1w.inputs ['ref']

		# Transforming the flirt transformation matrix to mrtrix txt file
		Transform_T1w_Flirt_Matrix = network.create_node (fastr.toollist['transformconvert'],id_='T1w_Flirt_matrix_Transform',stepid='Track_MW_Fibers_Preparations')
		Transform_T1w_Flirt_Matrix.required_memory = '8g'
		Transform_T1w_Flirt_Matrix.inputs ['operation'] = ['flirt_import']
		Unweighted_Corrected_Diffusion_Mean.outputs ['output_image'] >> Transform_T1w_Flirt_Matrix.inputs ['Input_File']
		if ( Fivettgen_Algorithm == 'Freesurfer' ):
			Freesurfer_Brain_Convert.outputs ['Output_Nifti_File'] >> Transform_T1w_Flirt_Matrix.inputs ['Ref_File']
		elif ( Fivettgen_Algorithm == 'fsl' ):
			Extract_Brain.outputs ['brain_image'] >> Transform_T1w_Flirt_Matrix.inputs ['Ref_File']
		Dwi_To_T1w.outputs ['omat'] >> Transform_T1w_Flirt_Matrix.inputs ['FSL_Mat_File']

		# Create a fivetissue file that is registered to the DWI file
		FiveTissues_Transform =  network.create_node (fastr.toollist['mrtransform'],id_='FiveTissues_Transform',stepid='Track_MW_Fibers_Preparations')
		FiveTissues_Transform.required_memory = '8g'
		FiveTissue.outputs ['out_5TT_File'] >> FiveTissues_Transform.inputs ['Input_File'] 
		Transform_T1w_Flirt_Matrix.outputs ['Output_Transformation'] >> FiveTissues_Transform.inputs ['linear']
		FiveTissues_Transform.inputs ['inverse_operation'] = ['inverse']
 
		# Create the generate a mask image appropriate for seeding streamlines
		Seeding_Mask = network.create_node (fastr.toollist['FivettTwogmwmi'],id_='5tt2gmwmi',stepid='Track_MW_Fibers')
		Seeding_Mask.required_memory = '8g'
		FiveTissues_Transform.outputs ['Output_File'] >> Seeding_Mask.inputs ['5tt_in']

		# Convert the gmwmi figure to nifti
		Gmwmi_To_Nifti = network.create_node (fastr.toollist['mrconvert_to_nifti'],id_='Gmwmi_To_Nifti',stepid='Track_MW_Fibers')
		Gmwmi_To_Nifti.required_memory = '8g'
		Seeding_Mask.outputs ['mask_out'] >> Gmwmi_To_Nifti.inputs ['Input_File_Mif']

		# Inverse the mat file Mat file of the transformation
		#Inverse_Mat_File = network.create_node (fastr.toollist['Convert_Xfm'],id_='Invert_XFM',stepid='Track_MW_Fibers')
		#Inverse_Mat_File.required_memory = '8g'
		#T1w_To_Dwi.outputs ['omat'] >> Inverse_Mat_File.inputs ['Inverse']

		# resample the DWI image so that is will have the dimensions of the gmwmi scan
		Resample_Dwi = network.create_node (fastr.toollist['flirt/' + fsl_Version ],id_='Resample_Dwi',stepid='Track_MW_Fibers')
		Resample_Dwi.required_memory = '8g'
		if ( Fivettgen_Algorithm == 'Freesurfer' ):
			Freesurfer_Brain_Convert.outputs ['Output_Nifti_File'] >> Resample_Dwi.inputs ['ref']
		elif ( Fivettgen_Algorithm == 'fsl' ):
			Extract_Brain.outputs ['brain_image'] >> Resample_Dwi.inputs ['ref']
		Resample_Dwi.inputs ['applyxfm_operation'] = ['applyxfm']
		Dwi_To_T1w.outputs ['out'] >> Resample_Dwi.inputs ['in']

		# Reoriate the Gmwmi image to the standard space
		Reoriated_Gmwmi_Scan = network.create_node (fastr.toollist['FSLReorient2Std/' + fsl_Version ],id_='Reoriated_Gmwmi_Scan',stepid='Track_MW_Fibers')
		Reoriated_Gmwmi_Scan.required_memory = '8g'
		Gmwmi_To_Nifti.outputs ['Output_Nifti_File'] >> Reoriated_Gmwmi_Scan.inputs ['image'] 

		# Reoriate the DWI image to the standard spcae
		Reoriated_DWI_Scan = network.create_node (fastr.toollist['FSLReorient2Std/' + fsl_Version ],id_='Reoriated_DWI_Scan',stepid='Track_MW_Fibers')
		Reoriated_DWI_Scan.required_memory = '8g'
		Resample_Dwi.outputs ['out'] >> Reoriated_DWI_Scan.inputs ['image'] 

		# Create an image of the streamline seed on the DWI file for quality control
		StreamLines_Seed_Composite = network.create_node (fastr.toollist['Write_Composite_PNG_File'],id_='StreamLines_Seed_Composite',stepid='Track_MW_Fibers')
		StreamLines_Seed_Composite.required_memory = '8g'
		StreamLines_Seed_Composite.required_time = '18000'
		Reoriated_DWI_Scan.outputs ['reoriented_image'] >> StreamLines_Seed_Composite.inputs ['BackGround_File']
		Reoriated_Gmwmi_Scan.outputs ['reoriented_image'] >> StreamLines_Seed_Composite.inputs ['ForGround_File']

		if Use_Tensor_Model == True : # Compute tracts directly from the DWI tensor

			# Convert the DWI file from a nifti format to a Mif one
			Convert_Dwi_To_Mif = network.create_node (fastr.toollist['mrconvert_to_mif'],id_='Convert_Dwi_To_Mif',stepid='Track_MW_Fibers')
			Convert_Dwi_To_Mif.required_time='18000'
			Convert_Dwi_To_Mif.required_memory = '8g'
			if PreProcessing == True :
				Eddy_Correction.outputs ['DWI_Corrected_File'] >> Convert_Dwi_To_Mif.inputs ['Input_File_Nifti']
			elif Get_FA_Values == True :
				Eddy_Correction.output >> Convert_Dwi_To_Mif.inputs ['Input_File_Nifti']
			else: 
				if not ('Eddy_Correction' in locals()):
					Eddy_Correction = network.create_source (fastr.typelist['NiftiImageFile'],id_='Eddy_corrected_source',stepid='WM_Tracking_Sources')
					Eddy_Correction.required_memory = '8g'
				Eddy_Correction.output >> Convert_Dwi_To_Mif.inputs ['Input_File_Nifti']

			# Perform streamlines deterministic tractography
			Streamlines_Tractography_Det = network.create_node (fastr.toollist['tckgen'],id_='Tckgen_Det',stepid='Track_MW_Fibers')
			Streamlines_Tractography_Det.required_memory = '8g'
			FiveTissues_Transform.outputs ['Output_File'] >> Streamlines_Tractography_Det.inputs ['act']
			Min_tracking_Length = network.create_source (fastr.typelist['Float'],id_='Min_tracking_Length',stepid='Fiber_Tracking_sources')
			Min_tracking_Length.output >> Streamlines_Tractography_Det.inputs ['minlength']
			Streamlines_Tractography_Source = network.create_source (fastr.typelist['Int'],id_='Tckgen_select',stepid='tckgen_Sources')
			Streamlines_Tractography_Link = network.create_link (Streamlines_Tractography_Source.output,Streamlines_Tractography_Det.inputs ['select'])
			Streamlines_Tractography_Det.inputs ['Algorithm'] = ['Tensor_Det']
			if PreProcessing == True : 
				Convert_Dwi_To_Mif.outputs ['Output_Mif_File'] >> Streamlines_Tractography_Det.inputs ['Input_DWI']
				if Eddy_Correction_Type == 'eddy_openmp' :
					Eddy_Correction.outputs ['Rotated_Bvecs'] >> Streamlines_Tractography_Det.inputs ['BVEC_File']
				elif Eddy_Correction_Type == 'eddy_correct' :
					Bval_Bvec_Files.outputs ['Out_BVEC_File'] >> Streamlines_Tractography_Det.inputs ['BVEC_File']
				if Dicom2NiftiConvert == True : 
					Dicom_convert.outputs['BVAL_File'] >>  Streamlines_Tractography_Det.inputs ['BVAL_File']
				else :
					bval_Source.output >> Streamlines_Tractography_Det.inputs ['BVAL_File']
				Mask_Formation.outputs['Mask_File'] >> Streamlines_Tractography_Det.inputs ['mask_image']
				Mask_Formation.outputs['Mask_File'] >> Streamlines_Tractography_Det.inputs ['seed_image']
			elif Get_FA_Values == True :
				if Mask_In_GZ_From==True:
					Un_GZ_Mask.outputs ['Output_File'] >> Streamlines_Tractography_Det.inputs ['mask_image']
					Un_GZ_Mask.outputs ['Output_File'] >> Streamlines_Tractography_Det.inputs ['seed_image']
				else:
					Mask_Formation.output >> Streamlines_Tractography_Det.inputs ['mask_image']
					Mask_Formation.output >> Streamlines_Tractography_Det.inputs ['seed_image']
				if Dicom2NiftiConvert == True :
					Dicom_convert.outputs['BVAL_File'] >>  Streamlines_Tractography_Det.inputs ['BVAL_File']
				else :
					bval_Source.output >> Streamlines_Tractography_Det.inputs ['BVAL_File']
				Convert_Dwi_To_Mif.outputs ['Output_Mif_File'] >> Streamlines_Tractography_Det.inputs ['Input_DWI']
				Rotated_Bvec.output >> Streamlines_Tractography_Det.inputs ['BVEC_File']
			else:
				if Mask_In_GZ_From==True :  
					Un_GZ_Mask = network.create_node (fastr.toollist['Un_GZ_Nifti_File'],id_='Un_GZ_Nifti_File',stepid='UnZip_Files')
					Un_GZ_Mask.required_memory = '8g'
					Mask_Formation = network.create_source (fastr.typelist['NiftiImageFileCompressed'],id_='Mask_File_source',stepid='WM_Tracking_Sources')
					Mask_Formation.required_memory = '8g'
					Mask_Formation.output >> Un_GZ_Mask.inputs ['Input_File']
					Un_GZ_Mask.outputs ['Output_File'] >> Streamlines_Tractography_Det.inputs ['mask_image']  
					Un_GZ_Mask.outputs ['Output_File'] >> Streamlines_Tractography_Det.inputs ['seed_image']
				else :
					Mask_Formation.output >> Streamlines_Tractography_Det.inputs ['mask_image']
					Mask_Formation.output >> Streamlines_Tractography_Det.inputs ['seed_image']
				if Dicom2NiftiConvert == True :
					Dicom_convert.outputs['BVAL_File'] >>  Streamlines_Tractography_Det.inputs ['BVAL_File']
				else :
					bval_Source.output >> Streamlines_Tractography_Det.inputs ['BVAL_File']
				Convert_Dwi_To_Mif.outputs ['Output_Mif_File'] >> Streamlines_Tractography_Det.inputs ['Input_DWI']
				Rotated_Bvec = network.create_source (fastr.typelist['BVectorFile'],id_='Eddy_rotated_bvec_source',stepid='WM_Tracking_Sources')
				Rotated_Bvec.required_memory = '8g'
				Rotated_Bvec.output >> Streamlines_Tractography_Det.inputs ['BVEC_File']

			# Perform streamlines probabilistic tractography
			Streamlines_Tractography_Prob = network.create_node (fastr.toollist['tckgen'],id_='Tckgen_Prob',stepid='Track_MW_Fibers')
			Streamlines_Tractography_Prob.required_memory = '8g'
			Seeding_Mask.outputs ['mask_out'] >> Streamlines_Tractography_Prob.inputs ['seed_gmwmi']
			FiveTissues_Transform.outputs ['Output_File'] >> Streamlines_Tractography_Prob.inputs ['act']
			Min_tracking_Length.output >> Streamlines_Tractography_Prob.inputs ['minlength']
			Streamlines_Tractography_Link = network.create_link (Streamlines_Tractography_Source.output,Streamlines_Tractography_Prob.inputs ['select'])
			Streamlines_Tractography_Prob.inputs ['Algorithm'] = ['Tensor_Prob']
			if PreProcessing == True :
				Convert_Dwi_To_Mif.outputs ['Output_Mif_File'] >> Streamlines_Tractography_Prob.inputs ['Input_DWI']
				if Eddy_Correction_Type == 'eddy_openmp' :
					Eddy_Correction.outputs ['Rotated_Bvecs'] >> Streamlines_Tractography_Prob.inputs ['BVEC_File']
				elif Eddy_Correction_Type == 'eddy_correct' :
					Bval_Bvec_Files.outputs ['Out_BVEC_File'] >> Streamlines_Tractography_Prob.inputs ['BVEC_File']
				if Dicom2NiftiConvert == True :
					Dicom_convert.outputs['BVAL_File'] >>  Streamlines_Tractography_Prob.inputs ['BVAL_File']
				else :
					bval_Source.output >> Streamlines_Tractography_Prob.inputs ['BVAL_File']
				Mask_Formation.outputs['Mask_File'] >> Streamlines_Tractography_Prob.inputs ['mask_image']
				#Mask_Formation.outputs['Mask_File'] >> Streamlines_Tractography_Prob.inputs ['seed_image']
			elif Get_FA_Values == True :
				if Mask_In_GZ_From==True:
					Un_GZ_Mask.outputs ['Output_File'] >> Streamlines_Tractography_Prob.inputs ['mask_image']
				else:
					Mask_Formation.output >> Streamlines_Tractography_Prob.inputs ['mask_image']
				if Dicom2NiftiConvert == True :
					Dicom_convert.outputs['BVAL_File'] >>  Streamlines_Tractography_Prob.inputs ['BVAL_File']
				else :
					bval_Source.output >> Streamlines_Tractography_Prob.inputs ['BVAL_File']
				Convert_Dwi_To_Mif.outputs ['Output_Mif_File'] >> Streamlines_Tractography_Prob.inputs ['Input_DWI']
				Rotated_Bvec.output >> Streamlines_Tractography_Prob.inputs ['BVEC_File']
			else:
				if Mask_In_GZ_From==True :
					Un_GZ_Mask.outputs ['Output_File'] >> Streamlines_Tractography_Prob.inputs ['mask_image']
				else :
					Mask_Formation.output >> Streamlines_Tractography_Prob.inputs ['mask_image']
				if Dicom2NiftiConvert == True :
					Dicom_convert.outputs['BVAL_File'] >>  Streamlines_Tractography_Prob.inputs ['BVAL_File']
				else :
					bval_Source.output >> Streamlines_Tractography_Prob.inputs ['BVAL_File']
				Convert_Dwi_To_Mif.outputs ['Output_Mif_File'] >> Streamlines_Tractography_Prob.inputs ['Input_DWI']
				Rotated_Bvec.output >> Streamlines_Tractography_Prob.inputs ['BVEC_File']

		else : # Compute tracts with default sperical harmonics functions 
			# Reading the unique B values from the Bval file
			UniqueB_Values = network.create_node (fastr.toollist['bUniqueValues'],id_='B_Values_UniqueValues',stepid='Track_MW_Fibers')
			UniqueB_Values.required_memory = '8g'
			if Dicom2NiftiConvert == True : 
				Dicom_convert.outputs['BVAL_File'] >> UniqueB_Values.inputs ['BVAL_File'] 
			else :
				bval_Source.output >> UniqueB_Values.inputs ['BVAL_File'] 

			# Estimate response function(s) for spherical deconvolution
			Response_Functions = network.create_node (fastr.toollist['dwi2response_fsl/' + fsl_Version ],id_='Dwi2response',stepid='Track_MW_Fibers')
			Response_Functions.required_memory = '8g'
			Response_Functions_Source = network.create_source (fastr.typelist['AnyType'],id_='Dwiresponse_algorithm',stepid='dwi2response_Sources')
			Response_Functions_Link = network.create_link (Response_Functions_Source.output,Response_Functions.inputs['Algorithm'])
			UniqueB_Values.outputs['Return_String'] >> Response_Functions.inputs['shells']
			if 	PreProcessing == True : 
				Eddy_Correction.outputs ['DWI_Corrected_File'] >> Response_Functions.inputs['Input_DWI'] 
				if Eddy_Correction_Type == 'eddy_openmp' :
					Eddy_Correction.outputs ['Rotated_Bvecs'] >> Response_Functions.inputs['BVEC_File'] 
				elif Eddy_Correction_Type == 'eddy_correct' :
					Bval_Bvec_Files.outputs ['Out_BVEC_File'] >> Response_Functions.inputs['BVEC_File'] 
				Bval_Bvec_Files.outputs ['Out_BVAL_File'] >> Response_Functions.inputs['BVAL_File']
				Mask_Formation.outputs['Mask_File'] >> Response_Functions.inputs['mask']
			elif Get_FA_Values == True :
				Eddy_Correction.output   >> Response_Functions.inputs['Input_DWI'] 
				Rotated_Bvec.output >> Response_Functions.inputs['BVEC_File'] 
				bval_Source.output >> Response_Functions.inputs['BVAL_File']
				if Mask_In_GZ_From==True:
					Un_GZ_Mask.outputs ['Output_File'] >> Response_Functions.inputs['mask']
				else: 
					Mask_Formation.output >> Response_Functions.inputs['mask']
			else :
				if not ('Eddy_Correction' in locals()):
					Eddy_Correction = network.create_source (fastr.typelist['NiftiImageFile'],id_='Eddy_corrected_source',stepid='WM_Tracking_Sources')
					Eddy_Correction.required_memory = '8g'
				Eddy_Correction.output >> Response_Functions.inputs['Input_DWI'] 
				Rotated_Bvec = network.create_source (fastr.typelist['BVectorFile'],id_='Eddy_rotated_bvec_source',stepid='WM_Tracts_Sources')
				Rotated_Bvec.required_memory = '8g'
				Rotated_Bvec.output >> Response_Functions.inputs['BVEC_File'] 
				bval_Source.output >> Response_Functions.inputs['BVAL_File']
				if Mask_In_GZ_From==True:
					Un_GZ_Mask = network.create_node (fastr.toollist['Un_GZ_Nifti_File'],id_='Un_GZ_Nifti_File',stepid='UnZip_Files')
					Un_GZ_Mask.required_memory = '8g'
					Mask_Formation = network.create_source (fastr.typelist['NiftiImageFileCompressed'],id_='Mask_File_source',stepid='WM_Tracts_Sources')
					Mask_Formation.required_memory = '8g'
					Mask_Formation.output >> Un_GZ_Mask.inputs ['Input_File']
					Un_GZ_Mask.outputs ['Output_File'] >> Response_Functions.inputs['mask']
				else :
					Mask_Formation = network.create_source (fastr.typelist['NiftiImageFileUncompressed'],id_='Mask_File_source',stepid='WM_tracts_Sources')
					Mask_Formation.required_memory = '8g'
					Mask_Formation.output >> Response_Functions.inputs['mask']

			# Estimate fibre orientation distributions from diffusion data using spherical deconvolution
			Fibers_Orientation = network.create_node (fastr.toollist['dwi2fod'],id_='Dwi2fod',stepid='Track_MW_Fibers')
			Fibers_Orientation.required_memory = '8g' 
			UniqueB_Values.outputs['Return_String'] >> Fibers_Orientation.inputs['shells']
			Fibers_Orientation.inputs['Algorithm'] = ['msmt_csd']
			Response_Functions.outputs ['out_sfwm'] >> Fibers_Orientation.inputs['First_Response_Input']
			Response_Functions.outputs ['out_gm'] >> Fibers_Orientation.inputs['Second_Response_Input']
			Response_Functions.outputs ['out_csf'] >> Fibers_Orientation.inputs['Third_Response_Input']
			if PreProcessing == True : 
				Eddy_Correction.outputs ['DWI_Corrected_File'] >> Fibers_Orientation.inputs['Input_DWI']
				Mask_Formation.outputs['Mask_File'] >> Fibers_Orientation.inputs['mask'] 
				if Eddy_Correction_Type == 'eddy_openmp' :
					Eddy_Correction.outputs ['Rotated_Bvecs'] >> Fibers_Orientation.inputs['BVEC_File'] 
				elif Eddy_Correction_Type == 'eddy_correct' :
					Bval_Bvec_Files.outputs ['Out_BVEC_File'] >> Fibers_Orientation.inputs['BVEC_File']
				Bval_Bvec_Files.outputs ['Out_BVAL_File'] >> Fibers_Orientation.inputs['BVAL_File']
			elif Get_FA_Values == True :
				Eddy_Correction.output   >> Fibers_Orientation.inputs['Input_DWI']
				if Mask_In_GZ_From==True:
					Un_GZ_Mask.outputs ['Output_File'] >> Fibers_Orientation.inputs['mask']
				else:
					Mask_Formation.output >> Fibers_Orientation.inputs['mask']
				bval_Source.output >> Fibers_Orientation.inputs['BVAL_File']
				Rotated_Bvec.output >> Fibers_Orientation.inputs['BVEC_File']
			else :	
				Eddy_Correction.output >> Fibers_Orientation.inputs['Input_DWI']
				Rotated_Bvec.output >> Fibers_Orientation.inputs['BVEC_File']
				bval_Source.output >> Fibers_Orientation.inputs['BVAL_File'] 
				if Mask_In_GZ_From==True:
					Un_GZ_Mask.outputs ['Output_File'] >> Fibers_Orientation.inputs['mask']
				else:
					Mask_Formation.output >> Fibers_Orientation.inputs['mask']

			# Normalise the FOD files
			FOD_Normalization = network.create_node (fastr.toollist['mtnormalise'],id_='Mtnormalise',stepid='Track_MW_Fibers')
			FOD_Normalization.required_memory = '8g'
			Fibers_Orientation.outputs ['First_Output'] >> FOD_Normalization.inputs ['Input_WM']
			#Fibers_Orientation.outputs ['Second_Output'] >> FOD_Normalization.inputs ['Input_GM']
			#Fibers_Orientation.outputs ['Third_Output'] >> FOD_Normalization.inputs ['Input_CSF']
			if PreProcessing == True :
				Mask_Formation.outputs['Mask_File'] >> FOD_Normalization.inputs ['Input_Mask']
			elif Get_FA_Values == True :
				if Mask_In_GZ_From==True:
					Un_GZ_Mask.outputs ['Output_File'] >> FOD_Normalization.inputs ['Input_Mask']
				else:
					Mask_Formation.output >> FOD_Normalization.inputs ['Input_Mask']
			else:
				if Mask_In_GZ_From==True :
					Un_GZ_Mask.outputs ['Output_File'] >> FOD_Normalization.inputs ['Input_Mask']
				else :
					Mask_Formation.output >> FOD_Normalization.inputs ['Input_Mask']

			# Perform probabilistic streamlines tractography
			Streamlines_Tractography_Prob = network.create_node (fastr.toollist['tckgen'],id_='Tckgen_Prob',stepid='Track_MW_Fibers')
			Streamlines_Tractography_Prob.required_memory = '8g'
			Seeding_Mask.outputs ['mask_out'] >> Streamlines_Tractography_Prob.inputs ['seed_gmwmi']
			FiveTissues_Transform.outputs ['Output_File'] >> Streamlines_Tractography_Prob.inputs ['act']
			FOD_Normalization.outputs ['Normalized_WM'] >> Streamlines_Tractography_Prob.inputs ['Input_DWI']
			Min_tracking_Length = network.create_source (fastr.typelist['Float'],id_='Min_tracking_Length',stepid='Fiber_Tracking_sources')
			Min_tracking_Length.output >> Streamlines_Tractography_Prob.inputs ['minlength']
			Streamlines_Tractography_Source = network.create_source (fastr.typelist['Int'],id_='Tckgen_select',stepid='tckgen_Sources')
			Streamlines_Tractography_Link = network.create_link (Streamlines_Tractography_Source.output,Streamlines_Tractography_Prob.inputs ['select'])
			if PreProcessing == True : 
				Mask_Formation.outputs['Mask_File'] >> Streamlines_Tractography_Prob.inputs ['mask_image']
				#Mask_Formation.outputs['Mask_File'] >> Streamlines_Tractography_Prob.inputs ['seed_image']
			elif Get_FA_Values == True :
				if Mask_In_GZ_From==True:
					Un_GZ_Mask.outputs ['Output_File'] >> Streamlines_Tractography_Prob.inputs ['mask_image']
				else:
					Mask_Formation.output >> Streamlines_Tractography_Prob.inputs ['mask_image']
			else:
				if Mask_In_GZ_From==True :          
					Un_GZ_Mask.outputs ['Output_File'] >> Streamlines_Tractography_Prob.inputs ['mask_image']
				else :
					Mask_Formation.output >> Streamlines_Tractography_Prob.inputs ['mask_image']

			# Perform deterministic streamlines tractography
			Streamlines_Tractography_Det = network.create_node (fastr.toollist['tckgen'],id_='Tckgen_Det',stepid='Track_MW_Fibers')
			Streamlines_Tractography_Det.required_memory = '8g'
			FiveTissues_Transform.outputs ['Output_File'] >> Streamlines_Tractography_Det.inputs ['act']
			FOD_Normalization.outputs ['Normalized_WM'] >> Streamlines_Tractography_Det.inputs ['Input_DWI']
			#Fibers_Orientation.outputs ['First_Output'] >> Streamlines_Tractography_Det.inputs ['Input_DWI']
			Min_tracking_Length.output >> Streamlines_Tractography_Det.inputs ['minlength']
			Streamlines_Tractography_Link = network.create_link (Streamlines_Tractography_Source.output,Streamlines_Tractography_Det.inputs ['select'])
			Streamlines_Tractography_Det.inputs ['Algorithm'] = ['SD_STREAM']
			if PreProcessing == True :
				Mask_Formation.outputs['Mask_File'] >> Streamlines_Tractography_Det.inputs ['mask_image']
				Mask_Formation.outputs['Mask_File'] >> Streamlines_Tractography_Det.inputs ['seed_image']
			elif Get_FA_Values == True :
				if Mask_In_GZ_From==True:
					Un_GZ_Mask.outputs ['Output_File'] >> Streamlines_Tractography_Det.inputs ['mask_image']
					Un_GZ_Mask.outputs ['Output_File'] >> Streamlines_Tractography_Det.inputs ['seed_image']
				else:
					Mask_Formation.output >> Streamlines_Tractography_Det.inputs ['mask_image']
					Mask_Formation.output >> Streamlines_Tractography_Det.inputs ['seed_image']
			else:
				if Mask_In_GZ_From==True :
					Un_GZ_Mask.outputs ['Output_File'] >> Streamlines_Tractography_Det.inputs ['mask_image']
					Un_GZ_Mask.outputs ['Output_File'] >> Streamlines_Tractography_Det.inputs ['seed_image']
				else :
					Mask_Formation.output >> Streamlines_Tractography_Det.inputs ['mask_image']
					Mask_Formation.output >> Streamlines_Tractography_Det.inputs ['seed_image']

		if (Do_Fit == True) and (Use_Tensor_Model == False): # Filter the number of tracts to reduce harmonic fitting bias of overestimation 
			# Filter fibre-tracking dataset to match the streamline to the 5 tissue image for the probabilistic case
			MatchStreamlines_Prob = network.create_node (fastr.toollist['tcksift'],id_='Tcksift_prob',stepid='Track_MW_Fibers')
			MatchStreamlines_Prob.required_memory = '8g'
			Streamlines_Tractography_Prob.outputs ['tracks'] >> MatchStreamlines_Prob.inputs ['In_tracks']
			FOD_Normalization.outputs ['Normalized_WM'] >> MatchStreamlines_Prob.inputs ['In_pod']
			MatchStreamlines_Prob.inputs ['force']=['-force']
			FiveTissues_Transform.outputs ['Output_File'] >> MatchStreamlines_Prob.inputs ['act']
			Sift_Number_Of_Fibers = network.create_source (fastr.typelist['Int'],id_='Sift_Number_Of_Fibers',stepid='WM_Tracts_Sources')
			Sift_Number_Of_Fibers.output >> MatchStreamlines_Prob.inputs ['term_number']

			# Filter fibre-tracking dataset to match the streamline to the 5 tissue image for the deterministic case
			MatchStreamlines_Det = network.create_node (fastr.toollist['tcksift'],id_='Tcksift_det',stepid='Track_MW_Fibers')
			MatchStreamlines_Det.required_memory = '8g'
			Streamlines_Tractography_Det.outputs ['tracks'] >> MatchStreamlines_Det.inputs ['In_tracks']
			FOD_Normalization.outputs ['Normalized_WM'] >> MatchStreamlines_Det.inputs ['In_pod']
			MatchStreamlines_Det.inputs ['force']=['-force']
			FiveTissues_Transform.outputs ['Output_File'] >> MatchStreamlines_Det.inputs ['act']
			Sift_Number_Of_Fibers.output >> MatchStreamlines_Det.inputs ['term_number']

	# compute a connectivity map
	if Connectivity == True : 

		if ( Fivettgen_Algorithm == 'fsl' ):	
			# Convert the the freesurfer T1w in a standard space to nifti format
			Parcellation_Model_Brain_Convert = network.create_node (fastr.toollist['mrconvert_to_nifti'],id_='Parcellation_Brain_Convert',stepid='Connectivity_Preparations')
			Parcellation_Model_Brain_Convert.required_memory = '8g'
			if FreeSurfer == True :
				Parcellation_Atlas.outputs['Brain_File'] >> Parcellation_Model_Brain_Convert.inputs ['Input_File_Mgz']
			else:
				Freesurfer_Extracted_Brain.output >> Parcellation_Model_Brain_Convert.inputs ['Input_File_Mgz']

			# Register the freesurfer T1w in a standard space to the b=0 unweighed DWI
			Freesurfer_T1w_Registrations = network.create_node (fastr.toollist['flirt/' + fsl_Version ],id_='Freesurfer_T1w_Registrations',stepid='Connectivity_Preparations')
			Freesurfer_T1w_Registrations.required_memory = '8g'
			Freesurfer_T1w_Registrations.inputs ['interp'] = ['nearestneighbour']
			Freesurfer_T1w_Registrations.inputs ['dof'] = [6]
			Unweighted_Corrected_Diffusion_Mean.outputs ['output_image'] >> Freesurfer_T1w_Registrations.inputs ['in']
			Parcellation_Model_Brain_Convert.outputs ['Output_Nifti_File'] >> Freesurfer_T1w_Registrations.inputs ['ref']

			# Transforming the flirt transformation matrix to mrtrix txt file for the freesurfer T1w file
			Transform_Freesurfer_T1w_Flirt_Matrix = network.create_node (fastr.toollist['transformconvert'],id_='Freesurfer_T1w_Flirt_matrix_Transform',stepid='Connectivity_Preparations')
			Transform_Freesurfer_T1w_Flirt_Matrix.required_memory = '8g'
			Transform_Freesurfer_T1w_Flirt_Matrix.inputs ['operation'] = ['flirt_import']
			Unweighted_Corrected_Diffusion_Mean.outputs ['output_image'] >> Transform_Freesurfer_T1w_Flirt_Matrix.inputs ['Input_File']
			Parcellation_Model_Brain_Convert.outputs ['Output_Nifti_File'] >> Transform_Freesurfer_T1w_Flirt_Matrix.inputs ['Ref_File']
			Freesurfer_T1w_Registrations.outputs ['omat'] >> Transform_Freesurfer_T1w_Flirt_Matrix.inputs ['FSL_Mat_File']

		# Create an altenative parcelation atlas if necessary
		if (Alternative_Parcellation_Atlas == True) :
			if FreeSurfer == True :
				# Obtain the Freesurfer Subject Name
				Obtain_FreeSurfer_Subject_Name = network.create_node (fastr.toollist['FreeSurfer_Path_To_Subject_ID'],id_='Freesurfer_Subject_ID',stepid='Connectivity_Preparations')
				Obtain_FreeSurfer_Subject_Name.required_memory = '8g'
				if FreeSurfer == True :
					Parcellation_Atlas.outputs['Subject_Folder_Path'] >> Obtain_FreeSurfer_Subject_Name.inputs ['FreeSurfer_Directory']

			# Calculate annotation for the left hemisphere 
			Left_Hemisphere_Annotation = network.create_node (fastr.toollist['mri_surf2surf'],id_='Left_Hemisphere_Annotation',stepid='Connectivity_Preparations')
			Left_Hemisphere_Annotation.required_memory = '8g'
			Left_Hemisphere_Annotation.inputs ['hemi'] = ['lh']
			if FreeSurfer == True :
				Obtain_FreeSurfer_Subject_Name.outputs ['SUBJECTS_DIR'] >> Left_Hemisphere_Annotation.inputs ['SUBJECT_DIR'] 
				Obtain_FreeSurfer_Subject_Name.outputs ['Subject_ID'] >> Left_Hemisphere_Annotation.inputs ['trgsubject']
			else:
				FreeSurfer_SUBJECTS_DIR.output >> Left_Hemisphere_Annotation.inputs ['SUBJECT_DIR'] 
				FreeSurfer_Subject_ID.output >> Left_Hemisphere_Annotation.inputs ['trgsubject']		
			Left_Hemisphere_Annotation.inputs ['srcsubject'] = ['fsaverage']
			Parcellation_Atlas_Left_Hemisphere.output >> Left_Hemisphere_Annotation.inputs ['sval_annot']

			# Calculate annotation for the right hemisphere
			Right_Hemisphere_Annotation = network.create_node (fastr.toollist['mri_surf2surf'],id_='Right_Hemisphere_Annotation',stepid='Connectivity_Preparations')
			Right_Hemisphere_Annotation.required_memory = '8g'
			Right_Hemisphere_Annotation.inputs ['hemi'] = ['rh']
			if FreeSurfer == True :
				Obtain_FreeSurfer_Subject_Name.outputs ['SUBJECTS_DIR'] >> Right_Hemisphere_Annotation.inputs ['SUBJECT_DIR']
				Obtain_FreeSurfer_Subject_Name.outputs ['Subject_ID'] >> Right_Hemisphere_Annotation.inputs ['trgsubject']
			else:
				FreeSurfer_SUBJECTS_DIR.output >> Right_Hemisphere_Annotation.inputs ['SUBJECT_DIR']
				FreeSurfer_Subject_ID.output >> Right_Hemisphere_Annotation.inputs ['trgsubject']
			Right_Hemisphere_Annotation.inputs ['srcsubject'] = ['fsaverage']
			Parcellation_Atlas_Right_Hemisphere.output >> Right_Hemisphere_Annotation.inputs ['sval_annot']

			# Copy the two annotation files to a to the subject SUBJECTS_DIR/Subject_ID
			Copy_Subject_Annotation_File = network.create_node (fastr.toollist['SymLink_Annotation_Files'],id_='SymLink_Annotations_Files',stepid='Connectivity_Preparations')
			Copy_Subject_Annotation_File.required_memory = '8g'
			if FreeSurfer == True :
				Obtain_FreeSurfer_Subject_Name.outputs ['SUBJECTS_DIR'] >> Copy_Subject_Annotation_File.inputs ['SUBJECT_DIR']
				Obtain_FreeSurfer_Subject_Name.outputs ['Subject_ID'] >> Copy_Subject_Annotation_File.inputs ['Subject']
			else:
				FreeSurfer_SUBJECTS_DIR.output >> Copy_Subject_Annotation_File.inputs ['SUBJECT_DIR']
				FreeSurfer_Subject_ID.output >> Copy_Subject_Annotation_File.inputs ['Subject']
			Left_Hemisphere_Annotation.outputs ['AnnotationFile'] >> Copy_Subject_Annotation_File.inputs ['Left_Hemisphere_Annotation_File']
			Right_Hemisphere_Annotation.outputs ['AnnotationFile'] >> Copy_Subject_Annotation_File.inputs ['Right_Hemisphere_Annotation_File']
			Connectivity_Atlas_Name = network.create_source (fastr.typelist['String'],id_='Connectivity_Atlas_Name',stepid='Connectivity_Preparations_Sources')
			Connectivity_Atlas_Name.required_memory = '8g'
			Connectivity_Atlas_Name.output >> Copy_Subject_Annotation_File.inputs ['Atlas_Name']

			# Create the Annotation Atlas for the subject
			Parcellation_Atlas_alternative = network.create_node (fastr.toollist['mri_aparc2aseg'],id_='Parcellation_Atlas_alternative',stepid='Connectivity_Preparations')
			Parcellation_Atlas_alternative.required_memory = '8g'
			if FreeSurfer == True :
				Obtain_FreeSurfer_Subject_Name.outputs ['SUBJECTS_DIR'] >> Parcellation_Atlas_alternative.inputs ['SUBJECT_DIR']
				Obtain_FreeSurfer_Subject_Name.outputs ['Subject_ID'] >> Parcellation_Atlas_alternative.inputs ['subject']
			else:
				FreeSurfer_SUBJECTS_DIR.output >> Parcellation_Atlas_alternative.inputs ['SUBJECT_DIR']
				FreeSurfer_Subject_ID.output >> Parcellation_Atlas_alternative.inputs ['subject']
			Parcellation_Atlas_alternative.inputs ['old_ribbon'] = ['old-ribbon']
			Copy_Subject_Annotation_File.outputs ['Atlas_Name'] >> Parcellation_Atlas_alternative.inputs ['annot']

		# convert the transformed atlas to mif
		Parcellation_Brain_Convert_To_Mif = network.create_node (fastr.toollist['mrconvert_to_mif'],id_='Parcellation_Brain_Convert_To_Mif',stepid='Connectivity_Preparations')
		Parcellation_Brain_Convert_To_Mif.required_memory = '8g'
		if FreeSurfer == True :
			if Alternative_Parcellation_Atlas == True :
				Parcellation_Atlas_alternative.outputs ['Subject_Atlas_File'] >> Parcellation_Brain_Convert_To_Mif.inputs ['Input_File_Mgz']
			else: 
				Parcellation_Atlas.outputs['Cortical_Parcellation_Atlas'] >> Parcellation_Brain_Convert_To_Mif.inputs ['Input_File_Mgz']
		else:
			if Alternative_Parcellation_Atlas == True :
				Parcellation_Atlas_alternative.outputs ['Subject_Atlas_File'] >> Parcellation_Brain_Convert_To_Mif.inputs ['Input_File_Mgz']
			else : 
				Parcellation_Atlas.output >> Parcellation_Brain_Convert_To_Mif.inputs ['Input_File_Mgz']

		# Create a connectivity Atlas that is registered to the DWI file
		Connectivity_Atalas_Transform =  network.create_node (fastr.toollist['mrtransform'],id_='Connectivity_Atalas_Transform',stepid='Connectivity_Preparations')
		Connectivity_Atalas_Transform.required_memory = '8g'
		Parcellation_Brain_Convert_To_Mif.outputs ['Output_Mif_File'] >> Connectivity_Atalas_Transform.inputs ['Input_File']
		if ( Fivettgen_Algorithm == 'Freesurfer' ):
			Transform_T1w_Flirt_Matrix.outputs ['Output_Transformation'] >> Connectivity_Atalas_Transform.inputs ['linear']
		elif ( Fivettgen_Algorithm == 'fsl' ):
			Transform_Freesurfer_T1w_Flirt_Matrix.outputs ['Output_Transformation'] >> Connectivity_Atalas_Transform.inputs ['linear']
		Transform_T1w_Flirt_Matrix.outputs ['Output_Transformation'] >> Connectivity_Atalas_Transform.inputs ['linear']
		Connectivity_Atalas_Transform.inputs ['inverse_operation'] = ['inverse'] 
		Connectivity_Atalas_Transform.inputs ['datatype'] = ['uint32']

		# Convert a connectome node image from one lookup table to another
		Convert_LUT = network.create_node (fastr.toollist['labelconvert'],id_='Convert_Conn_Atlas_To_Mrtrix_LUT',stepid='Connectivity')
		Convert_LUT.required_memory = '8g'
		freesurfer_LUT = network.create_source (fastr.typelist['TxtFile'],id_='Freesurfer_LUT',stepid='Connectivity_Sources')
		freesurfer_LUT.required_memory = '8g'
		mrtrix_LUT = network.create_source (fastr.typelist['TxtFile'],id_='Mrtrix_LUT',stepid='Connectivity_Sources')
		mrtrix_LUT.required_memory = '8g'
		Convert_LUT.inputs ['force'] = ['-force']
		freesurfer_LUT.output >> Convert_LUT.inputs ['Input_LUT'] 
		mrtrix_LUT.output >> Convert_LUT.inputs ['Output_LUT']
		Connectivity_Atalas_Transform.outputs ['Output_File'] >> Convert_LUT.inputs ['Input_Image']

		# Create the actual connectivity map for the probabilistic case
		Connectivity_Map_Prob = network.create_node (fastr.toollist['tck2connectome'],id_='Tck2connectome_prob',stepid='Connectivity')
		Connectivity_Map_Prob.required_memory = '8g' 
		if (Do_Fit == True) and (Use_Tensor_Model == False): 
			MatchStreamlines_Prob.outputs ['out_tracks'] >> Connectivity_Map_Prob.inputs ['tracks_in']
		else : 
			Streamlines_Tractography_Prob.outputs ['tracks'] >> Connectivity_Map_Prob.inputs ['tracks_in']
		Convert_LUT.outputs ['parcels'] >> Connectivity_Map_Prob.inputs ['nodes_in']
		Connectivity_Map_Prob.inputs ['force'] = ['-force']
		Connectivity_Map_Prob.inputs ['symmetric'] = ['-symmetric']
		Max_Forward_Dist_Search = network.create_source (fastr.typelist['Int'],id_='Max_forward_Dist_Search',stepid='Connectivity')
		Max_Forward_Dist_Search.output >> Connectivity_Map_Prob.inputs ['assignment_forward_search']
		#Max_Reverse_Dist_Search = network.create_source (fastr.typelist['Int'],id_='Max_reverse_Dist_Search',stepid='Connectivity')
		#Max_Reverse_Dist_Search.output >> Connectivity_Map_Prob.inputs ['assignment_reverse_search'] 

		# Create the actual connectivity map for the deterministic case
                Connectivity_Map_Det = network.create_node (fastr.toollist['tck2connectome'],id_='Tck2connectome_det',stepid='Connectivity')
                Connectivity_Map_Det.required_memory = '8g'
                if (Do_Fit == True) and (Use_Tensor_Model == False):
                        MatchStreamlines_Det.outputs ['out_tracks'] >> Connectivity_Map_Det.inputs ['tracks_in']
                else : 
                        Streamlines_Tractography_Det.outputs ['tracks'] >> Connectivity_Map_Det.inputs ['tracks_in']
                Convert_LUT.outputs ['parcels'] >> Connectivity_Map_Det.inputs ['nodes_in']
                Connectivity_Map_Det.inputs ['force'] = ['-force']
                Connectivity_Map_Det.inputs ['symmetric'] = ['-symmetric']
                Max_Forward_Dist_Search.output >> Connectivity_Map_Det.inputs ['assignment_forward_search']
		#Max_Reverse_Dist_Search.output >> Connectivity_Map_Det.inputs ['assignment_reverse_search']

		# create sample values of an associated image along tracks for the deterministic case
		Connectivity_Sample_Map_Det = network.create_node (fastr.toollist['tcksample'],id_='Tcksample_det',stepid='Connectivity')
		Connectivity_Sample_Map_Det.required_memory = '8g'
		TckSample_Method = network.create_source (fastr.typelist['AnyType'],id_='Tcksample_Method',stepid='Connectivity_Sources')
		TckSample_Method_Link = network.create_link (TckSample_Method.output, Connectivity_Sample_Map_Det.inputs['stat_tck'])
		if (Do_Fit == True) and (Use_Tensor_Model == False) :
			MatchStreamlines_Det.outputs ['out_tracks'] >> Connectivity_Sample_Map_Det.inputs ['tracks']
		else :
			Streamlines_Tractography_Det.outputs ['tracks'] >> Connectivity_Sample_Map_Det.inputs ['tracks']
		if Get_FA_Values == True :
			Simple_FA_MD.outputs['FA'] >> Connectivity_Sample_Map_Det.inputs['image']
		else: 
			# Create skeleton mask
			Simple_FA_MD = network.create_node (fastr.toollist['dtifit/' + fsl_Version ],id_='Dtifit',stepid='Compute_FA_Values')
			Simple_FA_MD.required_memory = '8g'
			if PreProcessing == True :
				Eddy_Correction.outputs ['DWI_Corrected_File'] >> Simple_FA_MD.inputs ['DWI_File']
				Mask_Formation.outputs['Mask_File'] >> Simple_FA_MD.inputs ['Mask']
				if Eddy_Correction_Type == 'eddy_openmp' :
					Eddy_Correction.outputs ['Rotated_Bvecs'] >> Simple_FA_MD.inputs ['bvec']
				elif Eddy_Correction_Type == 'eddy_correct' :
					Bval_Bvec_Files.outputs ['Out_BVEC_File'] >> Simple_FA_MD.inputs ['bvec']
				if Dicom2NiftiConvert == True :
					Dicom_convert.outputs['BVAL_File'] >>  Simple_FA_MD.inputs ['bval']
				else :
					bval_Source.output >> Simple_FA_MD.inputs ['bval']
			elif Get_FA_Values == True :
				Eddy_Correction.output >> Simple_FA_MD.inputs ['DWI_File']
				Rotated_Bvec.output >> Simple_FA_MD.inputs ['bvec']
				bval_Source.output >> Simple_FA_MD.inputs ['bval']
				if Mask_In_GZ_From==True:
					Un_GZ_Mask.outputs ['Output_File'] >> Simple_FA_MD.inputs ['Mask']
				else:
					Mask_Formation.output >> Simple_FA_MD.inputs ['Mask']
			else:
				if Mask_In_GZ_From==True :
					Un_GZ_Mask.outputs ['Output_File'] >> Simple_FA_MD.inputs ['Mask']
				else :
					Mask_Formation.output >> Simple_FA_MD.inputs ['Mask']
				if Dicom2NiftiConvert == True :
					Dicom_convert.outputs['BVAL_File'] >> Simple_FA_MD.inputs ['bval']
				else :
					bval_Source.output >> Simple_FA_MD.inputs ['bval']
				Eddy_Correction.output >> Simple_FA_MD.inputs ['DWI_File']
				Rotated_Bvec.output >> Simple_FA_MD.inputs ['bvec']
			Simple_FA_MD.outputs['FA'] >> Connectivity_Sample_Map_Det.inputs['image']

		# create sample values of an associated image along tracks for the probabilisitc case
		Connectivity_Sample_Map_Prob = network.create_node (fastr.toollist['tcksample'],id_='Tcksample_prob',stepid='Connectivity')
		Connectivity_Sample_Map_Prob.required_memory = '8g'
		TckSample_Method_Link = network.create_link (TckSample_Method.output, Connectivity_Sample_Map_Prob.inputs['stat_tck'])
		if (Do_Fit == True) and (Use_Tensor_Model == False) :
			MatchStreamlines_Prob.outputs ['out_tracks'] >> Connectivity_Sample_Map_Prob.inputs ['tracks']
		else :
			Streamlines_Tractography_Prob.outputs ['tracks'] >> Connectivity_Sample_Map_Prob.inputs ['tracks']
		if Get_FA_Values == True :
			Simple_FA_MD.outputs['FA'] >> Connectivity_Sample_Map_Prob.inputs['image']
		else:
			Simple_FA_MD.outputs['FA'] >> Connectivity_Sample_Map_Prob.inputs['image']

		# Create mean connectivity map for the deterministic case
		Connectivity_Mean_Map_Det = network.create_node (fastr.toollist['tck2connectome'],id_='Tck2connectome_mean_det',stepid='Connectivity')	
		Connectivity_Mean_Map_Det.required_memory = '8g' 
		if (Do_Fit == True) and (Use_Tensor_Model == False):
			MatchStreamlines_Det.outputs ['out_tracks'] >> Connectivity_Mean_Map_Det.inputs ['tracks_in']
		else : 
			Streamlines_Tractography_Det.outputs ['tracks'] >> Connectivity_Mean_Map_Det.inputs ['tracks_in']
		Convert_LUT.outputs ['parcels'] >> Connectivity_Mean_Map_Det.inputs ['nodes_in']
		Connectivity_Sample_Map_Det.outputs ['values'] >> Connectivity_Mean_Map_Det.inputs ['scale_file']
		Connectivity_Mean_Map_Det.inputs ['force'] = ['-force']
		Connectivity_Mean_Map_Det.inputs ['symmetric'] = ['-symmetric']
		Max_Forward_Dist_Search.output >> Connectivity_Mean_Map_Det.inputs ['assignment_forward_search']
		#Max_Reverse_Dist_Search.output >> Connectivity_Mean_Map_Det.inputs ['assignment_reverse_search']

		Connectivity_Mean_Map_Det.inputs ['stat_edge'] = ['mean']

		 # Create mean connectivity map for the probabilistic case
		Connectivity_Mean_Map_Prob = network.create_node (fastr.toollist['tck2connectome'],id_='Tck2connectome_mean_prob',stepid='Connectivity')
		Connectivity_Mean_Map_Prob.required_memory = '8g'
		if (Do_Fit == True) and (Use_Tensor_Model == False):
			MatchStreamlines_Prob.outputs ['out_tracks'] >> Connectivity_Mean_Map_Prob.inputs ['tracks_in']
		else :
			Streamlines_Tractography_Prob.outputs ['tracks'] >> Connectivity_Mean_Map_Prob.inputs ['tracks_in']
		Convert_LUT.outputs ['parcels'] >> Connectivity_Mean_Map_Prob.inputs ['nodes_in']
		Connectivity_Sample_Map_Prob.outputs ['values'] >> Connectivity_Mean_Map_Prob.inputs ['scale_file']
		Connectivity_Mean_Map_Prob.inputs ['force'] = ['-force']
		Connectivity_Mean_Map_Prob.inputs ['symmetric'] = ['-symmetric']
		Max_Forward_Dist_Search.output >> Connectivity_Mean_Map_Prob.inputs ['assignment_forward_search']
		#Max_Reverse_Dist_Search.output >> Connectivity_Mean_Map_Det.inputs ['assignment_reverse_search']
		Connectivity_Mean_Map_Prob.inputs ['stat_edge'] = ['mean']

		# Calculate network measures of the connectivity matrix for the deterministic case
		Connectivity_Map_Stat_Det = network.create_node (fastr.toollist['Connectivity_Matrix_Analysis'],id_='Connectivity_Matrix_Analysis_Det',stepid='Connectivity')
		Connectivity_Map_Stat_Det.required_memory = '8g'
		Connectivity_Map_Det.outputs ['connectome_out'] >> Connectivity_Map_Stat_Det.inputs ['Input_File']

		# Calculate network measures of the connectivity matrix for the deterministic case
		Connectivity_Map_Stat_Prob = network.create_node (fastr.toollist['Connectivity_Matrix_Analysis'],id_='Connectivity_Matrix_Analysis_Prob',stepid='Connectivity')
		Connectivity_Map_Stat_Prob.required_memory = '8g'
		Connectivity_Map_Prob.outputs ['connectome_out'] >> Connectivity_Map_Stat_Prob.inputs ['Input_File']

		# Calculate network measures of the connectivity matrix for the deterministic case for the mean connectivity
		Mean_Connectivity_Map_Stat_Det = network.create_node (fastr.toollist['Connectivity_Matrix_Analysis'],id_='Mean_Connectivity_Matrix_Analysis_Det',stepid='Connectivity')
		Mean_Connectivity_Map_Stat_Det.required_memory = '8g'
		Mean_Connectivity_Map_Stat_Det.required_time = '18000'
		Connectivity_Mean_Map_Det.outputs ['connectome_out'] >> Mean_Connectivity_Map_Stat_Det.inputs['Input_File']

		# Calculate network measures of the connectivity matrix for the probabilistic case for the mean connectivity
		Mean_Connectivity_Map_Stat_Prob = network.create_node (fastr.toollist['Connectivity_Matrix_Analysis'],id_='Mean_Connectivity_Matrix_Analysis_Prob',stepid='Connectivity')
		Mean_Connectivity_Map_Stat_Prob.required_memory = '8g'
		Mean_Connectivity_Map_Stat_Prob.required_time = '18000'
		Connectivity_Mean_Map_Prob.outputs ['connectome_out'] >> Mean_Connectivity_Map_Stat_Prob.inputs['Input_File']

		# Create a pdf image of the connectivity matrix for the deterministic case 
		Connectivity_Map_Image_Det = network.create_node (fastr.toollist['Connectivity_Matrix_To_Pdf'],id_='Connectivity_Matrix_Image_Det',stepid='Connectivity')
		Connectivity_Map_Image_Det.required_memory = '8g'
		Connectivity_Mean_Map_Det.outputs ['connectome_out'] >> Connectivity_Map_Image_Det.inputs['Connectivity_Matrix']

		# Create a pdf image of the connectivity matrix for the probabilistic case 
		Connectivity_Map_Image_Prob = network.create_node (fastr.toollist['Connectivity_Matrix_To_Pdf'],id_='Connectivity_Matrix_Image_Prob',stepid='Connectivity')
		Connectivity_Map_Image_Prob.required_memory = '8g'
		Connectivity_Mean_Map_Prob.outputs ['connectome_out'] >> Connectivity_Map_Image_Prob.inputs['Connectivity_Matrix']

		# Create a pdf image of the connectivity matrix for the deterministic case for the mean connectivity
		Mean_Connectivity_Map_Image_Det = network.create_node (fastr.toollist['Connectivity_Matrix_To_Pdf'],id_='Mean_Connectivity_Matrix_Image_Det',stepid='Connectivity')
		Mean_Connectivity_Map_Image_Det.required_memory = '8g'
		Connectivity_Mean_Map_Det.outputs ['connectome_out'] >> Mean_Connectivity_Map_Image_Det.inputs['Connectivity_Matrix']

		# Create a pdf image of the connectivity matrix for the probabilistic case for the mean connectivity
		Mean_Connectivity_Map_Image_Prob = network.create_node (fastr.toollist['Connectivity_Matrix_To_Pdf'],id_='Mean_Connectivity_Matrix_Image_Prob',stepid='Connectivity')
		Mean_Connectivity_Map_Image_Prob.required_memory = '8g'
		Connectivity_Mean_Map_Prob.outputs ['connectome_out'] >> Mean_Connectivity_Map_Image_Prob.inputs['Connectivity_Matrix']

	# Gzip the tck files if needed
	if (FiberTracking == True) and (Keep_Fibers == True):

		# Convert the deterministic tck image to vtk image
		#Vtk_Streamlines_Image_Det = network.create_node (fastr.toollist['tckconvert'],id_='Tckconvert_det',stepid='Track_MW_Fibers')
		#if (Do_Fit == True) and (Use_Tensor_Model == False) :
		# 	MatchStreamlines_Det ['out_tracks'] >> Vtk_Streamlines_Image_Det.inputs ['Input_File']
		#else: 
		# 	Streamlines_Tractography_Det.outputs ['tracks'] >> Vtk_Streamlines_Image_Det.inputs ['Input_File']
		#Vtk_Streamlines_Image_Det.required_memory = '8g'

		# gzip the deterministic tck image
		Compressed_Fibers_Streamlines_Image_Det = network.create_node (fastr.toollist['GZIP_File_Keep_Original'],id_='GZ_Fibers_Det',stepid='Track_MW_Fibers')
		Compressed_Fibers_Streamlines_Image_Det.required_memory = '8g'
		#Vtk_Streamlines_Image_Det.outputs ['Output_File_Vtk'] >> Compressed_Fibers_Streamlines_Image_Det.inputs['Input_File']
		if (Do_Fit == True) and (Use_Tensor_Model == False) :
			MatchStreamlines_Det ['out_tracks'] >> Compressed_Fibers_Streamlines_Image_Det.inputs['Input_File']
		else:
			Streamlines_Tractography_Det.outputs ['tracks'] >> Compressed_Fibers_Streamlines_Image_Det.inputs['Input_File']

		# Convert the probabilistic tck image to vtk image
		#Vtk_Streamlines_Image_Prob = network.create_node (fastr.toollist['tckconvert'],id_='Tckconvert_prob',stepid='Track_MW_Fibers')
		#if (Do_Fit == True) and (Use_Tensor_Model == False) :
		#	MatchStreamlines_Prob ['out_tracks'] >> Vtk_Streamlines_Image_Det.inputs ['Input_File']
		#else: 
		#	Streamlines_Tractography_Prob.outputs ['tracks'] >> Vtk_Streamlines_Image_Prob.inputs ['Input_File']
		#Vtk_Streamlines_Image_Prob.required_memory = '8g'

		# gzip the Probabilistic vtk image
		Compressed_Fibers_Streamlines_Image_Prob = network.create_node (fastr.toollist['GZIP_File_Keep_Original'],id_='GZ_Fibers_Prob',stepid='Track_MW_Fibers')
		Compressed_Fibers_Streamlines_Image_Prob.required_memory = '8g'
		#Vtk_Streamlines_Image_Prob.outputs ['Output_File_Vtk'] >> Compressed_Fibers_Streamlines_Image_Prob.inputs['Input_File']
		if (Do_Fit == True) and (Use_Tensor_Model == False) :
			MatchStreamlines_Prob ['out_tracks'] >> Compressed_Fibers_Streamlines_Image_Prob.inputs['Input_File']
		else: 
			Streamlines_Tractography_Prob.outputs ['tracks'] >> Compressed_Fibers_Streamlines_Image_Prob.inputs['Input_File']

	# Creating the sinks and linking them

	if (Keep_Original == True) and (Dicom2NiftiConvert == True): # sinks of the original image if it was converted. 
		Original_NIFTII =  network.create_sink (fastr.typelist['NiftiImageFile'],id_='Original_Niftii',stepid='Original_Sinks')
		Original_NIFTII.required_memory = '8g'
		Dicom_convert.outputs['DWI_File'] >> Original_NIFTII.input
		Original_Bval =  network.create_sink (fastr.typelist['BValueFile'],id_='Original_Bval',stepid='Original_Sinks')
		Original_Bval.required_memory = '8g'	
		Dicom_convert.outputs['BVAL_File'] >> Original_Bval.input	
		Original_Bvec =  network.create_sink (fastr.typelist['BVectorFile'],id_='Original_Bvec',stepid='Original_Sinks')	
		Original_Bvec.required_memory = '8g'	
		Dicom_convert.outputs['BVEC_File'] >> Original_Bvec.input
	
	if PreProcessing == True : # Sinks for the Preprocessing part
		Eddy_Output = network.create_sink (fastr.typelist['NiftiImageFile'],id_='DWI_Corrected_File',stepid='PreProcessing_Sinks')
		Eddy_Output.required_memory = '8g' 
		Eddy_Correction.outputs ['DWI_Corrected_File'] >> Eddy_Output.input
		Eddy_Rotated_Bvec = network.create_sink (fastr.typelist['Eddy_BVectorFile'],id_='Rotated_Bvec',stepid='PreProcessing_Sinks')
		Eddy_Rotated_Bvec.required_memory = '8g'
		Eddy_Correction.outputs ['Rotated_Bvecs'] >> Eddy_Rotated_Bvec.input
		Mask_File = network.create_sink (fastr.typelist['NiftiImageFile'],id_='Mask_Image',stepid='PreProcessing_Sinks')
		Mask_File.required_memory = '8g'
		Mask_Formation.outputs['Mask_File'] >> Mask_File.input
		if Eddy_Correction_Type == 'eddy_openmp' :
			Movement_RMS = network.create_sink (fastr.typelist['UnknownFile'],id_='Movement_RMS',stepid='PreProcessing_Sinks')
			Movement_RMS.required_memory = '8g'
			Eddy_Correction.outputs ['Movement_RMS'] >> Movement_RMS.input
			Outlier_Map = network.create_sink (fastr.typelist['UnknownFile'],id_='Outlier_Map',stepid='PreProcessing_Sinks')
			Outlier_Map.required_memory = '8g'
			Eddy_Correction.outputs ['Outlier_Map'] >> Outlier_Map.input
			Outlier_N_Stdev_Map = network.create_sink (fastr.typelist['UnknownFile'],id_='Outlier_N_Stdev_Map',stepid='PreProcessing_Sinks')
			Outlier_N_Stdev_Map.required_memory = '8g'
			Eddy_Correction.outputs ['Outlier_N_Stdev_Map'] >> Outlier_N_Stdev_Map.input
			Outlier_Report = network.create_sink (fastr.typelist['UnknownFile'],id_='Outlier_Report',stepid='PreProcessing_Sinks')
			Outlier_Report.required_memory = '8g'
			Eddy_Correction.outputs ['Outlier_Report'] >> Outlier_Report.input
			Parameters = network.create_sink (fastr.typelist['UnknownFile'],id_='Parameters',stepid='PreProcessing_Sinks')
			Parameters.required_memory = '8g'
			Eddy_Correction.outputs ['Parameters'] >> Parameters.input
			Post_Eddy_Shell_Alignment_Parameters = network.create_sink (fastr.typelist['UnknownFile'],id_='Post_Eddy_Shell_Alignment_Parameters',stepid='PreProcessing_Sinks')
			Post_Eddy_Shell_Alignment_Parameters.required_memory = '8g'
			Eddy_Correction.outputs ['Post_Eddy_Shell_Alignment_Parameters'] >> Post_Eddy_Shell_Alignment_Parameters.input

	if Get_FA_Values == True : # sinks for the FA values part
		FA_Output_Skeleton = network.create_sink (fastr.typelist['TxtFile'],id_='FA_Biomarker_Skeleton',stepid='Diffusivity_Sinks')
		FA_Output_Skeleton.required_memory = '8g'	
		Final_FA_Values_File.outputs ['Out_Diffusivity_Values_File'] >> FA_Output_Skeleton.input
		FA_Output = network.create_sink (fastr.typelist['TxtFile'],id_='FA_Biomarker',stepid='Diffusivity_Sinks')
		FA_Output.required_memory = '8g'
		Final_FA_Full_Values_File.outputs ['Out_Diffusivity_Values_File'] >> FA_Output.input
		Mean_FA_Skeleton =network.create_sink (fastr.typelist['NiftiImageFile'],id_='Mean_FA_Skeleton',stepid='Diffusivity_Sinks')
		Mean_FA_Skeleton.required_memory = '8g'
		TBSS_Analysis.outputs['mean_FA_skeleton'] >> Mean_FA_Skeleton.input
		All_FA_Skeleton =network.create_sink (fastr.typelist['NiftiImageFile'],id_='All_FA_Skeleton',stepid='Diffusivity_Sinks')
		All_FA_Skeleton.required_memory = '8g'
		TBSS_Analysis.outputs['all_FA_skeletonised'] >> All_FA_Skeleton.input
		Mean_FA =network.create_sink (fastr.typelist['NiftiImageFile'],id_='Mean_FA',stepid='Diffusivity_Sinks')
		Mean_FA.required_memory = '8g'
		TBSS_Analysis.outputs['mean_FA'] >> Mean_FA.input
		All_FA =network.create_sink (fastr.typelist['NiftiImageFile'],id_='All_FA',stepid='Diffusivity_Sinks')
		All_FA.required_memory = '8g'
		TBSS_Analysis.outputs['all_FA'] >> All_FA.input
		T1w_Composite_Output = network.create_sink (fastr.typelist['PNGFile'],id_='T1w_Composite_Figure',stepid='Diffusivity_Sinks')
		T1w_Composite_Output.required_memory = '8g'
		T1w_Composite.outputs ['Output_File'] >> T1w_Composite_Output.input
		DWI_Composite_Output = network.create_sink (fastr.typelist['PNGFile'],id_='DWI_Composite_Figure',stepid='Diffusivity_Sinks')
		DWI_Composite_Output.required_memory = '8g'
		DWI_Composite.outputs ['Output_File'] >> DWI_Composite_Output.input

	if (Get_Diffusivity_Values == True) and (Get_FA_Values == True) :  # sinks the other diffusivity values (Mean diffusivity, Axial Diffusivity Radial Diffusivity)
		# Mean Diffusivity
		MD_Output = network.create_sink (fastr.typelist['TxtFile'],id_='MD_Biomarker_Skeleton',stepid='Other_Diffusivity_Sinks')
		MD_Output.required_memory = '8g'
		Final_MD_Values_File.outputs ['Out_Diffusivity_Values_File'] >> MD_Output.input
		All_MD_Skeleton = network.create_sink (fastr.typelist['NiftiImageFile'], id_='All_MD_Skeleton', stepid='Other_Diffusivity_Sinks')
		All_MD_Skeleton.required_memory = '8g'
		MD_tbss.outputs['Output_Image'] >> All_MD_Skeleton.input
		MD_Output_Full = network.create_sink (fastr.typelist['TxtFile'],id_='MD_Biomarker',stepid='Other_Diffusivity_Sinks')
		MD_Output_Full.required_memory = '8g'
		Final_MD_Full_Values_File.outputs ['Out_Diffusivity_Values_File'] >> MD_Output_Full.input
		All_MD = network.create_sink (fastr.typelist['NiftiImageFile'], id_='All_MD', stepid='Other_Diffusivity_Sinks')
		All_MD.required_memory = '8g'
		MD_Wrap.outputs ['warped_image'] >> All_MD.input
		# Radial Diffusivity
		RD_Output = network.create_sink (fastr.typelist['TxtFile'],id_='RD_Biomarker_Skeleton',stepid='Other_Diffusivity_Sinks')
		RD_Output.required_memory = '8g'
		Final_RD_Values_File.outputs ['Out_Diffusivity_Values_File'] >> RD_Output.input
		All_RD_Skeleton =network.create_sink (fastr.typelist['NiftiImageFile'],id_='All_RD_Skeleton',stepid='Other_Diffusivity_Sinks')
		All_RD_Skeleton.required_memory = '8g'
		RD_tbss.outputs['Output_Image'] >> All_RD_Skeleton.input
		RD_Output_Full = network.create_sink (fastr.typelist['TxtFile'],id_='RD_Biomarker',stepid='Other_Diffusivity_Sinks')
		RD_Output_Full.required_memory = '8g'
		Final_RD_Full_Values_File.outputs ['Out_Diffusivity_Values_File'] >> RD_Output_Full.input
		All_RD = network.create_sink (fastr.typelist['NiftiImageFile'], id_='All_RD', stepid='Other_Diffusivity_Sinks')
		All_RD.required_memory = '8g'
		RD_Wrap.outputs ['warped_image'] >> All_RD.input
		# Axial Diffusivity
		AD_Output = network.create_sink (fastr.typelist['TxtFile'],id_='AD_Biomarker_Skeleton',stepid='Other_Diffusivity_Sinks')
		AD_Output.required_memory = '8g'
		Final_AD_Values_File.outputs ['Out_Diffusivity_Values_File'] >> AD_Output.input
		All_AD_Skeleton =network.create_sink (fastr.typelist['NiftiImageFile'],id_='All_AD_Skeleton',stepid='Other_Diffusivity_Sinks')
		All_AD_Skeleton.required_memory = '8g'
		AD_tbss.outputs['Output_Image'] >> All_AD_Skeleton.input
		AD_Output_Full = network.create_sink (fastr.typelist['TxtFile'],id_='AD_Biomarker',stepid='Other_Diffusivity_Sinks')
		AD_Output_Full.required_memory = '8g'
		Final_AD_Full_Values_File.outputs ['Out_Diffusivity_Values_File'] >> AD_Output_Full.input
		All_AD = network.create_sink (fastr.typelist['NiftiImageFile'], id_='All_AD', stepid='Other_Diffusivity_Sinks')
		All_AD.required_memory = '8g'
		AD_Wrap.outputs ['warped_image'] >> All_AD.input

	if (FiberTracking == True) : # sinks for the Fiber tracking part 
		Fiber_Tracking_Seeding_Image = network.create_sink (fastr.typelist['PNGFile'],id_='Fiber_Tracking_Seeding_Image',stepid='FiberTracking_Sinks')
		Fiber_Tracking_Seeding_Image.required_memory = '8g'
		StreamLines_Seed_Composite.outputs ['Output_File'] >> Fiber_Tracking_Seeding_Image.input
		Gmwmi_Scan_Output = network.create_sink (fastr.typelist['NiftiImageFile'],id_='Gmwmi_Seeding',stepid='FiberTracking_Sinks')
		Gmwmi_Scan_Output.required_memory = '8g'
		Gmwmi_To_Nifti.outputs ['Output_Nifti_File'] >> Gmwmi_Scan_Output.input

		if (Keep_Fibers == True) : 
			Fiber_Tracking_Image_Det = network.create_sink (fastr.typelist['CompressedTck_Image'],id_='Fiber_Tracking_Image_Det',stepid='FiberTracking_Sinks')
			Fiber_Tracking_Image_Det.required_memory = '8g'
			Compressed_Fibers_Streamlines_Image_Det.outputs['Output_File'] >> Fiber_Tracking_Image_Det.input

			Fiber_Tracking_Image_Prob = network.create_sink (fastr.typelist['CompressedTck_Image'],id_='Fiber_Tracking_Image_Prob',stepid='FiberTracking_Sinks')
			Fiber_Tracking_Image_Prob.required_memory = '8g'
			Compressed_Fibers_Streamlines_Image_Prob.outputs['Output_File'] >> Fiber_Tracking_Image_Prob.input

	if Connectivity == True : # sinks for the connectivity part
		Connectivity_Det =  network.create_sink (fastr.typelist['CsvFile'],id_='Connectivity_Matrix_Det',stepid='Connectivity_Sinks')
		Connectivity_Det.required_memory = '8g'
		Connectivity_Map_Det.outputs ['connectome_out'] >> Connectivity_Det.input
		Mean_Connectivity_Det =  network.create_sink (fastr.typelist['CsvFile'],id_='Mean_Connectivity_Matrix_Det',stepid='Connectivity_Sinks')	
		Mean_Connectivity_Det.required_memory = '8g'
		Connectivity_Mean_Map_Det.outputs ['connectome_out'] >> Mean_Connectivity_Det.input

		Connectivity_Stat_Det =  network.create_sink (fastr.typelist['TxtFile'],id_='Connectivity_Matrix_Det_Stat',stepid='Connectivity_Sinks')
                Connectivity_Stat_Det.required_memory = '8g'
		Connectivity_Map_Stat_Det.outputs ['Output_File'] >> Connectivity_Stat_Det.input
		Mean_Connectivity_Stat_Det =  network.create_sink (fastr.typelist['TxtFile'],id_='Mean_Connectivity_Matrix_Det_Stat',stepid='Connectivity_Sinks')
                Mean_Connectivity_Stat_Det.required_memory = '8g'
                Mean_Connectivity_Map_Stat_Det.outputs['Output_File'] >> Mean_Connectivity_Stat_Det.input

		Connectivity_Det_Image =  network.create_sink (fastr.typelist['PdfFile'],id_='Connectivity_Matrix_Det_Image',stepid='Connectivity_Sinks')
		Connectivity_Det_Image.required_memory = '8g'
		Connectivity_Map_Image_Det.outputs ['Connectivity_Plot'] >> Connectivity_Det_Image.input
		Mean_Connectivity_Det_Image =  network.create_sink (fastr.typelist['PdfFile'],id_='Mean_Connectivity_Matrix_Det_Image',stepid='Connectivity_Sinks')
		Mean_Connectivity_Det_Image.required_memory = '8g'
		Mean_Connectivity_Map_Image_Det.outputs ['Connectivity_Plot'] >> Mean_Connectivity_Det_Image.input

		Connectivity_Prob =  network.create_sink (fastr.typelist['CsvFile'],id_='Connectivity_Matrix_Prob',stepid='Connectivity_Sinks')
		Connectivity_Prob.required_memory = '8g'
		Connectivity_Map_Prob.outputs ['connectome_out'] >> Connectivity_Prob.input
		Mean_Connectivity_Prob =  network.create_sink (fastr.typelist['CsvFile'],id_='Mean_Connectivity_Matrix_Prob',stepid='Connectivity_Sinks')
		Mean_Connectivity_Prob.required_memory = '8g'
		Connectivity_Mean_Map_Prob.outputs ['connectome_out'] >> Mean_Connectivity_Prob.input

		Connectivity_Stat_Prob =  network.create_sink (fastr.typelist['TxtFile'],id_='Connectivity_Matrix_Prob_Stat',stepid='Connectivity_Sinks')
		Connectivity_Stat_Prob.required_memory = '8g'
		Connectivity_Map_Stat_Prob.outputs ['Output_File'] >> Connectivity_Stat_Prob.input
		Mean_Connectivity_Stat_Prob =  network.create_sink (fastr.typelist['TxtFile'],id_='Mean_Connectivity_Matrix_Prob_Stat',stepid='Connectivity_Sinks')
		Mean_Connectivity_Stat_Prob.required_memory = '8g'
		Mean_Connectivity_Map_Stat_Prob.outputs['Output_File'] >> Mean_Connectivity_Stat_Prob.input

		Connectivity_Prob_Image =  network.create_sink (fastr.typelist['PdfFile'],id_='Connectivity_Matrix_Prob_Image',stepid='Connectivity_Sinks')
		Connectivity_Prob_Image.required_memory = '8g'
		Connectivity_Map_Image_Prob.outputs ['Connectivity_Plot'] >> Connectivity_Prob_Image.input
		Mean_Connectivity_Prob_Image =  network.create_sink (fastr.typelist['PdfFile'],id_='Mean_Connectivity_Matrix_Prob_Image',stepid='Connectivity_Sinks')
		Mean_Connectivity_Prob_Image.required_memory = '8g'
		Mean_Connectivity_Map_Image_Prob.outputs ['Connectivity_Plot'] >> Mean_Connectivity_Prob_Image.input

	if not network.is_valid():
		print  ('Network is not valid')
		return None

	return network


def JSON_Read_Arguments (dict_file, experiments):

	with open(dict_file) as input_file:
		data = json.load(input_file)

	JSON_sources = {'T1w_Source': {}, 'DWI_Source': {} , 'Subject_Name': {}, 'Experiments': {}, 'bval_Source': {}, 'bvec_Source': {}, 'Eddy_corrected_source': {}, 'Eddy_rotated_bvec_source': {}, 'Mask_File_source': {}, 'Parcellation_Atlas_source': {}, 'T1w_Model_Brain_For_Connectivity_source': {},'B0acqp_source': {}, 'Fullacqp_source': {}, 'Index_file_source': {}, 'Freesurfer_SUBJECTS_DIR_source': {}, 'Freesurfer_Subject_ID_source': {}, 'Freesurfer_Extracted_Brain_source': {},'Freesurfer_Five_Tissues_Segmented_source': {},
	}

	for experiment in experiments:
		if experiment in data:
			experiment_data = data[experiment]
			JSON_sources['Experiments'] [experiment] = experiment
			JSON_sources['Subject_Name'][experiment] = experiment_data['subject']
			if 'b0acqp' in experiment_data:
				JSON_sources['B0acqp_source'] [experiment] = experiment_data['b0acqp']
			if 'fullacqp' in experiment_data:
				JSON_sources['Fullacqp_source'] [experiment] = experiment_data['fullacqp']
			if 'index_file' in experiment_data: 
				JSON_sources['Index_file_source'] [experiment] = experiment_data['index_file'] 
			if 'DWI_dicom' in experiment_data:
				JSON_sources['DWI_Source'][experiment] = experiment_data['DWI_dicom']
			elif 'DWI_nifti' in experiment_data:	
				JSON_sources['DWI_Source'][experiment] = experiment_data['DWI_nifti']
				JSON_sources['bvec_Source'][experiment] = experiment_data['bvec_Source']
				JSON_sources['bval_Source'][experiment] = experiment_data['bval_Source']
			if 'T1w_nifti' in experiment_data:	
				JSON_sources['T1w_Source'][experiment] = experiment_data['T1w_nifti']
			elif 'T1w_dicom' in experiment_data : 
				JSON_sources['T1w_Source'][experiment] = experiment_data['T1w_dicom']	
			if  'Eddy_Corrected' in experiment_data:
				JSON_sources['Eddy_corrected_source'][experiment] = experiment_data['Eddy_Corrected'] 
				JSON_sources['Eddy_rotated_bvec_source'][experiment] = experiment_data['Eddy_rotated_bvec'] 
				JSON_sources['Mask_File_source'][experiment] = experiment_data['Mask_File'] 
			if 'Parcellation_Atlas' in experiment_data:
				JSON_sources['Parcellation_Atlas_source'][experiment] = experiment_data['Parcellation_Atlas']  	
			if 'T1w_Model_Brain_For_Connectivity' in experiment_data:
				JSON_sources['T1w_Model_Brain_For_Connectivity_source'][experiment] = experiment_data['T1w_Model_Brain_For_Connectivity']
			if 'Freesurfer_Extracted_Brain' in experiment_data:
				JSON_sources['Freesurfer_Extracted_Brain_source'][experiment] = experiment_data['Freesurfer_Extracted_Brain']
			if 'Freesurfer_Five_Tissues_Segmented' in experiment_data:
				JSON_sources['Freesurfer_Five_Tissues_Segmented_source'][experiment] = experiment_data['Freesurfer_Five_Tissues_Segmented']
			if 'Freesurfer_SUBJECTS_DIR' in experiment_data:
				JSON_sources['Freesurfer_SUBJECTS_DIR_source'][experiment] = experiment_data['Freesurfer_SUBJECTS_DIR']
			if 'Freesurfer_Subject_ID' in experiment_data:
				JSON_sources['Freesurfer_Subject_ID_source'][experiment] = experiment_data['Freesurfer_Subject_ID']

	return JSON_sources

def Remove_Annotation_Links (JSON_sources,Connectivity_Atlas_Name) : 

	for Subject in JSON_sources ['Freesurfer_SUBJECTS_DIR_source'] : 
		Freesurfer_SUBJECTS_DIR = JSON_sources ['Freesurfer_SUBJECTS_DIR_source'] [Subject] 
		Freesurfer_Subject_ID = JSON_sources ['Freesurfer_Subject_ID_source'] [Subject]
		vfs_indicate = Freesurfer_SUBJECTS_DIR.find('vfs://')
		if vfs_indicate == 0 : 
			Freesurfer_SUBJECTS_DIR_Full_Path = fastr.vfs.url_to_path(Freesurfer_SUBJECTS_DIR) 
		else: 
			Freesurfer_SUBJECTS_DIR_Full_Path = Freesurfer_SUBJECTS_DIR
		Left_Hemisphere_Annotation_File = Freesurfer_SUBJECTS_DIR_Full_Path + '/' + Freesurfer_Subject_ID + '/label/lh.' + Connectivity_Atlas_Name + '.annot'
		Right_Hemisphere_Annotation_File = Freesurfer_SUBJECTS_DIR_Full_Path + '/' + Freesurfer_Subject_ID + '/label/rh.' + Connectivity_Atlas_Name + '.annot'
		if os.path.islink(Left_Hemisphere_Annotation_File) : 
			os.unlink(Left_Hemisphere_Annotation_File)
			print 'Unlinked : ' + Left_Hemisphere_Annotation_File
		if os.path.islink(Right_Hemisphere_Annotation_File) : 
			os.unlink(Right_Hemisphere_Annotation_File)
			print 'Unlinked : ' + Right_Hemisphere_Annotation_File

def main():

	# Parse the program command line parameters  
	parser = argparse.ArgumentParser()
	parser.add_argument('-Dict', type=unicode, required=True, default='', help='Dictionary file with source data.')
	parser.add_argument('-White_Matter_Atlas', type=unicode, required=False, default='vfs://fastr-data/atlasses/fsl/JHU-ICBM-labels-1mm.nii.gz', help='Path to the white matter atlas - note all atlases and model brains should have the same size')
	parser.add_argument('-FA_Model_Brain', type=unicode, required=False, default='vfs://fastr-data/atlasses/fsl/FMRIB58_FA_1mm.nii.gz', help='Path to an FA model brain - note all atlases and model brains should have the same size')
	parser.add_argument('-LowerCingulum_Atlas', type=unicode, required=False, default='vfs://fastr-data/atlasses/fsl/LowerCingulum_1mm.nii.gz', help='Path to Lower Cingulum atlas - note all atlases and model brains should be of the same size')
	parser.add_argument('-T1w_Model_Brain', type=unicode, required=False, default='vfs://fastr-data/atlasses/fsl/MNI152_T1_1mm.nii.gz', help='Path to T1w model brain - note all atlases and model brains should be of the same size')
	parser.add_argument('-DWI_Model_Brain', type=unicode, required=False, default='vfs://fastr-data/atlasses/fsl/JHU-ICBM-DWI-1mm.nii.gz', help='Path to DWI model brain - note all atlases and model brains should be of the same size')
	parser.add_argument('-freesurfer_LUT', type=unicode, required=False, default='vfs://fastr-data/LUT/freesurfer/FreeSurferColorLUT.txt', help='FreeSurfer Look Up Table')
	parser.add_argument('-mrtrix_LUT', type=unicode, required=False, default='vfs://fastr-data/LUT/mrtrix/fs_default.txt', help='mrtrix Look Up Table')
	parser.add_argument('-topupconfig', type=unicode, required=False, default='vfs://fastr-data/config/b02b0.cnf', help='config file for topup')
	parser.add_argument('-Parcellation_Atlas_Left_Hemisphere', type=unicode, required=False, default='vfs://fastr-data/atlasses/freesurfer/lh.HCP-MMP1.annot', help='A possible left hemisphere parcellation Atlas for connectivity calculation')
	parser.add_argument('-Parcellation_Atlas_Right_Hemisphere', type=unicode, required=False, default='vfs://fastr-data/atlasses/freesurfer/rh.HCP-MMP1.annot', help='A possible right hemisphere parcellation Atlas for connectivity calculation')
	parser.add_argument('-FreeSurfer', dest='FreeSurfer', help='Determines whether to run the freesurfer recon-all pipeline to produce White Matter mask', type=lambda x:bool(distutils.util.strtobool(x)), default=False)
	parser.add_argument('-PreProcessing', dest='PreProcessing', help='Determines if a preprocessing step is required at the begining of the pipeline analysis', type=lambda x:bool(distutils.util.strtobool(x)), default=True)
	parser.add_argument('-BiasCorrect', dest='BiasCorrect', help='Determines if to run a dwibiascorrect correction, use only if extensive B1 field distortions exist', type=lambda x:bool(distutils.util.strtobool(x)), default=False)
	parser.add_argument('-Unringing_Correct', dest='Unringing_Correct', help='Determine wheter to preform gibbs unringing correction', type=lambda x:bool(distutils.util.strtobool(x)), default=False)
	parser.add_argument('-RunTopup', dest='RunTopup', help='Determines if to run a topup correction', type=lambda x:bool(distutils.util.strtobool(x)), default=True)
	parser.add_argument('-Eddy_Correction_Type', type=unicode, dest='Eddy_Correction_Type',required=False, default='eddy_openmp', help='Type of eddy correction, old type-eddy_correct, new type-eddy_openmp') 
	parser.add_argument('-Get_FA_Values', dest='Get_FA_Values', help='Determines whether to calculate FA values', type=lambda x:bool(distutils.util.strtobool(x)), default=True)
	parser.add_argument('-Get_Diffusivity_Values', dest='Get_Diffusivity_Values', help='Determines whether to calculate other (non-FA) diffusivity  values', type=lambda x:bool(distutils.util.strtobool(x)), default=True)
	parser.add_argument('-FiberTracking', dest='FiberTracking', help='Determines if fiber tracking is needed at the end of the pipeline analysis', type=lambda x:bool(distutils.util.strtobool(x)), default=True)
	parser.add_argument('-Fivettgen_Algorithm', dest='Fivettgen_Algorithm', help='Algorithm that is used by 5ttgen to produce five tissues segmented brain can be either fsl or Freesurfer ', type=str, default='Freesurfer')
	parser.add_argument('-Use_Tensor_Model', dest='Use_Tensor_Model', help='Determines tractography is calculated directetly using the DTI tensor (True - Fits low magnetic fields such as 1.5T) or using spherical harmonies (False - for higher filed strengths)', type=lambda x:bool(distutils.util.strtobool(x)), default=True)
	parser.add_argument('-Connectivity', dest='Connectivity', help='Determines if connectivity matices are produced at the end of the pipeline analysis', type=lambda x:bool(distutils.util.strtobool(x)), default=True) 
	parser.add_argument('-Alternative_Parcellation_Atlas', dest='Alternative_Parcellation_Atlas', help='Determine whether to use an alternative parcelation atlas. If yes uses the atlas annotation files from Parcellation_Atlas_Left_Hemisphere and Parcellation_Atlas_Right_Hemisphere', type=lambda x:bool(distutils.util.strtobool(x)), default=False)
	parser.add_argument('-Do_Fit', dest='Do_Fit', help='Determines if fitting to 5 tissue type image of during the connectivity calculation of the pipeline analysis is done', type=lambda x:bool(distutils.util.strtobool(x)), default=True)
	parser.add_argument('-mrdegibbs_axes', dest='mrdegibbs_axes', help='Axis for mrdegibbs', type=str, default='0,1')
	parser.add_argument('-acqparams_1', type=int, nargs=4, dest='acqparams_1',required=False, default=(0, -1, 0, 0.05), help='acquisition parameters for eddy corrrection line 1')
	parser.add_argument('-acqparams_2', type=int, nargs=4, dest='acqparams_2',required=False, default=(0, 1, 0, 0.05), help='acquisition parameters for eddy corrrection line 2')
	parser.add_argument('-tbss_threshold', dest='tbss_threshold', help='Threshold level for the tbss calculation', type=float, default=0.2)
	#parser.add_argument('-ATLASVALUES', type=int, dest='ATLASVALUES',required=False, default='1', help='Determine whether to compute mean FA values from JHU atlas and skeleton')
	parser.add_argument('-Min_tracking_Length', dest='Min_tracking_Length', help='Minimum length of any track in mm', type=int, default=5) 
	parser.add_argument('-Number_Of_FA_Slices', dest='Number_Of_FA_Slices', help='Number of slices for FA values calculations', type=int, default=48)
	parser.add_argument('-Number_Of_Streamlines', dest='Number_Of_Streamlines', help='Desired number of streamlines to be selected by tckgens', type=int, default=500000) 
	parser.add_argument('-Sift_Number_Of_Fibers', dest='Sift_Number_Of_Fibers', help='Desired number of streamlines to be selected by tcksift', type=int, default=100000) 
	parser.add_argument('-Max_forward_Dist_Search', dest='Max_forward_Dist_Search', help='extention of tracts in last known direction when running tck2connectome -assignment_forward_search', type=int, default=5)
	parser.add_argument('-Max_reverse_Dist_Search', dest='Max_reverse_Dist_Search', help='extention of tracts in last known direction when running tck2connectome -assignment_forward_search', type=int, default=5)
	parser.add_argument('-Connectivity_Atlas_Name', dest='Connectivity_Atlas_Name', help='Name of the connectivity annotation atlas that will be produced and used', type=str, default='hcpmmp1')
	parser.add_argument('-Keep_Original', dest='Keep_Original', help='Determines whether to output original file in NIFTII format', type=lambda x:bool(distutils.util.strtobool(x)), default=False)
	parser.add_argument('-Keep_Fibers', dest='Keep_Fibers', help='Determines whether to output the full fiber image in tck.gz format (heavy, keep only if you really need) - deafult False ', type=lambda x:bool(distutils.util.strtobool(x)), default=False)
	parser.add_argument('-Output_URI', dest='Output_URI', help='URI of the output folder relative to the vfs://results location', type=str, default='')
	parser.add_argument('-Temp', type=unicode, dest='Temp_Dir',required=False, default='vfs://tmp', help='Temp directory')
	parser.add_argument('-query', type=unicode, required=False, help='URL query string that will be appended to Fastr output.', default="" )
	parser.add_argument('-Subject_Name', dest='Subject_Name', help='Name of the subjects. Used to read Subject details from the dictionary file', type=str, required=False, nargs='+', default='')
	parser.add_argument('-Verbose', dest='Verbose', help='Determines if the program prints all the source and sink data', type=lambda x:bool(distutils.util.strtobool(x)), default=False)
	parser.add_argument('-FSL_Version', dest='FSL_Version', help='Set the version of FSL that will be used for processing', type=unicode, default='5.0.9') 

	args = parser.parse_args()

	# Setup the temp directory
	Temp_Dir_Rel_Index = args.Temp_Dir.find ('vfs://tmp')
	if Temp_Dir_Rel_Index == 0 :
		Temp_Dir=  args.Temp_Dir 
	else : 
		Temp_Dir= 'vfs://tmp/' + args.Temp_Dir

	# Read the dict file and check that all sources are nifti or dicom
	JSON_Sources=JSON_Read_Arguments(args.Dict, args.Subject_Name)
	empty_keys = [k for k,v in JSON_Sources.iteritems() if not v]	
	for k in empty_keys:
		del JSON_Sources[k]

	# perform all sort of checks to see that the sources comply with what is expected

	# Check that both the freesurfer Cortical Parcellation Atlas and the T1w processed brain are present
	if ((args.FreeSurfer != True) and (args.Connectivity == True)) :
		if ( args.Fivettgen_Algorithm == 'fsl' ):
			if bool('T1w_Model_Brain_For_Connectivity_source' in  JSON_Sources.keys()):
				for Freesurfer_Model_Brain in dict.values(JSON_Sources['T1w_Model_Brain_For_Connectivity_source']) :
					if Freesurfer_Model_Brain == '':
						sys.exit('if FreeSurfer flag is False and Connectivity flag is True, then a source for the T1.mgz file from freesurfer pipeline must be given as a source for all subjects')
			else: 
				sys.exit('if FreeSurfer flag is False and Connectivity flag is True, then a source for the T1.mgz file from freesurfer pipeline must be given as a source for all subjects')
			if bool('Freesurfer_Extracted_Brain_source' in  JSON_Sources.keys()):
				for Freesurfer_Model_Extracted_Brain in dict.values(JSON_Sources['Freesurfer_Extracted_Brain_source']) :
					if Freesurfer_Model_Extracted_Brain == '':
						sys.exit('if FreeSurfer flag is False and Connectivity flag is True, then a source for the brain.mgz file from freesurfer pipeline must be given as a source for all subjects')
			else:
				sys.exit('if FreeSurfer flag is False and Connectivity flag is True, then a source for the brain.mgz file from freesurfer pipeline must be given as a source for all subjects')
		if ( args.Alternative_Parcellation_Atlas == True ):
			if bool('Freesurfer_SUBJECTS_DIR_source' in  JSON_Sources.keys()):
				for Freesurfer_Subject_Dir in dict.values(JSON_Sources['Freesurfer_SUBJECTS_DIR_source']) :
					if Freesurfer_Subject_Dir == '':
						sys.exit('if FreeSurfer flag is False and Connectivity flag is True and Alternative_Parcellation_Atlas is True, then a source to set the freesurfer SUBJECTS_DIR environmental variable must be given as a source for all subjects')
			else:
				sys.exit('if FreeSurfer flag is False and Connectivity flag is True and Alternative_Parcellation_Atlas is True, then a source to set the freesurfer SUBJECTS_DIR environmental variable must be given as a source for all subjects')
			if bool('Freesurfer_Subject_ID_source' in  JSON_Sources.keys()):
				for Freesurfer_Subject_ID in dict.values(JSON_Sources['Freesurfer_Subject_ID_source']) :
					if Freesurfer_Subject_Dir == '':
						sys.exit('if FreeSurfer flag is False and Connectivity flag is True and Alternative_Parcellation_Atlas is True, then a source to set the freesurfer subject must be given as a source for all subjects')
			else:
				sys.exit('if FreeSurfer flag is False and Connectivity flag is True and Alternative_Parcellation_Atlas is True, then a source to set the freesurfer subject must be given as a source for all subjects')
		else: 
			if bool('Parcellation_Atlas_source' in  JSON_Sources.keys()):
				for Freesurfer_Parcellation_Atlas in dict.values(JSON_Sources['Parcellation_Atlas_source']) :
					if Freesurfer_Parcellation_Atlas == '':
						sys.exit('if FreeSurfer flag is False and Connectivity flag is True, then a source for the parcellation atlas file (aparc+aseg.mgz) from the freesurfer pipeline must be given as a source for all subjects')
			else:
				sys.exit('if FreeSurfer flag is False and Connectivity flag is True, then a source for the parcellation atlas file (aparc+aseg.mgz) from the freesurfer pipeline must be given as a source for all subjects')

	if ((args.FreeSurfer != True) and (args.FiberTracking == True)) :
		if bool('Freesurfer_Five_Tissues_Segmented_source' in  JSON_Sources.keys()):
			for Freesurfer_FiveTissue_File in dict.values(JSON_Sources['Freesurfer_Five_Tissues_Segmented_source']) :
				if Freesurfer_FiveTissue_File == '':
					sys.exit('if FreeSurfer flag is False and FiberTracking flag is True, then a source for the aseg.mgz file from freesurfer pipeline must be given as a source for all subjects')
		else:
			sys.exit('if FreeSurfer flag is False and FiberTracking flag is True, then a source for the aseg.mgz file from freesurfer pipeline must be given as a source for all subjects')
		if ( args.Fivettgen_Algorithm == 'Freesurfer' ):
			if bool('Freesurfer_Extracted_Brain_source' in  JSON_Sources.keys()):
				for Freesurfer_Model_Extracted_Brain in dict.values(JSON_Sources['Freesurfer_Extracted_Brain_source']) :
					if Freesurfer_Model_Extracted_Brain == '':
						sys.exit('if FreeSurfer flag is False and Connectivity flag is True, then a source for the brain.mgz file from freesurfer pipeline must be given as a source for all subjects')
			else:
				sys.exit('if FreeSurfer flag is False and Connectivity flag is True, then a source for the brain.mgz file from freesurfer pipeline must be given as a source for all subjects')

	# Check that the bval and bvec files are present in case that the files are not dicom
	if bool('bvec_Source' in  JSON_Sources.keys()):
		Dicom2NiftiConvert = False
		for bvec_Source in dict.values(JSON_Sources['bvec_Source']) :
			if bvec_Source == '' :
				sys.exit('All DTI/DWI sources should be either dicom format or all should be in nifti format (in which case, they should be accompiened by bval and bvec soures)')
	else :
		Dicom2NiftiConvert = True
                
	# Check that all mask files are either nii.gz or nii
	if (args.PreProcessing == False) : 
		Mask_In_GZ_From = False
		if ('Mask_File_source' in  JSON_Sources.keys()):
			substring = ".gz"
			for Mask in dict.values(JSON_Sources['Mask_File_source']) :
				if substring in Mask :
					Mask_In_GZ_From=True

			if Mask_In_GZ_From == True :
				for Mask in dict.values(JSON_Sources['Mask_File_source']) :
					Start=Mask.find(substring, 0, len(Mask))
					if Start == -1 :
						sys.exit('All Mask files should be either nii.gz or .nii format')
	else:
		Mask_In_GZ_From = False

	# Check that all T1w sources are either nifti or dicom
	T1w_Source_Nifti = False
	substring = '.nii.gz'

	for T1w_Source in dict.values(JSON_Sources['T1w_Source']) :
		Start=T1w_Source.find(substring, 0, len(T1w_Source))
		if Start > 0 :
			T1w_Source_Nifti = True

	if T1w_Source_Nifti == True :
		for T1w_Source in dict.values(JSON_Sources['T1w_Source']) :
			Start=T1w_Source.find(substring, 0, len(T1w_Source))
			if Start == -1 :
				sys.exit('All T1w sources should be either nifti format or all should be in dicom format')

	# Check for acq and index files
	if (args.PreProcessing == True) :
		if ('Fullacqp_source' in JSON_Sources.keys()):
			Use_deafult_acpq_and_index = False
			for acqp_file in dict.values(JSON_Sources['Fullacqp_source']) :
				if ((acqp_file == '') and (Use_deafult_acpq_and_index == True)) :
					sys.exit('All subjects must have associated b0 acqp and index files for eddy correction -  or not and then deafult (in args.acqparams ) is used')

		else :
			Use_deafult_acpq_and_index = True

		if ('Index_file_source' in JSON_Sources.keys()):
			for indx_file in dict.values(JSON_Sources['Index_file_source']) :
				if ((indx_file == '') and (Use_deafult_acpq_and_index == True)) :
					sys.exit('All subjects must have associated b0 acqp and index files for eddy correction -  or not and then deafult (in args.acqparams ) is used')

		if ('B0acqp_source' in JSON_Sources.keys()):
			for acqp_file in dict.values(JSON_Sources['B0acqp_source']) :
				if ((acqp_file == '') and (Use_deafult_acpq_and_index == True)) :
					sys.exit('All subjects must have associated b0 acqp and index files for eddy correction and a b0 acqp file for topup correction -  or not, and then deafult (in args.acqparams ) is used')
	else: 
		Use_deafult_acpq_and_index = True

	# check that the 5ttgen algorithm is either 'fsl' or 'Freesurfer'
	if not ((args.Fivettgen_Algorithm == 'fsl') or ( args.Fivettgen_Algorithm == 'Freesurfer')):
		sys.exit('The Fivettgen_Algorithm must be either fsl or Freesurfer')

	# Remove old link of alternative annotations
	if (args.Alternative_Parcellation_Atlas == True) : 
		Remove_Annotation_Links (JSON_Sources, args.Connectivity_Atlas_Name)

	# Get the source and sink data 
	sourcedata = source_data(JSON_Sources, args.acqparams_1, args.acqparams_2, args.White_Matter_Atlas, args.LowerCingulum_Atlas, args.FA_Model_Brain, args.T1w_Model_Brain, args.DWI_Model_Brain, args.Number_Of_FA_Slices, args.Number_Of_Streamlines, args.freesurfer_LUT, args.mrtrix_LUT, args.Max_forward_Dist_Search, args.Max_reverse_Dist_Search, args.Min_tracking_Length, args.Sift_Number_Of_Fibers, args.Use_Tensor_Model, args.Alternative_Parcellation_Atlas, args.Parcellation_Atlas_Left_Hemisphere, args.Parcellation_Atlas_Right_Hemisphere, args.Connectivity_Atlas_Name)
	sinkdata   = sink_data(args.Output_URI, args.Keep_Original, args.PreProcessing, args.Get_FA_Values, args.Get_Diffusivity_Values, args.FiberTracking, args.Keep_Fibers, args.Connectivity, JSON_Sources ,args.query)

	# Build the network
	network = create_network(Dicom2NiftiConvert, T1w_Source_Nifti, args.FreeSurfer, args.PreProcessing, args.Unringing_Correct, args.mrdegibbs_axes, args.BiasCorrect, Mask_In_GZ_From, args.Get_FA_Values, args.Get_Diffusivity_Values, args.FiberTracking, args.Keep_Fibers, args.Connectivity, args.Keep_Original, args.Eddy_Correction_Type, args.RunTopup, Use_deafult_acpq_and_index, args.topupconfig, args.tbss_threshold, args.Number_Of_FA_Slices, args.Do_Fit, args.Use_Tensor_Model, args.Alternative_Parcellation_Atlas, args.FSL_Version, args.Fivettgen_Algorithm)

	# Print the source and the sink data if needed
	if args.Verbose:
		print 'The source data is:\n'
		print sourcedata
		print '\n'
		print 'The sink data is:\n'
		print sinkdata
		print '\n'

	# Execute the network    
	network.draw_network(name = network.id, img_format='svg',draw_dimension = True)	
	network.execute (sourcedata,sinkdata,tmpdir= Temp_Dir)

	# Remove old link of alternative annotations
	if (args.Alternative_Parcellation_Atlas == True) :
		Remove_Annotation_Links (JSON_Sources, args.Connectivity_Atlas_Name)	

	return 0 

if __name__ == '__main__':
	main();


