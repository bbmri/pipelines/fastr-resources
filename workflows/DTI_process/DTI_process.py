import fastr 
fastr.config.source_job_limit = 1
import os
import datetime 
import sys
import argparse
import distutils
import json
from str2bool import str2bool
from os import walk
import pdb #pdb.set_trace()  	

def source_data(JSON_Sources, acqparams_1, acqparams_2 , JHU_ICMB_Atals, Num_FA_Values, Number_Of_Streamlines):
	
	sourcedata = {
		'acqparams_1': {}, 'acqparams_2': {}, 'JHU_ICMB_Atals' : {}, 'fslmaths_operator': {},
		'flsmaths_Slices_Operator1' : {}, 'flsmaths_Slices_Operator2': {} , 'flsmaths_Slices_Operator3': {},
		'flsmaths_Slices_Operator1_number': {}, 'flsmaths_Slices_Operator2_number': {}, 
		'dwiresponse_algorithm': {}, 'tckgen_select' : {}, 
		}
	Slices = [str(int(x)) for x in range (1,Num_FA_Values+1)]

	for Experiment in JSON_Sources['Experiments'].keys(): 
		sourcedata['acqparams_1']  = acqparams_1
		sourcedata['acqparams_2']  = acqparams_2
		for Experiment in JSON_Sources['Experiments'] :
			sourcedata ['JHU_ICMB_Atals'][Experiment]  = JHU_ICMB_Atals
			sourcedata ['fslmaths_operator'][Experiment]  = '-mas'
			sourcedata ['dwiresponse_algorithm'][Experiment] = 'dhollander'
                        sourcedata ['tckgen_select'][Experiment] = Number_Of_Streamlines
			sourcedata ['flsmaths_Slices_Operator1'][Experiment] = 'thr'
			sourcedata ['flsmaths_Slices_Operator2'][Experiment] = 'uthr'
			sourcedata ['flsmaths_Slices_Operator3'][Experiment] = 'bin'
			sourcedata ['flsmaths_Slices_Operator1_number'] [Experiment]= Slices
			sourcedata ['flsmaths_Slices_Operator2_number'] [Experiment]= Slices
	sourcedata.update(JSON_Sources)

	return sourcedata 

def sink_data(Output_Base_Dir, FiberTracking, Get_FA_Values, Keep_Original , JSON_Sources, query_string=""):
	
	st=datetime.datetime.now().strftime("%Y_%m_%d_%H_%M%p")
	
	if Output_Base_Dir.find ('xnat://') == 0 :
		if FiberTracking == True :
			Fiber_Tracking_sinkdata = {
				'Fiber_Tracking_Image' : Output_Base_Dir + '/{sample_id}/{sample_id}.vtk.gz' +  query_string,
				}
		if 	Get_FA_Values == True : 
			FA_Values_sinkdata = {
				'FA_Biomarker' : Output_Base_Dir + '/{sample_id}/{sample_id}_FA.txt' +  query_string,
			}	
			
		if 	Keep_Original == True : 
			Original_sinkdata = {
				'Original_Niftii' : Output_Base_Dir + '/{sample_id}/{sample_id}_Original.nii.gz' +  query_string,
				'Original_Bval' : Output_Base_Dir + '/{sample_id}/{sample_id}_Original_bval.bval' +  query_string,
				'Original_Bvec' : Output_Base_Dir + '/{sample_id}/{sample_id}_Original_bvec.bvec' +  query_string,
			}	
	else: 
		if FiberTracking == True : 
			Fiber_Tracking_sinkdata = {
				'Fiber_Tracking_Image' : 'vfs://data/' + Output_Base_Dir + '/{sample_id}/{sample_id}_' + st +'.vtk.gz' ,
				}
		if 	Get_FA_Values == True :
			FA_Values_sinkdata = {
				'FA_Biomarker': 'vfs://data/' + Output_Base_Dir + '/{sample_id}/{sample_id}_FA_' + st + '.txt' 
				}
		if 	Keep_Original == True : 
			Original_sinkdata = {
				'Original_Niftii' : 'vfs://data/' + Output_Base_Dir  + '/{sample_id}/{sample_id}_Original_' + st + '.nii.gz',
				'Original_Bval' : 'vfs://data/' + Output_Base_Dir + '/{sample_id}/{sample_id}_Original_bval_' + st + '.bval',
				'Original_Bvec' : 'vfs://data/' + Output_Base_Dir + '/{sample_id}/{sample_id}_Original_bvec_' + st + '.bvec',
			}					
			
	sinkdata = {}
	
	if FiberTracking == True :
		sinkdata.update(Fiber_Tracking_sinkdata)
	
	if 	Get_FA_Values == True : 
		sinkdata.update(FA_Values_sinkdata)	
		
	if 	Keep_Original == True : 
		sinkdata.update(Original_sinkdata)						
				
	return sinkdata


def create_network(FiberTracking, Get_FA_Values, Keep_Original ,Min_tracking_Length , Number_Of_Streamlines, JHU_ICMB_Atals, Number_Of_FA_Slices, Eddy_Correction_Type):
	# Build the Network	 
	network = fastr.Network(id_='Process_DTI')			
	
	# Download the files and and convert them to niftii
	Subject_Files 	= network.create_source (fastr.typelist['DicomImageFile'],id_='DWI_source',stepid='Sources')
	Subject_Files.required_memory = '8g'	
	Dicom_convert 	= network.create_node (fastr.toollist['dcm2nii-DWI'],id_='DCM2NII',stepid='Source_Conversion')
	Subject_Files.output >> Dicom_convert.inputs ['Processed_XNAT_DICOM']
    	Dicom_convert.required_time   = '9000'
    	Dicom_convert.required_memory = '8g'
	
	# convert the dwi file to a mask file using dwi2mask
	Mask_Formation 	=  network.create_node (fastr.toollist['dwi2mask'],id_='dwi2mask',stepid='MRI_Correction')
	Dicom_convert.outputs['DWI_File'] >>  Mask_Formation.inputs ['DWI_File']
	Dicom_convert.outputs['BVEC_File'] >>  Mask_Formation.inputs ['BVEC_File']
	Dicom_convert.outputs['BVAL_File'] >>  Mask_Formation.inputs ['BVAL_File']
	Mask_Formation.required_memory = '8g'
	
	# Denoise the mask
	Denoise 	=  network.create_node (fastr.toollist['dwidenoise'],id_='dwidenoise',stepid='MRI_Correction')
	Mask_Formation.outputs['Mask_File'] >> Denoise.inputs['MASK_File']
	Dicom_convert.outputs['DWI_File'] >> Denoise.inputs['DWI_File']	
	Denoise.required_memory = '8g' 
	
	# B1 field inhomogeneity correction
	B1_correction = network.create_node (fastr.toollist['dwibiascorrect'],id_='dwibiascorrect',stepid='MRI_Correction')
	Dicom_convert.outputs['BVEC_File'] >> B1_correction.inputs['BVEC_File']
	Dicom_convert.outputs['BVAL_File'] >> B1_correction.inputs['BVAL_File']
	Denoise.outputs['Denoised_File'] >> B1_correction.inputs['Denoised_DWI_File']
	B1_correction.inputs['fsl_Correct'] = ['-fsl']
	B1_correction.required_memory = '8g'	
	
	if Eddy_Correction_Type == 'eddy_openmp' :
		
		# Determine all diffusion unweighted images
		
		B0indices = network.create_node (fastr.toollist['b0indices'],id_='B0indices',stepid='Correct_Eddy')
		B0indices.required_memory = '4g'
		Dicom_convert.outputs['BVAL_File'] >> B0indices.inputs['BVAL_File']
		
		# Select volumes from a 4D time series and output a subset 4D volume
		
		SelectVectors = network.create_node (fastr.toollist['fslselectvols'],id_='fslselectvols',stepid='Correct_Eddy')
		SelectVectors.required_memory = '4g'
		B0indices.outputs ['Return_String'] >> SelectVectors.inputs['b0indices']
		SelectVectors.inputs['Output_Mean'] = ['-m']
		B1_correction.outputs['B1_Corrected_File'] >> SelectVectors.inputs['DWI_File']
		
		# Calculate mean accros time for the diffusion unweighted series  
		
		Unweighted_Diffusion_Mean = network.create_node (fastr.toollist['FSLMaths'],id_='FSLMaths_1',stepid='Correct_Eddy')
		Unweighted_Diffusion_Mean.required_memory = '4g'
		SelectVectors.outputs ['B0s']	>> Unweighted_Diffusion_Mean.inputs['image1']
		Unweighted_Diffusion_Mean.inputs['operator1'] = ['-Tmean']
		
		# Extracting the brain mask 
		
		Brain_Mask = network.create_node (fastr.toollist['fsl.Bet'],id_='Brain_Mask',stepid='Correct_Eddy')
		Brain_Mask.required_memory = '4g'
		Unweighted_Diffusion_Mean.outputs['output_image']	>>  Brain_Mask.inputs['image']
		Brain_Mask.outputs['mask_image'] = [' ']
		
		# Writing an index file for using with the eddy correction node
		
		Index_File = network.create_node (fastr.toollist['dwi-fslinfo'],id_='dwi_fslinfo',stepid='Correct_Eddy')
		Index_File.required_memory = '4g'
		B1_correction.outputs['B1_Corrected_File'] >> Index_File .inputs ['Input_File']
		
		# Producing an acqparams file for the use with the eddy correction node
		
		acqparams_File = network.create_node (fastr.toollist['acqparams_file'],id_='acqparams_file',stepid='Correct_Eddy')
		acqparams_File.required_memory = '4g'
		acqparams_1 	= network.create_source (fastr.typelist['Float'],id_='acqparams_1',stepid='Bet_Sources')
		acqparams_2 	= network.create_source (fastr.typelist['Float'],id_='acqparams_2',stepid='Bet_Sources')
		acqparams_link_1 = network.create_link(acqparams_1.output, acqparams_File.inputs['acqparams_1'])
		acqparams_link_2 = network.create_link(acqparams_2.output, acqparams_File.inputs['acqparams_2'])
		acqparams_link_1.collapse = 'acqparams_1'	
		acqparams_link_2.collapse = 'acqparams_2'

		# Produce bval and bvec files to be used with eddy correction 
		
		Bval_Bvec_Files = network.create_node (fastr.toollist['write_bval_bvec'],id_='write_bval_bvec',stepid='Correct_Eddy')
		Bval_Bvec_Files.required_memory = '4g'
		Dicom_convert.outputs['BVEC_File'] >> Bval_Bvec_Files.inputs['In_BVEC_File']
		Dicom_convert.outputs['BVAL_File'] >> Bval_Bvec_Files.inputs['In_BVAL_File']
		
		# Run Eddy correction
		
		Eddy_Correction = network.create_node (fastr.toollist['eddy_openmp'],id_='eddy_openmp',stepid='Correct_Eddy')
		B1_correction.outputs['B1_Corrected_File'] >> Eddy_Correction.inputs ['imain']
		Brain_Mask.outputs['mask_image'] >> Eddy_Correction.inputs ['mask']
		#acqparams_File.outputs['Output_acqparams_File'] >> Eddy_Correction.inputs ['acqp']
		acqparams_eddy_link = network.create_link(acqparams_File.outputs['Output_acqparams_File'], Eddy_Correction.inputs ['acqp'])
		acqparams_eddy_link.input_group = 'moving'
		Bval_Bvec_Files.outputs ['Out_BVEC_File'] >> Eddy_Correction.inputs ['bvec']
		Bval_Bvec_Files.outputs ['Out_BVAL_File'] >> Eddy_Correction.inputs ['bval']
		Index_File.outputs['Output_File'] >> Eddy_Correction.inputs ['index']
		Eddy_Correction.required_memory = '16g'
		#Eddy_Correction.required_cores = 8
		
	elif Eddy_Correction_Type == 'eddy_correct' :
		
		# Produce bval and bvec files to be used with eddy correction 
		
		Bval_Bvec_Files = network.create_node (fastr.toollist['write_bval_bvec'],id_='write_bval_bvec',stepid='Correct_Eddy')
		Bval_Bvec_Files.required_memory = '4g'
		Dicom_convert.outputs['BVEC_File'] >> Bval_Bvec_Files.inputs['In_BVEC_File']
		Dicom_convert.outputs['BVAL_File'] >> Bval_Bvec_Files.inputs['In_BVAL_File']
		
		# Run Eddy correction
		Eddy_Correction = network.create_node (fastr.toollist['eddy_correct'],id_='eddy_correct',stepid='Correct_Eddy')
		B1_correction.outputs['B1_Corrected_File'] >> Eddy_Correction.inputs ['4dinput']
		Refference_Number_source = network.create_constant('Int', [64], id_='Refference_Number_source')
		Refference_Number_source.output >> Eddy_Correction.inputs['reference_no'] 
		Eddy_Correction.required_memory = '4g'
	
	else :
		print 'Eddy correction type must be either eddy_openmp or eddy_correct'
		exit(1)
		
	# Compute simple FA sliced biomarkers
	
	if Get_FA_Values == True : 
		
		# Create skeleton mask
		
		Simple_FA_MD = network.create_node (fastr.toollist['dtifit'],id_='dtifit',stepid='Compute_FA_Values')
		Simple_FA_MD.required_memory = '4g'
		Eddy_Correction.outputs ['DWI_Corrected_File'] >> Simple_FA_MD.inputs ['DWI_File']
		if Eddy_Correction_Type == 'eddy_openmp' :
			Eddy_Correction.outputs ['Rotated_Bvecs'] >> Simple_FA_MD.inputs ['bvec']
		elif Eddy_Correction_Type == 'eddy_correct' :
			Bval_Bvec_Files.outputs ['Out_BVEC_File'] >> Simple_FA_MD.inputs ['bvec']
		Dicom_convert.outputs['BVAL_File'] >>  Simple_FA_MD.inputs ['bval']
		Mask_Formation.outputs['Mask_File'] >> Simple_FA_MD.inputs ['Mask']
			
		# Run tbss analysis
		
		TBSS_Analysis = network.create_node (fastr.toollist['tbss'],id_='tbss',stepid='Compute_FA_Values')
		TBSS_Analysis.required_memory = '4g'
		Simple_FA_MD.outputs['FA'] >> TBSS_Analysis.inputs['Input_Files']

		# Run fslmaths to produces a general FA mask file
		
		Skeleton_Atlas = network.create_node (fastr.toollist['FSLMathsBinary'],id_='FSLMaths_Skeleton',stepid='Compute_FA_Values')
		Skeleton_Atlas.required_memory = '4g'
		Atlas_Source   = network.create_source (fastr.typelist['NiftiImageFile'],id_='JHU_ICMB_Atals',stepid='Atlas_Sources')
		Atlas_Source.required_memory = '4g'
		Operator_Source   = network.create_source ('String',id_='fslmaths_operator',stepid='FSLMaths_Sources')
		Skeleton_Atlas_Operator_Link = network.create_link(Atlas_Source.output, Skeleton_Atlas.inputs['left_hand'])
		Skeleton_Atlas_Operator_Link = network.create_link(Operator_Source.output, Skeleton_Atlas.inputs['operator'])
		Skeleton_Atlas.inputs['right_hand'] = TBSS_Analysis.outputs['mean_FA_skeleton']

		# Slice the mask to produce Number_Of_FA_Slices masks

		Slices=list(range(1,Number_Of_FA_Slices+1))
		Mask_Slices = network.create_node (fastr.toollist['fslmaths_mask_binary'],id_='fslmaths_mask_binary',nodegroup ='Mask_Slices',stepid='Compute_FA_Values')
		Mask_Slices.required_memory = '4g'
		Mask_Slices_Link_1 = network.create_link(Skeleton_Atlas.outputs['result'], Mask_Slices.inputs ['image1'])
		Mask_Slices_Source_1 = network.create_source (fastr.typelist['String'],id_='flsmaths_Slices_Operator1',stepid='Atlas_Sources')
		Mask_Slices_Link_2 = network.create_link(Mask_Slices_Source_1.output, Mask_Slices.inputs['operator1'])
		Mask_Slices_Source_2 = network.create_source (fastr.typelist['String'],id_='flsmaths_Slices_Operator2',stepid='Atlas_Sources')
                Mask_Slices_Link_3 = network.create_link(Mask_Slices_Source_2.output, Mask_Slices.inputs['operator2'])
		Mask_Slices_Source_3 = network.create_source (fastr.typelist['String'],id_='flsmaths_Slices_Operator3',stepid='Atlas_Sources')
                Mask_Slices_Link_4 = network.create_link(Mask_Slices_Source_3.output, Mask_Slices.inputs['operator3'])
		Mask_Slices_Source_4 = network.create_source (fastr.typelist['AnyType'],id_='flsmaths_Slices_Operator1_number',stepid='Atlas_Sources')
                Mask_Slices_Link_5 = network.create_link(Mask_Slices_Source_4.output, Mask_Slices.inputs['operator1_number'])
		Mask_Slices_Source_5 = network.create_source (fastr.typelist['AnyType'],id_='flsmaths_Slices_Operator2_number',stepid='Atlas_Sources')
                Mask_Slices_Link_6 = network.create_link(Mask_Slices_Source_5.output, Mask_Slices.inputs['operator2_number'])

		# Calsulate an FA value for each slice
		
		Single_FA_Value = network.create_node (fastr.toollist['fslmeants_mask_value'],id_='FSLMEANTS',nodegroup ='FA_Slices',stepid='Compute_FA_Values')
		Single_FA_Value.required_memory = '4g'
		TBSS_Analysis.outputs['all_FA_skeletonised'] >> Single_FA_Value.inputs ['mri_image']
		Single_FA_Value_Mask_Link = network.create_link(Mask_Slices.outputs['output_image'], Single_FA_Value.inputs ['mask'])

		# Write single FA values file 

		Final_FA_Values_File = network.create_node (fastr.toollist['Write_FA_File'],id_='Write_FA_File',stepid='Compute_FA_Values')
		Final_FA_Values_File.required_memory = '4g'
		FA_Files_Link= network.create_link (Single_FA_Value.outputs ['output_file'], Final_FA_Values_File.inputs['Input_Files'])	
		#FA_Files_Link.collapse = ('DWI_source')
	
	if FiberTracking == True : 
		# Reading the unique B values from the Bval file
		
		UniqueB_Values = network.create_node (fastr.toollist['bUniqueValues'],id_='bUniqueValues',stepid='Track_MW_Fibers')
		UniqueB_Values.required_memory = '4g'
		Dicom_convert.outputs['BVAL_File'] >> UniqueB_Values.inputs ['BVAL_File'] 
		
		# Estimate response function(s) for spherical deconvolution
		
		Response_Functions = network.create_node (fastr.toollist['dwi2response'],id_='dwi2response',stepid='Track_MW_Fibers')
		Response_Functions_Source = network.create_source (fastr.typelist['AnyType'],id_='dwiresponse_algorithm',stepid='dwi2response_Sources')
                Response_Functions_Link = network.create_link (Response_Functions_Source.output,Response_Functions.inputs['Algorithm'])		
		Eddy_Correction.outputs ['DWI_Corrected_File'] >> Response_Functions.inputs['Input_DWI'] 
		if Eddy_Correction_Type == 'eddy_openmp' :
			Eddy_Correction.outputs ['Rotated_Bvecs'] >> Response_Functions.inputs['BVEC_File'] 
		elif Eddy_Correction_Type == 'eddy_correct' :	
			Bval_Bvec_Files.outputs ['Out_BVEC_File'] >> Response_Functions.inputs['BVEC_File'] 
		Bval_Bvec_Files.outputs ['Out_BVAL_File'] >> Response_Functions.inputs['BVAL_File']
		UniqueB_Values.outputs['Return_String'] >> Response_Functions.inputs['shells']
		Mask_Formation.outputs['Mask_File'] >> Response_Functions.inputs['mask']
		Response_Functions.required_memory = '8g'
		
		# Estimate fibre orientation distributions from diffusion data using spherical deconvolution
		
		Fibers_Orientation = network.create_node (fastr.toollist['dwi2fod'],id_='dwi2fod',stepid='Track_MW_Fibers')
		if Eddy_Correction_Type == 'eddy_openmp' :
			Eddy_Correction.outputs ['Rotated_Bvecs'] >> Fibers_Orientation.inputs['BVEC_File'] 
		elif Eddy_Correction_Type == 'eddy_correct' :	
			Bval_Bvec_Files.outputs ['Out_BVEC_File'] >> Fibers_Orientation.inputs['BVEC_File']	
		Bval_Bvec_Files.outputs ['Out_BVAL_File'] >> Fibers_Orientation.inputs['BVAL_File']
		Mask_Formation.outputs['Mask_File'] >> Fibers_Orientation.inputs['mask']
		UniqueB_Values.outputs['Return_String'] >> Fibers_Orientation.inputs['shells']
		Fibers_Orientation.inputs['Algorithm'] = ['msmt_csd']
		Eddy_Correction.outputs ['DWI_Corrected_File'] >> Fibers_Orientation.inputs['Input_DWI'] 
		Response_Functions.outputs ['out_sfwm'] >> Fibers_Orientation.inputs['First_Response_Input']
		Fibers_Orientation.required_memory = '8g' 
		
		# Perform streamlines tractography
		
		Streamlines_Tractography = network.create_node (fastr.toollist['tckgen'],id_='tckgen',stepid='Track_MW_Fibers')
		Fibers_Orientation.outputs ['First_Output'] >> Streamlines_Tractography.inputs ['Input_DWI']
		Mask_Formation.outputs['Mask_File'] >> Streamlines_Tractography.inputs ['mask_image']
		Mask_Formation.outputs['Mask_File'] >> Streamlines_Tractography.inputs ['seed_image']
		Streamlines_Tractography.inputs ['minlength'] = [Min_tracking_Length]
		Streamlines_Tractography_Source = network.create_source (fastr.typelist['Int'],id_='tckgen_select',stepid='tckgen_Sources')
		Streamlines_Tractography_Link = network.create_link (Streamlines_Tractography_Source.output,Streamlines_Tractography.inputs ['select'])
		Streamlines_Tractography.required_memory = '8g'
		
		# Convert the tck image to vtk image
		
		Vtk_Streamlines_Image = network.create_node (fastr.toollist['tckconvert'],id_='tckconvert',stepid='Track_MW_Fibers')
		Streamlines_Tractography.outputs ['tracks'] >> Vtk_Streamlines_Image.inputs ['Input_File']
		Vtk_Streamlines_Image.required_memory = '8g'
		
		# gzip the vtk image
		
		Compressed_Vtk_Streamlines_Image = network.create_node (fastr.toollist['GZ_File'],id_='GZ_File',stepid='Track_MW_Fibers')
		Compressed_Vtk_Streamlines_Image.required_memory = '8g'
		Vtk_Streamlines_Image.outputs ['Output_File_Vtk'] >> Compressed_Vtk_Streamlines_Image.inputs['Input_File']
	
		
	# Creating the sinks and linking them
	
	if FiberTracking == True : 
		Fiber_Tracking_Image = network.create_sink (fastr.typelist['CompressedVtk_Image'],id_='Fiber_Tracking_Image',stepid='Sinks')
		Fiber_Tracking_Image.required_memory = '8g'
		Compressed_Vtk_Streamlines_Image.outputs['Output_File'] >> Fiber_Tracking_Image.input
	
	if Get_FA_Values == True : 
		FA_Output = network.create_sink (fastr.typelist['TxtFile'],id_='FA_Biomarker',stepid='Sinks')
		FA_Output.required_memory = '4g'	
		Final_FA_Values_File.outputs ['Out_FA_File'] >> FA_Output.input
		
	if Keep_Original == True : 
		Original_NIFTII =  network.create_sink (fastr.typelist['NiftiImageFile'],id_='Original_Niftii',stepid='Sinks')
		Original_NIFTII.required_memory = '8g'
		Dicom_convert.outputs['DWI_File'] >> Original_NIFTII.input
		Original_Bval =  network.create_sink (fastr.typelist['BValueFile'],id_='Original_Bval',stepid='Sinks')
		Original_Bval.required_memory = '4g'	
		Dicom_convert.outputs['BVAL_File'] >> Original_Bval.input	
		Original_Bvec =  network.create_sink (fastr.typelist['BVectorFile'],id_='Original_Bvec',stepid='Sinks')	
		Original_Bvec.required_memory = '4g'	
		Dicom_convert.outputs['BVEC_File'] >> Original_Bvec.input

	if not network.is_valid():
	        return None

	return network

	
def JSON_Read_Arguments (dict_file, experiments):

	with open(dict_file) as input_file:
		data = json.load(input_file)
	JSON_sources = { 'DWI_source': {} , 'Subject_Name': {}, 'Experiments': {},}
	
	for experiment in experiments:
		if experiment in data:
			experiment_data = data[experiment]
			JSON_sources['Experiments'] [experiment] = experiment
			JSON_sources['Subject_Name'][experiment] = experiment_data['subject'] 
			JSON_sources['DWI_source'][experiment] = experiment_data['DWI_dicom']

	return JSON_sources


def main():
			
	# Parse the program command line parameters  
	parser = argparse.ArgumentParser()
	parser.add_argument('-Dict', type=unicode, required=True, default='', help='Dictionary file with source data.')
	parser.add_argument('-JHU_ICBM_Atlas', type=unicode, required=False, default='vfs://fastr-data/atlasses/fsl/JHU-ICBM-labels-1mm.nii.gz', help='Path to JHU_ICBM Atlas')
	parser.add_argument('-Verbose', dest='Verbose', help='Determine if the program prints all the source and sink data', type=bool, default=False) 
	parser.add_argument('-FiberTracking', dest='FiberTracking', help='Determine if fiber tracking is needed at the end of the pipeline analysis', type=bool, default=True) 
	parser.add_argument('-Keep_Original', dest='Keep_Original', help='Determine whether to output original file in NIFTII format', type=bool, default=False) 
	parser.add_argument('-Get_FA_Values', dest='Get_FA_Values', help='Determine whether to calculate FA values', type=bool, default=True) 
	parser.add_argument('-Min_tracking_Length', dest='Min_tracking_Length', help='Minimum length of any track in mm', type=int, default=50) 
	parser.add_argument('-Number_Of_FA_Slices', dest='Number_Of_FA_Slices', help='Number of slices for FA values calculations', type=int, default=47)
	parser.add_argument('-Number_Of_Streamlines', dest='Number_Of_Streamlines', help='Desired number of streamlines to be selected by tckgens', type=int, default=10000) 
	parser.add_argument('-Output_URI', dest='Output_URI', help='URI of the output folder relative to the vfs://data location', type=str, default='')
	parser.add_argument('-Temp', type=unicode, dest='Temp_Dir',required=False, default='vfs://tmp', help='Temp directory')
	parser.add_argument('-query', type=unicode, required=False, help='URL query string that will be appended to Fastr output.', default="" )
	parser.add_argument('-Subject_Name', dest='Subject_Name', help='Name of the subjects. Used to read Subject details from the dictionary file', 
		type=str, required=False, nargs='+', default='') 
	parser.add_argument('-ATLASVALUES', type=int, dest='ATLASVALUES',required=False, default='1', help='Determine whether to compute mean FA values from JHU atlas and skeleton')	
	parser.add_argument('-acqparams_1', type=int, nargs=4, dest='acqparams_1',required=False, default=[0, -1, 0, 0.05], help='acquisition parameters for eddy corrrection line 1')
	parser.add_argument('-acqparams_2', type=int, nargs=4, dest='acqparams_2',required=False, default=[0, 1, 0, 0.05], help='acquisition parameters for eddy corrrection line 2')
	parser.add_argument('-Eddy_Correction_Type', type=unicode, dest='Eddy_Correction_Type',required=False, default='eddy_openmp', help='Type of eddy correction, old type-eddy_correct, new type-eddy_openmp')	
		
	args = parser.parse_args()
	
	# Setup the temp directory
	Temp_Dir_Rel_Index = args.Temp_Dir.find ('vfs://tmp')
	if Temp_Dir_Rel_Index == 0 :
		Temp_Dir=  args.Temp_Dir 
	else : 
		Temp_Dir= 'vfs://tmp/' + args.Temp_Dir
		
	# Read the dict file 
	JSON_Sources=JSON_Read_Arguments(args.Dict, args.Subject_Name)	 
			
	# Get the source and sink data 
	sourcedata = source_data(JSON_Sources, args.acqparams_1, args.acqparams_2, args.JHU_ICBM_Atlas, args.Number_Of_FA_Slices, args.Number_Of_Streamlines)
	sinkdata   = sink_data(args.Output_URI, args.FiberTracking, args.Get_FA_Values, args.Keep_Original, JSON_Sources ,args.query)
	
	# Build the network
	network = create_network(args.FiberTracking, args.Get_FA_Values, args.Keep_Original ,args.Min_tracking_Length , args.Number_Of_Streamlines, args.JHU_ICBM_Atlas, args.Number_Of_FA_Slices, args.Eddy_Correction_Type)

	# Print the source and the sink data if needed
	if args.Verbose:
		print 'The source data is:\n'
		print sourcedata
		print '\n'
		print 'The sink data is:\n'
		print sinkdata
		print '\n'

	# Execute the network    
	network.draw_network(name = network.id, img_format='svg',draw_dimension = True)	
	network.execute (sourcedata,sinkdata,tmpdir= Temp_Dir)
	return 0 
			
	
	
if __name__ == '__main__':
    main();

