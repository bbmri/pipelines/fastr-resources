#!/usr/bin/env python

# Copyright 2011-2014 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

IS_TEST = True


def create_network():
	
    # Import the faster environment and set it up
    import fastr
    
    # Create a new network
    network = fastr.Network(id_='FreeSurfer_Test')

    # Create a sources in the network
    Name = network.create_source(fastr.typelist['String'], id_='Name')
    
    T1_File = network.create_source(fastr.typelist['NiftiImageFile'], id_='T1_File')   

    # Create a new node in the network using toollist
    node = network.create_node(fastr.toollist['recon-all-GM-WM-parcellation'], id_="GM_MW_Pipeline", memory="8g")

    # Create a link between the sources output and inputs of the node
    node.inputs['Name'] = Name.output

    node.inputs['T1_File'] = T1_File.output
    
    node.inputs['all'] = ['-all']

    # Create a sink to save the data
    sink = network.create_sink(fastr.typelist['MgzImageFile'], id_='Output_File')

    # Link the addint node to the sink
    sink.input = node.outputs['Gray_Matter_White_Matter_Mask']

    return network


def source_data(network):
    return {'T1_File': '/mnt/cortexraid/ycaspi/Data/DTI_Data/EMC00003_20091203_1232_MRI_HERS_OMM/scans/T1/20091203_123233ERGO5s006a1001.nii.gz', 'Name': 'FreeSurfer_Test' , 'freesurfer_directory': '/mnt/cortexraid/ycaspi/Data/DTI_Data/EMC00003_20091203_1232_MRI_HERS_OMM/scans/T1/EMC00003_Second-Process'}


def sink_data(network):
    return {'Output_File': 'vfs://data/GM-WM.mgz'}


def main():
    network = create_network()

    # Execute
    network.draw_network()
    network.execute(source_data(network), sink_data(network),tmpdir= 'vfs://tmp/FreeSurfer_Tmp')


if __name__ == '__main__':
    main()
