import fastr 
import os
import argparse
import xnat
import json
import pdb #pdb.set_trace()

def source_data_XNAT(xnat, project, label, Subject_Name, Subject_Label, Aggregated_Stat_File): 
	
	sourcedata = {'XNAT_Server': {}, 'Project': {}, 'Label': {}, 'Subject_Name': {},  'Subject_Label': {}, 'Aggregated_Statistics': {} ,}
	
	sourcedata	['XNAT_Server'] = xnat
	sourcedata	['Project'] = project
	sourcedata	['Label'] = label
	sourcedata	['Subject_Name'] = Subject_Name
	sourcedata	['Subject_Label'] = Subject_Label
	sourcedata	['Aggregated_Statistics'] = Aggregated_Stat_File	
	
	return sourcedata   	

def source_data(Subject_Name, Aggregated_File_Name, Stat_File_Address): 
	
	sourcedata = {'Subject_Statistics': {}, 'Aggregated_Statistics': {} , 'Subject_Name': {},}
	
	sourcedata	['Subject_Statistics'] = Stat_File_Address
	sourcedata	['Aggregated_Statistics'] = Aggregated_File_Name
	sourcedata	['Subject_Name']  = Subject_Name

	return sourcedata 

def sink_data(temp_dir):
		
	sinkdata = {'Subject_Statistics_Script_Output': {},}
	
	sinkdata ['Subject_Statistics_Script_Output'] = temp_dir + '/Subject_Statistics_Script_Output/Subject_Statistics_Script_Output.txt'
				
	return sinkdata
	
def create_network(XNAT):
	
	network = fastr.Network(id_='Download_Statistics')
	
	Aggregated_Statistics = network.create_source(fastr.typelist['TxtFile'],id_='Aggregated_Statistics',stepid='Sources')
	Subject_Name = 	network.create_source(fastr.typelist['String'],id_='Subject_Name',stepid='Sources')
	
	Script_Results = network.create_sink (fastr.typelist['Int'],id_='Subject_Statistics_Script_Output',stepid='Sinks')		
	
	if XNAT : 
		
		Download_Stat_File = network.create_node(fastr.toollist['Read_XNAT_ICV_Stat_File'],id_='Read_XNAT_ICV_Stat_File',stepid='Download_Operation')	
		
		Subject_Label = network.create_source(fastr.typelist['String'],id_='Subject_Label',stepid='Sources')
		XNAT_Server = network.create_source(fastr.typelist['String'],id_='XNAT_Server',stepid='Sources')
		Project = network.create_source(fastr.typelist['String'],id_='Project',stepid='Sources')
		Label = network.create_source(fastr.typelist['String'],id_='Label',stepid='Sources')
		
		Subject_Name.output >> Download_Stat_File.inputs ['Subject_Name']
		Subject_Label.output >> Download_Stat_File.inputs ['Subject_Label']
		XNAT_Server.output >> Download_Stat_File.inputs ['XNAT_Server']
		Project.output >> Download_Stat_File.inputs ['Project']
		Label.output >> Download_Stat_File.inputs ['Label']
		Aggregated_Statistics.output >> Download_Stat_File.inputs ['Out_File']		
		
		Sink_link = network.create_link(Download_Stat_File.outputs['Return_Value'], Script_Results.inputs['input'])
	
	else:
		ICV_Build_Model = network.create_node(fastr.toollist['Read_ICV_Output'],id_='ICV_Stat',stepid='Main_Operation')	
		
		Subject_Statistics = network.create_source(fastr.typelist['TxtFile'],id_='Subject_Statistics',stepid='Sources')	
		
		Aggregated_Statistics.output >> ICV_Build_Model.inputs ['Out_File']		
		Subject_Statistics.output >> ICV_Build_Model.inputs ['Stat_File']
		Subject_Name.output >> ICV_Build_Model.inputs ['Name']
		
		Sink_link = network.create_link(ICV_Build_Model.outputs['Return_Value'], Script_Results.inputs['input'])
														 
	return network
	
def pad_to_aggregated_stat_file (file_name, Subject_Name) :

	Aggregated_File_Exist = os.path.isfile(file_name) 
	
	if Aggregated_File_Exist :
		Aggregated_file = open(file_name, 'a')
		Aggregated_file.write("{}\t{}\t{}\n".format(Subject_Name, '-', '-' ))
		Aggregated_file.close()
		return 1
	else : 
		return 0

def JSON_Read_Arguments (dict_file):

	with open(dict_file) as input_file:
		data = json.load(input_file)
		input_file.close
		
	uri_source_path = data	

	return uri_source_path

def main():
	
	# Parse the program command line parameters  
	parser = argparse.ArgumentParser()
	parser.add_argument('--xnat', type=unicode, dest='xnat', default='xnat.bmia.nl', required=False, help='xnat host url')
	parser.add_argument('--project', type=unicode, dest='project', default='bbmri-wp3-dev', required=False, help='xnat project')
	parser.add_argument('--subjects', type=unicode, required=False, nargs='+', help='xnat subjects')
	parser.add_argument('--label', type=unicode, dest='label', default='ICV', required=False, help='Label to use for the xnat search')
	parser.add_argument('--Dict', type=unicode, dest='Dict', required=True, default='', help='Dictionary file with source data.')
	parser.add_argument('--Aggregated_Stat_File', dest='Aggregated_Stat_File', type=unicode, required=True, default='', help='File that will include all the aggregated statistics results')
	parser.add_argument('--Temp', type=unicode, dest='Temp_Dir',required=False, default='vfs://tmp', help='Temp directory')
	
	args = parser.parse_args()
	
	# Checking that the aggregated statistics file exist and creating it if it does not. 
	
	Aggregated_File_Exist = os.path.isfile(args.Aggregated_Stat_File) 
	
	if not Aggregated_File_Exist : 
		file = open(args.Aggregated_Stat_File, 'w+')
		file.close()
		
	Dict_File_Exist = os.path.isfile(args.Dict) 
	
	if Dict_File_Exist :
		Data=JSON_Read_Arguments (args.Dict)
	else:
		print 'Can not find the Dict file to read. Double check the dict file path'
		return 0
		
	for item in Data.iteritems():
		if args.subjects is not None :
				if item[0] not in args.subjects :
					continue
		Subject_Name=item [0]
		Subject_Label = item [1] ['subject']
		if 't1w_dicom' in item [1]:
			Read_Stat_File_Address = item [1]['t1w_dicom']
		elif 't1w_nifti' in item [1]:	
			Read_Stat_File_Address = item [1]['t1w_nifti']
		elif 't1w_minc' in item [1]:	
			Read_Stat_File_Address = item [1]['t1w_minc']
		elif 'Stat_File' in item [1]:
			Read_Stat_File_Address = item [1]['Stat_File']
			
		if args.Temp_Dir != 'vfs://tmp':
                    Temp_Dir = 'vfs://tmp' + '/' + args.Temp_Dir        + '/' + Subject_Name
                else:
                    Temp_Dir=args.Temp_Dir
		
		# Get the sink data 
		sinkdata   = sink_data(Temp_Dir)	
		
		XNAT_Path=Read_Stat_File_Address.find ('xnat://')
		
		if XNAT_Path == 0 :
			# Get the source data 
			sourcedata = source_data_XNAT(args.xnat, args.project, args.label, Subject_Label, Subject_Name, args.Aggregated_Stat_File)
			# Build the network
			XNAT = True
			network    = create_network(XNAT)			
		else : 
			Stat_File_Address = Read_Stat_File_Address
			# Get the source data 
			sourcedata = source_data(Subject_Name, args.Aggregated_Stat_File, Stat_File_Address)
			# Build the network
			XNAT = False
			network    = create_network(XNAT)
			
		# Execute the network    
		network.draw_network(name = network.id, img_format='svg',draw_dimension = True)
		network.execute (sourcedata,sinkdata,tmpdir= Temp_Dir) 	
		
		# Read the Script Output file and add a line padded with zeros if needed
		
		Script_Output_File = fastr.vfs.url_to_path(Temp_Dir) + '/Subject_Statistics_Script_Output/Subject_Statistics_Script_Output.txt'
		
		Script_Output_File_Exist = os.path.isfile(Script_Output_File) 
		
		if Script_Output_File_Exist :
			Script_file = open(Script_Output_File, 'r')
			Output=Script_file.readline()
			Script_file.close()
			if Output != '1' :
				pad_to_aggregated_stat_file (args.Aggregated_Stat_File, Subject_Name)
		else : 
			pad_to_aggregated_stat_file (args.Aggregated_Stat_File, Subject_Name)		
	
if __name__ == '__main__':
    main();

