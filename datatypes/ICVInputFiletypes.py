from fastr.datatypes import TypeGroup


class ICVInputFiletypes(TypeGroup):
    description = 'ICV Input Files Types'
    _members = frozenset(['NiftiImageFileCompressed',
	'NiftiImageFileUncompressed',
	'MincImageFile',
	'CompressedMincImageFile',
	'DicomImageFile'])
