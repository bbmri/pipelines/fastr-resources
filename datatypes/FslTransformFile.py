# Copyright 2011-2014 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from fastr.datatypes import URLType
import pandas as pd

class FslTransformFile(URLType):
    description = 'fsl transform file'
    extension = 'mat'

    def _validate(self):
        try:
            df = pd.read_csv(self.parsed_value, header=None, delim_whitespace=True)
            # print("Read file", self.parsed_value, df)
            df2 = df.select_dtypes(include=['float64'])
            # print("selected float64")
            return df.equals(df2)
        except ValueError:
            return False
        except:
            return False
