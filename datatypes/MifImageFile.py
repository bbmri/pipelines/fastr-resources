from fastr.datatypes import URLType


class MifImageFile(URLType):
    description = 'Mif Image format'
    extension = 'mif'
