from fastr.datatypes import URLType


class MgzImageFile(URLType):
    description = 'MGZ Image format'
    extension = 'mgz'
