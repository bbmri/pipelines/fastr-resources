from fastr.datatypes import URLType

class ConFile(URLType):
    description = 'FSL FEAT design contrast file'
    extension = 'con'
