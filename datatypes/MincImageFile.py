from fastr.datatypes import URLType


class MincImageFile(URLType):
    description = 'MINC Image format'
    extension = 'mnc'
