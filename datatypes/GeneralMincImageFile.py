from fastr.datatypes import TypeGroup


class GeneralMincImageFile(TypeGroup):
    description = 'MincFileGroup'
    _members = frozenset(['MincImageFile',
                          'CompressedMincImageFile'])
      
