from fastr.datatypes import TypeGroup


class MRtrixImageFile(TypeGroup):
	description = 'MRtrix Image File Types'
	_members = frozenset(['VTKMeshFile',
		'TxtFile',
		'TckImage',
		'RIBFile'])
