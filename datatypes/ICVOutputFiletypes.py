from fastr.datatypes import TypeGroup


class ICVOutputFiletypes(TypeGroup):
	description = 'ICV Output File Types'
	_members = frozenset(['NiftiImageFileCompressed',
		'NiftiImageFileUncompressed',
		'MincImageFile',
		'CompressedMincImageFile'])
