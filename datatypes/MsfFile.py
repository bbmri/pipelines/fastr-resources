from fastr.datatypes import URLType

class MsfFile(URLType):
    description = 'msf file'
    extension = 'msf'
