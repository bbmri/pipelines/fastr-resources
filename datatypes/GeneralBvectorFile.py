from fastr.datatypes import TypeGroup


class GeneralBvectorFile(TypeGroup):
    description = 'GeneralBvectorFile'
    _members = frozenset(['Eddy_BVectorFile',
                          'BVectorFile'])
      
