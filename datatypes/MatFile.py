from fastr.datatypes import URLType

class MatFile(URLType):
    description = 'FSL FEAT design matrix file'
    extension = 'mat'
