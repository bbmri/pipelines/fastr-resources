from fastr.datatypes import URLType


class PNGFile(URLType):
    description = 'png Image format'
    extension = 'png'
