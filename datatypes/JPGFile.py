from fastr.datatypes import URLType


class JPGFile(URLType):
    description = 'jpg Image format'
    extension = 'jpg'
