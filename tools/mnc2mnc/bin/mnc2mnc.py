#!/usr/bin/env python

# creates a dynamic link from a minc file to a destination

import os
import sys
import argparse
import pdb
    

def main():
	
	if len(sys.argv) !=3 :
		return 1
	else :
		Split_input = sys.argv[1].split ('/',sys.argv[1].count('/'))
		Name = Split_input[sys.argv[1].count('/')]
		dst =  sys.argv[2] + '/' + Name
		dst_exist=os.path.isfile(dst)
		if not dst_exist :
			os.symlink(sys.argv[1], dst)	
		return 0
		
if __name__ == '__main__':
    main()
