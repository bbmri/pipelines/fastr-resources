#!/usr/bin/env python
"""
Helper tool for extracting scan parameters and patient details from the dcm2nii report

Created on Sun Oct 15 10:41:30 2017

@author: marzwi
"""

import argparse
import json

def parsereport(reportfile):

    with open(reportfile,'r') as f:
        report = f.read()
    report = ('File:\t' + report.rstrip('\n')).split('\t')

    # Format the output according to the JsonCollector documentation (i.e. [value1, value2, value3])
    parameters = {}
    for n,key in enumerate(report):
        if key and key[-1]==':':
            parameters[key.rstrip(':')] = '[' + report[n + 1].rstrip()
            m = n + 2   # Check if there are more value fields, i.e. key\tvalue\tvalue[..]
            while m<len(report) and report[m] and report[m][-1]!=':':
                parameters[key.rstrip(':')] += ', ' + report[m].rstrip()
                m += 1
            parameters[key.rstrip(':')] += ']'

    return parameters


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Extracts scan parameters and patient details from the dcm2nii report')
    parser.add_argument('--report', type=unicode, required=True, help='The dcm2nii report file')
    parser.add_argument('--key', type=unicode, required=True, help='The name of the key for which the value(s) are to be extracted')

    args = parser.parse_args()

    parameters = parsereport(args.report)

    if args.key not in parameters.keys():
        raise ValueError('"{}" input key not found in the dcm2nii report!'.format(args.key))

    print('VALUE={}'.format(json.dumps(parameters[args.key])))
