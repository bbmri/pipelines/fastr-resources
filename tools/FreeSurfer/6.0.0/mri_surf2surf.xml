<tool id="mri_surf2surf" description="resamples one Cortical Surface onto another " name="mri_surf2surf" version="1.0">
  <authors>
    <author name="Yaron Caspi" email="y.caspi@umcutrecht.nl" url="www.neuromri.nl" />
  </authors>
  <command version="1.0" >
  <authors>
	<author name="Unknown" email="freesurfer@nmr.mgh.harvard.edu" url="nmr.mgh.harvard.edu"/>
  </authors>
   <targets>
      	<target os="linux" arch="*" bin="mri_surf2surf">
       		<modules>
                	<module> freesurfer/6.0.0 </module>
               	</modules>
    	</target>
    </targets>
    <description>
       mri_surf2surf - Resamples one Cortical Surface onto another 
    </description>
  </command>
  <interface>
  	 <inputs>
		<input id="SUBJECT_DIR" name="Freesurfer SUBJECTS_DIR directory" datatype="Directory" environ="SUBJECTS_DIR" cardinality="1" required="true"/>
		<input id="subject" name="use subject as src and target" datatype="String" prefix="--s" cardinality="1" required="false"/>
		<input id="srcsubject" name="Name of the source files to process" required="false" datatype="String" prefix="--srcsubject" cardinality="1"/>
		<input id="trgsubject" name="target subject" required="false" datatype="String" prefix="--trgsubject" cardinality="1"/>
		<input id="sval" name="path of file with input values" required="false" datatype="AnyFile" prefix="--sval" cardinality="1"/>
		<input id="sval_xyz" name="surfname : use xyz of surfname as input" required="false" prefix="--" nospace="true" cardinality="1">
			<enum>sval-xyz</enum>
		</input>
		<input id="projfrac" name="use projected xyz of surfname as input" required="false" datatype="String" prefix="--projfrac" cardinality="2"/>
		<input id="projabs" name="use projected xyz of surfname as input" required="false" datatype="String" prefix="--projabs" cardinality="2"/>
		<input id="sval_tal_xyz" name="use tal xyz of surfname as input" required="false" datatype="String" prefix="--sval-tal-xyz" cardinality="1"/>
		<input id="sval_area" name="use vertex area of surfname as input" required="false" datatype="String" prefix="--sval-area" cardinality="1"/>
		<input id="sval_annot" name="map annotation" required="false" datatype="AnnotFile" prefix="--sval-annot" cardinality="1"/>
		<input id="sval_nxyz" name="use surface normals of surfname as input" required="false" datatype="String" prefix="--sval-nxyz" cardinality="1"/>
		<input id="srcicoorder" name="when srcsubject=ico and src is .w" required="false" prefix="--" nospace="true" cardinality="1">
			<enum>srcicoorder</enum>
		</input>
		<input id="trgicoorder" name="when trgsubject=ico" required="false" prefix="--" nospace="true" cardinality="1">
			<enum>trgicoorder</enum>
		</input>
		<input id="tval_xyz" name="save tval as a surface file with source xyz (volume for geometry)" required="false" datatype="String" prefix="--tval-xyz" cardinality="1"/>
		<input id="hemi" name="hemisphere : (lh or rh) for both source and targ" required="false" prefix="--hemi" cardinality="1">
			<enum>lh</enum>
			<enum>rh</enum>
		</input>
		<input id="srchemi" name="hemisphere : (lh or rh) for source" required="false" prefix="--srchemi" cardinality="1">
			<enum>lh</enum>
			<enum>rh</enum>
		</input>
		<input id="trghemi" name="hemisphere : (lh or rh) for target" required="false" prefix="--trghemi" cardinality="1">
			<enum>lh</enum>
			<enum>rh</enum>
		</input>
		<input id="dual_hemi" name="assume source ?h.?h.surfreg file name" required="false" prefix="--" nospace="true" cardinality="1">
			<enum>dual-hemi</enum>
		</input>
		<input id="surfreg" name="source and targ surface registration (sphere.reg)" required="false" prefix="--" nospace="true" cardinality="1">
			<enum>surfreg</enum>
		</input>
		<input id="srcsurfreg" name="source surface registration (sphere.reg)" required="false" prefix="--" nospace="true" cardinality="1">
			<enum>srcsurfreg</enum>
		</input>
		
		<input id="mapmethod" name="map method" required="false" prefix="--mapmethod" cardinality="1">
			<enum>nnfr</enum>
			<enum>nnf</enum>
		</input>
		<input id="frame" name="save only nth frame (with --trg_type paint)" required="false" prefix="--" nospace="true" cardinality="1">
			<enum>frame</enum>
		</input>
		<input id="fwhm_src" name="smooth the source to value" required="false" datatype="Float" prefix="--srcsubject" cardinality="1"/>
		<input id="fwhm_trg" name="smooth the target to value" required="false" datatype="Float" prefix="--fwhm-trg" cardinality="1"/>
		<input id="nsmooth_in" name="smooth the input" required="false" datatype="Int" prefix="--nsmooth-in" cardinality="1"/>
		<input id="nsmooth_out" name="smooth the output" required="false" datatype="Int" prefix="--nsmooth-out" cardinality="1"/>
		<input id="cortex" name="use ?h.cortex.label as a smoothing mask" required="false" prefix="--" nospace="true" cardinality="1">
			<enum>cortex</enum>
		</input>
		<input id="no_cortex" name="do NOT use ?h.cortex.label as a smoothing mask (default)" required="false" prefix="--" nospace="true" cardinality="1">
			<enum>no-cortex</enum>
		</input>
		<input id="label_src" name="source smoothing mask" required="false" datatype="String" prefix="--label-src" cardinality="1"/>
		<input id="label_trg" name="target smoothing mask" required="false" datatype="String" prefix="--label-trg" cardinality="1"/>
		<input id="reshape" name="reshape output to multiple slices" required="false" prefix="--" nospace="true" cardinality="1">
			<enum>reshape</enum>
		</input>
		<input id="reshape_factor" name="reshape to Nfactor slices" required="false" datatype="Int" prefix="--reshape-factor" cardinality="1"/>
		<input id="reshape3d" name="reshape fsaverage (ico7) into 42 x 47 x 83" required="false" prefix="--" nospace="true" cardinality="1">
			<enum>reshape3d</enum>
		</input>
		<input id="synth" name="replace input with WGN" required="false" prefix="--" nospace="true" cardinality="1">
			<enum>synth</enum>
		</input>
		<input id="ones" name="replace input with 1s" required="false" prefix="--" nospace="true" cardinality="1">
			<enum>ones</enum>
		</input>
		<input id="normvar" name="rescale so that stddev=1 (good with --synth)" required="false" prefix="--" nospace="true" cardinality="1">
			<enum>normvar</enum>
		</input>
		<input id="seed" name="seed for synth (default is auto)" required="false" datatype="Int" prefix="--seed" cardinality="1"/>
		<input id="prune" name="remove any voxel that is zero in any time point (for smoothing)" required="false" prefix="--" nospace="true" cardinality="1">
			<enum>prune</enum>
		</input>
		<input id="no_prune" name="do not prune (default)" required="false" prefix="--" nospace="true" cardinality="1">
			<enum>no-prune</enum>
		</input>
	</inputs>
	<outputs>
		<output id="AnnotationFile" name="Annotation File" required="true" automatic="false" datatype="AnnotFile" prefix="--tval" cardinality="1"/>
		<output id="TargatDistFile" name="save distance from source to target vtx" required="false" automatic="false" datatype="VtxFile" prefix="--trgdist" cardinality="1"/>
	</outputs>
  </interface>
 <help>
	USAGE: mri_surf2surf 

   --srcsubject source subject
   --sval path of file with input values 
   --sval-xyz  surfname : use xyz of surfname as input 
   --projfrac surfname 0.5 : use projected xyz of surfname as input 
   --projabs  surfname 0.5 : use projected xyz of surfname as input 
   --sval-tal-xyz  surfname : use tal xyz of surfname as input 
   --sval-area surfname : use vertex area of surfname as input 
   --sval-annot annotfile : map annotation 
   --sval-nxyz surfname : use surface normals of surfname as input 
   --patch srcpatchfile targsurf ndilations
   --sfmt   source format
   --reg register.dat {volgeom} : apply register.dat to sval-xyz
   --reg-inv register.dat {volgeom} : apply inv(register.dat) to sval-xyz
   --srcicoorder when srcsubject=ico and src is .w
   --trgsubject target subject
   --trgicoorder when trgsubject=ico
   --tval path of file in which to store output values
   --tval-xyz volume: save tval as a surface file with source xyz (volume for geometry)
   --tfmt target format
   --trgdist distfile : save distance from source to target vtx
   --s subject : use subject as src and target
   --hemi       hemisphere : (lh or rh) for both source and targ
   --srchemi    hemisphere : (lh or rh) for source
   --trghemi    hemisphere : (lh or rh) for target
   --dual-hemi  : assume source ?h.?h.surfreg file name
   --surfreg    source and targ surface registration (sphere.reg)  
   --srcsurfreg source surface registration (sphere.reg)  
   --trgsurfreg target surface registration (sphere.reg)  
   --mapmethod  nnfr or nnf
   --frame      save only nth frame (with --trg_type paint)
   --fwhm-src fwhmsrc: smooth the source to fwhmsrc
   --fwhm-trg fwhmtrg: smooth the target to fwhmtrg
   --nsmooth-in N  : smooth the input
   --nsmooth-out N : smooth the output
   --cortex : use ?h.cortex.label as a smoothing mask
   --no-cortex : do NOT use ?h.cortex.label as a smoothing mask (default)
   --label-src label : source smoothing mask
   --label-trg label : target smoothing mask
   --reshape  reshape output to multiple 'slices'
   --reshape-factor Nfactor : reshape to Nfactor 'slices'
   --reshape3d : reshape fsaverage (ico7) into 42 x 47 x 83
   --split : output each frame separately
   --synth : replace input with WGN
   --ones  : replace input with 1s
   --normvar : rescale so that stddev=1 (good with --synth)
   --seed seed : seed for synth (default is auto)
   --prune - remove any voxel that is zero in any time point (for smoothing)
   --no-prune - do not prune (default)

   --reg-diff reg2 : subtract reg2 from --reg (primarily for testing)
   --rms rms.dat   : save rms of reg1-reg2 (primarily for testing)
   --rms-mask mask : only compute rms in mask (primarily for testing)

	$Id: mri_surf2surf.c,v 1.103 2015/11/05 22:07:33 greve Exp $
	
	Note: these options were not implemented in the tool definition: 
		--patch srcpatchfile targsurf ndilations
		--sfmt   source format
		--reg register.dat {volgeom} : apply register.dat to sval-xyz
		--reg-inv register.dat {volgeom} : apply inv(register.dat) to sval-xyz
		--tfmt target format
		--split : output each frame separately
		
		--reg-diff reg2 : subtract reg2 from --reg (primarily for testing)
		--rms rms.dat   : save rms of reg1-reg2 (primarily for testing)
		--rms-mask mask : only compute rms in mask (primarily for testing)

 </help> 
</tool>
