#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  recon-all-GM-WM-parcellation.py
#  
#  Copyright 2018 Yaron Caspi <ycaspi@BBMRI-II>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import os
import argparse
import subprocess
import pdb
import drmaa
#import fastr

def main():
	
	parser = argparse.ArgumentParser()
	parser.add_argument('-freesurfer_directory', type=unicode, required=True, default='', help='Freesurfer Directory')	
	parser.add_argument('-T1_File', type=unicode, required=True, default='', help='Path to Input T1w File')	
	parser.add_argument('-Name', type=unicode, required=True, default='', help='Subject Name')
	parser.add_argument('-Additional_arguments', type=unicode, required=False, default='', help='Additional Arguments for the freesurfer pipeline')

	args = parser.parse_args()
	
	Old_SGE_ROOT = os.getenv('SGE_ROOT', '')
	os.environ["SGE_ROOT"] = ""	
	
	os.environ['SUBJECTS_DIR'] = args.freesurfer_directory

	cmd = 'recon-all ' + 	'-i ' + args.T1_File + ' ' + '-subjid ' + args.Name

	p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
	
	(output, err) = p.communicate()
	 
	## Wait for date to terminate. Get return returncode ##
	p_status = p.wait()

	if (p_status != 0):
		print output
		print err
		return 1

	with drmaa.Session() as s:
		s.initialize()
	        print('Creating job template')
        	jt = s.createJobTemplate()
        	jt.remoteCommand = os.path.join(os.getcwd(), 'recon-all')
        	jt.args = ['-all', '-subjid', args.Name]
        	jt.joinFiles = True

        	jobid = s.runJob(jt)
        	print('Your job has been submitted with ID %s' % jobid)

        	retval = s.wait(jobid, drmaa.Session.TIMEOUT_WAIT_FOREVER)
        	print('Job: {0} finished with status {1}'.format(retval.jobId, retval.hasExited))

	        print('Cleaning up')
        	s.deleteJobTemplate(jt)
	
	if len(Old_SGE_ROOT) > 0:
		os.environ["SGE_ROOT"] = Old_SGE_ROOT
	return 1
	
	#fastr.execution_plugin="LinearExecution"

	#cmd= 'recon-all ' +    '-all ' +  '-subjid ' + args.Name

	#p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)

        #(output, err) = p.communicate()

        ## Wait for date to terminate. Get return returncode ##
        #p_status = p.wait()

        #if (p_status != 0):
        #        print output
        #        print err
        #        return 1

	#fastr.execution_plugin="DRMAAExecution"

	#subprocess.call(["recon-all", "-i", args.T1_File, "-subjid", args.Name])
	
	#subprocess.call(["recon-all", "-all", "-subjid", args.Name])
	
	#subprocess.call(["recon-all", "-autorecon2-inflate1", "-subjid", args.Name])
	
	return 0

if __name__ == '__main__':
     main();
