#!/bin/bash
#
# Process a T1 file using a Freesurfer recon-all pipeline
#
# Yaron Caspi 
#

T1_File=$1
Name=$2
freesurfer_directory=$3

SGE_ROOT=/usr/share/gridengine

export SUBJECTS_DIR=${freesurfer_directory}

cmd="recon-all -i ${T1_File} -subjid ${Name}"

qrsh -cwd -V ${cmd}

#recon-all -i ${T1_File} -subjid ${Name}

cmd2="recon-all -all -subjid ${Name}"

qrsh -cwd -V ${cmd2}

#recon-all -all -subjid ${Name}





