#!/bin/bash

function usage {
        echo ""
        echo "annotate a brain areas based on freesurfer recon-all results and an atlas annotation file"
        echo "Inputs (according to this order)"
        echo "<SUBJECTS_DIR> - freesurfer main subjects dir"
	echo "<Subject_Name> - The name of the subject to process"
	echo "<RH_Atlas_Path> - Path to the right hemisphere annotation file"
	echo "<LH_Atlas_Path> - Path to the right hemisphere annotation file"
	echo "<Atlas_Name> - name of the subject new annotation - STRING"
	echo "<Output_File> - MGZ file for output"
        exit 1
}

if [ $# -lt 6 ]; then # Check that number of input variables is exactly 6
        echo "Usage:"
        usage
else # assign variables 
	SUBJECTS_DIR=$1
	Subject_Name=$2
	RH_Atlas_Path=$3
	LH_Atlas_Path=$4
	Atlas_Name=$5
	Output_File=$6
fi

# Check inputs

if [ ! -d $SUBJECTS_DIR ] ; then
	echo "Freesurfer subjects directory is not valid (Environemtal variable - SUBJECTS_DIR)" >&2
	echo "Usage:"
        usage
fi

if [ ! -d $SUBJECTS_DIR$Subject_Name ] ; then
    	echo "No Subject by the name Subject_Name is found in the freesurfer SUBJECTS_DIR" >&2
	echo "Usage:"
        usage
fi

if [ ! -f $RH_Atlas_Path ] ; then
	echo "Path to Right Hemisphere Annotation File is not valid" >&2
	echo "Usage:"
        usage
fi

if [ ! -f $RH_Atlas_Path ] ; then
	echo "Path to Left Hemisphere Annotation File is not valid" >&2
	echo "Usage:"
        usage
fi

# export the $SUBJECTS_DIR variable

export SUBJECTS_DIR=$SUBJECTS_DIR 

# run the mri_surf2surf_Commands

mri_surf2surf --srcsubject fsaverage --trgsubject $Subject_Name --hemi rh --sval-annot $RH_Atlas_Path --tval $SUBJECTS_DIR/$Subject_Name/label/rh.$Atlas_Name.annot

mri_surf2surf --srcsubject fsaverage --trgsubject $Subject_Name --hemi lh --sval-annot $LH_Atlas_Path --tval $SUBJECTS_DIR/$Subject_Name/label/lh.$Atlas_Name.annot

# Run mri_aprac2asag to create the annotation file for thatsubject using the input atlas. 

mri_aparc2aseg --old-ribbon --s $Subject_Name --annot $Atlas_Name --o $Output_File

