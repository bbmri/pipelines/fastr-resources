#!/bin/sh

ICA_DIR=$1
MASK=$2
OUTPUT_FILE=$3

fslmerge -t $ICA_DIR/stats/tempMerge $ICA_DIR/stats/probmap_* 
fslmaths $ICA_DIR/stats/tempMerge -Tmaxn -add 1 -mas $MASK $OUTPUT_FILE
