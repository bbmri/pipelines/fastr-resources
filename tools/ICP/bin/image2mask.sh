#!/bin/bash
#function usage {
#    echo "Usage: ${BASH_SOURCE[0]} -i <input_image> -p 15 -o <output_image>"
#    echo "Error: $1"
#    exit 1
#}
#default values
while [[ $# -gt 1 ]] ; do
    key="$1"

    case $key in
        -i)
            input_image="$2"
            shift # past argument
        ;;
        -p)
            percentile="$2"
            shift # past argument
        ;;
        -o)
            output_image="$2"
            shift
        ;;
    esac
    shift # past argument or value
done
[[ -z $input_image ]] && usage "No input image is provided"
[[ -z $percentile ]] && usage "No percentile is provided"
[[ -z $output_image ]] && usage "No output image location is provided"
thresh="fslstats $input_image -P $percentile"
fslmaths $input_image -thr "$thresh" -bin $output_image
