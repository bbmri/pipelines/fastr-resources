#!/bin/bash

command=`basename $0`

usage ()
{
  cat <<EOF

$command V 0.2 - script to perform instacorr segmentation on single subject or group level

Usage: $command [options] <command>

$command -i <filename> -m <filename> -o <outdir>
$command -i <filename> -m <filename> -o <outdir> -G -s 10

Compulsory arguments
  -i <filename>         Input data a single subject nifti file.
  -m <filename>         Input mask.
  -o                    Main output directory.
Optional arguments
  -s                    Parcellation scale, ICA components (Default 5).
  -w			White matter mask for white matter regresion. It should be in the same common space as the rest of data (e.g., 2mm MNI space)
  -v			Ventricles mask for CSF regresion. It should be in the same common space as the rest of data (e.g., 2mm MNI space)
  -b 			BET MNI brain mask. Different masks for different templates can be found in $FSLDIR/data/standard/ .

EOF

  exit 1
}

nargs=$#
if [ $nargs -eq 0 ] ; then
  usage
fi

# get arguments
set -- `getopt i:m:o:b:w:v:s: $*`
result=$?
if [ $result != 0 ] ; then
  echo "What? Your arguments make no sense!"
fi

if [ $nargs -eq 0 ] || [ $result != 0 ] ; then
  usage
fi

# process arguments
scale=5

while [ $1 != -- ] ; do
  case $1 in
    -i)
	inputFile=$2;
	shift;;
    -m)
      	mask=$2;
      	shift;;
    -o)
      	outputDir=$2;
      	shift;;
    -s)
	scale=$2;
	shift;;
    -w)
	wmMask=$2;
	shift;;
    -v)
	csfMask=$2;
	shift;;
    -b)
	brainMask=$2;
	shift;;
  esac
  shift  # next flag
done
shift

# part zero; prepare subjects and create output dir structure
numberOfSubjects=1
subject[1]=$inputFile


if ! [ -d "$outputDir" ];
then
   mkdir $outputDir
   mkdir $outputDir/logs
   mkdir $outputDir/timecourses 
   mkdir $outputDir/temp
fi


numberOfMasks=1

# mask input mask with BET MNI brain mask (yes, masking of a mask with a mask) to reduce effect of CSF near border of the brain. Need to make it a bit smarter, to also work for different spaces.
if [ ${brainMask:+1} ];
then
	fslmaths $mask -mas $brainMask $outputDir/ROI
else
	fslchfiletype NIFTI_GZ $mask $outputDir/ROI.nii.gz
fi

# part one: smoothing, masking, nuisance regression, fslmeants to get timecourse

subj=`zeropad 1 4`
i=1
# nuisance regression
if [ ${wmMask:+1} ] && ! [ ${csfMask:+1} ];
then
	fslmeants -i ${subject[$i]} -m $wmMask -o $outputDir/timecourses/nuisance_subject$subj.txt
	fsl_glm -i ${subject[$i]} -d $outputDir/timecourses/nuisance_subject$subj.txt --out_res=$outputDir/temp/subject"$subj"_cleaned.nii.gz
elif [ ${csfMask:+1} ] && ! [ ${wmMask:+1} ];
then
	fslmeants -i ${subject[$i]} -m $csfMask -o $outputDir/timecourses/nuisance_subject$subj.txt
	fsl_glm -i ${subject[$i]} -d $outputDir/timecourses/nuisance_subject$subj.txt --out_res=$outputDir/temp/subject"$subj"_cleaned.nii.gz
elif [ ${wmMask:+1} ] && [ ${csfMask:+1} ];
then
	fslmeants -i ${subject[$i]} -m $wmMask -o $outputDir/timecourses/wm_subject$subj.txt
	fslmeants -i ${subject[$i]} -m $csfMask -o $outputDir/timecourses/ventricles_subject$subj.txt				
	paste $outputDir/timecourses/ventricles_subject$subj.txt $outputDir/timecourses/wm_subject$subj.txt > $outputDir/timecourses/nuisance_subject$subj.txt 
	fsl_glm -i ${subject[$i]} -d $outputDir/timecourses/nuisance_subject$subj.txt --out_res=$outputDir/temp/subject"$subj"_cleaned.nii.gz 
else
	mv ${subject[$i]} $outputDir/temp/subject"$subj"_cleaned.nii.gz
fi
# Masking
fslmaths $outputDir/temp/subject"$subj"_cleaned.nii.gz -mas $outputDir/ROI.nii.gz $outputDir/temp/subject"$subj"_cleaned.nii.gz 
fslmeants -i $outputDir/temp/subject"$subj"_cleaned.nii.gz -m $outputDir/ROI.nii.gz -o $outputDir/timecourses/subject$subj.txt


# part two, data normalisation
subj=`zeropad 1 4`
fslmaths $outputDir/temp/subject"$subj"_cleaned.nii.gz -Tstd $outputDir/temp/subject"$subj"_std
fslmaths $outputDir/temp/subject"$subj"_cleaned.nii.gz -Tmean $outputDir/temp/subject"$subj"_mean
fslmaths $outputDir/temp/subject"$subj"_cleaned.nii.gz -sub $outputDir/temp/subject"$subj"_mean -div $outputDir/temp/subject"$subj"_std -mas $outputDir/ROI.nii.gz $outputDir/temp/subject"$subj"_data_norm
rm $outputDir/temp/subject"$subj"_cleaned.nii.gz


# part three, create instacorrs
python $(dirname $0)/mm_instacorrprod.py $outputDir/temp/subject"$subj"_data_norm.nii.gz $outputDir/timecourses/subject$subj.txt $outputDir/temp/subject"$subj"_instacorr.nii.gz
rm $outputDir/temp/subject"$subj"_data_norm.nii.gz


# part four, remean and mask data
fslmaths $outputDir/temp/subject"$subj"_instacorr -add $outputDir/temp/subject"$subj"_mean -mas $outputDir/ROI.nii.gz -nan $outputDir/subject"$subj"_RSN_instacorr_remeaned_masked

# part 5, ICA
for (( scaleLoop=2 ; scaleLoop<=$scale ; scaleLoop++ ))
do
	melodic -i $outputDir/subject"$subj"_RSN_instacorr_remeaned_masked.nii.gz -o $outputDir/ICA_RSN_dim$scaleLoop --Ostats -d $scaleLoop -m $outputDir/ROI.nii.gz --report -a concat --nl=pow3
	fslmerge -t $outputDir/ICA_RSN_dim$scaleLoop/stats/tempMerge $outputDir/ICA_RSN_dim$scaleLoop/stats/probmap_* 
	fslmaths $outputDir/ICA_RSN"$index"_dim$scaleLoop/stats/tempMerge -Tmaxn -add 1 -mas $outputDir/ROI.nii.gz $outputDir/ICA_RSN_dim$scaleLoop/stats/segmentation
	rm $outputDir/ICA_RSN_dim$scaleLoop/stats/tempMerge.*
done

