# script to multiply a 4D file with a 1D vector
# it does dotproduct, without the summing


# subfunction to save images
def savefile(mydata,img, outfile):
	
	import nibabel
	
	temp_image = nibabel.Nifti1Image(mydata, img.get_affine(), img.get_header())
	nibabel.save(temp_image, outfile)


# main function that does the calculations
def do_instacorrprod(inputimage,mulvec,outfile):

	import nibabel
	import numpy as np
		
	# load image
	img = nibabel.load(inputimage)
	data = img.get_data()
	
	# normalize data
	# dataMean = np.mean(data,4)
	# dataSTD = np.std(data,4)
	# for x in range(0, data.shape[4]):
		# data[:,:,:,x] = np.subtract(data[:,:,:,x],dataMean)
		# data = np.true_divide(data[:,:,:,x],dataSTD,dtype = 'float32')
	
	# load multiplication file
	mul = np.loadtxt(mulvec)
	
	# check
	if not mul.shape[0] == data.shape[3]:
		print 'size of data, does not match size of multiplication vector:'
		print 'data:', data.shape
		print 'vector:', mul.shape
		return

	# for each column in mul
	# if mul has only 1 column
	if len(mul.shape) == 1:
	
		# normalize timecourse
		mul = mul - np.mean(mul)
		standardDev = np.std(mul)
		mul = np.true_divide(mul,standardDev,dtype = 'float32')
		
		# do matrix multiplication
		# i.e., multiply each image in the 4th dimension with its equivalent index in the mul series
		temp_data = np.multiply(data,mul,dtype = 'float32')
		# save result
		savefile(temp_data,img,outfile)
		
	# if mul has more columns
	else:
		for x in range(0, mul.shape[1]):
			# normalise timecourse
			vec = mul[:,x]
			vec = vec - np.mean(vec)
			standardDev = np.std(vec)
			vec = np.true_divide(vec,standardDev,dtype = 'float32')
			# do matrix multiplication
			# i.e., multiply each image in the 4th dimension with its equivalent index in the mul series
			temp_data = np.multiply(data,vec,dtype = 'float32')
			# adjust outfile name if mulvec has multiple columns
			myoutfile = outfile.split('.nii.gz')[0] + '_' + str(x) + '.nii.gz'
			# save result
			savefile(temp_data,img,myoutfile)


# if you run it from the commandline
if __name__ == "__main__":
	import sys
	do_instacorrprod(sys.argv[1],sys.argv[2],sys.argv[3])