<tool id="dwibiascorrect_fsl/6.0.1" description=" Perform B1 field inhomogeneity correction for a DWI volume series" name="dwibiascorrect" version="2.0">
  <authors>
    <author name="Yaron Caspi" />
  </authors>
  <command version="1.0" >
    <authors>
      <author name="Yaron Caspi" url="http://a.b/c" />
    </authors>
   <targets>
		<target os="linux" arch="*" bin="dwibiascorrect">
			<modules>
				<module> fsl/6.0.1  </module>
				<module> mrtrix/3.0 </module>
			</modules>
		</target>	
    </targets>
    <description>
       dwibiascorrect -  Perform B1 field inhomogeneity correction for a DWI volume series
    </description>
  </command>
  <interface>
  	 <inputs>
		<input id="fsl_Correct" name="Use FSL FAST to estimate the inhomogeneity field" required="false" order="1" datatype="String" cardinality="1"/>
		<input id="Ants" name="Use ANTS N4 to estimate the inhomogeneity field" required="false" order="1" datatype="String" cardinality="1"/>
		<input id="BVEC_File" name="BVEC File" required="false" prefix="-fslgrad" order="2" datatype="BVectorFile" cardinality="1"/>
		<input id="BVAL_File" name="BVAL File" required="false" order="3" datatype="BValueFile" cardinality="1"/> 
		<input id="Mask" name="Manually provide a mask image for bias field estimation" required="false" order="3" datatype="NiftiImageFile" cardinality="1"/> 
		
		<input id="Denoised_DWI_File" name="Input Denoised DWI File" required="true" order="4" datatype="NiftiImageFileUncompressed" cardinality="1"/>
	</inputs>
    	<outputs>
		<output id="B1_Corrected_File" name="B1 Corrected File" required="true" automatic="false" datatype="NiftiImageFileUncompressed" cardinality="1" order="5"/>
		<output id="Bias" name="Output the estimated bias field" required="false" automatic="false" datatype="NiftiImageFileUncompressed" cardinality="1" order="7" prefix="-bias"/>	
    	</outputs>
  </interface>
 <help>
	MRtrix 3.0_RC2-123-gfce21191    dwibiascorrect   bin version: 3.0_RC2-123-gfce21191


		 dwibiascorrect: Script using the MRtrix3 Python library

	SYNOPSIS

		 Perform B1 field inhomogeneity correction for a DWI volume series

	USAGE

		 dwibiascorrect [ options ] input output

			input        The input image series to be corrected

			output       The output corrected image series

	Options for the dwibiascorrect script

	  -mask MASK
		 Manually provide a mask image for bias field estimation

	  -bias BIAS
		 Output the estimated bias field

	  -ants
		 Use ANTS N4 to estimate the inhomogeneity field

	  -fsl
		 Use FSL FAST to estimate the inhomogeneity field

	  -grad GRAD
		 Pass the diffusion gradient table in MRtrix format

	  -fslgrad bvecs bvals
		 Pass the diffusion gradient table in FSL bvecs/bvals format

	Standard options

	  -continue [TempDir] [LastFile]
		 Continue the script from a previous execution; must provide the temporary
		 directory path, and the name of the last successfully-generated file

	  -force
		 Force overwrite of output files if pre-existing

	  -help
		 Display help information for the script

	  -nocleanup
	Do not delete temporary files during script, or temporary directory at
		 script completion

	  -nthreads number
		 Use this number of threads in MRtrix multi-threaded applications (0
		 disables multi-threading)

	  -tempdir /path/to/tmp/
		 Manually specify the path in which to generate the temporary directory

	  -quiet
		 Suppress all console output during script execution

	  -info
		 Display additional information and progress for every command invoked

	  -debug
		 Display additional debugging information over and above the output of -info

	AUTHOR
		 Robert E. Smith (robert.smith@florey.edu.au)

	COPYRIGHT
		 Copyright (c) 2008-2017 the MRtrix3 contributors.  This Source Code Form is
		 subject to the terms of the Mozilla Public License, v. 2.0. If a copy of
		 the MPL was not distributed with this file, you can obtain one at
		 http://mozilla.org/MPL/2.0/.  MRtrix is distributed in the hope that it
		 will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
		 of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  For more details,
		 see http://www.mrtrix.org/.

	REFERENCES

		 * If using -fast option:
		 Zhang, Y.; Brady, M. and Smith, S. Segmentation of brain MR images through a
		 hidden Markov random field model and the expectation-maximization
		 algorithm. IEEE Transactions on Medical Imaging, 2001, 20, 45-57

		 * If using -fast option:
		 Smith, S. M.; Jenkinson, M.; Woolrich, M. W.; Beckmann, C. F.; Behrens, T.
		 E.; Johansen-Berg, H.; Bannister, P. R.; De Luca, M.; Drobnjak, I.;
		 Flitney, D. E.; Niazy, R. K.; Saunders, J.; Vickers, J.; Zhang, Y.; De
		 Stefano, N.; Brady, J. M. and Matthews, P. M. Advances in functional and
		 structural MR image analysis and implementation as FSL. NeuroImage, 2004,
		 23, S208-S219

		 * If using -ants option:
		 Tustison, N.; Avants, B.; Cook, P.; Zheng, Y.; Egan, A.; Yushkevich, P. and
		 Gee, J. N4ITK: Improved N3 Bias Correction. IEEE Transactions on Medical
		 Imaging, 2010, 29, 1310-1320
 </help> 
</tool>
