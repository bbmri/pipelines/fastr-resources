#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  acqparams_file.py
#  
#  Copyright 2018 Yaron Caspi <ycaspi@umcutrecht.nl>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

# import pdb #pdb.set_trace()  

import argparse

def main():
	
	parser = argparse.ArgumentParser()
	parser.add_argument('-b0slices', type=str, dest='b0slices',required=False, help='list of b=0 slices that were extracted from the original scan', default='1,2')
	parser.add_argument('-acqparams_1', type=float, nargs='+', dest='acqparams_1',required=True, help='acquisition parameters for eddy corrrection line 1')
	parser.add_argument('-acqparams_2', type=float, nargs='+', dest='acqparams_2',required=True, help='acquisition parameters for eddy corrrection line 2')
	parser.add_argument('-out', type=str, dest='out',required=True, help='acquisition parameters for eddy corrrection line 2')
	
	args = parser.parse_args()

	slices=args.b0slices.split(',')
	num_slices = len(slices)

	# Write acqparam file
	with open(args.out, 'w') as acqparams_File:
		if num_slices % 2 == 0:
			for s in args.acqparams_1:
				acqparams_File.write('%f ' % s)
			acqparams_File.write('\n')	
			for s in args.acqparams_2:
				acqparams_File.write('%f ' % s)	
		else :
			for i in range (0,num_slices-1,2) :
				for s in args.acqparams_1:
					acqparams_File.write('%f ' % s)
				acqparams_File.write('\n')
				for s in args.acqparams_2:
					acqparams_File.write('%f ' % s) 
				acqparams_File.write('\n')
			for s in args.acqparams_1:
				acqparams_File.write('%f ' % s)
						
	return 1

if __name__ == '__main__':
	main();

