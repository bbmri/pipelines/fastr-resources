#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  Write_FA_File.py
#  
#  Copyright 2018 Yaron Caspi <ycaspi@BBMRI-II>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import os
import argparse

def main():
	
	parser = argparse.ArgumentParser()
	parser.add_argument('-Out_Diffusivity_Values_File', type=unicode, required=True, default='', help='Path to Output File with List of FA values')	
	parser.add_argument('-Input_Files', type=unicode, required=True, default='', nargs='+', help='Path to Input Files with Single FA value')	
	parser.add_argument('-Name_of_Atlas', type=unicode, required=False, default='JHU-ICBM-labels-1mm', help='Name of the MW Atlas')
	args = parser.parse_args()
	
	#args.Input_Files.sort()
	print args.Input_Files	
	
	with open(args.Out_Diffusivity_Values_File, 'w') as Out_Diffusivity_Values_File:
		Out_Diffusivity_Values_File.write('Track of WM based on atlas %s \n' % args.Name_of_Atlas)	
		Out_Diffusivity_Values_File.write('# \t id \t Mean Diffusivity Value \n')
		for i in range(0,len(args.Input_Files)) :
			with open(args.Input_Files[i], 'r') as Read_File :
				FA=Read_File.read()
				Last_Separator = args.Input_Files[i].rfind('/')
				#PreLast_Separator = args.Input_Files[i].rfind('/',0,Last_Separator)
				#id = args.Input_Files[i] [PreLast_Separator+1:Last_Separator]
				ext=args.Input_Files[i].rfind('.txt')
				id = args.Input_Files[i] [Last_Separator+1:ext]
				Out_Diffusivity_Values_File.write('%d \t %s \t %s ' % (i+1, id, FA)) 
	
	return 0

if __name__ == '__main__':
     main();
