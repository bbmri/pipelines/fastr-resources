#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  Read_tbss_Threshold.py
#  
#  Copyright 2019 ycaspi <ycaspi@ycaspi-Satellite-L675>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import pdb

def main(args):
	
	Directory=args[1]
	File_Name = Directory + '/thresh.txt' 	

	with open(File_Name,'r') as fh :
		lines = fh.readlines()

	ThreshHold = float(lines [0].rstrip())
	
	return ThreshHold

if __name__ == '__main__':
    import sys
    import json
    #sys.exit(main(sys.argv))
    Threshold = main(sys.argv)
    print('Threshold={}'.format(json.dumps(Threshold)))
    #sys.exit(main(sys.argv))
