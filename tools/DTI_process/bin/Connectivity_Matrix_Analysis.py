#!/mnt/cortexraid/ycaspi/venv/bin/python
#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  Connectivity_Matrix_Analysis.sh
#  
#  Copyright 2019 ycaspi <ycaspi@umcutrecht.nl>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import csv
import numpy
import pdb
import bct
import sys
import math

def main(args):

	Warnning_MSG = ''
	
	# getting the input and output files. 
	
	InFile = args[1]
	OutFile = args[2]
	if (len(args) == 4):
		Threshold = float(args[3])
	else: 
		Threshold = 0.95
	
	# Reading the CSV file
	with open(InFile) as csvfile:
		readCSV = csv.reader(csvfile, delimiter=' ')
		Connectome_List = list(readCSV)		
	
	# Convert List to array
	for i in range (0, len(Connectome_List)):	
		Temp_List = list()
		for j in range (0, len(Connectome_List[i])):
			Temp_List.append(float(Connectome_List[i][j]))
		if i == 0 :
			Connectome_array = numpy.array(Temp_List)
		else : 		
			Temp_Connectome_array = numpy.array(Temp_List)
			Connectome_array = numpy.append(Connectome_array,Temp_Connectome_array)
			del Temp_Connectome_array
	
	# replace nan and infinite in the array with zero		
	NumNANS = 0
	Final_Array = Connectome_array.reshape(len(Connectome_List), len(Connectome_List[1]))
	for i in numpy.argwhere(numpy.isnan(Final_Array)) :
		Final_Array [i[0],i[1]]=0
		NumNANS = NumNANS + 1

	NumInFinite = 0
	for i in numpy.argwhere(numpy.isinf(Final_Array)) :
                Final_Array [i[0],i[1]]=0
		NumInFinite = NumInFinite + 1

	# Calculate Array trace
        Matrix_Trace=numpy.trace(Final_Array)
        Matrix_Trace_Str = 'Trace = '+ '\t' + str(Matrix_Trace) + '\n'

	# Set diagonal values to zero 
	for i in range (0, Final_Array.shape [0]) :
		 Final_Array[i,i] = 0

	# Check if there are nodes without connections
	Non_Connected_Nodes = False
	Sum_Of_Array_Lines = numpy.sum(Final_Array,0)
	Zero_Sum_Indx = []
	tmp = 0 
	for i in Sum_Of_Array_Lines :
		if i == 0 :
			Non_Connected_Nodes = True
			Zero_Sum_Indx = Zero_Sum_Indx + [tmp]
		tmp = tmp + 1
	Non_Connected_Nodes_Str = 'Non Connected Nodes = ' + '\t' + str(Non_Connected_Nodes) + '\n'	
	Non_Connected_Nodes_Ind_Str = 'Non Connected Nodes Indexes = ' + '\t' + str(Zero_Sum_Indx) + '\n'

	# Create binary and normalized matrixes
	Final_Array_Normalized = bct.normalize(Final_Array, copy=True)

	# Treshold the matrix
        bct.threshold_proportional(Final_Array_Normalized, Threshold, copy=False)
	
	# Create the binarized matrix 
	Final_Array_Binary = bct.binarize(Final_Array_Normalized, copy=True)

	# Create Connected matrix
	if (Non_Connected_Nodes == True):
		Tmp_matrix = numpy.delete(Final_Array_Normalized, Zero_Sum_Indx, 0)
		Final_Array_Normalized_Connected = numpy.delete(Tmp_matrix, Zero_Sum_Indx, 1)

	# Create the distance Array	
	Final_Array_Distance_Tmp = bct.weight_conversion(Final_Array_Normalized, 'lengths', copy=True)
	for i in numpy.argwhere(numpy.isinf(Final_Array_Distance_Tmp)) :
               Final_Array_Distance_Tmp [i[0],i[1]] = numpy.nan
	Final_Array_Distance = Final_Array_Distance_Tmp.astype('float64')
	
	# Calculate Total or Mean connectivity of the Network
	Total_Connections = 0 
	k=0
	for i in range (0, Final_Array_Normalized.shape [0]) :
		for j in range (i, Final_Array_Normalized.shape [1]) :
			Total_Connections = Total_Connections + Final_Array_Normalized[i,j]
			k = k+1
	Mean_Connections = Total_Connections/k
	Total_Connection_Str = 'Total Connections = ' + '\t' + str(Total_Connections) + '\n'
	Mean_Connections_Str = 'Mean Connections = ' + '\t' + str(Mean_Connections) + '\n'

	# Calcualte Global Path Length
	try:
		with numpy.errstate(divide='ignore'):
			(lambdaa,global_efficiency,eccentricity,radius,diameter) = bct.charpath(Final_Array_Distance)
		Lambda_Str = 'Path Length = ' + '\t' + str(lambdaa) + '\n'
		Graph_Diameter_Str = 'Graph Diameter = ' + '\t' + str(diameter) + '\n'
		if (Non_Connected_Nodes == True):
			with numpy.errstate(divide='ignore'):
				(lambdaa2,global_efficiency2,eccentricity2,radius2,diameter2) = bct.charpath(Final_Array_Normalized_Connected)
			Graph_Radius_Str = 'Graph Radius = ' + '\t' + str(radius2) + '\n'
		else:
			Graph_Radius_Str = 'Graph Radius = ' + '\t' + str(radius) + '\n'
		Eccentricity_Str = str(eccentricity)
		if (global_efficiency == numpy.inf) :
			Temp_Array = Final_Array_Distance[Final_Array_Distance != 0]
			Dv = Temp_Array[numpy.logical_not(numpy.isnan(Temp_Array))].ravel()
			Sum = numpy.sum(1/Dv)
			global_efficiency = Sum/((Final_Array_Distance.shape[0]*Final_Array_Distance.shape[1])-(Final_Array_Distance.shape[0]))
		Distance_Global_Efficiency_Str = 'Global Efficiency based on distance = ' + '\t' + str(global_efficiency) + '\n'
	except Warning as e:
		#e = sys.exc_info()[0]
		Warnning_MSG = str(e)

	# Calculate Modularity
	(Modularity_Array, Modularity)= bct.modularity_und(Final_Array_Normalized,0.5)
	Modularity_Str = 'Modularity (under 0.5 threshold) = ' + '\t' + str (Modularity)  + '\n'
	
	# Calculate Efficiency
	Eglob =  bct.efficiency_wei(Final_Array_Normalized, local=False)
	Eloc_Vec = bct.efficiency_wei(Final_Array_Normalized, local=True)  
	Eloc=Eloc_Vec.sum()/Eloc_Vec.shape[0]
	Eglob_Str = 'Global Efficiency = ' + '\t' + str(Eglob) + '\n'
	Eloc_Str = 'Local Efficiency = ' + '\t' + str(Eloc) + '\n' 		 
	
	# Calculate Transitivity
	Transitivity = bct.transitivity_bu(Final_Array_Binary) 
	Transitivity_Str = 'Transitivity = ' + '\t' + str(Transitivity) + '\n'

	# Calculate assortativity
	try:
		with numpy.errstate(divide='ignore'):
			Assortativity = bct.assortativity_wei(Final_Array_Normalized,0)
	except Warning as e:
                #e = sys.exc_info()[0]
                Warnning_MSG = str(e)
	Assortativity_str = 'Assortativity = ' + '\t' + str(Assortativity) + '\n'	

	# Calculate  Density
	(kden,Vertices, Edges) = bct.density_und(Final_Array_Normalized)
	Density_Str = 'Density = ' + '\t' + str(kden) + '\n'
	Vertices_Str = 'Number of Vertices = ' + '\t' + str(Vertices) + '\n'
	Edges_Str = 'Number of Edges = ' + '\t' + str(Edges) + '\n'

	# Calculate Density for the binary case
	(kden_bin,Vertices_bin, Edges_bin) = bct.density_und(Final_Array_Normalized)
        Bin_Density_Str = 'Binarry Density = ' + '\t' + str(kden_bin) + '\n'
        Bin_Vertices_Str = 'Number of Vertices in binary matrix = ' + '\t' + str(Vertices_bin) + '\n'
        Bin_Edges_Str = 'Number of Edges in binary matrix = ' + '\t' + str(Edges_bin) + '\n'

	# Calculate Betweenness
	Betweenness = bct.betweenness_wei(Final_Array_Normalized) 
	Mean_Betweenness = numpy.mean(Betweenness)
	Mean_Betweenness_Str = 'Mean Betweenness = ' + '\t' + str(Mean_Betweenness) + '\n'

	# Calculate participation coeffiants
	(Community_Affiliation,optimized_q_statistic) = bct.community_louvain(Final_Array_Normalized)
	try:
                with numpy.errstate(all='ignore'):
			Participation_Coefficiants = bct.participation_coef(Final_Array_Normalized, Community_Affiliation, 'undirected') 	
			Mean_Participation_Coefficiants = numpy.mean(Participation_Coefficiants)
	except Warning as e:
                #e = sys.exc_info()[0]
                Warnning_MSG = str(e)
	Mean_Participation_Coefficiants_Str = 'Mean Participation Coefficiants = ' + '\t' + str(Mean_Participation_Coefficiants) + '\n'

	# Calculate within nodal Z-score
	try:
                with numpy.errstate(all='ignore'):
			Z_score = bct.module_degree_zscore(Final_Array_Normalized, Community_Affiliation, 0)
	except Warning as e:
                #e = sys.exc_info()[0]
                Warnning_MSG = str(e)

	# Calculate Rich Club
	try:
                with numpy.errstate(all='ignore'): 
			Rich_club_coef = bct.rich_club_wu(Final_Array_Normalized, None)
	except Warning as e:
		#e = sys.exc_info()[0]
		Warnning_MSG = str(e)

	#Calculate nodal degrees 
	deg = bct.degrees_und(Final_Array_Normalized)

	# Calculate nodeal degrees for the binary case
	Binary_deg = bct.degrees_und(Final_Array_Binary)	
	
	# Calculate Clustering coefficients
        Clustering_coef = bct.clustering_coef_wu(Final_Array_Normalized)

	# Calculate the average Clustering coefficients
	k = 0
        for i in range (0, len(Clustering_coef)):
		if (deg[i] >=2.0):
                	k = k +(2*float(Clustering_coef[i]))/(float(deg[i])*(float(deg[i])-1.0))
		else : 
			k = k +(2*float(Clustering_coef[i]))/(10000000000.0*(10000000000.0-1.0))
	Weighted_Clustering_Coefficients = k/len(Clustering_coef)
        Weighted_Clustering_Coefficients_Str = 'Weighted Clustering Coefficients = ' + '\t' + str(Weighted_Clustering_Coefficients) + '\n'

	# Calculate small worldness: 
	Number_Of_Itterations = 100
	Number_Of_Random_Networks = 100
	Random_Clustering_Coefficients_List = numpy.array([])
	Random_Path_Length_List = numpy.array([])
	for i  in range (0,Number_Of_Random_Networks):
		if (Non_Connected_Nodes == False):
			(Random_Network,eff) = bct.randmio_und_connected(Final_Array_Normalized, Number_Of_Itterations)
		elif (Non_Connected_Nodes == True):
			(Random_Network,eff) = bct.randmio_und_connected(Final_Array_Normalized_Connected, Number_Of_Itterations)
		k = 0
		Random_Network_Clustering_coef_array = bct.clustering_coef_wu(Random_Network)
		Random_Network_deg = bct.degrees_und(Random_Network)
	        for i in range (0, len(Random_Network_Clustering_coef_array)):
        	        if (Random_Network_deg[i] >=2.0):
                	        k = k +(2*float(Random_Network_Clustering_coef_array[i]))/(float(Random_Network_deg[i])*(float(Random_Network_deg[i])-1.0))
               		else : 
                        	k = k +(2*float(Random_Network_Clustering_coef_array[i]))/(10000000000.0*(10000000000.0-1.0))
		Tmp_Random_Clustering_Coefficients = k/len(Clustering_coef)
		with numpy.errstate(divide='ignore'):
			Random_Network_Distance_Tmp = bct.weight_conversion(Random_Network, 'lengths', copy=True)
			for i in numpy.argwhere(numpy.isinf(Random_Network_Distance_Tmp)) :
				Random_Network_Distance_Tmp [i[0],i[1]] = numpy.nan
			Random_Network_Distance = Random_Network_Distance_Tmp.astype('float64')
			(Tmp_Path_Length, Random_global_efficiency,Random_eccentricity,Random_radius,Random_diameter) = bct.charpath(Random_Network_Distance)
		Random_Clustering_Coefficients_List = numpy.append(  Random_Clustering_Coefficients_List,[Tmp_Random_Clustering_Coefficients])
		Random_Path_Length_List = numpy.append(Random_Path_Length_List, [Tmp_Path_Length])
	Random_Clustering_Coefficients = numpy.mean(Random_Clustering_Coefficients_List)
	Random_Path_Length = numpy.mean(Random_Path_Length_List)
	Random_Clustering_Coefficients_SD = numpy.std(Random_Clustering_Coefficients_List)
	Random_Path_Length_SD = numpy.std(Random_Path_Length_List) 
	Small_worldness = (Weighted_Clustering_Coefficients/Random_Clustering_Coefficients)/(lambdaa/Random_Path_Length)
	Small_Worldness_SD = math.pow((Random_Path_Length*Random_Clustering_Coefficients_SD/Random_Clustering_Coefficients),2)
	Small_Worldness_SD = math.pow(Small_Worldness_SD + math.pow(Random_Path_Length_SD,2),0.5)
	Small_Worldness_SD = (Weighted_Clustering_Coefficients/(lambdaa*Random_Clustering_Coefficients))*Small_Worldness_SD
	Random_Clustering_Coefficients_Str = 'Random Network Weighted_Clustering_Coefficients = ' + '\t' + str(Random_Clustering_Coefficients) + '\n'
	Random_Path_Length_Str = 'Random Network Path Length = ' + '\t' + str(Random_Path_Length) + '\n'
	Small_worldness_Str = 'Small Worldness = ' + '\t' + str(Small_worldness) + '\n'
	Random_Clustering_Coefficients_SD_Str = 'Random Network Weighted_Clustering_Coefficients SD = ' + '\t' + str(Random_Clustering_Coefficients_SD) + '\n' 
	Random_Path_Length_SD_Str = 'Random Network Path Length SD = ' + '\t' + str(Random_Path_Length_SD) + '\n'
	Small_worldness_SD_Str = 'Small Worldness SD = ' + '\t' + str(Small_Worldness_SD) + '\n'

	with open(OutFile,  mode='w') as Connectome_Stat:
		Connectome_Stat.write('General Measures: \n')
		Connectome_Stat.write(str(Total_Connection_Str))
		Connectome_Stat.write(str(Mean_Connections_Str))
		Connectome_Stat.write(str(Matrix_Trace_Str))
		Connectome_Stat.write('Measures of network distance: \n')
		Connectome_Stat.write(str(Lambda_Str))
		Connectome_Stat.write(str(Distance_Global_Efficiency_Str))
                Connectome_Stat.write(str(Graph_Radius_Str))
                Connectome_Stat.write(str(Graph_Diameter_Str))
		Connectome_Stat.write(str(Modularity_Str))
		Connectome_Stat.write('Measures of network density: \n')
		Connectome_Stat.write(str(Density_Str))
                Connectome_Stat.write(str(Bin_Density_Str))
		Connectome_Stat.write(str(Vertices_Str))
                Connectome_Stat.write(str(Edges_Str))
		Connectome_Stat.write('Measures of network seggregation: \n')
		Connectome_Stat.write(str(Weighted_Clustering_Coefficients_Str))
		Connectome_Stat.write(str(Transitivity_Str))
		Connectome_Stat.write('Measures of network integration: \n')
		Connectome_Stat.write(str(Eglob_Str))
                Connectome_Stat.write(str(Eloc_Str))
		Connectome_Stat.write('Measures of network centrality: \n')
		Connectome_Stat.write(str(Mean_Betweenness_Str))
		Connectome_Stat.write(str(Mean_Participation_Coefficiants_Str))
		Connectome_Stat.write('Measures of network resilienece: \n')
		Connectome_Stat.write(str(Assortativity_str))
		Connectome_Stat.write('Measures of Network Small-Worldnessnes: \n')
		Connectome_Stat.write(str(Random_Path_Length_Str))
		Connectome_Stat.write(str(Random_Path_Length_SD_Str))
		Connectome_Stat.write(str(Random_Clustering_Coefficients_Str))
		Connectome_Stat.write(str(Random_Clustering_Coefficients_SD_Str))
		Connectome_Stat.write(str(Small_worldness_Str))	
		Connectome_Stat.write(str(Small_worldness_SD_Str))
		Connectome_Stat.write('Misc information: \n')
		Connectome_Stat.write(str(Non_Connected_Nodes_Str))
		Connectome_Stat.write(str(Non_Connected_Nodes_Ind_Str))
		Connectome_Stat.write('Number of NaNs replaced with 0 is '+ '\t' + str(NumNANS) + '\n')
                Connectome_Stat.write('Number of Infinite replaced with 0 is ' + '\t' + str(NumInFinite) + '\n')
		Connectome_Stat.write('-------------------------------- \n')
		Connectome_Stat.write('-------------------------------- \n')
		Connectome_Stat.write('Detailed measures \n')
		Connectome_Stat.write('Measures of network centrality: \n')
                Connectome_Stat.write('Within nodal Z-score \n')
                Connectome_Stat.write(str(Z_score))
                Connectome_Stat.write('\n')
		Connectome_Stat.write('Measures of network distance: \n')
		Connectome_Stat.write('Eccentricity \n')
                Connectome_Stat.write(str(Eccentricity_Str))
                Connectome_Stat.write('\n')
		Connectome_Stat.write('Modularity \n')
                Connectome_Stat.write(str(Modularity_Array))
                Connectome_Stat.write('\n')
		Connectome_Stat.write('Measures of network density: \n')
		Connectome_Stat.write('Nodes Degree \n')
                Connectome_Stat.write(str(deg))
                Connectome_Stat.write('\n')
		Connectome_Stat.write('Binary Nodes Degree \n')
                Connectome_Stat.write(str(Binary_deg))
                Connectome_Stat.write('\n')
		Connectome_Stat.write('Measures of network seggregation: \n')		
		Connectome_Stat.write('Clustering Coefficients \n')
                Connectome_Stat.write(str(Clustering_coef))
                Connectome_Stat.write('\n')
		Connectome_Stat.write('Measures of network integration: \n')
		Connectome_Stat.write('Local Efficiency \n')
		Connectome_Stat.write(str(Eloc_Vec))
		Connectome_Stat.write('\n')
		Connectome_Stat.write('Measures of network centrality: \n')				
		Connectome_Stat.write('Nodal Betweenness \n')
		Connectome_Stat.write(str(Betweenness))
		Connectome_Stat.write('\n')
		Connectome_Stat.write('Nodal Participation_Coefficiants \n')
                Connectome_Stat.write(str(Participation_Coefficiants))
                Connectome_Stat.write('\n')
		Connectome_Stat.write('Measures of network resilienece: \n')
		Connectome_Stat.write('Rich Club Coefficients \n')
                Connectome_Stat.write(str(Rich_club_coef))
                Connectome_Stat.write('\n')
	
	if (Warnning_MSG != ''):
		print Warnning_MSG	
	return 0

if __name__ == '__main__':
	import sys
	sys.exit(main(sys.argv))     
