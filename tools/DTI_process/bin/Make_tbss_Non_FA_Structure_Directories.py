#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  Make_tbss_Non_FA_Structure_Directories.py
#  
#  Copyright 2019 Yaron Caspi <ycaspi@BBMRI-II>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import os
import pdb

def makemydir(Base,Sub):
  try:
	  path= Base + '/' + Sub
	  if not os.path.isdir(path):
		os.makedirs(path)
	  return path
  except OSError:
		#logger.error(OSError)
		raise              

def main(args):
	
	In_Directory = args [3]
	Type = args [2]
	Subject_Name= args [1]
	
	New_Path = makemydir (In_Directory, Type)
	
	OriginData_Path = makemydir (New_Path, 'origdata')
	
	New_Path = makemydir (In_Directory, (Type + '_individ'))
	
	New_Path = makemydir (New_Path, Subject_Name)
	
	Stat_Path = makemydir (New_Path, 'stats')
	
	Type_Path = makemydir (New_Path, Type)
	
	return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
