#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  acqparams_file.py
#  
#  Copyright 2018 Yaron Caspi <ycaspi@umcutrecht.nl>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

# import pdb #pdb.set_trace()  


def main(args):
	
	if len(args)<4 :
		return 0
	
	In_Bval_File = args [1]
	In_Bvec_File = args [2]
	
	Out_Bval_File = args[3]
	Out_Bvec_File = args[4]
	
	# Write a new bval file:
	 
	with open(In_Bval_File, 'r') as In_Bval:
		Bval_Lines = In_Bval.readlines()
		with open(Out_Bval_File, 'w') as Out_Bval:
			Out_Bval.writelines(Bval_Lines)
	
	# Write a new bvec file:
	
	with open(In_Bvec_File, 'r') as In_Bvec:
		Bvec_Lines = In_Bvec.readlines()
		if len(Bvec_Lines)<3: 
			return 0
		with open(Out_Bvec_File, 'w') as Out_Bvec:
			for i in range(0,3):		
				Out_Bvec.writelines(Bvec_Lines[i])

	return 1

if __name__ == '__main__':
	import sys
	sys.exit(main(sys.argv))

