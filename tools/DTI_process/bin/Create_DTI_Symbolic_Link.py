#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  Create_Symbolic_Link.py
#  
#  Copyright 2019 Yaron Caspi <ycaspi@BBMRI-II>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import os
import pdb
import argparse
import json
from str2bool import str2bool
import distutils

def symboliclink(src, dst):
  try:
	  os.symlink(src, dst)
	  return True
  except OSError:
		#logger.error(OSError)
		raise   

def main(args):
	parser = argparse.ArgumentParser()

	parser.add_argument('--Directory', type=unicode, dest='Directory', required=False, default='', help='Dictionary where the symbolic link will be created.')
	parser.add_argument('--File', type=unicode, dest='File', required=True, default='', help='File to link to symbolically.')
	parser.add_argument('--Second_File', type=unicode, dest='Second_File', required=False, default='', help='Second File to link to symbolically.')
	parser.add_argument('--Extension', type=unicode, dest='Extension', required=False, default='', help='Dummmy variable for fastr tool - extension of the link file')
	parser.add_argument('--Replace', type=lambda x:bool(distutils.util.strtobool(x)), dest='Replace', required=False, default='False', help='Set Either to replace pattern in file directory to create the link or use the Directory variable to set the symbolic link.')
	parser.add_argument('--Pattern1', type=unicode, dest='Pattern1', required=False, default='', help='Pattern to search in the File Name.')
	parser.add_argument('--Pattern2', type=unicode, dest='Pattern2', required=False, default='', help='Pattern to replace in the File Name to set directory for a the symbolic link')
	args = parser.parse_args()
	
	if (args.Replace == False) :
		Directory = args.Directory
		File = args.File
	
		File_Name = os.path.basename(File)

		if (os.path.exists(Directory) == True) :	
			Dst = Directory + '/' + File_Name
			if not (os.path.lexists(Dst)):
				symboliclink(File, Dst)
			Message = File_Name
		else : 
			Message = 'Directory Does not exist please create it'
	
	else :
		File = args.File
		if (File.find (args.Pattern1) > -1) :
			Dst = File.replace (args.Pattern1, args.Pattern2)
		else : 
			Message = 'Pattern1 is not in File name - must exist'
			return Message
		Last = Dst.rfind ('/')
		Directory = Dst [0:Last]
		if (args.Second_File != ''):
			if (args.Second_File.find (args.Pattern1) > -1) :
                        	Second_Dst = args.Second_File.replace (args.Pattern1, args.Pattern2)
                	else :
                        	Message = 'Pattern1 is not in Second File name - must exist' 
				return Message
			Second_Last = Second_Dst.rfind('/')
			Second_Directory = Second_Dst [0:Second_Last]
			if (Directory != Second_Directory) : 
				Message = 'Pattern of end directory in both first and second files must be the same'
				return Message 
		if (os.path.exists(Directory) == True) :
			Dot = Dst.rfind('_')
                        Message =  Dst [Last+1:Dot]
			if (args.Second_File != ''):
				Second_Dot = Second_Dst.rfind('_')
				Second_Message = Second_Dst [Second_Last+1:Second_Dot]
				if (Message != Second_Message):
					return 'Both files should have the same basename pattern - abort'
			if not (os.path.lexists(Dst)):
				symboliclink(File, Dst)
			if (args.Second_File != ''):
				if not (os.path.lexists(Second_Dst)):
                                	symboliclink(args.Second_File, Second_Dst)
		else : 
			os.makedirs(Directory)
                        Dot = Dst.rfind('_')
                        Message =  Dst [Last+1:Dot]
			if (args.Second_File != ''):
                                Second_Dot = Second_Dst.rfind('_')
                                Second_Message = Second_Dst [Second_Last+1:Second_Dot]
                                if (Message != Second_Message):
                                        return 'Both files should have the same basename pattern - abort'
			symboliclink(File, Dst)
			if (args.Second_File != ''):
				symboliclink(args.Second_File, Second_Dst)
	
	#file = open('Message.txt','w')
        #file.write(Message)
        #file.close()

	return Message
		
if __name__ == '__main__':
	import sys
	#sys.exit(main(sys.argv))
	Message = main(sys.argv)
   	print('Message={}'.format(json.dumps(Message)))
