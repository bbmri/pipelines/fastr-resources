#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  bUniqueValues.py
#  
#  Copyright 2018 Yaron Caspi <ycaspi@umcutrecht.nl>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

import pdb #pdb.set_trace()
import itertools  

def main(args):
	
	ReturnString=''
	
	# read bval file
	with open(args [1], 'r') as bval_File:
		lines = bval_File.readlines()
	
	# split the lines	
	values = []	
	for i in range (0,len(lines)):
		tmp = set (lines[i].strip(' \n').split(' '))
		values.append(list(tmp))
		
	merged = list(itertools.chain.from_iterable(values))

	UniqueList = list(set(merged))
		
	ReturnString = ','.join(UniqueList)
		 
	return ReturnString

if __name__ == '__main__':
    import sys
    import json
    #sys.exit(main(sys.argv))
    Unique = main(sys.argv)
    print('Unique={}'.format(json.dumps(Unique)))
