#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  b0indices.py
#  
#  Copyright 2018 Yaron Caspi <ycaspi@umcutrecht.nl>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

# import pdb #pdb.set_trace()  

def main(args):
	
	# read bval file
	with open(args [1], 'r') as bval_File:
		#lines = bval_File.readlines()
		lines = bval_File.read().splitlines()
	
	# split the lines	
	values = []	
	for i in range (0,len(lines)):
			tmp=lines[i].strip()
			values.append(tmp.split(" "))
	
	# Check for values == 0		
	n = -2
	indices = []
	for i in range (0,len(values)):
		Add_Value=True
		for j in range (0,len(values[i])):
			if (j == 0) and (int(values[i][j]) <= 20): 
				indices.append(j)
				n = j
                                Add_Value = False 
			elif (int(values[i][j]) <= 20) and (Add_Value == True) :
				indices.append(j)
				n = j
				Add_Value = False
			elif (int(values[i][j]) > 20) and (Add_Value == False) and (len(indices) > 0):
				Add_Value = True
				indices.append(j-1)
				n = j
		if (int(values[i][j]) <= 20) :
			indices.append(len(values[i])-1)
	
	Seq = []
	for i in range (1,len(indices),2) :
		Seq.append(indices [i-1])
		Seq.append(indices [i] - indices [i-1] + 1)
	
	# Construct output string			
	ReturnString = ''
	for i in range(0,len(Seq)): 
		ReturnString = ReturnString + str(Seq[i]) + ' '
						
	SizeOfString = len (ReturnString)
	
	ReturnString = ReturnString[0:SizeOfString-1]					
		 
	return ReturnString

if __name__ == '__main__':
    import sys
    import json
    #sys.exit(main(sys.argv))
    Indices = main(sys.argv)
    print('Indices={}'.format(json.dumps(Indices)))
