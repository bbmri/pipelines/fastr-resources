#!/usr/bin/env python

# Collpase Several minc files to a common folder

import os.path
import sys
import glob
import pdb   

def main():
	
	Number_Of_Files = len(sys.argv)
	All_Shared = True
	
	Directory = os.path.dirname(sys.argv[1])
	
	for n in range(2, Number_Of_Files):
		if ((os.path.dirname(sys.argv[n]) != Directory) or (not(os.path.isfile(sys.argv[n]))) ) :
			All_Shared = False
	
	if All_Shared: 
		print Directory 
		return glob.glob(Directory) 
	else:
		sys.exit(1)
	

		
if __name__ == '__main__':
    main()
