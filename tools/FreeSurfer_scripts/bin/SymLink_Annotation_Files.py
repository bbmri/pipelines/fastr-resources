#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  Write_FA_File.py
#  
#  Copyright 2018 Yaron Caspi <ycaspi@BBMRI-II>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import os
import argparse
import pdb 

def main():
	
	parser = argparse.ArgumentParser()
	parser.add_argument('-subject', type=unicode, required=True, default='', help='Subject Name')	
	parser.add_argument('-SUBJECTS_DIR', type=unicode, required=True, default='', help='freesurfer SUBJECTS_DIR')	
	parser.add_argument('-lh_Annot', type=unicode, required=True, default='', help='Left hemisphere annotation file')
	parser.add_argument('-rh_Annot', type=unicode, required=True, default='', help='Right hemisphere annotation file')
	parser.add_argument('-Atlas_Name', type=unicode, required=True, default='hcpmmp1', help='Right hemisphere annotation file')

	args = parser.parse_args()
	
	# Check that all files and folders exist
	Subject_Dir = args.SUBJECTS_DIR + '/' + args.subject
	if (not os.path.isdir(args.SUBJECTS_DIR)) :
		exit ('cannot find the freesurfer SUBJECTS_DIR')
	if (not os.path.isdir(Subject_Dir)): 
		exit ('cannot find the subject directory in the freesurfer SUBJECTS_DIR environmental variable path')
	if (not os.path.isfile (args.lh_Annot)) : 
		exit ('cannot find the left hemisphere annotation file')
	if (not os.path.isfile (args.rh_Annot)) : 
		exit ('cannot find the right hemisphere annotation file')
		
	# Check that the two atlases have a common name	
	Left_Hemisphere_Atlas_Full = os.path.basename(args.lh_Annot)
	Split = Left_Hemisphere_Atlas_Full.split('.annot', 1)
        if len(Split) > 1 :
                Left_Hemisphere_Atlas = Split [1]
        else:
                Left_Hemisphere_Atlas = Split [0]
	
	Right_Hemisphere_Atlas_Full = os.path.basename(args.rh_Annot)
        Split = Right_Hemisphere_Atlas_Full.split('.annot', 1)
        if len(Split) > 1:
                Right_Hemisphere_Atlas = Split [1]
        else:
                Right_Hemisphere_Atlas = Split [0]

	Split = Right_Hemisphere_Atlas.split('rh.', Right_Hemisphere_Atlas.count('.'))
	if len(Split) > 1:
		Right_Atlas_Name = Split [1]
	else : 
		Right_Atlas_Name = Split [0]
	
	Split = Left_Hemisphere_Atlas.split('lh.', Left_Hemisphere_Atlas.count('.'))
	if len(Split) > 1:
		Left_Atlas_Name = Split [1]
	else: 
		Left_Atlas_Name = Split [0]
	
	if ((Left_Atlas_Name  != Right_Atlas_Name) or ((Left_Atlas_Name == '') and (Right_Atlas_Name == ''))):
		Left_Hemisphere_Atlas = 'lh.' + args.Atlas_Name + '.annot'
		Right_Hemisphere_Atlas = 'rh.' + args.Atlas_Name + '.annot'
		Atlas_Name = args.Atlas_Name
	#	exit('Both atlases must have the same name format lh.NAME.annot and rh.NAME.annot. Abort !')
	else : 
		Atlas_Name = Left_Atlas_Name
		
	# Delete symbolic links if needed
	Left_Hemisphere_Atlas_Link = Subject_Dir + '/label/' + Left_Hemisphere_Atlas
	Right_Hemisphere_Atlas_Link = Subject_Dir + '/label/' + Right_Hemisphere_Atlas
	
	if os.path.islink(Left_Hemisphere_Atlas_Link):
		 os.unlink(Left_Hemisphere_Atlas_Link)
	
	if os.path.islink(Right_Hemisphere_Atlas_Link):	
		os.unlink(Right_Hemisphere_Atlas_Link) 
	
	# Create the new symbolic links
	os.symlink(args.lh_Annot, Left_Hemisphere_Atlas_Link)
	os.symlink(args.rh_Annot, Right_Hemisphere_Atlas_Link)
		
	return Atlas_Name

if __name__ == '__main__':
	import sys
	import json
	Atlas_Name = main()
	print('Atlas_Name={}'.format(json.dumps(Atlas_Name)))
    #sys.exit(main(sys.argv))
     
