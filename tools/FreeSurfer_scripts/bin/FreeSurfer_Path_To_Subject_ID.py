#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  FirstB0.py
#  
#  Copyright 2019 ycaspi <ycaspi@ycaspi-Satellite-L675>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  


def main(args):
	String=args[1]
	Split=String.split ('/', String.count('/'))
	num = len(Split)-1
	if (Split [num] == '') :
		Subject_ID=Split[num-1]
	else: 
		Subject_ID=Split[num]
	num2 = String.rfind("Subject_ID")
	Path = String [0:num2]
	Details = (Path , Subject_ID) 
	return Details

if __name__ == '__main__':
	import sys
	import json
	Details = main(sys.argv)
	print('Subject_ID={}'.format(json.dumps(Details[1])))
	print('Subjects_Dir={}'.format(json.dumps(Details[0])))	
	#sys.exit(main(sys.argv))
