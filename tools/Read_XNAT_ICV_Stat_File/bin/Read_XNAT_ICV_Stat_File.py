import argparse
import sys
import os.path
import json
import xnat
import StringIO
import pdb #pdb.set_trace()

def get_stat_for_subject(xnat_subject, label, Subject_Name):
    subject_data = ''
    for experiments_id, xnat_experiment in xnat_subject.experiments.items():
        for assessor_id, xnat_assessor in xnat_experiment.assessors.items():
			if label in xnat_assessor.label :
				resource_id, xnat_resource = sorted(xnat_assessor.resources.items())[-1]
				Full_Label = Subject_Name +  '_icmask_statistics.txt'
				stat_filename = Full_Label.format(experiment=xnat_experiment.label)
				#Check if filename exists
				if stat_filename not in xnat_resource.files:
					return subject_data
				txt_file = StringIO.StringIO()
				xnat_resource.files[stat_filename].download_stream(txt_file)
				txt_file.seek(0)
				subject_data = txt_file.getvalue()
				#subject_data ['xnat_resource']= xnat_resource
				#subject_data ['stat_filename']= stat_filename
				return subject_data
				
    return subject_data    

def main():	
	parser = argparse.ArgumentParser()
	parser.add_argument('--Subject_Name', type=str, dest='Subject_Name', default='', required=False, help='Subject Name')
	parser.add_argument('--Subject_Label', type=str, dest='Subject_Label', default='', required=False, help='Subject Label')
	parser.add_argument('--XNAT_Server', type=str, dest='XNAT_Server', default='', required=False, help='XNAT Server')
	parser.add_argument('--Project', type=str, dest='Project', default='', required=False, help='XNAT Project')
	parser.add_argument('--Label', type=str, dest='Label', default='', required=False, help='Label of ICV file for filtering')
	parser.add_argument('--Out_File',  type=str, dest='Out_File', required=True,  help='Output file to use for aggregated ICV results')
	
	args = parser.parse_args()
	
	XNAT_Full_Path ='https://'+ args.XNAT_Server
	XNAT_PATH=xnat.connect(XNAT_Full_Path)
	xnat_project = XNAT_PATH.projects[args.Project]
	
	if args.Subject_Name in xnat_project.subjects:
		Full_Label = args.Subject_Label + '_' + args.Label
		subject_data = get_stat_for_subject(xnat_project.subjects[args.Subject_Name], Full_Label, args.Subject_Label)
		if subject_data == '':
			return 0
		else : 
			Lines = subject_data.split('\n')
			First_Line = Lines [0]
			Second_Line = Lines [1]
			Third_Line = Lines [2]
			
	
	XNAT_PATH.disconnect()	
	
	Name_Starts 		= First_Line.find('ID is:') 
	Vol_Pixels_Starts	= Second_Line.find('volume is') 
	Vol_MM3_Starts		= Third_Line.find('volume is') 
			
	if Name_Starts > -1 :
		Name = First_Line [Name_Starts+7:-3]
	else :
		return 0
			
	if Vol_Pixels_Starts > -1 :
		Vol_Pixels	= Second_Line [Vol_Pixels_Starts+10:-9]
	else :
		return 0
		
	if Vol_MM3_Starts > -1 :
		Vol_MM3	= Third_Line [Vol_MM3_Starts+10:-7]
	else :
		return 0
				
	Out_file_exist = os.path.isfile(args.Out_File) 
	
	if not Out_file_exist : 
		return 0
			
	with open(args.Out_File, 'a') as Out_File:
		Out_File.write("{}\t{}\t{}\n".format(args.Subject_Label, Vol_Pixels, Vol_MM3  ))
		Out_File.closed
			
	return 1	
		

if __name__ == '__main__':
	
	 #sys.exit(main())
	
	Output = main ()
	print('Success={}'.format(json.dumps(Output)))		

   
    
