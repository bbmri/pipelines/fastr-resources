#!/usr/bin/env python
import time
import xml.etree.ElementTree
import csv
import argparse
import os
import getpass
import random


def getScriptDirectory():
    return os.path.dirname(os.path.realpath(__file__))


def main(description_xml_data, csv_file_data):
    print('{}'.format(description_xml_data))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    qib_template_name = getScriptDirectory() + '/qib_template.xml_template'
    parser.add_argument("--tool", type=unicode, required=True, help="Analysis Tool name")
    parser.add_argument("--tool_version", type=unicode, required=True, help="Analyasis Tool Version")
    parser.add_argument("--description", type=unicode, required=True, help="pipeline description")

    parser.add_argument("--begin_date", type=unicode, required=True, help="Begin date/time of processing")
    parser.add_argument("--username", type=unicode, required=False, default=getpass.getuser(), help="Username")
    parser.add_argument("--processing_site", type=unicode, required=True, help="Name of processing site")

    parser.add_argument("--paper_title", type=unicode, required=False, help="Title of paper")
    parser.add_argument("--paper_link", type=unicode, required=False, help="Link to paper")
    parser.add_argument("--paper_notes", type=unicode, required=False, help="Paper notes")
    
    parser.add_argument("--csvs", type=unicode, nargs="+", required=True, help="csv_file")
    parser.add_argument("--categories", type=unicode, nargs="+", required=True, help="catagories")
    parser.add_argument("--xnat_sessions", type=unicode, nargs="+", required=False, help="The MR session where this was derived from")
    parser.add_argument("--qib_session", type=unicode, required=True, help="qib_session")
    args = parser.parse_args()

    print('tool        :{}'.format(args.tool))
    print('tool_version:{}'.format(args.tool_version))
    print('description :{}'.format(args.description))
    print('Username    :{}'.format(args.username))
    for csv_file in args.csvs:
        print('  csv_file    :{}'.format(csv_file))
    for categorie in args.categories:
        print('  categorie   :{}'.format(categorie))
    print('qib_session :{}'.format(args.qib_session))
    print('qib_template:{}'.format(qib_template_name))

    # Read QIB session template
    xml.etree.ElementTree.register_namespace('bigr', "http://www.bigr.nl/bigr")
    element_tree = xml.etree.ElementTree.parse(qib_template_name)
    root = element_tree.getroot()
    derived_results = root.find('{http://www.bigr.nl/bigr}biomarkerCategories')
    # Set general information
    root.find('{http://www.bigr.nl/bigr}analysisTool').text = args.tool
    root.find('{http://www.bigr.nl/bigr}analysisToolVersion').text = args.tool_version
    root.find('{http://www.bigr.nl/bigr}description').text = args.description

    root.find('{http://www.bigr.nl/bigr}paperURL').EMtext = args.paper_link
    root.find('{http://www.bigr.nl/bigr}paperTitle').text = 'http://' + args.paper_title
    root.find('{http://www.bigr.nl/bigr}paperNotes').text = args.paper_notes

    root.find('{http://www.bigr.nl/bigr}processingStartDateTime').text = args.begin_date
    root.find('{http://www.bigr.nl/bigr}processingEndDateTime').text = time.strftime("%Y-%m-%dT%H:%M")
    root.find('{http://www.bigr.nl/bigr}processingUserName').text = args.username
    root.find('{http://www.bigr.nl/bigr}processingSiteName').text = args.processing_site
    
    # Start Reading CSV
    for csv_file, categorie in zip(args.csvs, args.categories):
        results_category = xml.etree.ElementTree.SubElement(derived_results, 'bigr:biomarkerCategory')
        results_category.attrib['categoryName'] = categorie
        print('reading  {}:{}'.format(csv_file, categorie))
        with open(csv_file, 'r') as csvfile:
            csv_data = csv.DictReader(csvfile)
            for row in csv_data:
                for key in row:
                    biomarker_xml_list = xml.etree.ElementTree.SubElement(results_category, 'bigr:biomarkers')
                    derived_result = xml.etree.ElementTree.SubElement(biomarker_xml_list, 'bigr:biomarker')
                    derived_result.attrib['ontologyName'] = 'Uberon' # '' 
                    derived_result.attrib['ontologyIRI'] = '0000955' # ''
                    derived_result.attrib['value'] = row[key]
                    derived_result.attrib['variableDescription'] = key
                    derived_result.attrib['unit'] = key[key.rfind('(')+1:key.rfind(')')]
    # Add MR session if on is provided
    if(args.xnat_sessions):
        base_sessions = root.find('{http://www.bigr.nl/bigr}baseSessions')
        for mr_session in args.xnat_sessions:
            base_session = xml.etree.ElementTree.SubElement(base_sessions, 'bigr:baseSession')
            base_session.attrib['accessionIdentifier'] = mr_session
#            mr_session_list.set('accessionIdentifier',args.mr_session)
        # xml.etree.ElementTree.SubElement(mr_session_list, 'bigr:baseSession')


    # Write QIB Session
    element_tree.write(args.qib_session, xml_declaration=True)
