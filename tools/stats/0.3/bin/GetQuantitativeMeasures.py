import argparse
import csv
import collections
import itertools
import os

import nibabel
import numpy


def max_selection(labels):
    return collections.OrderedDict([(str(x), [x]) for x in range(numpy.max(labels) + 1)])


def minmax_selection(labels):
    return collections.OrderedDict([(str(x), [x]) for x in range(numpy.min(labels), numpy.max(labels) + 1)])


def unique_selection(labels):
    return collections.OrderedDict([(str(x), [x]) for x in numpy.unique(labels)])


def bitmask_selection(labels):
    labels = 2**numpy.arange(0, numpy.floor(numpy.log2(numpy.max(labels))))
    labels = collections.OrderedDict([(str(x), [x]) for x in labels.astype('int')])
    return labels



LABEL_METHODS = {
        'max': max_selection,
        'minmax': minmax_selection,
        'unique': unique_selection,
        'bitmask': bitmask_selection,
}


def readable_file(value):
    if not os.path.isfile(value):
        raise ValueError('{} is not a file!'.format(value))
    if not os.access(value, os.R_OK):
        raise ValueError('{} is not readable!'.format(value))
    return value


def selection_option(value):
    if value in LABEL_METHODS:
        return value

    return readable_file(value)


def read_selection(filename):
    selections = collections.OrderedDict()

    with open(filename) as csvfile:
        dialect = csv.Sniffer().sniff(csvfile.read(1024))
        csvfile.seek(0)
        reader = csv.reader(csvfile, dialect)

        for line in reader:
            selections[line[0]] = [int(x) for x in line[1:]]

    print('Area selections read: {}'.format(selections))
    return selections


def generate_selection(labels, method):
    print('Generating selection using: {}'.format(method))
    result = LABEL_METHODS[method](labels)
    print('Area selections created: {}'.format(result))
    return result

def flatten_ordered_dict(original_dict, fieldnames):
    flattened = collections.OrderedDict()
    for region, region_values in original_dict.items():
        for fieldname in fieldnames:
            flattened[region + ' ' + fieldname] = region_values[fieldname]
    return flattened


def main():
    parser = argparse.ArgumentParser('Get Quantitative Measures from image and multi label segmentation')
    parser.add_argument('-l', '--label', metavar='LABEL.nii.gz',
                        dest='label_images', type=readable_file, required=True,
                        help='Label images', nargs='+')
    parser.add_argument('-s', '--selection', metavar='SELECTION.txt',
                        dest='selections', type=selection_option, required=False,
                        help='Selection files', nargs='+', default='max')
    parser.add_argument('-i', '--image', metavar='IMAGE.nii.gz',
                        dest='image', type=readable_file, required=False,
                        help='Value image')
    parser.add_argument('--mask', metavar='MASK.nii.gz',
                        dest='mask', type=readable_file, required=False,
                        help='Mask of voxels to consider')
    parser.add_argument('-o', '--output', metavar='RESULT.csv',
                        dest='output', type=str, required=True,
                        help='Output file')
    parser.add_argument('-f', '--flatten', dest='flatten', required=False, action='store_true',
                        help='Flatten data structure to single row')
    parser.add_argument('-v', '--volume', metavar='ml',
                        dest='volume', type=str, required=False,
                        help='Unit of the volume')
    parser.add_argument('-m', '--mean', metavar='unit',
                        dest='mean', type=str, required=False,
                        help='Unit of the mean')
    parser.add_argument('--stddev', metavar='unit',
                        dest='stddev', type=str, required=False,
                        help='Unit of the stddev')
    parser.add_argument('--ddof', metavar='N', default=1,
                        dest='ddof', type=int, required=False,
                        help='ddof used for stddev (divides by N - ddof), 0 gives population std, 1 gives default sample std (see numpy.std)')
    parser.add_argument('--ratio', metavar='ratio', default=1,
                        dest='ratio', type=float, required=False, 
                        help='ratio used for volume measurements(to convert from ul to ml for example)')

    args = parser.parse_args()

    # Get label size
    label_images = [nibabel.load(x) for x in args.label_images]
    voxel_sizes = [x.header.get_zooms() for x in label_images]

    if not all(x == voxel_sizes[0] for x in voxel_sizes):
        raise ValueError('Images have varying voxel sizes, cannot combine! (Found {})'.format(voxel_sizes))

    voxel_size = voxel_sizes[0]

    # Get actual data for label iamges
    label_images = [x.get_data() for x in label_images]

    if not all(x.shape == label_images[0].shape for x in label_images):
        raise ValueError('Images have varying sizes, cannot combine! (Found {})'.format(x.shape for x in label_images))
    image_shape = label_images[0].shape

    # Get value image
    if (args.mean is not None or args.stddev is not None):
        if args.image is None:
            raise ValueError('Cannot compute the mean or standard deviation of a value image if no image is given!')

    if args.image is not None:
        value_image = nibabel.load(args.image)
        if voxel_size != value_image.header.get_zooms():
            raise ValueError('Voxel spacing of label image and value image are not consistent ({} vs {})'.format(voxel_size, value_image.header.get_zooms()))

        value_image = value_image.get_data()

        if label_images[0].shape != value_image.shape:
            raise ValueError('Size of label image and value image are not consistent ({} vs {})'.format(label_images[0].shape, value_image.shape))

    # Get mask image
    if args.mask is not None:
        mask_image = nibabel.load(args.mask)
        if voxel_size != mask_image.header.get_zooms():
            raise ValueError('Voxel spacing of label image and mask image are not consistent ({} vs {})'.format(voxel_size, mask_image.header.get_zooms()))

        mask_image = mask_image.get_data().astype('bool')

        if image_shape != mask_image.shape:
            raise ValueError('Size of label image and mask image are not consistent ({} vs {})'.format(image_shape, mask_image.shape))
    else:
        mask_image = None

    selections = args.selections

    if not isinstance(selections, list):
        selections = [selections]

    if len(selections) == 1 and len(label_images) != 1:
        selections = [selections] * len(label_images)

    # Read all the selections
    selections = [generate_selection(label_image, selection) if selection in LABEL_METHODS else read_selection(selection) for selection, label_image in zip(selections, label_images)]

    result = collections.OrderedDict()

    # Find column names to use
    name_columns = 'region name'
    volume_column = 'volume ({})'.format(args.volume) if args.volume else None
    mean_column = 'mean ({})'.format(args.mean) if args.mean else None
    stddev_column = 'stddev ({})'.format(args.stddev) if args.stddev else None

    # Prepend the None, None
    selections = [itertools.chain([(None, None)], selection.items()) for selection in selections]
    selections = list(itertools.product(*selections))

    # Sort so that first no selections, than single, than double etc... but
    # otherwise keep original order as intact as possible
    selections = [((-x.count((None, None)), index), x) for index, x in enumerate(selections)]
    selections = [x[1] for x in sorted(selections)]


    # Process all selections
    for selection in selections:
        print('Processing selection {}'.format(selection))
        row = collections.OrderedDict()

        mask = numpy.ones(label_image.shape, 'bool')

        combined_name = []

        for label_image, (name, areas), orig_selection in zip(label_images, selection, args.selections):
            print('Processing area {} ({})'.format(name, areas))
            if name is None or areas is None:
                continue

            combined_name.append(name)

            if orig_selection == 'bitmask':
                if len(areas) != 1:
                    raise ValueError('Bitmask area can only work with one value per region!')

                # Check if the bit of the label is set for this area
                mask = numpy.logical_and(mask, (label_image & areas[0] != 0))
            elif len(areas) == 1:
                mask = numpy.logical_and(mask, label_image == areas[0])
            else:
                # Select any voxel whose value is in areas
                mask.flat[numpy.logical_not(numpy.in1d(label_image.flat, areas))] = False

        # Apply the mask image
        if mask_image is not None:
            mask = numpy.logical_and(mask, mask_image)

        combined_name = ' & '.join(combined_name) if len(combined_name) > 0 else 'entire (masked) image'
        row[name_columns] = combined_name
        print('Resulting row name: {}'.format(row[name_columns]))

        if volume_column is not None:
            if args.image is not None:
                row[volume_column] = args.ratio * numpy.sum(value_image[mask]) * numpy.prod(voxel_size)
            else:
                row[volume_column] = args.ratio * numpy.sum(mask) * numpy.prod(voxel_size)

        if mean_column is not None:
            row[mean_column] = numpy.mean(value_image[mask])

        if stddev_column is not None:
            row[stddev_column] = numpy.std(value_image[mask], ddof=args.ddof)

        result[combined_name] = row

    with open(args.output, 'w') as csvfile:
        fieldnames = [name_columns, volume_column, mean_column, stddev_column]
        fieldnames = [x for x in fieldnames if x is not None]
        if args.flatten:
            flat_out = flatten_ordered_dict(result,fieldnames[1:])
            writer = csv.DictWriter(csvfile, fieldnames=flat_out.keys())
            writer.writeheader()
            writer.writerow(flat_out)
        else:
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()
            for row in result.values():
                writer.writerow(row)

if __name__ == '__main__':
    main()
