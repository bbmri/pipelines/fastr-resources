import argparse
import sys
import os.path
import json
import xnat
import StringIO
import pdb #pdb.set_trace()

def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

def main():	
	parser = argparse.ArgumentParser()
	parser.add_argument('--Subject_Name', type=str, dest='Subject_Name', default='', required=False, help='Subject Name')
	parser.add_argument('--Subject_Label', type=str, dest='Subject_Label', default='', required=False, help='Subject Label')
	parser.add_argument('--XNAT_Server', type=str, dest='XNAT_Server', default='', required=False, help='XNAT Server')
	parser.add_argument('--Project', type=str, dest='Project', default='', required=False, help='XNAT Project')
	parser.add_argument('--Label', type=str, dest='Label', default='', required=False, help='Label of ICV file for filtering')
	parser.add_argument('--Out_File',  type=str, dest='Out_File', required=True,  help='Output file to use for aggregated ICV results')
	parser.add_argument('--Delete_Files',  type=str2bool, dest='Delete_Files', required=True, default=False, help='Determine if files should be deleted or not')
	
	
	args = parser.parse_args()
	
	XNAT_Full_Path ='https://'+ args.XNAT_Server
	XNAT_PATH=xnat.connect(XNAT_Full_Path)
	xnat_project = XNAT_PATH.projects[args.Project]
	
	if args.Subject_Name in xnat_project.subjects:
		Full_Label = args.Subject_Label + '_' + args.Label
	else: 
		return 0
			
	Action_To_Do = 0	
	for experiments_id, xnat_experiment in xnat_project.subjects[args.Subject_Name].experiments.items():
		for assessor_id, xnat_assessor in xnat_experiment.assessors.items():
			if Full_Label in xnat_assessor.label :
				Action_To_Do = 1
				Number_Of_Resources = len (xnat_assessor.resources.items())
				with open(args.Out_File, 'a') as Out_File:
					Out_File.write("Subject: {} \n".format(args.Subject_Label))	
					for i in range (0, Number_Of_Resources):
						resource_id, xnat_resource = sorted(xnat_assessor.resources.items())[i]	
						if i != Number_Of_Resources-1 : 
							if args.Delete_Files :
								 Out_File.write("Deleted: {} \n".format(xnat_resource.fulluri))
								 XNAT_PATH.delete(xnat_resource.fulluri)
							else :
								 Out_File.write("XNAT MR Session: {} \t resource: {} \t Number of files= {} \n".format(args.Subject_Label, xnat_resource.label ,xnat_resource.file_count))
						else :
							 if args.Delete_Files :
							 	Out_File.write("Kept: {} \n".format(xnat_resource.fulluri))
							 else : 
								Out_File.write("XNAT MR Session: {} \t resource: {} \t Number of files= {} \n".format(args.Subject_Label, xnat_resource.label ,xnat_resource.file_count))	
					Out_File.write("{}\n".format('End of Subject Details'))			
				Out_File.closed		
			
	XNAT_PATH.disconnect()	
			
	return Action_To_Do	
		

if __name__ == '__main__':
	
	 #sys.exit(main())
	
	Output = main ()
	print('Success={}'.format(json.dumps(Output)))		

   
    
