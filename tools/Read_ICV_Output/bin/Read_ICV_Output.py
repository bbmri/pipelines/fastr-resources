import argparse
import sys
import os.path
import json

def main():	
	parser = argparse.ArgumentParser()
	parser.add_argument('--Name', type=str, dest='Name', default='', required=False, help='Subject Name')
	parser.add_argument('--Stat_File', type=str, dest='Stat_File', required=True, help='ICV Statistic file to read from')
	parser.add_argument('--Out_File',  type=str, dest='Out_File', required=True,  help='Output file to use for aggregated ICV results')
	args = parser.parse_args()
	
	Stat_file_exist = os.path.isfile(args.Stat_File) 
	
	if not Stat_file_exist : 
		return 0
	
	with open(args.Stat_File, 'r') as Read_File:
		First_Line 	= Read_File.readline()
		Second_Line = Read_File.readline()
		Third_Line = Read_File.readline()
		Read_File.closed
		
		Name_Starts 		= First_Line.find('ID is:') 
		Vol_Pixels_Starts	= Second_Line.find('volume is') 
		Vol_MM3_Starts		= Third_Line.find('volume is') 
		
		if args.Name != '':
			Name = args.Name
		else : 	
			if Name_Starts > -1 :
				Name = First_Line [Name_Starts+7:-3]
			else :
				return 0
			
		if Vol_Pixels_Starts > -1 :
			Vol_Pixels	= Second_Line [Vol_Pixels_Starts+10:-10]
		else :
			return 0
		
		if Vol_MM3_Starts > -1 :
			Vol_MM3	= Third_Line [Vol_MM3_Starts+10:-7]
		else :
			return 0
				
		Out_file_exist = os.path.isfile(args.Out_File) 
	
		if not Out_file_exist : 
			return 0	
			 
		with open(args.Out_File, 'a') as Out_File:
			Out_File.write("{}\t{}\t{}\n".format(Name, Vol_Pixels, Vol_MM3  ))
			Out_File.closed
			
		return 1	
		

if __name__ == '__main__':
	
	 #sys.exit(main())
	
	Output = main ()
	print('Success={}'.format(json.dumps(Output)))		

   
    
