<tool id="eddy_openmp" name="Correct eddy current distortions in a DTI file" version="1.0">
  <authors>
    <author name="Yaron Caspi" email="y.caspi@umcutrecht.nl" url="http://a.b/c"/>
  </authors>
  <command version="6.0.1" url="https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/eddy_openmp">
    <authors>
		<author name="Jesper Andersson"/>
    </authors>
    <targets>
		<target os="linux" arch="*" bin="eddy_openmp">
			<environment_variables>
				<OMP_NUM_THREADS>1</OMP_NUM_THREADS>
			</environment_variables>
			<modules>
				<module> fsl/6.0.1  </module>
			</modules>
		</target>
    </targets>
    <description>
		Eddy_Openmp - Correct eddy current distortions in a DTI file
    </description>
  </command>
  <interface>
    <inputs>
      <input id="imain" name="imain" datatype="NiftiImageFile" order="1" cardinality="1" nospace="true" required="true" prefix="--imain="/>
      <input id="mask" name="mask" datatype="NiftiImageFile" order="2" cardinality="1" nospace="true" required="false" prefix="--mask="/>
      <input id="index" name="index" datatype="TxtFile" order="3" cardinality="1" nospace="true" required="false" prefix="--index="/>
      <input id="acqp" name="acqp" datatype="TxtFile" order="4" cardinality="1" nospace="true" required="false" prefix="--acqp="/>
      <input id="bval" name="bval" datatype="BValueFile" order="5" cardinality="1" nospace="true" required="false" prefix="--bvals="/>
      <input id="bvec" name="bvec" datatype="BVectorFile" order="6" cardinality="1" nospace="true" required="false" prefix="--bvecs="/>
	  <input id="topup" name="topup" prefix="--topup=" datatype="String" cardinality="1" nospace="true" required="false"/>
	  <input id="flm" name="flm" prefix="--flm=" cardinality="1" nospace="true" required="false">
		<enum>linear</enum>
		<enum>quadratic</enum>
		<enum>cubic</enum> 
	  </input>
	  <input id="slm" name="slm" prefix="--slm=" cardinality="1" nospace="true" required="false">
		<enum>none</enum>
		<enum>linear</enum>
		<enum>quadratic</enum> 
	  </input>  
	  <input id="fwhm" name="fwhm" prefix="--fwhm=" datatype="Int" cardinality="1" nospace="true" required="false"/>
	  <input id="niter" name="niter" prefix="--niter=" datatype="Int" cardinality="1" nospace="true" required="false"/>
	  <input id="fep" name="fep" prefix="--fep=" datatype="Float" cardinality="1" nospace="true" required="false"/>
	  <input id="interp" name="interp" prefix="--interp=" cardinality="1" nospace="true" required="false">
		<enum>spline</enum>
		<enum>trilinear</enum>
	  </input> 
	  <input id="resamp" name="resamp" prefix="--resamp=" cardinality="1" nospace="true" required="false">
		<enum>jac</enum>
		<enum>lsr</enum>
	  </input>
	  <input id="repol" name="Detect and replace outlier slices (default false))" datatype="String" cardinality="1" required="false"/> 
	  <input id="nvoxhp" name="nvoxhp" prefix="--nvoxhp=" datatype="Int" cardinality="1" nospace="true" required="false"/>
	  <input id="ff" name="ff" prefix="--ff=" datatype="Float" cardinality="1" nospace="true" required="false"/>
	  <input id="dont_sep_offs_move" name="dont_sep_offs_move" prefix="--dont_sep_offs_move=" datatype="Boolean" cardinality="1" nospace="true" required="false"/>	
      <input id="dont_peas" name="dont_peas" prefix="--dont_peas=" datatype="Boolean" cardinality="1" nospace="true" required="false"/>		
    </inputs>
    <outputs>
		<output id="DWI_Corrected_File" name="DWI-Corrected-file" required="true" nospace="true" prefix="--out=" automatic="false" 
			datatype="NiftiImageFile" cardinality="1"/>
		<output id="Movement_RMS" name="Movement_RMS" automatic="true" required="false" datatype="UnknownFile" 
			cardinality="1" location="{output.DWI_Corrected_File[0]}.eddy_movement_rms" method="path"/>
		<output id="Outlier_Map" name="Outlier-Map" automatic="true" required="false" datatype="UnknownFile" 
			cardinality="1" location="{output.DWI_Corrected_File[0]}.eddy_outlier_map" method="path"/>
		<output id="Outlier_N_Stdev_Map" name="Outlier_N_Stdev-Map" automatic="true" required="false" datatype="UnknownFile" 
			cardinality="1" location="{output.DWI_Corrected_File[0]}.eddy_outlier_n_stdev_map" method="path"/>
		<output id="Outlier_Report" name="Outlier-Report" automatic="true" required="false" datatype="UnknownFile" 
			cardinality="1" location="{output.DWI_Corrected_File[0]}.eddy_outlier_report" method="path"/>
		<output id="Parameters" name="Parameters" automatic="true" required="false" datatype="UnknownFile" 
			cardinality="1" location="{output.DWI_Corrected_File[0]}.eddy_parameters" method="path"/>	
		<output id="Post_Eddy_Shell_Alignment_Parameters" name="Post-Eddy-Shell-Alignment-Parameters" automatic="true" required="false" datatype="UnknownFile" 
			cardinality="1" location="{output.DWI_Corrected_File[0]}.eddy_post_eddy_shell_alignment_parameters" method="path"/>
		<output id="Rotated_Bvecs" name="Rotated-Bvecs" automatic="true" required="false" datatype="Eddy_BVectorFile" 
			cardinality="1" location="{output.DWI_Corrected_File[0]}.eddy_rotated_bvecs" method="path"/>					
     </outputs>
 </interface>	
  <help>
	(fastr)-bash-4.1$ eddy_openmp 
	Part of FSL (build 509)
	eddy 
	Copyright(c) 2015, University of Oxford (Jesper Andersson)

	Usage: 
	eddy --monsoon

	Compulsory arguments (You MUST set one or more of):
		--imain	File containing all the images to estimate distortions for
		--mask	Mask to indicate brain
		--index	File containing indices for all volumes in --imain into --acqp and --topup
		--acqp	File containing acquisition parameters
		--bvecs	File containing the b-vectors for all volumes in --imain
		--bvals	File containing the b-values for all volumes in --imain
		--out	Basename for output

	Optional arguments (You may optionally specify one or more of):
		--topup	Base name for output files from topup
		--flm	First level EC model (linear/quadratic/cubic, default quadratic)
		--slm	Second level EC model (none/linear/quadratic, default none)
		--fwhm	FWHM for conditioning filter when estimating the parameters (default 0)
		--niter	Number of iterations (default 5)
		--fep	Fill empty planes in x- or y-directions
		--interp	Interpolation model for estimation step (spline/trilinear, default spline)
		--resamp	Final resampling method (jac/lsr, default jac)
		--nvoxhp	# of voxels used to estimate the hyperparameters (default 1000)
		--ff	Fudge factor for hyperparameter error variance (default 10.0)
		--dont_sep_offs_move	Do NOT attempt to separate field offset from subject movement (default false)
		--dont_peas	Do NOT perform a post-eddy alignment of shells (default false)
		-v,--verbose	switch on diagnostic messages
		-h,--help	display this message

 </help>
</tool>
