#!/bin/bash

command=`basename $0`

usage ()
{
  cat <<EOF

$command V 0.01 - This is a simple wrapper for fsl_glm command.

Usage: $command [options] <command>

$command -i <filename>  -o <outdir> -d <design>

Compulsory arguments
  	-i <filename>	Input data a single subject nifti file.
	-o              Main output directory.
	-d              File name of the GLM design matrix (text time courses for temporal regression or an image file for spatial regression).

EOF

  exit 1
}
#Optional arguments
#	-c		Matrix of t-statistics contrasts.
#	-m		Mask image file name if input is image.
#	-f		Set degrees-of-freedom explicitly.
#	-D		Switch on normalisation of the design matrix columns to unit std.
#	-N		Switch on normalisation of the data time series to unit std. deviation.
#	-M		Perform MELODIC variance-normalisation on data.
#	-E		Switch on de-meaning of design and data.
#	-h		Display this help text.
#	-B		Output GLM parameter estimates flag(GLM betas).
#	-C		Output COPEs flag.
#	-Z		Output Z-stats flag.
#	-T		Output T-stats flag.
#	-P		Output p-values of Z-stats flag.
#	-F		Output F-value of full model fit.
#	-V		Output p-value for full model fit.
#	-R		Output residuals.
#	-W		Output variance of COPEs.
#	-S		Output residual noise variance sigma-square.
#	-U		Output the pre-processed data.
#	-X		Output scaling factors for variance normalisation.

nargs=$#
if [ $nargs -eq 0 ] ; then
  usage
fi

# get arguments
set -- `getopt i:d:o: $*`
result=$?
if [ $result != 0 ] ; then
  echo "Please check your arguments."
fi

if [ $nargs -eq 0 ] || [ $result != 0 ] ; then
  usage
fi

# process arguments
while [ $1 != -- ] ; do
  case $1 in
    -i)
	inputFile=$2;
	shift;;
    -o)
      	outputDir=$2;
      	shift;;
    -d)
      	glmMatFile=$2;
      	shift;;
#    -c)
#      	contrastsMatFile=$2;
#      	shift;;
#    -m)
#      	mask=$2;
#      	shift;;
#    -f)
#      	dof=$2;
#      	shift;;
#    -D)
#	des_norm=1
#	;;
  esac
  shift  # next flag
done
shift

if ! [ -d "$outputDir" ];
then
   mkdir $outputDir
fi

fsl_glm -i $inputFile -d $glmMatFile -o $outputDir/betas.nii.gz --out_cope=$outputDir/copes.nii.gz --out_z=$outputDir/z_stats.nii.gz --out_t=$outputDir/t_stats.nii.gz --out_res=$outputDir/res.nii.gz --out_p=$outputDir/p_values.nii.gz


