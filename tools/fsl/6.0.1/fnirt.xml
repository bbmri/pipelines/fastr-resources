<tool id="fnirt" name="Non-Linear Registration tool FSL" version="1.0">
  <authors>
	<author name="Yaron Caspi" email="y.caspis@umcutrecht.nl" url="http://www.neuromri.nl"/>
  </authors>
  <command version="6.0.1" url="https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/BET">
    <authors>
		<author name="Mark Jenkinson"/>
		<author name="Steve Smith"/>
    </authors>
    <targets>
		<target os="linux"  arch="*" module="fsl/6.0.1" bin="fnirt"/>
    </targets>
    <description>
		fnirt - Non Linear Registration tool FSL
    </description>
  </command>
  <interface>
    <inputs>
		<input id="in" name="Input image" datatype="NiftiImageFile" prefix="--in=" nospace="True" cardinality="1" required="True"/>
		<input id="ref" name="Reference image" datatype="NiftiImageFile" prefix="--ref=" nospace="True" cardinality="1" required="True"/>
		<input id="aff" name="affine transform" datatype="TxtFile" description="name of file containing affine transform" prefix="--aff=" nospace="True" cardinality="1" required="False"/>
		<input id="inwarp" name="non-linear warps" datatype="TxtFile" description="name of file containing initial non-linear warps" prefix="--inwarp=" nospace="True" cardinality="1" required="False"/>
		<input id="intin" name="intensity mapping" datatype="TxtFile" description="name of file/files containing initial intensity mapping" prefix="--intin=" nospace="True" cardinality="1" required="False"/>
		<input id="config" name="config file" datatype="TxtFile" description="Name of config file specifying command line arguments" prefix="--config=" nospace="True" cardinality="1" required="False"/>
		<input id="refmask" name="Mask in reference space" datatype="NiftiImageFile" description="name of file with mask in reference space" prefix="--refmask=" nospace="True" cardinality="1" required="False"/>
		<input id="inmask" name="Mask in image space" datatype="NiftiImageFile" description="name of file with mask in input image space" prefix="--inmask=" nospace="True" cardinality="1" required="False"/>
		<input id="applyrefmask" name="reference-mask" datatype="Int" description="Use specified refmask if set, default 1 (true)" prefix="--applyrefmask=" nospace="True" cardinality="1" required="False"/>
		<input id="applyinmask" name="in-mask" datatype="Int" description="Use specified inmask if set, default 1 (true)" prefix="--applyinmask=" nospace="True" cardinality="1" required="False"/>
		<input id="imprefm" name="use implicit reference masking" datatype="Int" description="If =1, use implicit masking based on value in --ref image. Default =1 " prefix="--imprefm=" nospace="True" cardinality="1" required="False"/>
		<input id="impinm" name="use implicit masking" datatype="Int" description="If =1, use implicit masking based on value in --in image, Default =1" prefix="--impinm=" nospace="True" cardinality="1" required="False"/>
		<input id="imprefval" name="mask out in ref image" datatype="Float" description="Value to mask out in --ref image. Default =0.0" prefix="--imprefval=" nospace="True" cardinality="1" required="False"/>
		<input id="impinval" name="mask out in input image" datatype="Float" description="Value to mask out in --in image. Default =0.0" prefix="--impinval=" nospace="True" cardinality="1" required="False"/>
		<input id="minmet" name="non-linear minimisation" description="non-linear minimisation method [lm | scg] (Levenberg-Marquardt or Scaled Conjugate Gradient)" prefix="--minmet=" nospace="True" cardinality="1" required="False">
			<enum>lm</enum>
			<enum>scg</enum>
		</input>
		<input id="miter" name="non-linear iterations" datatype="String" description="Max # of non-linear iterations, default 5,5,5,5" prefix="--miter=" nospace="True" cardinality="1" required="False"/>
		<input id="subsamp" name="sub-sampling scheme" datatype="String" description="sub-sampling scheme, default 4,2,1,1" prefix="--subsamp=" nospace="True" cardinality="1" required="False"/>
		<input id="warpres" name="resolution in mm of warp basis" datatype="String" description="approximate resolution (in mm) of warp basis in x-, y- and z-direction, default 10,10,10" prefix="--warpres=" nospace="True" cardinality="1" required="False"/>
		<input id="splineorder" name="spline order" description="Order of spline, 2->Quadratic spline, 3->Cubic spline. Default=3" prefix="--splineorder=" nospace="True" cardinality="1" required="False">
			<enum>1</enum>
			<enum>2</enum>
			<enum>3</enum>
		</input>
		<input id="infwhm" name="Input FWHM Smoothing kernel" datatype="String" description="FWHM (in mm) of gaussian smoothing kernel for input volume, default 6,4,2,2" prefix="--infwhm=" nospace="True" cardinality="1" required="False"/>
		<input id="reffwhm" name="Reference FWHM Smoothing kernel" datatype="String" description="FWHM (in mm) of gaussian smoothing kernel for ref volume, default 4,2,0,0" prefix="--reffwhm=" nospace="True" cardinality="1" required="False"/>
		<input id="regmod" name="regularisation of warp-field" description="Model for regularisation of warp-field [membrane_energy bending_energy], default bending_energy" prefix="--regmod=" nospace="True" cardinality="1" required="False">
			<enum>bending_energy</enum>
			<enum>membrane_energy</enum>
		</input>
		<input id="lambda" name="Weight of Regularisation" datatype="Float" description="Weight of regularisation, default depending on --ssqlambda and --regmod switches. See user documentation." prefix="--lambda=" nospace="True" cardinality="1" required="False"/>
		<input id="ssqlambda" name="weighted by current ssq" datatype="Int" description="If set (=1), lambda is weighted by current ssq, default 1" prefix="--ssqlambda=" nospace="True" cardinality="1" required="False"/>
		<input id="jacrange" name="Range of Jacobian Determinant" datatype="String" description="Allowed range of Jacobian determinants, default 0.01,100.0" prefix="--jacrange=" nospace="True" cardinality="1" required="False"/>
		<input id="refderiv" name="Calculate derivatives" description="If =1, ref image is used to calculate derivatives. Default =0" prefix="--refderiv=" nospace="True" cardinality="1" required="False">
			<enum>0</enum>
			<enum>1</enum>
		</input>
		<input id="intmod" name="Model for intensity-mapping" description="Model for intensity-mapping [none global_linear global_non_linear local_linear global_non_linear_with_bias local_non_linear]" prefix="--intmod=" nospace="True" cardinality="1" required="False">
			<enum>none</enum>
			<enum>global_linear</enum>
			<enum>global_non_linear</enum>
			<enum>local_linear</enum>
			<enum>global_non_linear_with_bias</enum>
			<enum>local_non_linear</enum>
		</input>
		<input id="intorder" name="Order of mapping polynomial" datatype="Int" description="Order of polynomial for mapping intensities, default 5" prefix="--intorder=" nospace="True" cardinality="1" required="False"/>
		<input id="biasres" name="Resolution of bias-field" datatype="String" description="Resolution (in mm) of bias-field modelling local intensities, default 50,50,50	" prefix="--biasres=" nospace="True" cardinality="1" required="False"/>
		<input id="biaslambda" name="biaslambda" datatype="Int" description="Weight of regularisation for bias-field, default 10000" prefix="--biaslambda=" nospace="True" cardinality="1" required="False"/>
		<input id="estint" name="estint" datatype="Float" description="Estimate intensity-mapping if set, default 1 (true)" prefix="--estint=" nospace="True" cardinality="1" required="False"/>
		<input id="numprec" name="numprec" description="Precision for representing Hessian, double or float. Default double" prefix="--inumprec=" nospace="True" cardinality="1" required="False">
			<enum>Hessian</enum>
			<enum>double</enum>
			<enum>float</enum>
		</input>
		<input id="interp" name="interp" description="Image interpolation model, linear or spline. Default linear" prefix="--interp=" nospace="True" cardinality="1" required="False">
			<enum>linear</enum>
			<enum>spline</enum>
		</input>
    </inputs>
    <outputs>
		<output id="iout" name="iout" description="Non-Linear Registrated Output Image" datatype="NiftiImageFile" prefix="--iout=" cardinality="1" nospace="True" automatic="False" required="True"/>
		<output id="fout" name="fout" description="name of output file with field" datatype="NiftiImageFile" prefix="--fout=" cardinality="1" nospace="True" automatic="False" required="True"/>
		<output id="jout" name="jout" description="name of file for writing out the Jacobian of the field (for diagnostic or VBM purposes)" datatype="NiftiImageFile" prefix="--jout=" cardinality="1" nospace="True" automatic="False" required="True"/>
		<output id="refout" name="refout" description="name of file for writing out intensity modulated --ref (for diagnostic purposes)" datatype="NiftiImageFile" prefix="--refout=" cardinality="1" nospace="True" automatic="False" required="True"/>
		<output id="intout" name="intout" description="name of files for writing information pertaining to intensity mapping" datatype="NiftiImageFile" prefix="--intout=" cardinality="1" nospace="True" automatic="False" required="True"/>
		<output id="logout" name="logout" description="Name of log-file" datatype="TxtFile" prefix="--logout=" cardinality="1" nospace="True" automatic="False" required="True"/>
		<output id="cout" name="cout" description="name of output file with field coefficients" datatype="NiftiImageFile" prefix="--cout=" cardinality="1" nospace="True" automatic="False" required="True"/>
	</outputs>
  </interface>
  <cite>
    @AReoprt {Andersson10,
      author  = "JLR Andersson and  M Jenkinson and S Smith",
      title   = Non-linear registration, aka spatial normalisation",
      journal = "FMRIB technical report TR07JA2",
      year    = "2010",
    }
  </cite>
  <help>
	fnirt

	Usage: 
	fnirt --ref={some template} --in={some image}
	fnirt --ref={some template} --in={some image} --infwhm=8,4,2 --subsamp=4,2,1 --warpres=8,8,8

	  Compulsory arguments (You MUST set one or more of):
	--ref		name of reference image
	--in		name of input image

	Optional arguments (You may optionally specify one or more of):
	--aff		name of file containing affine transform
	--inwarp	name of file containing initial non-linear warps
	--intin		name of file/files containing initial intensity mapping
	--cout		name of output file with field coefficients
	--iout		name of output image
	--fout		name of output file with field
	--jout		name of file for writing out the Jacobian of the field (for diagnostic or VBM purposes)
	--refout	name of file for writing out intensity modulated --ref (for diagnostic purposes)
	--intout	name of files for writing information pertaining to intensity mapping
	--logout	Name of log-file
	--config	Name of config file specifying command line arguments
	--refmask	name of file with mask in reference space
	--inmask	name of file with mask in input image space
	--applyrefmask	Use specified refmask if set, default 1 (true)
	--applyinmask	Use specified inmask if set, default 1 (true)
	--imprefm	If =1, use implicit masking based on value in --ref image. Default =1
	--impinm	If =1, use implicit masking based on value in --in image, Default =1
	--imprefval	Value to mask out in --ref image. Default =0.0
	--impinval	Value to mask out in --in image. Default =0.0
	--minmet	non-linear minimisation method [lm | scg] (Levenberg-Marquardt or Scaled Conjugate Gradient)
	--miter		Max # of non-linear iterations, default 5,5,5,5
	--subsamp	sub-sampling scheme, default 4,2,1,1
	--warpres	(approximate) resolution (in mm) of warp basis in x-, y- and z-direction, default 10,10,10
	--splineorder	Order of spline, 2->Quadratic spline, 3->Cubic spline. Default=3
	--infwhm	FWHM (in mm) of gaussian smoothing kernel for input volume, default 6,4,2,2
	--reffwhm	FWHM (in mm) of gaussian smoothing kernel for ref volume, default 4,2,0,0
	--regmod	Model for regularisation of warp-field [membrane_energy bending_energy], default bending_energy
	--lambda	Weight of regularisation, default depending on --ssqlambda and --regmod switches. See user documentation.
	--ssqlambda	If set (=1), lambda is weighted by current ssq, default 1
	--jacrange	Allowed range of Jacobian determinants, default 0.01,100.0
	--refderiv	If =1, ref image is used to calculate derivatives. Default =0
	--intmod	Model for intensity-mapping [none global_linear global_non_linear local_linear global_non_linear_with_bias local_non_linear]
	--intorder	Order of polynomial for mapping intensities, default 5
	--biasres	Resolution (in mm) of bias-field modelling local intensities, default 50,50,50
	--biaslambda	Weight of regularisation for bias-field, default 10000
	--estint	Estimate intensity-mapping if set, default 1 (true)
	--numprec	Precision for representing Hessian, double or float. Default double
	--interp	Image interpolation model, linear or spline. Default linear
	-v,--verbose	Print diagnostic information while running
	-h,--help	display help info
  </help>
</tool>
<!--
-->
