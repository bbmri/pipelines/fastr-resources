#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  Write_Composite_PNG_File.py
#  
#  Copyright 2019 Yaron Caspi <ycaspi@BBMRI-II>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import nibabel
import sys
#import png
import pdb
import numpy
from skimage.io import imread, imsave

def main(args):
	
	# Reading arguments
	In_File_Back = args [1]
	In_File_For = args [2]
	Out_File = args [3]
	
	# loading the data from the nifti files
	BackGround_Image = nibabel.loadsave.load(In_File_Back)
	#BackGround_Image = BackGround_Image.affine
	ForGround_Image = nibabel.loadsave.load(In_File_For)
	#ForGround_Image = ForGround_Image.affine
	
	# converting the data to numpy ndarray
	BackGround_Matrix = BackGround_Image.get_data()
	ForGround_Matrix = ForGround_Image.get_data()
	
	BackGround_Matrix=numpy.rot90(BackGround_Matrix, k=-1, axes=(0,2))
	BackGround_Matrix=numpy.rot90(BackGround_Matrix, k=-2, axes=(0,1))
	BackGround_Matrix=numpy.rot90(BackGround_Matrix, k=-2, axes=(1,2))
	ForGround_Matrix=numpy.rot90(ForGround_Matrix, k=-1, axes=(0,2))
	ForGround_Matrix=numpy.rot90(ForGround_Matrix, k=-2, axes=(0,1))
	ForGround_Matrix=numpy.rot90(ForGround_Matrix, k=-2, axes=(1,2))
	
	if len(BackGround_Matrix.shape) > 4 :
		sys.exit("Cannot work with more than 4D scans.")
	elif len(BackGround_Matrix.shape) == 4 :	# Assumes the last dimension is time 
		BackGround_Matrix = BackGround_Matrix [:,:,:,1] 
		
	if len(ForGround_Matrix.shape) > 4 :
		sys.exit("Cannot work with more than 4D scans.")
	elif len(ForGround_Matrix.shape) == 4 :	# Assumes the last dimension is time 
		ForGround_Matrix = ForGround_Matrix [:,:,:,1] 
	
	(X_dim, Y_dim, Z_dim) = BackGround_Matrix.shape
	
	(X_dim_For, Y_dim_For, Z_dim_For) = ForGround_Matrix.shape
	
	if (X_dim, Y_dim, Z_dim) != (X_dim_For, Y_dim_For, Z_dim_For) : 
		sys.exit("The X, Y, Z sizes of both figures must be the same. This error can occur, e.g., if the forgraund and background scans were not registrated to atlases with the same zise - exiting.");
		
	# determine the start and end slices to construct the figure	
	for i in range(0, X_dim):
		Sum = BackGround_Matrix[i,:,:].sum (axis=1).sum ()
		if (Sum != 0):
			Min_X = i
			break
	
	for i in range (X_dim-1, 0, -1)	:
		Sum = BackGround_Matrix[i,:,:].sum (axis=1).sum ()
		if (Sum != 0):
			Max_X = i
			break
			
	for i in range(0, Y_dim):
		Sum = BackGround_Matrix[:,i,:].sum (axis=1).sum () 
		if (Sum != 0):
			Min_Y = i
			break
	
	for i in range (Y_dim-1, 0, -1) :	
		Sum = BackGround_Matrix[:,i,:].sum (axis=1).sum ()
		if (Sum != 0):
			Max_Y = i
			break	
			
	for i in range(0, Z_dim):
		Sum = BackGround_Matrix[:,:,i].sum (axis=1).sum ()
		if (Sum != 0):
			Min_Z = i
			break
	
	for i in range (Z_dim-1, 0, -1):
		Sum = BackGround_Matrix[:,:,i].sum (axis=1).sum ()
		if (Sum != 0):
			Max_Z = i
			break
			
	# setting variables for constrcting composite image 		
			
	Jump_X = 10	
	Jump_Y = 10	
	Jump_Z = 10	
			
	X_Step = int((Max_X -Jump_X - Min_X -Jump_X)/10)
	Y_Step = int((Max_Y -Jump_Y - Min_Y -Jump_Y)/10)
	Z_Step = int((Max_Z -Jump_Z - Min_Z -Jump_Z)/10)
	
	# building first row of the sagital, corronal and axial images
	Axial_1 = BackGround_Matrix [Min_X + Jump_X,:,:]
	Corronal_1 = BackGround_Matrix [:,Min_Y + Jump_Y,:]	
	Sagital_1 = BackGround_Matrix [:,:,Min_Z + Jump_Z]
	Axial_3 = ForGround_Matrix [Min_X + Jump_X,:,:]
	Corronal_3 = ForGround_Matrix [:,Min_Y + Jump_Y,:]	
	Sagital_3 = ForGround_Matrix [:,:,Min_Z + Jump_Z]
	
	#print ('Coronal dim =', Corronal_1.shape)
	#print ('Axial dim =', Axial_1.shape)
	#print ('Sagital dim =', Sagital_1.shape)
	
	Max_Y_Dim = max(Corronal_1.shape[1],Axial_1.shape[1], Sagital_1.shape[1])
	Min_Y_Dim = min(Corronal_1.shape[1],Axial_1.shape[1], Sagital_1.shape[1])
	#print ('max dim =', Max_Y_Dim)
	
	Margin_Width = 10
	
	Sagital_Margin = int((5*(float(Max_Y_Dim - Sagital_1.shape[1]))/4)+Margin_Width)
	ForGround_Sagital_Rim = numpy.zeros((Sagital_1.shape[0],Sagital_Margin), dtype=numpy.uint16)
	#ForGround_Sagital_Rim [...] = ForGround_Matrix.max()
	BackGround_Sagital_Rim = numpy.zeros((Sagital_1.shape[0],Sagital_Margin), dtype=numpy.uint16)
	
	Corronal_Margin = int((5*(float(Max_Y_Dim - Corronal_1.shape[1]))/4)+Margin_Width)
	ForGround_Corronal_Rim = numpy.zeros((Corronal_1.shape[0],Corronal_Margin), dtype=numpy.uint16)
	#ForGround_Corronal_Rim [...] = ForGround_Matrix.max()
	BackGround_Corronal_Rim = numpy.zeros((Corronal_1.shape[0],Corronal_Margin), dtype=numpy.uint16)
	
	Axial_Margin = int((5*(float(Max_Y_Dim - Axial_1.shape[1]))/4)+Margin_Width)
	ForGround_Axial_Rim = numpy.zeros((Axial_1.shape[0],Axial_Margin), dtype=numpy.uint16)
	#ForGround_Axial_Rim [...] = ForGround_Matrix.max()
	BackGround_Axial_Rim = numpy.zeros((Axial_1.shape[0],Axial_Margin), dtype=numpy.uint16)
	
	for i in range (2,6) :
		
		Axial_1 = numpy.hstack ([BackGround_Axial_Rim, Axial_1])
		Axial_1 = numpy.hstack([BackGround_Matrix [(Min_X +Jump_X)+(i*X_Step),:,:], Axial_1])
		
		Corronal_1 = numpy.hstack ([BackGround_Corronal_Rim, Corronal_1])
		Corronal_1 = numpy.hstack([BackGround_Matrix [:,(Min_Y +Jump_Y)+(i*Y_Step),:], Corronal_1])
		
		Sagital_1 = numpy.hstack ([BackGround_Sagital_Rim, Sagital_1])
		Sagital_1 = numpy.hstack([BackGround_Matrix [:,:,(Min_Z +Jump_Z)+(i*Z_Step)], Sagital_1])
		
		Axial_3 = numpy.hstack([ForGround_Axial_Rim, Axial_3])
		Axial_3 = numpy.hstack([ForGround_Matrix [(Min_X +Jump_X)+(i*X_Step),:,:], Axial_3])
		
		Corronal_3 = numpy.hstack([ForGround_Corronal_Rim, Corronal_3])
		Corronal_3 = numpy.hstack([ForGround_Matrix [:,(Min_Y +Jump_Y)+(i*Y_Step),:], Corronal_3])
		
		Sagital_3 = numpy.hstack([ForGround_Sagital_Rim, Sagital_3])
		Sagital_3 = numpy.hstack([ForGround_Matrix [:,:,(Min_Z +Jump_Z)+(i*Z_Step)], Sagital_3])
	
	# building second row of the sagital, corronal and axial images	
	Axial_2 = BackGround_Matrix [(Min_X + Jump_X)+(6*X_Step),:,:]
	Corronal_2 = BackGround_Matrix [:,(Min_Y + Jump_X)+(6*Y_Step),:]	
	Sagital_2 = BackGround_Matrix [:,:,(Min_Z + Jump_Z)+(6*Z_Step)]
	Axial_4 = ForGround_Matrix [(Min_X + Jump_X)+(6*X_Step),:,:]
	Corronal_4 = ForGround_Matrix [:,(Min_Y + Jump_X)+(6*Y_Step),:]	
	Sagital_4 = ForGround_Matrix [:,:,(Min_Z + Jump_Z)+(6*Z_Step)]
	
	for i in range (7,11) : 
		
		Axial_2 = numpy.hstack ([BackGround_Axial_Rim, Axial_2])
		Axial_2 = numpy.hstack([BackGround_Matrix [(Min_X +Jump_X)+(i*X_Step),:,:], Axial_2]) 
		
		Corronal_2 = numpy.hstack ([BackGround_Corronal_Rim, Corronal_2])
		Corronal_2 = numpy.hstack([BackGround_Matrix [:,(Min_Y +Jump_Y)+(i*Y_Step),:], Corronal_2])
		
		Sagital_2 = numpy.hstack ([BackGround_Sagital_Rim, Sagital_2])
		Sagital_2 = numpy.hstack([BackGround_Matrix [:,:,(Min_Z +Jump_Z)+(i*Z_Step)], Sagital_2])
		
		Axial_4 = numpy.hstack([ForGround_Axial_Rim, Axial_4])
		Axial_4 = numpy.hstack([ForGround_Matrix [(Min_X +Jump_X)+(i*X_Step),:,:], Axial_4]) 
		
		Corronal_4 = numpy.hstack([ForGround_Corronal_Rim,Corronal_4])
		Corronal_4 = numpy.hstack([ForGround_Matrix [:,(Min_Y +Jump_Y)+(i*Y_Step),:], Corronal_4])
		
		Sagital_4 = numpy.hstack([ForGround_Sagital_Rim, Sagital_4])
		Sagital_4 = numpy.hstack([ForGround_Matrix [:,:,(Min_Z +Jump_Z)+(i*Z_Step)], Sagital_4])
		
	# Pasting the rows	
	BackGround_Sagital = 	numpy.vstack([Sagital_2, Sagital_1])
	BackGround_Corronal =  numpy.vstack([Corronal_2, Corronal_1])
	BackGround_Axial = numpy.vstack([Axial_2, Axial_1])
	ForGround_Sagital = 	numpy.vstack([Sagital_4, Sagital_3])
	ForGround_Corronal =  numpy.vstack([Corronal_4, Corronal_3])
	ForGround_Axial = numpy.vstack([Axial_4, Axial_3])
	
	# Creating the final matrix	
	
	if max(ForGround_Corronal.shape[1],ForGround_Axial.shape[1],ForGround_Sagital.shape[1]) != min(ForGround_Corronal.shape[1],ForGround_Axial.shape[1],ForGround_Sagital.shape[1]):
				if (ForGround_Corronal.shape[1] != max(ForGround_Corronal.shape[1],ForGround_Axial.shape[1],ForGround_Sagital.shape[1])):
					ForGround_Corronal= numpy.hstack([ForGround_Corronal,numpy.zeros((ForGround_Corronal.shape[0],max(ForGround_Corronal.shape[1],ForGround_Axial.shape[1],ForGround_Sagital.shape[1])-ForGround_Corronal.shape[1]), dtype=numpy.uint16)])
				if (ForGround_Axial.shape[1] != max(ForGround_Corronal.shape[1],ForGround_Axial.shape[1],ForGround_Sagital.shape[1])):
					ForGround_Axial= numpy.hstack([ForGround_Axial,numpy.zeros((ForGround_Axial.shape[0],max(ForGround_Corronal.shape[1],ForGround_Axial.shape[1],ForGround_Sagital.shape[1])-ForGround_Axial.shape[1]), dtype=numpy.uint16)])
				if (ForGround_Sagital.shape[1] != max(ForGround_Corronal.shape[1],ForGround_Axial.shape[1],ForGround_Sagital.shape[1])):
					ForGround_Sagital= numpy.hstack([ForGround_Sagital,numpy.zeros((ForGround_Sagital.shape[0],max(ForGround_Corronal.shape[1],ForGround_Axial.shape[1],ForGround_Sagital.shape[1])-ForGround_Sagital.shape[1]), dtype=numpy.uint16)])
				if (BackGround_Corronal.shape[1] != max(ForGround_Corronal.shape[1],ForGround_Axial.shape[1],ForGround_Sagital.shape[1])):
					BackGround_Corronal= numpy.hstack([BackGround_Corronal,numpy.zeros((BackGround_Corronal.shape[0],max(ForGround_Corronal.shape[1],ForGround_Axial.shape[1],ForGround_Sagital.shape[1])-BackGround_Corronal.shape[1]), dtype=numpy.uint16)])
				if (BackGround_Axial.shape[1] != max(ForGround_Corronal.shape[1],ForGround_Axial.shape[1],ForGround_Sagital.shape[1])):
                                        BackGround_Axial= numpy.hstack([BackGround_Axial,numpy.zeros((BackGround_Axial.shape[0],max(ForGround_Corronal.shape[1],ForGround_Axial.shape[1],ForGround_Sagital.shape[1])-BackGround_Axial.shape[1]), dtype=numpy.uint16)])	
				if (BackGround_Sagital.shape[1] != max(ForGround_Corronal.shape[1],ForGround_Axial.shape[1],ForGround_Sagital.shape[1])):
                                        BackGround_Sagital= numpy.hstack([BackGround_Sagital,numpy.zeros((BackGround_Sagital.shape[0],max(ForGround_Corronal.shape[1],ForGround_Axial.shape[1],ForGround_Sagital.shape[1])-BackGround_Sagital.shape[1]), dtype=numpy.uint16)])
	
	End_Matrix_ForGround = numpy.vstack([ForGround_Corronal, ForGround_Axial])
	End_Matrix_BackGround = numpy.vstack([BackGround_Corronal, BackGround_Axial])
	End_Matrix_ForGround = numpy.vstack([ForGround_Sagital, End_Matrix_ForGround])
	End_Matrix_BackGround = numpy.vstack([BackGround_Sagital, End_Matrix_BackGround])
		
	# converting the arrays to 3d arrays with the appropriate values
	Max_Value_ForGround = End_Matrix_ForGround.max()
	if (Max_Value_ForGround<=0):
		return 1	
	for y in numpy.nditer(End_Matrix_ForGround, op_flags=['readwrite']):
		y[...] = int((float(y[...])/Max_Value_ForGround)*65535)
		
	Max_Value_BackGround = End_Matrix_BackGround.max()	
	for y in numpy.nditer(End_Matrix_BackGround, op_flags=['readwrite']):
		y[...] = int((float(y[...])/Max_Value_BackGround)*65535*0.9)
	
	# Building one RGB Matrix 
	(End_Matrix_X_Dim, End_Matrix_Y_Dim) = End_Matrix_BackGround.shape
	
	RGB_Matrix = numpy.zeros((End_Matrix_X_Dim, End_Matrix_Y_Dim ,3),dtype=numpy.uint16)
	for i in range (0, End_Matrix_X_Dim) :
		for j in range (0,End_Matrix_Y_Dim) :
			if End_Matrix_ForGround [i,j] != 0 :
				#RGB_Matrix [i,j,1] =  End_Matrix_ForGround [i,j]
				RGB_Matrix [i,j,1] =  65535
			else :
				RGB_Matrix [i,j,1] = End_Matrix_BackGround [i,j] 
				RGB_Matrix [i,j,0] = End_Matrix_BackGround [i,j] 
				RGB_Matrix [i,j,2] = End_Matrix_BackGround [i,j]  
	
	# saving the image
	imsave(Out_File, RGB_Matrix)
		
	#png.from_array(Sagital.astype(dtype=int),'L').save(Out_File)	
	#with open(Out_File, 'wb') as f:
	#	writer = png.Writer(176, 1, greyscale=True)
	#	writer.write(f, png.from_array(Sagital.astype(dtype=int),'L'))				
	
	return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))    

