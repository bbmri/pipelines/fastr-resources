#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  Connectivity_Matrix_To_Tiff.py
#  
#  Copyright 2019 ycaspi <ycaspi@ycaspi-Satellite-L675>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

def main(args):
	
	# Initiation
	import os
	import numpy as np
	import csv
	from PIL import Image
	import pdb
	#from matplotlib import ticker, cm
	import math
	
	Input_File = args [1]
	Output_File = args [2]
	#Truncated_Fraction = float(args[3])
	
	# Create LUT
	LUT = []
	#for i in range(128):
	#	LUT.extend([255-i, i, i])
		
	#for i in range(129, 256):
	#	LUT.extend([255-i, 128-((128-i)), i])	
	
	for i in range(256):
		if i>128: #LOW HALF
			j=128
			k=i
		else: 
			k=128
			j=i
		r = (k*2-255)
		g = (i)
		b = (j*2-1)
		if r<0:r=0
		if g<0:g=0
		if b<0:b=0
		if r>255:r=255
		if g>255:g=255
		if b>255:g=255
		LUT.extend([r, g, b]) 

	# Load the csv 
	with open(Input_File) as csvDataFile:
		csvMatrix = np.genfromtxt(csvDataFile, delimiter=' ')
	
	# Set the max value of the matrix to the fraction of the max values as is defined by the variable: Truncated_Fraction
	
	Max_Value = np.nanmax(csvMatrix)
	
	Min_Value = np.nanmin(csvMatrix)
	
	if Min_Value <= 0 :
		#BaseLine_Value = 0.0001
		BaseLine_Value = Max_Value/1000
	else :
		BaseLine_Value = Min_Value 
	
	#New_Max_Value = Max_Value*Truncated_Fraction
	
	#csvMatrix[csvMatrix > New_Max_Value] = New_Max_Value 
	
	with np.nditer(csvMatrix, op_flags=['readwrite']) as it:
		for x in it:
			if math.isnan(x [...]) : 
				if Max_Value > 1:
					x[...] = int(255*(math.log10((9*((Min_Value-Min_Value)/(Max_Value-Min_Value)))+1)))
				elif Max_Value <= 1 :
					x[...] = int(255*(Min_Value-Min_Value)/(Max_Value-Min_Value))
			elif x [...] <= 0 :
				if Max_Value > 1:
					x[...] = int(255*(math.log10((9*((Min_Value-Min_Value)/(Max_Value-Min_Value)))+1)))
				elif Max_Value <= 1 :
					x[...] = int(255*(x[...]-Min_Value)/(Max_Value-Min_Value))
			else:	
				if Max_Value > 1:
					x[...] = int(255*(math.log10((9*((x[...]-Min_Value)/(Max_Value-Min_Value)))+1)))
				elif Max_Value <= 1 :
					x[...] = int(255*(x[...]-Min_Value)/(Max_Value-Min_Value))
	
	# Create a 3D np array with the corresponding LUT values
	ThreeD_csvMatrix = np.empty ([len(csvMatrix[1]),len(csvMatrix),3])
	for i in range (len(csvMatrix[1])):
		for j in range (len(csvMatrix)):
			num=int(csvMatrix[i,j])
			ThreeD_csvMatrix [i,j,0] = LUT[3*num]
			ThreeD_csvMatrix [i,j,0] = LUT[3*num+1]
			ThreeD_csvMatrix [i,j,0] = LUT[3*num+2]

	image_out = Image.fromarray(ThreeD_csvMatrix.astype('uint8'))
	image_out.save(Output_File)

	return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
