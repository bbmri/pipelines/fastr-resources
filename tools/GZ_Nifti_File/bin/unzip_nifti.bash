#!/bin/bash

FilePath=$1
Directory=$2

FullFileName=$(basename -- "$FilePath")
shortfilename="${FullFileName%.*}"

NewPath=$Directory/$shortfilename

gzip -d < $FilePath > $NewPath

