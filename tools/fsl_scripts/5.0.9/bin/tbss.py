#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  tbss.py
#  
#  Copyright 2018 Yaron Caspi <ycaspi@umcutrecht.nl>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

import pdb #pdb.set_trace() 
import os
import subprocess
from shutil import copy
import argparse

def main():
	
	# Define input arguments
	
	# Parse the program command line parameters  
	parser = argparse.ArgumentParser()
	parser.add_argument('-tbss_2_reg_options', type=str, dest='tbss_2_reg_options', required=False, default='-T', help='Option for tbss_2_reg')
	parser.add_argument('-tbss_2_reg_target', type=str, dest='tbss_2_reg_target', required=False, default='', help='target for tbss_2_reg -t')
	parser.add_argument('-tbss_3_postreg_options', dest='tbss_3_postreg_options', type=unicode, required=False, default='-T', help='Option for tbss_3_postreg')
	parser.add_argument('-tbss_4_prestat_treshold', dest='tbss_4_prestat_treshold', type=float, required=False, default=0.2, help='Threshold for tbss_4_prestat')
	parser.add_argument('-Input_Files', type=unicode, dest='Input_Files', required=True, nargs='+', help='Input files to process')
	parser.add_argument('-Out_Directory', type=unicode, dest='Out_Directory', required=True, help='Output directory where files will be processed and output will be formed')
	
	args = parser.parse_args()
	
	# Initiation
	
	Directory = args.Out_Directory
	
	# Set environment variables
	
	Old_SGE_ROOT = os.getenv('SGE_ROOT', '')
	os.environ["SGE_ROOT"] = ""	
	os.environ["FSLPARALLEL"] = "0"

	# Copy the file to the directory
	#for i in range(0,len(args.Input_Files)) :
	#	copy(args.Input_Files[i], Directory)

	# Set current working directory 

	os.chdir(Directory)

	# make an FA Directory 
	
	newpath = Directory + '/FA'
	if not os.path.exists(newpath):
    		os.makedirs(newpath)

	# Copy the file to the directory
        for i in range(0,len(args.Input_Files)) :
                copy(args.Input_Files[i], newpath)

	# Run tbss_1 program
	
	#cmd = 'tbss_1_preproc *.nii.gz'  
	#cmd = 'tbss_1_preproc ' + Directory
	cmd = 'tbss_1_preproc FA' 
	print cmd
	
	## call tbss_1_preproc command ##
	p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
	
	(output, err) = p.communicate()
	 
	## Wait for date to terminate. Get return returncode ##
	p_status = p.wait()

	if (p_status != 0):
		if len(Old_SGE_ROOT) > 0:
			os.environ["SGE_ROOT"] = Old_SGE_ROOT 
		return 'tbss_1_preproc failed'
	else: 
		print output
		
	# Run tbss_2_reg program
	
	if args.tbss_2_reg_options == '-t':
		cmd = 'tbss_2_reg ' + '-t ' +  args.tbss_2_reg_target
	else: 
		cmd = 'tbss_2_reg ' + args.tbss_2_reg_options	
	
	print cmd
	## call tbss_2_reg command ##
	p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
	
	(output, err) = p.communicate()
	 
	## Wait for date to terminate. Get return returncode ##
	p_status = p.wait()

	if (p_status != 0):
		os.environ["SGE_ROOT"] = Old_SGE_ROOT
		return 'tbss_2_reg failed'
	else : 
		print output
	
	# Run tbss_3_postreg program
	
	cmd = 'tbss_3_postreg ' + args.tbss_3_postreg_options 
	print cmd
	
	## call tbss_3_postreg command ##
	p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
	
	(output, err) = p.communicate()
	 
	## Wait for date to terminate. Get return returncode ##
	p_status = p.wait()

	if (p_status != 0):
		if len(Old_SGE_ROOT) > 0:
			os.environ["SGE_ROOT"] = Old_SGE_ROOT
		return 'tbss_3_postreg failed'
	else : 
		print output
	
	# Run tbss_4_prestats 0.2 program
	
	cmd = 'tbss_4_prestats ' + str(args.tbss_4_prestat_treshold) 
	print cmd	

	## call tbss_4_prestats command ##
	p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
	
	(output, err) = p.communicate()
	 
	## Wait for date to terminate. Get return returncode ##
	p_status = p.wait()

	if (p_status != 0):
		if len(Old_SGE_ROOT) > 0:
			os.environ["SGE_ROOT"] = Old_SGE_ROOT
		return 'tbss_4_prestats failed'
	else : 
		print output

	if len(Old_SGE_ROOT) > 0:
		os.environ["SGE_ROOT"] = Old_SGE_ROOT
	return 1

if __name__ == '__main__':
    main();
    
