#!/bin/bash
# 
# Script to run tbss_skeleton inside fastr environment
#

if [ $# -lt 8 ]; then
	echo "Usage: tbss_skeleton <Out_Dir> <Input_File> <Threshold> <distancemap> <search_rule_mask> <4Ddata> <projected_4Ddata> <alternative_4D> [<alt_skeleton>] "
    	echo "e.g.: tbss_skeleton /home ~/tbss/FA/mean_FA.nii.gz 0.2 ~/tbss/stat/mean_FA_skeleton_mask_dst.nii.gz ${FSLDIR}/data/standard/LowerCingulum_1mm,nii.gz ~/tbss/stat/all_FA.nii,gz all_MD_skeletonised ~/tbss/MD/all_MD.nii.gz"
   	echo "<Out_Dir> - directorty for output"
	echo "<Input_File> - Input file with FA values over the whole brain"
	echo "<Threshold> - threshold of FA image can be found in thresh.txt in the stat subdirectory of the tbss run"
	echo "<distancemap> - skeleton mask as was produced by tbss (also found in the stat subdirectory of tbss)"
	echo "<search_rule_mask> - atlas mask of how to search for skeleton. Can be found, e.g., in the data/standard subdirectory of fsl"
	echo "<4Ddata> - the FA 4D data. Can be found in the stat subdirectory of tbss"
	echo "<projected_4Ddata> - name of the file to be produced with the projected skeleton"
	echo "<alternative_4D> - 4D data with an alternative values of diffusivity (e.g., axial diffusivity or mean diffusivity) that was produced in previous steps" 
   	echo "<alt_skeleton> - optional - alternative skeleton file"
    exit 1
else
    	OutDir=$1
	Input_File=$2
	Threshold=$3
	Distancemap=$4
	Search_Rule_Mask=$5
	FourDdata=$6
	Projected_4Ddata=$7
	Alternative_4D=$8
	if [ $# -eq 9 ]; then
		Alt_Skeleton=$9
	else
		Alt_Skeleton=""
	fi
fi

cd ${OutDir}

if [ $# -eq 9 ]; then
	tbss_skeleton -i ${Input_File} -p ${Threshold} ${Distancemap} ${Search_Rule_Mask} ${FourDdata} ${Projected_4Ddata} -a ${Alternative_4D} -s ${Alt_Skeleton}
else
	tbss_skeleton -i ${Input_File} -p ${Threshold} ${Distancemap} ${Search_Rule_Mask} ${FourDdata} ${Projected_4Ddata} -a ${Alternative_4D} 
fi
 
