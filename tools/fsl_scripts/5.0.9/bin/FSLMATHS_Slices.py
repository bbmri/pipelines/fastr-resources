#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  FSLMATH_Slices.py
#  
#  Copyright 2018 Yaron Caspi <yaroncaspi@yaroncaspi-Satellite-L675>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
#import pdb #pdb.set_trace() 
import subprocess
import argparse
import ast
import types

def main():
	
	parser = argparse.ArgumentParser()
    
    	parser.add_argument('-Directory', type=str, required=True, default='', help='Dictionary file to write output data.')
    	parser.add_argument('-Input_File', type=str, required=True, default='', help='Input file.')
    	parser.add_argument('-Operator1', type=str, required=False, default='-thr', help='first fslmaths operator')
    	parser.add_argument('-Operator2', type=str, required=False, default='-uthr', help='second fslmaths operator')
    	parser.add_argument('-Operator3', type=unicode, required=False, default='-bin', help='thirds fslmaths operator')
    	parser.add_argument('-Operator1_number', type=unicode, required=True, nargs='+', help='first fslmaths operator number')
    	parser.add_argument('-Operator2_number', type=unicode, required=True, nargs= '+', help='second fslmaths operator number')
    
	args = parser.parse_args()

	if type(args.Operator1_number) is types.ListType:
		str1=''.join(str(e) for e in args.Operator1_number)
		Operator1_number = ast.literal_eval(str1)
	else : 
		Operator1_number = args.Operator1_number

	if type(args.Operator2_number) is types.ListType:
                str1=''.join(str(e) for e in args.Operator2_number)
                Operator2_number = ast.literal_eval(str1)
        else : 
                Operator2_number = args.Operator2_number

  	Len1 = len(Operator1_number)
	Len2 = len(Operator2_number)
    
    	if Len1 != Len2 :
		return 1

    	operator1 = '-' + args.Operator1
	operator2 = '-' + args.Operator2
	operator3 = '-' + args.Operator3	
	
	for i in range (0,Len1): 
		
		Out_File = args.Directory + '/Sekeleton_mask_id_' + str(i) + '.nii.gz'
		
		cmd = 'fslmaths '+  args.Input_File + ' ' + operator1 + ' ' +  str(Operator1_number[i]) + ' ' + operator2 + ' ' + str(Operator2_number[i]) + ' ' + operator3 + ' ' +  Out_File
		print cmd
		p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
	
		(output, err) = p.communicate()
	 
		## Wait for date to terminate. Get return returncode ##
		p_status = p.wait()

		if (p_status != 0):
			print output
			print err
			return 1
		
    	return 0

if __name__ == '__main__':
   	 main();
