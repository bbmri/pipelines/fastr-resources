#!/bin/bash

function usage {
    echo "Error: $1"
}


module load fsl/5.0.9

while [[ $# -gt 1 ]] ; do
    key="$1"

    case $key in
        --input)
            input_image="$2"
            shift # past argument
        ;;
        -n)
            number="$2"
            shift # past argument
        ;;
        --output)
            output_image="$2"
        ;;
        *)
            # unknown option
        ;;
    esac
    shift # past argument or value
done

[[ -z $input_image ]] && usage "No input image provided"
[[ -z $number ]] && usage "No compnent provided"
[[ -z $output_image ]] && usage "No output image location provided"

max=$(fslstats $input_image -R|cut -d' ' -f2)
histo_bins=$(echo $max + 1|bc)
label=$(fslstats $input_image -h $histo_bins |grep -n 0 |sort -r -k2 -n -t':' | head -$((number+1))|tail -1|cut -d':' -f1)

fslmaths ${input_image} -thr $((label -2)).5 -uthr $((label -1)).5 ${output_image} 
