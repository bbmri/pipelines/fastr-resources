#!/bin/bash
#
# In this script slices with b=0 are extracted from a DTI file and are merged to form a new file
#
# Yaron Caspi  - y.caspi@umcutrecht.nl; www.neuromri.nl
#


# set -x
set -e
# set -u
set -o pipefail


function usage {
    	echo ""
    	echo "Usage: merge_b0_slices.sh input_file output_file [Slice sequence]"
    	echo ""
    	echo "input_file : DTI file with many slices, some with b = 0 and some with not should be a nifti file (.nii.gz or .nii)"
    	echo "output_file : File name for output merged file - Should be a nifti name (.nii.gz or .nii)" 
    	echo ""
    	echo "in this case the program will extract slices 0-2 and 34-36 from the DTI scan and will merge them to form the new output_file"
    	echo ""
    	exit
}

if [ $# -lt 2 ]; then 
	echo "Must have at least 4 input arguments"
    	echo "Usage:"	
    	usage
elif [ $# -eq 3 ]; then
	infile=$1
	outfile=$2
	shift
	shift

	arr=($1)
	end=$[${#arr[@]}-1]
	ind=1
	for (( i = 0; i <=$end; i=i+2 ))
	do
		echo $[$i+1]
		j=$[$i+1]
        	echo "now extracting slices " ${arr[$i]} " to " $[${arr[$j]}+${arr[$i]}]
        	fslroi $infile tmp_merge_$ind ${arr[$i]} ${arr[$j]}
        	ind=$[$ind+1]
	done
	
else
	infile=$1
	outfile=$2
	shift
	shift

	ind=1
	while [ $# -gt 0 ];
	do	
		echo "now extracting slices " $1 " to " $[$1+$2]
		fslroi $infile tmp_merge_$ind $1 $2
		ind=$[$ind+1]
		shift
  		shift
	done
fi


if [ $ind -ge 2 ]; then
	Str=tmp_merge_1.nii.gz
	for (( i = 2; i <=$[$ind-1]; i++ ))
	do
        	tmp=" tmp_merge_"$i".nii.gz"
        	Str=$Str$tmp
	done
	echo $Str
	fslmerge -t $outfile $Str
else
	mv tmp_merge_1.nii.gz $outfile
fi

IFS=' '

tmp=`fslinfo $outfile | grep dim1`
read -ra ADDR <<< "$tmp"
Dim1=${ADDR[1]}

tmp=`fslinfo $outfile | grep dim2`
read -ra ADDR <<< "$tmp"
Dim2=${ADDR[1]}

tmp=`fslinfo $outfile | grep dim3`
read -ra ADDR <<< "$tmp"
Dim3=${ADDR[1]}

tmp=`fslinfo $outfile | grep dim4`
read -ra ADDR <<< "$tmp"
Dim4=${ADDR[1]}

TakeRoi=False
if (( $Dim1 % 2 )); then
	let Dim1--
	TakeRoi=True 
fi

if (( $Dim2 % 2 )); then
        let Dim2--
	TakeRoi=True
fi

if (( $Dim3 % 2 )); then
        let Dim3--
	TakeRoi=True
fi

if [ "$TakeRoi" == "True" ]; then  
	fslroi $outfile $outfile 0 $Dim1 0 $Dim2 0 $Dim3 0 $Dim4
fi
