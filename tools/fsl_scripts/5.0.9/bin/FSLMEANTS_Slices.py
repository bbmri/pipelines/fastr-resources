#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  FSLMEANTS_Slices.py
#  
#  Copyright 2018 Yaron Caspi <yaroncaspi@yaroncaspi-Satellite-L675>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
#import pdb #pdb.set_trace() 
import subprocess
import argparse
import ast
import types

def main():
	
	parser = argparse.ArgumentParser()
    
    	parser.add_argument('-Directory', type=str, required=True, default='', help='Dictionary file to write output data.')
    	parser.add_argument('-Input_File', type=str, required=True, default='', help='Input files.')
    	parser.add_argument('-Mask', type=str, required=False, default='', nargs='+', help='Mask image')
    
	args = parser.parse_args()

	#if type(args.Mask) is types.ListType:
	#	str1=''.join(str(e) for e in args.Mask)
	#	Masks = ast.literal_eval(str1)
	#else : 
	#	Masks = args.Mask
	
	Masks = args.Mask

	Len1=len(Masks)

    	operator1 = '-i'
	operator2 = '-m' 
	operator3 = '-o' 	
	
	for i in range (0,Len1): 
		
		Out_File = args.Directory + '/Series_Value_id_' + str(i) + '.txt'
		
		cmd = 'fslmeants ' + operator1 + ' ' + args.Input_File + ' ' + operator2 + ' ' + str(Masks[i]) + ' ' + operator3 + ' ' + Out_File
		print cmd
		p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
	
		(output, err) = p.communicate()
	 
		## Wait for date to terminate. Get return returncode ##
		p_status = p.wait()

		if (p_status != 0):
			print output
			print err
			return 1
		
    	return 0

if __name__ == '__main__':
   	 main();
