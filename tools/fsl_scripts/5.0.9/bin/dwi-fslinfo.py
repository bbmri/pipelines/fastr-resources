#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  dwi-fslinfo.py
#  
#  Copyright 2018 Yaron Caspi <ycaspi@umcutrecht.nl>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

import pdb #pdb.set_trace() 

import subprocess
 

 
## Talk with date command i.e. read data from stdout and stderr. Store this info in tuple ##
## Interact with process: Send data to stdin. Read data from stdout and stderr, until end-of-file is reached.  ##
## Wait for process to terminate. The optional input argument should be a string to be sent to the child process, ##
## or None, if no data should be sent to the child.


def main(args):
	
	Input_File = args[1]
	Output_File = args[2]
	
	cmd = 'fslinfo ' + Input_File
	
	## call fslinfo command ##
	p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
	
	(output, err) = p.communicate()
	 
	## Wait for date to terminate. Get return returncode ##
	p_status = p.wait()

	if (p_status == 0):
		Lines = output.splitlines()
		for i in range (0,len(Lines)) :
			if Lines [i].find ('dim4')>-1 and Lines[i].find('pixdim4')<0:
				Num_Time_Points = int(Lines [i].split() [1])
	else:
		return 0
		
	with open(args [2], 'w') as Index_File:
		for i in range (0,Num_Time_Points,2) :
			Index_File.write('%d ' % 1)
			Index_File.write('%d ' % 2)	

	return 1

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
    
